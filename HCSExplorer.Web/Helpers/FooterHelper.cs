﻿using System;
using System.Reflection;
using HCSExplorer.Web.Models.Application;

namespace HCSExplorer.Web.Helpers
{
    public static class FooterHelper
    {
        public static void Initialize(this Footer footer)
        {
            var currentVersion = Assembly.GetExecutingAssembly().GetName().Version;

            footer.date = (new DateTime(2000, 1, 1)).AddDays(currentVersion.Build)
                                                    .AddSeconds(currentVersion.Revision*2);

            if (TimeZone.IsDaylightSavingTime(DateTime.Now,
                                              TimeZone.CurrentTimeZone.GetDaylightChanges(DateTime.Now.Year)))
            {
                footer.date = footer.date.AddHours(1);
            }

            footer.Major = currentVersion.Major;
            footer.Minor = currentVersion.Minor;
            footer.Build = currentVersion.Build;
            footer.Revision = currentVersion.Revision;

            footer.Copyright = DateTime.Now.Year.ToString();
        }
    }
}
