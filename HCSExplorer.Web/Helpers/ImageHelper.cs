﻿using System.Web;
using System.Web.Mvc;

namespace HCSExplorer.Web.Helpers
{
    public static class ImageHelper
    {
        private static string wellImgNotFoundUrl;
        private static string plateImgNotFoundUrl;
        private static string overlayDisableImgUrl;

        public static string GetWellImgNotFoundUrl()
        {
            if (wellImgNotFoundUrl == null)
                wellImgNotFoundUrl = UrlHelper.GenerateContentUrl("~/Content/Images/noImageNoBorder.png",
                                                                  new HttpContextWrapper(HttpContext.Current));
            return wellImgNotFoundUrl;
        }

        public static string GetPlateImgNotFoundUrl()
        {
            if (plateImgNotFoundUrl == null)
                plateImgNotFoundUrl = UrlHelper.GenerateContentUrl("~/Content/Images/noImage.png",
                                                                   new HttpContextWrapper(HttpContext.Current));
            return plateImgNotFoundUrl;
        }

        public static string GetOverlayDisableImgUrl()
        {
            if (overlayDisableImgUrl == null)
                overlayDisableImgUrl = UrlHelper.GenerateContentUrl("~/Content/Images/overlay-disabled.png",
                                                                    new HttpContextWrapper(HttpContext.Current));
            return overlayDisableImgUrl;
        }

        public static string GetWellImgNotFoundUrl(string appPath)
        {
            if (wellImgNotFoundUrl == null)
                wellImgNotFoundUrl = appPath + "Content/Images/noImageNoBorder.png"; 
            return wellImgNotFoundUrl;
        }

        public static string GetPlateImgNotFoundUrl(string appPath)
        {
            if (plateImgNotFoundUrl == null)
                plateImgNotFoundUrl = appPath + "Content/Images/noImage.png";
            return plateImgNotFoundUrl;
        }

        public static string GetOverlayDisableImgUrl(string appPath)
        {
            if (overlayDisableImgUrl == null)
                overlayDisableImgUrl = appPath + "Content/Images/overlay-disabled.png"; 
            return overlayDisableImgUrl;
        }


    }
}
