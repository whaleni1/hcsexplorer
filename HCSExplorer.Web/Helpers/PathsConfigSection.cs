﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace HCSExplorer.Web.Helpers
{
    public class PathsConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
        public PathsConfigSitesCollection Sites
        {
            get { return (PathsConfigSitesCollection)this[""]; }
        }

        public class PathsConfigSitesCollection : ConfigurationElementCollection
        {
            protected override ConfigurationElement CreateNewElement()
            {
                return new PathsConfigSiteElement();
            }

            protected override object GetElementKey(ConfigurationElement element)
            {
                return ((PathsConfigSiteElement)element).Site;
            }

            new public PathsConfigSiteElement this[string key]
            {
                get
                {
                    return (PathsConfigSiteElement)BaseGet(key);
                }
            }
        }

        public class PathsConfigSiteElement : ConfigurationElement
        {
            [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
            public PathsConfigDataSourceCollection DataSources
            {
                get { return (PathsConfigDataSourceCollection)this[""]; }
            }

            [ConfigurationProperty("site", IsKey = true, IsRequired = true)]
            public string Site
            {
                get { return (string)base["site"]; }
            }

        }

        public class PathsConfigDataSourceCollection : ConfigurationElementCollection
        {
            protected override ConfigurationElement CreateNewElement()
            {
                return new PathsConfigDataSourceElement();
            }

            protected override object GetElementKey(ConfigurationElement element)
            {
                return ((PathsConfigDataSourceElement)element).DataSource;
            }

            new public PathsConfigDataSourceElement this[string key]
            {
                get
                {
                    return (PathsConfigDataSourceElement)BaseGet(key);
                }
            }
        }

        public class PathsConfigDataSourceElement : ConfigurationElement
        {
            [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
            public PathsConfigPathsCollection PathsObjects
            {
                get { return (PathsConfigPathsCollection)this[""]; }
            }

            [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
            public string[] Paths
            {
                get
                {
                    var objs = (PathsConfigPathsCollection)this[""];
                    string[] res = objs.GetAllKeys();
                    return res;
                }
            }

            [ConfigurationProperty("datasource", IsKey = true, IsRequired = true)]
            public string DataSource
            {
                get { return (string)base["datasource"]; }
            }
        }

        public class PathsConfigPathsCollection : ConfigurationElementCollection
        {
            protected override ConfigurationElement CreateNewElement()
            {
                return new PathsConfigPathElement();
            }

            protected override object GetElementKey(ConfigurationElement element)
            {
                return ((PathsConfigPathElement)element).Path;
            }

            internal string[] GetAllKeys()
            {
                object[] objs = BaseGetAllKeys();
                string[] res = objs.Select(x => x.ToString()).ToArray();
                return res;
            }

        }

        public class PathsConfigPathElement : ConfigurationElement
        {
            [ConfigurationProperty("path", IsKey = true, IsRequired = true)]
            public string Path
            {
                get
                {
                    string p = (string)base["path"];
                    if (p.EndsWith("\\"))
                    {
                        p = p.TrimEnd(new [] { '\\' });
                    }
                    return p; 
                }
            }

            [ConfigurationProperty("datalocation", IsKey = false, IsRequired = false)]
            public string DataLocation
            {
                get
                {
                    return (string)base["datalocation"];
                }
            }

        }
    }
}