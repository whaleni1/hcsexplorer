﻿using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using HCS.Shared.Helpers;
using HCS.Shared.Context;
using HCS.Shared.Logger;
using HCSExplorer.Web.Models.Application;

namespace HCSExplorer.Web.Helpers
{
    internal static class MenuHelper
    {
        private static XmlMenu XmlMenu { get; set; }

        /// <summary>
        /// CreateMenuFor
        /// </summary>
        /// <param name="xmlMenu"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private static Menu CreateMenuFor(XmlMenu xmlMenu , string user)
        {
            return CreateMenuFomXmlMenuNode(xmlMenu , user);
        }

        /// <summary>
        /// CreateMenuFomXmlMenuNode
        /// </summary>
        /// <param name="xmlMenuNode"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private static Menu CreateMenuFomXmlMenuNode(XmlMenu xmlMenuNode , string user)
        {
            Menu retMenu = null;
            if (HasRights(xmlMenuNode , user))
            {
                retMenu = new Menu(xmlMenuNode.Name , xmlMenuNode.Link);
                if (xmlMenuNode.Items != null)
                {
                    var items = new List<Menu>() { };
                    foreach (var node in xmlMenuNode.Items)
                    {
                        var item = CreateMenuFomXmlMenuNode(node , user);
                        if (item != null)
                        {
                            items.Add(item);
                        }
                    }
                    retMenu.Items = items.ToArray();
                }
            }
            return retMenu;
        }

        /// <summary>
        /// HasRights
        /// </summary>
        /// <param name="xmlMenu"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private static bool HasRights(XmlMenu xmlMenu , string user)
        {
            // show "runs" link for INT and TEST configurations (hide for PROD)
            if ("#runs".Equals(xmlMenu.Link))
            {
                bool res = false;

                // show "ANALYSIS" tab only to specified users on all environments
                res = AnalysisAccessChecker.HasAccess(user);

                return res;
            }

            return true;
        }

        /// <summary>
        /// CreateMenu
        /// </summary>
        /// <returns></returns>
        public static Menu CreateMenu()
        {
            if (XmlMenu == null)
            {
                var configMenuFile =
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[Consts.Config.MenuFile]);

                var serializer = new XmlSerializer(typeof(XmlMenu));
                using (var xmlEl = XmlReader.Create(configMenuFile))
                {
                    XmlMenu = (XmlMenu) serializer.Deserialize(xmlEl);
                }
            }
            var ctx = HttpContext.Current;
            var app = ctx.ApplicationInstance as WebApiApplication;
            var executionContext = app.Container.Resolve<IExecutionContext>();
            FileLogger.Debug("Creating menu. User: " + executionContext.UserName);

            return CreateMenuFor(XmlMenu , executionContext.UserName);
        }
    }
}
