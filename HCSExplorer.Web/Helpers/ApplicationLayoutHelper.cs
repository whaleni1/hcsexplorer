﻿using System;
using System.IO;
using System.Web;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSExplorer.Web.Models.Application;

namespace HCSExplorer.Web.Helpers
{
    internal static class ApplicationLayoutHelper
    {
        const string csSITE_BS = "BS";
        const string csSITE_CA = "CA";

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="layout"></param>
        public static void Initialize(this ApplicationLayout layout)
        {
            layout.Footer = new Footer();
            layout.Footer.Initialize();
            layout.Menu = MenuHelper.CreateMenu();
            layout.AppHelpContent = System.Configuration.ConfigurationManager.AppSettings["appHelpContent"];
            layout.Version = ReadVersion();

#if DEV || TEST
            layout.DebugMode = true;
#endif
        }

        /// <summary>
        /// GetLocalSite
        /// </summary>
        /// <returns></returns>
        public static string GetLocalSite()
        {
            string serverName = System.Environment.MachineName;
            FileLogger.Info("Server name: " + serverName);

            if (!string.IsNullOrEmpty(serverName))
            {
                serverName = serverName.ToUpper();
                if (serverName.StartsWith("NRCHBS"))
                    return csSITE_BS;
                if (serverName.StartsWith("NRUSCA") || serverName.StartsWith("PHUSCA"))
                    return csSITE_CA;
            }
            return string.Empty;
        }

        /// <summary>
        /// ReadVersion
        /// </summary>
        /// <returns></returns>
        private static string ReadVersion()
        {
            string res = string.Empty;
            var ctx = HttpContext.Current;
            string filePath = Path.Combine(ctx.Server.MapPath("~") , "version.txt");
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    res = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                FileLogger.Error("ApplicationLayoutHelper:ReadVersion, " + ex.Message , ex);
                throw new CException("ApplicationLayoutHelper:ReadVersion, " + ex.Message , ex); ;
            }
            return res;
        }
    }
}
