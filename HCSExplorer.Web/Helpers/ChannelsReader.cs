﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using HCS.Shared.DTO;
using HCS.Shared.Logger;

namespace HCSExplorer.Web.Helpers
{
    public class ChannelsReader
    {
        // Filename example: F - 6(fld 1 wv FITC - FITC).tif
        private static readonly string[] ExcludeNames = { "." , ".." };

        /// <summary>
        /// ScanDirectories
        ///  - Performs scanning of input paths for the plate directories.
        /// </summary>
        /// <param name="model">Model contains a list of directories for scanning and selected datasource.</param>
        /// <param name="wasTimeout">Indicates whether scanning was stopped by timeout or not.</param>
        /// <returns>Directory path of first found plate or null if no plate was found.</returns>
        public static string ScanDirectories(PathsModel model , out bool wasTimeout)
        {
            bool plateDirFound = false;
            string searchPattern = GetMetadataFilePattern(model.DatabaseId);
            string plateDir = null;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            plateDirFound = ScanDirectoriesInternal(model.Paths , searchPattern , stopWatch , out plateDir , out wasTimeout);

            return plateDir;
        }

        /// <summary>
        /// ScanDirectoriesInternal
        /// </summary>
        /// <param name="dirs"></param>
        /// <param name="searchPattern"></param>
        /// <param name="stopWatch"></param>
        /// <param name="plateDir"></param>
        /// <param name="wasTimeout"></param>
        /// <returns></returns>
        private static bool ScanDirectoriesInternal(string[] dirs , string searchPattern , Stopwatch stopWatch , out string plateDir , out bool wasTimeout)
        {
            wasTimeout = false;
            plateDir = null;
            bool plateDirFound = false;

            foreach (string d in dirs)
            {
                if (!Directory.Exists(d))
                    continue;
                List<string> childDirs = new List<string>();
                using (FileEnumerator enumerator = new FileEnumerator(d))
                {
                    while (enumerator.MoveNext())
                    {

                        if (ExcludeNames.Contains(enumerator.Current.Name))
                        {
                            continue;
                        }

                        if (enumerator.Current.IsFolder)
                        {
                            string path = Path.Combine(enumerator.Current.Path , enumerator.Current.Name);
                            if (!childDirs.Contains(path))
                                childDirs.Add(path);
                        }
                        else
                        {
                            if (enumerator.Current.Name.ToLower().EndsWith(searchPattern))
                            {
                                FileLogger.Debug("Metadata file: " + enumerator.Current.Name);
                                plateDirFound = true;
                                plateDir = d;
                                break;
                            }
                        }

                        TimeSpan ts = stopWatch.Elapsed;

                        if (ts.TotalMilliseconds > Hubs.BrowseFileConfigSettings.MaxLoadingTime)
                        {
                            wasTimeout = true;
                            break;
                        }
                    }
                    if (!wasTimeout && !plateDirFound && childDirs.Count > 0)
                        plateDirFound = ScanDirectoriesInternal(childDirs.ToArray() , searchPattern , stopWatch , out plateDir , out wasTimeout);
                    if (plateDirFound || wasTimeout)
                        break;
                }

                if (plateDirFound || wasTimeout)
                    break;
            }
            return plateDirFound;
        }

        /// <summary>
        /// GetMetadataFilePattern
        ///  - Maps metadata file format to each datasource.
        /// </summary>
        /// <param name="dbId">Datasource ID.</param>
        /// <returns>Metadata file pattern for the required datasource.</returns>
        public static string GetMetadataFilePattern(string dbId)
        {
            switch (dbId)
            {
                case "INCELL_1":
                case "INCELL_2":
                    return ".xdce";
                case "OPERA_1":
                    return ".mea";
                case "YOKOGAWA_1":
                case "YOKOGAWA_2":
                case "YOKOGAWA_3":
                    return "measurementdetail.mrf";
                default:
                    throw new ApplicationException(string.Format("Datasource {0} is incorrect" , dbId));
            }
        }
    }
}