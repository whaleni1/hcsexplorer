﻿using HCSExplorer.Web.Models;

namespace HCSExplorer.Web.Helpers
{
    internal static class ErrorModelHelper
    {
        public static void PadErrorText(this Error model)
        {
            model.PadString = string.Empty.PadRight(512);
        }
    }
}
