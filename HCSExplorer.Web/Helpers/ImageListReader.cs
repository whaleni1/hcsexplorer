﻿using System;
using System.IO;
using System.Collections.Generic;
using HCS.Shared.DTO;
using System.Web.Configuration;

namespace HCSExplorer.Web.Helpers
{
    public static class ImageListReader
    {
        private const string HDR_CHANNEL_PREFIX = "Image_FileName_";
        private const string HDR_IMG_PATH = "Image_PathName";

        private const char CSV_SEPARATOR = ',';

        private static PathsConfigSection _pathsConfig = null;
        private static PathsConfigSection PathsConfig
        {
            get
            {
                if (null == _pathsConfig)
                {
                    _pathsConfig = (PathsConfigSection)WebConfigurationManager.GetSection("pathsConfigSection");
                }
                return _pathsConfig;
            }
        }

        public static UploadImageListModel Read(TextReader reader)
        {
            UploadImageListModel res = new UploadImageListModel();

            List<string> channles = new List<string>();

            string line;
            int lineNum = -1, imgPathColumn = -1;
            while ((line = reader.ReadLine()) != null)
            {
                lineNum++;

                if (lineNum == 0) // read header
                {
                    string[] values = line.Split(CSV_SEPARATOR);

                    for (int i = 0; i < values.Length; i++)
                    {
                        string v = values[i];

                        if (v.StartsWith(HDR_CHANNEL_PREFIX, StringComparison.InvariantCultureIgnoreCase))
                        {
                            string ch = v.Substring(HDR_CHANNEL_PREFIX.Length);
                            channles.Add(ch);
                        }
                        else if (v.Equals(HDR_IMG_PATH, StringComparison.InvariantCultureIgnoreCase))
                        {
                            imgPathColumn = i;
                        }
                    }

                    if (channles.Count == 0)
                    {
                        throw new ApplicationException("Couldn't parse channels.");
                    }

                    if (imgPathColumn < 0)
                    {
                        throw new ApplicationException(string.Format("Couldn't find column {0}.", HDR_IMG_PATH));
                    }

                    res.Channels = channles;
                }
                else if (lineNum == 1) // read first data row
                {
                    string[] values = line.Split(CSV_SEPARATOR);

                    string imgPath = values[imgPathColumn];
                    DataSourcceObj dso = GetDataLocationByPath(imgPath);

                    if (string.IsNullOrEmpty(dso.SiteId) || string.IsNullOrEmpty(dso.DatabaseId) || string.IsNullOrEmpty(dso.DataLocation))
                    {
                        throw new Exception(String.Format("Couldn't identify DataLocation for the image path: {0}.", imgPath));
                    }

                    res.SiteId = dso.SiteId;
                    res.DatabaseId = dso.DatabaseId;
                    res.DataLocation = dso.DataLocation;
                }
                else
                {
                    break;
                }
            }

            return res;
        }

        private static DataSourcceObj GetDataLocationByPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return null;

            DataSourcceObj res = new DataSourcceObj();

            foreach (PathsConfigSection.PathsConfigSiteElement site in PathsConfig.Sites)
            {
                foreach (PathsConfigSection.PathsConfigDataSourceElement ds in site.DataSources)
                {
                    foreach (PathsConfigSection.PathsConfigPathElement pathItem in ds.PathsObjects)
                    {
                        if (path.StartsWith(pathItem.Path, StringComparison.InvariantCultureIgnoreCase))
                        {
                            res.SiteId = site.Site;
                            res.DatabaseId = ds.DataSource;
                            res.DataLocation = pathItem.DataLocation;
                            return res;
                        }
                    }
                }
            }

            return res;
        }
    }

    public class DataSourcceObj
    {
        public string SiteId { get; set; }
        public string DatabaseId { get; set; }
        public string DataLocation { get; set; }
    }
}