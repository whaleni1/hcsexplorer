﻿namespace HCSExplorer.Web.Helpers
{
    public class XmlMenu
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public XmlMenu[] Items { get; set; }
    }
}
