﻿using System.Web.Mvc;

namespace HCSExplorer.Web.Helpers
{
    public static class UrlHelperExtension
    {
        #region Content

        public static string Image(this UrlHelper helper, string fileName)
        {
            return helper.Content(string.Format("~/Content/images/{0}", fileName));
        }


        public static string Config(this UrlHelper helper, string keyName)
        {
            string value = System.Configuration.ConfigurationManager.AppSettings[keyName];
            if (string.IsNullOrEmpty(value))
            {
                throw new System.Exception(string.Format("The {0} key not found in web.config", keyName));
            }
            if (value.StartsWith("~")) return helper.Content(value);
            return value;
        }

        #endregion
    }
}
