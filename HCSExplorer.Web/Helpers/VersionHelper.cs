﻿using System;
using System.Reflection;

namespace HCSExplorer.Web.Helpers
{
    public static class VersionHelper
    {
        public static string ResolveVersion()
        {
            var assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            var timeVersion = DateTime.UtcNow.Ticks;

            return string.Format(Consts.VersionHelper.VersionFmt, assemblyVersion, timeVersion);
        }
    }
}
