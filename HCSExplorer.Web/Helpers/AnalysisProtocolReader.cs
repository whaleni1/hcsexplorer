﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HCS.Shared.DTO;

namespace HCSExplorer.Web.Helpers
{
    public static class AnalysisProtocolReader
    {
        private static readonly List<string> ConceptNameHeaders = new List<string> {
            "Select the input image",
            "Select an image to measure",
            "Select the image to measure",
            "Select the skeletonized image",
            "Select the images to measure",
            "Select the first image",
            "Select the first input image", //HCSE-850
            "Select the second input image" //HCSE-850
        };

        private static readonly List<string> ExcludeConceptNameHeaders = new List<string> {
            "Name the output image",
            "Name the first output image", //HCSE-850
            "Name the second output image" //HCSE-850
        };
        private const string Delimiter = ":";
        private const string RequiredModule = "LoadData";
        private const string ModuleStart = "module_num";

        private const string MeasureImageQualityModule = "MeasureImageQuality";
        private const string AlignModule = "Align";
        private const string CalculateMetricsWhichImages = "Calculate metrics for which images?";
        private const string CalculateMetricsAllImages = "All loaded images";
        private const string CalculateMetricsSelect = "Select...";

        //private static string[] RequiredLines = { "CellProfiler Pipeline", "Version", "SVNRevision" };
        //"SVNRevision" doesn't exist in pipeline 2.1 example provided by Imtiaz
        private static string[] RequiredLines = { "CellProfiler Pipeline" , "Version" };

        public static Protocol Read(TextReader reader)
        {
            Protocol protocol = new Protocol();


            string line;
            string measureImageQualityWhichImages = null;

            List<string> includeConcepts = new List<string>();
            List<string> excludeConcepts = new List<string>();

            //string lastModule = null;
            string currentModule = null;
            bool moduleFound = false;
            int lineNum = -1;
            while ((line = reader.ReadLine()) != null)
            {
                lineNum++;
                if (line.Length > 0)
                {
                    int k = line.IndexOf(Delimiter , StringComparison.OrdinalIgnoreCase);
                    if (k <= 0)
                        throw new ProtocolFormatException(string.Format("Line {0} . Property:Value line format error." , lineNum + 1));// Property:Value line format

                    string parName = line.Substring(0 , k).Trim();
                    string parVal = line.Substring(k + 1).Trim();


                    if (lineNum < RequiredLines.Length)
                    {
                        if (!RequiredLines[lineNum].Equals(parName , StringComparison.OrdinalIgnoreCase))
                            throw new ProtocolFormatException(string.Format("Line {0} . Initial  property is absent: {1}" , lineNum = 1 , RequiredLines[lineNum]));
                        else
                            continue;

                    }

                    if (parVal.IndexOf(ModuleStart , StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        currentModule = parName;
                        if (RequiredModule.Equals(currentModule , StringComparison.InvariantCultureIgnoreCase))
                        {
                            moduleFound = true;
                        }
                        continue;
                    }

                    // Supporting MeasureImageQuality module (HCSE-578)
                    if (MeasureImageQualityModule.Equals(currentModule , StringComparison.OrdinalIgnoreCase))
                    {
                        if (CalculateMetricsWhichImages.Equals(parName , StringComparison.OrdinalIgnoreCase))
                        {
                            measureImageQualityWhichImages = parVal;
                        }
                        else if (ConceptNameHeaders.Contains(parName))
                        {
                            if (CalculateMetricsAllImages.Equals(measureImageQualityWhichImages , StringComparison.OrdinalIgnoreCase)) // "All loaded images"
                            {
                                // do nothing
                            }
                            else if (CalculateMetricsSelect.Equals(measureImageQualityWhichImages , StringComparison.OrdinalIgnoreCase)) // "Select..."
                            {
                                if (string.IsNullOrEmpty(parVal))
                                {
                                    protocol.WarningMessage = string.Format("{0}: {1}" , MeasureImageQualityModule , "\"Select...\" is specified but no images selected.");
                                }
                            }
                        }
                        continue;
                    }
                    // End of supporting MeasureImageQuality module
                    if (ConceptNameHeaders.Contains(parName))
                    {
                        if (!includeConcepts.Contains(parVal) && !string.IsNullOrWhiteSpace(parVal))
                        {
                            includeConcepts.Add(parVal);
                        }
                    }

                    if (ExcludeConceptNameHeaders.Contains(parName))
                    {
                        if (!excludeConcepts.Contains(parVal))
                        {
                            excludeConcepts.Add(parVal);
                        }
                    }
                }
                else
                {
                    currentModule = null;
                }
            }

            protocol.BiologicalConcepts = includeConcepts.Where(x => !excludeConcepts.Contains(x)).ToList();

            if (!moduleFound)
            {

                throw new ProtocolModuleException();
            }

            if (protocol.BiologicalConcepts.Count == 0)
            {
                throw new ProtocolFormatException("Biological Concepts are not found");
            }

            return protocol;
        }
    }

    public class ProtocolFormatException : ApplicationException
    {
        public ProtocolFormatException()
        {
        }

        public ProtocolFormatException(string err) : base(err)
        {
        }

        public ProtocolFormatException(string message , Exception innerException) : base(message , innerException)
        {
        }
    }

    public class ProtocolModuleException : ApplicationException
    {
        public ProtocolModuleException()
        {
        }

        public ProtocolModuleException(string err) : base(err)
        {
        }
    }

    public class ProtocolWarningException : ApplicationException
    {
        public ProtocolWarningException()
        {
        }

        public ProtocolWarningException(string err) : base(err)
        {
        }
    }
}