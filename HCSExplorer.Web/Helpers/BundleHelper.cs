﻿using System.Web.Optimization;

namespace HCSExplorer.Web.Helpers
{
    public static class BundleHelper
    {
        public static string ResolveModulesBundle(string virtualPath)
        {
            const string BundlePath = "~/bundles/";

            return BundleTable.EnableOptimizations ? Scripts.Url(BundlePath + virtualPath).ToHtmlString() : virtualPath;
        }
    }
}
