﻿using System.Linq;
using System.Collections.Generic;
using HCS.Shared.DTO;
using System.Configuration;

namespace HCSExplorer.Web.Helpers
{
    public static class DatasourceHelper
    {
        private static string[] ExcludeDBs
        {
            get
            {
                string csvStr = ConfigurationManager.AppSettings["runAnalysisExcludeDatasources"];
                return csvStr.Split(',');
            }
        }

        public static IList<ObjectBase> ExcludeNotUsedDBs(IList<ObjectBase> databases)
        {
            IList<ObjectBase> filtered = new List<ObjectBase>();
            foreach (ObjectBase db in databases)
            {
                if (!ExcludeDBs.Contains(db.Id))
                {
                    filtered.Add(db);
                }
            }

            return filtered;
        }
    }
}