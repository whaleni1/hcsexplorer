﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;

namespace HCSExplorer.Web.Helpers
{
  public static class MultipartStreamHelper
  {
    public static string GetValue(this  MultipartMemoryStreamProvider provider, string key)
    {
      if (provider == null)
      {
        throw new ArgumentNullException(" MultipartMemoryStreamProvider = null");
      }

      string value = string.Empty;
      // Find HttpContent in provider.Contents
      var item = provider.Contents.FirstOrDefault(x => x.Headers.ContentDisposition.DispositionType == "form-data" && 
                                                  x.Headers.ContentDisposition.Name == "\"" + key + "\"");
      if (item != null)
      {
        using (Stream stream = item.ReadAsStreamAsync().Result)
        {
          if (stream != null)
          {
            //Convert Stream to Bytes or something
            StreamReader reader = new StreamReader(stream);
            value = reader.ReadToEnd();
          }
        }
      }
      return value;
    }

    public static Stream GetStream(this MultipartStreamProvider provider, string key)
    {
        if (provider == null)
        {
            throw new ArgumentNullException(" Multipart Stream Provider = null");
        }

        // Find HttpContent in provider.Contents
        var item = provider.Contents.FirstOrDefault(x => x.Headers.ContentDisposition.DispositionType == "form-data" &&
                                                    x.Headers.ContentDisposition.Name == "\"" + key + "\"");
        if (item != null)
        {
            return item.ReadAsStreamAsync().Result;
        }
        return null;
    }

  }
}