﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using HCS.Shared.Logger;
using HCSExplorer.Web.Models.Application;

namespace HCSExplorer.Web.Helpers
{
    public static class ScriptHelper
    {
        private static XmlScript XmlScript { get; set; }

        private static IEnumerable<ScriptPair> AddModuleList(ScriptElement[] module, string aim)
        {
            if (aim == ScriptPairType.Layout.ToString())
            {
                return module.Select(node => new ScriptPair(node.Name, BundleHelper.ResolveModulesBundle(node.Bundle)));
            }
            if (aim == ScriptPairType.Bundle.ToString())
            {
                return
                    module.Select(
                        node =>
                        new ScriptPair(string.Format(Consts.Scripts.BundlePrefix, node.Bundle),
                                       string.Format(Consts.Scripts.ScriptsPrefix, node.Path)));
            }

            return Enumerable.Empty<ScriptPair>();
        }

        public static IEnumerable<ScriptPair> GetScriptList(string aim)
        {
            try
            {
                if (XmlScript == null)
                {
                    var configScriptFile =
                        HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[Consts.Config.ScriptFile]);

                    var serializer = new XmlSerializer(typeof (XmlScript));
                    using (var xmlEl = XmlReader.Create(configScriptFile))
                    {
                        XmlScript = (XmlScript) serializer.Deserialize(xmlEl);
                    }
                }
                return AddModuleList(XmlScript.Utils, aim).Concat(AddModuleList(XmlScript.Views, aim))
                                                          .Concat(AddModuleList(XmlScript.Models, aim));
            }
            catch (Exception ex)
            {
                FileLogger.GetLogger()
                          .Error(Consts.Messages.FailedToGetScriptList, ex);
                throw;
            }
        }
    }
}
