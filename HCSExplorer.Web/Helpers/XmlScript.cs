﻿using System.Xml.Serialization;

namespace HCSExplorer.Web.Helpers
{
    [XmlRoot("scripts")]
    public class XmlScript
    {
        [XmlArray(ElementName = "utils")]
        [XmlArrayItem(ElementName = "script")]
        public ScriptElement[] Utils { get; set; }


        [XmlArray(ElementName = "models")]
        [XmlArrayItem(ElementName = "script")]
        public ScriptElement[] Models { get; set; }


        [XmlArray(ElementName = "views")]
        [XmlArrayItem(ElementName = "script")]
        public ScriptElement[] Views { get; set; }
    }

    public class ScriptElement
    {
        [XmlElement("path")]
        public string Path { get; set; }

        [XmlElement("bundle")]
        public string Bundle { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }
    }
}
