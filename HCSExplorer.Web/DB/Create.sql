﻿-- Create sequence 
create sequence SEQ_RUN_ID_CTR
minvalue 1
start with 1
increment by 1
NOCACHE
ORDER;
