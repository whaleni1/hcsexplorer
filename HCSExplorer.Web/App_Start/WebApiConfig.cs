﻿using System.Web.Http;
using HCSExplorer.Web.Filters;
using Newtonsoft.Json.Converters;

namespace HCSExplorer.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "Plate",
                routeTemplate: "api/plate/plateParameters/{id}/{siteId}/{databaseId}"
                );

            config.Routes.MapHttpRoute(
                name: "PlateBarcode",
                routeTemplate: "api/plate/plateParBarcode/{barcode}/{siteId}/{databaseId}"
                );


            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new {id = RouteParameter.Optional}
                );


            config.Formatters.Remove(config.Formatters.XmlFormatter);

            var formatter = config.Formatters.JsonFormatter;
            var enumConverter = new StringEnumConverter();
            var stringEncoder = new StringEncodingConverter();

            formatter.SerializerSettings.Converters.Add(enumConverter);
            formatter.SerializerSettings.Converters.Add(stringEncoder);
        }
    }
}
