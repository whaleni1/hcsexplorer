﻿using System.Web.Http;
using Castle.Windsor;
using HCSExplorer.Web.Windsor;
using HCSExplorer.Web.Windsor.DependencyResolver;

namespace HCSExplorer.Web
{
    internal class WindsorContainerConfig
    {
        public static IWindsorContainer Install()
        {
            var container = new WindsorContainer().Install(new WindsorInstaller());

            GlobalConfiguration.Configuration.DependencyResolver = new WindsorDependencyResolver(container);
            return container;
        }
    }
}
