﻿using System.Web.Mvc;

namespace HCSExplorer.Web
{
    internal class MvcFilterConfig
    {
        public static void RegisterGlobalMvcFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
