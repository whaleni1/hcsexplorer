﻿using System.Web.Http.Filters;
using HCSExplorer.Web.Filters;

namespace HCSExplorer.Web
{
    internal class WebApiFilterConfig
    {
        public static void RegisterGlobalMvcFilters(HttpFilterCollection filters)
        {
            filters.Add(new ModelValidationAttribute());
        }
    }
}
