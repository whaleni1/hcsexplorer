﻿using System.Web.Optimization;
using HCSExplorer.Web.Helpers;

namespace HCSExplorer.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(
                new StyleBundle("~/Content/css/styles").Include("~/Content/css/sig-previewcard.css",
                                                                "~/Content/css/dynatree/ui.dynatree.css",
                                                                "~/Content/css/jquery.bootstrap.gethelpis.css",
                                                                 "~/Content/css/dataTables.bootstrap.css",
                                                                 "~/Content/css/nibr-bootstrap.min.css",
                                                                "~/Content/css/site.css"
                    ));

           
            bundles.Add(new ScriptBundle("~/bundles/libs/requirejs").Include("~/Scripts/libs/require.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/jquery").Include("~/Scripts/libs/jquery-1.11.1.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/jqueryui").Include("~/Scripts/libs/jquery-ui-1.11.1.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/modernizr").Include("~/Scripts/libs/modernizr-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/backbone").Include("~/Scripts/libs/backbone.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/mbinder").Include("~/Scripts/libs/backbone.ModelBinder.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/cbinder").Include("~/Scripts/libs/backbone.CollectionBinder.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/validator").Include("~/Scripts/libs/backbone-validation-amd.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/underscore").Include("~/Scripts/libs/underscore.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/text").Include("~/Scripts/libs/text.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/jquery-ui-menubar").Include("~/Scripts/libs/jquery-ui-menubar.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/datatables").Include("~/Scripts/libs/jquery.dataTables.js", "~/Scripts/libs/dataTables.bootstrap.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/datatablesscroller").Include("~/Scripts/libs/dataTables.scroller.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/signalR").Include("~/Scripts/libs/jquery.signalR.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/jquerycookie").Include("~/Scripts/libs/jquery.cookie.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/timepicker").Include("~/Scripts/libs/jquery.timepicker.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/iframe-transport").Include("~/Scripts/libs/jquery.iframe-transport.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/dynatree").Include("~/Scripts/libs/jquery.dynatree.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/filetree").Include("~/Scripts/libs/jqueryFileTree.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/inputfile").Include("~/Scripts/libs/jQuery.fileinput.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/filedownload").Include("~/Scripts/libs/jquery.fileDownload.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/listbox").Include("~/Scripts/libs/js-inherit.js",
                                                                           "~/Scripts/libs/js-listbox.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/bootstrap").Include("~/Scripts/libs/nibr-bootstrap.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/bootbox").Include("~/Scripts/libs/bootbox.js"));
            bundles.Add(new ScriptBundle("~/bundles/libs/sig").Include( "~/Scripts/libs/sig/jquery.sig.gethelp-2.0.0.js",
                                                                       "~/Scripts/libs/sig/jquery.sig.previewcard.js",
                                                                       "~/Scripts/libs/sig/auth.js", // getDomainFromSiteCode() is in use
                                                                       "~/Scripts/libs/sig/jquery.bootstrap.gethelpis.js",
                                                                       "~/Scripts/libs/sig/jquery.form.js",
                                                                       "~/Scripts/libs/sig/sig-common.js"
                                                                       ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include("~/Scripts/app.js"));
            bundles.Add(new ScriptBundle("~/bundles/router").Include("~/Scripts/router.js"));

            bundles.Add(new ScriptBundle("~/bundles/libs/ifvisible").Include("~/scripts/libs/ifvisible.js"));

            //xml utils-models-views
            foreach (var x in ScriptHelper.GetScriptList(ScriptPairType.Bundle.ToString()))
            {
                bundles.Add(new ScriptBundle(x.Name).Include(x.Value));
            }
        }
    }
}
