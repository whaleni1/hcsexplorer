﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace HCSExplorer.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class FileDownloadAttribute : ActionFilterAttribute
    {
        public FileDownloadAttribute(string cookieName = "fileDownload", string cookiePath = "/")
        {
            CookieName = cookieName;
            CookiePath = cookiePath;
        }

        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        /// <summary>
        /// If the current response is not null then write a
        /// cookie to inform jquery.fileDownload that a successful file download has occured
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckAndHandleFileResult(HttpActionExecutedContext actionContext)
        {
            if (actionContext.Response == null) // exception was thrown
            {
                //actionContext.Response = actionContext.Request
                //                                      .CreateResponse(HttpStatusCode.InternalServerError, "errW");

                actionContext.Response = new HttpResponseMessage()
                {
                    Content = new StringContent(
                        "<body>" + actionContext.Exception.Message + "</body>",
                        Encoding.UTF8,
                        "text/html"
                    )
                };

                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (GetCookie(actionContext.Request, CookieName) != null)
                {
                    var cookie = new CookieHeaderValue(CookieName, "true");
                    cookie.Expires = DateTimeOffset.Now.AddYears(-1);
                    cookie.Path = CookiePath;
                    actionContext.Response.Headers.AddCookies(new CookieHeaderValue[] { cookie });
                }
            }
            else
            {
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                var cookie = new CookieHeaderValue(CookieName, "true");
                cookie.Path = CookiePath;
                actionContext.Response.Headers.AddCookies(new CookieHeaderValue[] { cookie });
            }
        }

        public override void OnActionExecuted(HttpActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            CheckAndHandleFileResult(filterContext);
        }

        private string GetCookie(HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie != null)
                return cookie[cookieName].Value;

            return null;
        }
    }
}