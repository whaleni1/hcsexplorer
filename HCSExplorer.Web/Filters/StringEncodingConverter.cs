﻿using System;
using System.Web;
using System.Web.Security.AntiXss;
using System.Web.Util;
using Newtonsoft.Json;

namespace HCSExplorer.Web.Filters
{
    internal class StringEncodingConverter : JsonConverter
    {
        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (string);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return null;
            }

            var content = (string) reader.Value;
            var validator = new RequestValidator();
            int errorIndex = 0;

            if (!validator.InvokeIsValidRequestString(null,
                                                      content,
                                                      RequestValidationSource.Form,
                                                      null,
                                                      out errorIndex))
            {
                var ctx = HttpContext.Current;
                var app = ctx.ApplicationInstance as WebApiApplication;

                throw new JsonReaderException(string.Format(Resources.Application.XssError, errorIndex));
            }

            return content;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(AntiXssEncoder.HtmlEncode((string) value, true));
        }
    }
}
