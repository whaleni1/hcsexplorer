﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace HCSExplorer.Web.Filters
{
    class ModelValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                var items = new List<object>();
                foreach (var keyValue in actionContext.ModelState)
                {
                    // Get only property name without object name  (e.g. "Email" instead of "user.Email")
                    var elements = keyValue.Key.Split(new char[] { '.' });
                    var level = elements.Length - 2;
                    var elementName = elements[elements.Length - 1];

                    var modelName = (elements.Length > 1) ? elements[elements.Length - 2]
                                                          : string.Empty;
                    var row = -1;
                    var beginIdx = modelName.IndexOf('[');
                    var endIdx = modelName.IndexOf(']');
                    if (beginIdx != -1 && endIdx > 0 && endIdx > beginIdx + 1)
                    {
                        var sub = modelName.Substring(beginIdx + 1, endIdx - beginIdx - 1);
                        if (int.TryParse(modelName.Substring(beginIdx + 1, endIdx - beginIdx - 1), out row))
                        {
                            modelName = modelName.Substring(0, beginIdx);
                        }
                    }

                    items.Add(new
                              {
                                  ElementName = elementName[0].ToString().ToLower() + elementName.Substring(1, elementName.Length - 1),
                                  ModelName = string.IsNullOrEmpty(modelName) ? string.Empty : modelName[0].ToString().ToLower() + modelName.Substring(1, modelName.Length - 1),
                                  Level = level,
                                  Row = row,
                                  Description = keyValue.Value
                                                        .Errors
                                                        .Select(e => !string.IsNullOrEmpty(e.ErrorMessage) ?
                                                                                             e.ErrorMessage
                                                                                             :
                                                                                             e.Exception.Message)
                                                        .ToArray()
                              });
                }

                var headers = actionContext.Request.Headers;

                actionContext.Response = actionContext.Request
                                                      .CreateResponse(HttpStatusCode.InternalServerError,
                                                                      new
                                                                      {
                                                                          ValidationErrors = items.ToArray()
                                                                      });
            }
        }
    }
}