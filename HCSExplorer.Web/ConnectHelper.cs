﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HCS.DataAccess;
using HCS.DataAccess.Helpers;
using HCS.Shared.Caching;
using HCS.Shared.Common;
using HCS.Shared.DTO;
using HCS.Shared.Logger;
using HCS.Shared.Utils;
using HCSConnect.Library;
using HCSExplorer.Services;
using HCSExplorer.Web.Models;

namespace HCSExplorer.Web
{
    /// <summary>
    /// ConnectHelper
    /// </summary>
    public class ConnectHelper
    {

        /// <summary>
        /// GetDatabases
        /// </summary>
        /// <returns></returns>
        public IList<ObjectBase> GetDatabases()
        {
            var connect = new Connect();
            var databases = new List<ObjectBase>();

            try
            {
                FileLogger.Debug("ConnectHelper:GetDatabases");

                DBList dbs = connect.GetDatabases();
                foreach (HCSDatabase db in dbs)
                {
                    ObjectBase obj = new ObjectBase();
                    obj.Id = db.id;
                    obj.Name = db.description;
                    databases.Add(obj);
                }
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetDatabases, " + eX.Message);
                throw new CException("ConnectHelper:GetDatabases, " + eX.Message , eX);
            }

            return databases;
        }

        /// <summary>
        /// GetDatabases
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public IList<ObjectBase> GetDatabases(string siteId)
        {
            var connect = new Connect();
            var databases = new List<ObjectBase>();

            try
            {
                FileLogger.Debug("ConnectHelper:GetDatabases: For site = " + siteId);

                DBList dbs = connect.GetDatabases(siteId);
                foreach (HCSDatabase db in dbs)
                {
                    ObjectBase obj = new ObjectBase();
                    obj.Id = db.id;
                    obj.Name = db.description;
                    databases.Add(obj);
                }
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetDatabases, " + eX.Message);
                throw new CException("ConnectHelper:GetDatabases, " + eX.Message , eX);
            }

            return databases;
        }

        /// <summary>
        /// GetDatabaseTitle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="db_id"></param>
        /// <returns></returns>
        public string GetDatabaseTitle(string db_id)
        {
            string strDBName = string.Empty;

            try
            {
                List<ObjectBase> databases = (List<ObjectBase>) HCSCache.GetDatabases();
                if (databases == null)
                {
                    try
                    {
                        databases = (List<ObjectBase>) GetDatabases();
                        HCSCache.SetDatabases(databases);
                    }
                    catch (Exception ex)
                    {
                        FileLogger.Error("ConnectHelper:GetDatabaseTitle, " + ex.Message);
                        throw;
                    }
                }
                if (databases == null)
                    return db_id;
                ObjectBase db = databases.Where(x => x.Id.Equals(db_id , StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (db == null)
                    strDBName = db_id;
                else
                    strDBName = db.Name;
            }
            catch (Exception eX)
            {
                throw new CException("ConnectHelper:GetDatabaseTitle, " + eX.Message , eX);
            }

            return strDBName;
        }

        /// <summary>
        /// GetPlates
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IList<HCS.Shared.DTO.Plate> GetPlates(SearchPlatesModel model)
        {
            var plate = new HCS.DataAccess.Plate();
            IList<HCS.Shared.DTO.Plate> plates;

            try
            {
                FileLogger.Info("ConnectHelper:GetPlates, barcode = " + model.Barcode + " siteId = " + model.SiteId);

                plate.barcode = model.Barcode;
                plates = GetPlates(model , plate);

                FileLogger.Info("ConnectHelper:GetPlates, barcode = " + model.Barcode + " num plates = " + plates.Count);
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetPlates, " + eX.Message);
                throw new CException("ConnectHelper:GetPlates, " + eX.Message , eX);
            }

            return plates;
        }

        /// <summary>
        /// GetPlatesByPath
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IList<HCS.Shared.DTO.Plate> GetPlatesByPath(SearchPlatesModel model)
        {
            var plate = new HCS.DataAccess.Plate();
            IList<HCS.Shared.DTO.Plate> plates;

            try
            {
                FileLogger.Info("ConnectHelper:GetPlatesByPath, path = " + model.Barcode + " siteId = " + model.SiteId);

                plate.path = model.Barcode;

                plates = GetPlates(model , plate);
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetPlatesByPath, " + eX.Message);
                throw new CException("ConnectHelper:GetPlatesByPath, " + eX.Message , eX);
            }

            return plates;
        }

        /// <summary>
        /// GetPlates
        /// </summary>
        /// <param name="model"></param>
        /// <param name="plate"></param>
        /// <returns></returns>
        private IList<HCS.Shared.DTO.Plate> GetPlates(SearchPlatesModel model , HCS.DataAccess.Plate plate)
        {
            var connect = new Connect();
            var connectPlates = new PlateList();
            var plates = new List<HCS.Shared.DTO.Plate>();

            try
            {
                connectPlates = connect.GetPlates(model.DatabaseId , plate);

                foreach (HCS.DataAccess.Plate p in connectPlates)
                {
                    int plateid = p.plateId == string.Empty ? 0 : Convert.ToInt32(p.plateId);
                    string barcode = p.barcode;
                    string pname = p.name;
                    string ts = p.timestamp;
                    DateTime? dt = DateTimeUtils.ParseDateTime(ts , model.SiteId);
                    string path = p.path;
                    int format = p.format;
                    plates.Add(new HCS.Shared.DTO.Plate
                    {
                        Id = plateid ,
                        Barcode = barcode ,
                        Name = pname ,
                        DateSrc = ts ,
                        Path = path ,
                        Format = format ,
                        Date = dt
                    });
                }
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetPlates, " + eX.Message);
                throw new CException("ConnectHelper:GetPlates, " + eX.Message , eX);
            }

            return plates;
        }

        /// <summary>
        /// GetImageLayoutByPlateId
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="plateId"></param>
        /// <param name="well"></param>
        /// <returns></returns>
        public HCS.Shared.DTO.ImageParameters GetImageLayoutByPlateId(string siteId , string databaseId , string plateId , HCS.Shared.DTO.Well well = null)
        {
            var connect = new Connect();
            HCS.Shared.DTO.ImageParameters il;
            var pl = new HCS.DataAccess.Plate();
            var col = 0;
            var row = 0;

            try
            {
                FileLogger.Info("ConnectHelper:GetImageLayoutByPlateId, siteId = " + siteId + " database = " + databaseId + " plateId = " + plateId);

                if (well != null)
                {
                    col = well.Col;
                    row = well.Row;
                }

                pl.plateId = plateId;

                il = GetImageParameters(siteId , databaseId , pl , row , col);
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetImageLayoutByPlateId, " + eX.Message);
                throw new CException("ConnectHelper:GetImageLayoutByPlateId, " + eX.Message , eX);
            }

            return il;
        }

        /// <summary>
        /// GetImageLayoutByBarcode
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="barcode"></param>
        /// <param name="well"></param>
        /// <returns></returns>
        public HCS.Shared.DTO.ImageParameters GetImageLayoutByBarcode(string siteId , string databaseId , string barcode , HCS.Shared.DTO.Well well = null)
        {
            var connect = new Connect();
            HCS.Shared.DTO.ImageParameters il;
            var pl = new HCS.DataAccess.Plate();
            var col = 0;
            var row = 0;

            try
            {
                FileLogger.Info("ConnectHelper:GetImageLayoutByBarcode, siteId = " + siteId + " database = " + databaseId + " barcode = " + barcode);

                if (well != null)
                {
                    col = well.Col;
                    row = well.Row;
                }

                pl.barcode = barcode;

                il = GetImageParameters(siteId , databaseId , pl , row , col);
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetImageLayoutByBarcode, " + eX.Message);
                throw new CException("ConnectHelper:GetImageLayoutByBarcode, " + eX.Message , eX);
            }

            return il;
        }

        /// <summary>
        /// GetImageParameters
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private HCS.Shared.DTO.ImageParameters GetImageParameters(string siteId , string databaseId , HCS.DataAccess.Plate pl , int row , int col)
        {
            var connect = new Connect();
            HCS.Shared.DTO.ImageParameters il;

            try
            {
                var imgParms = connect.GetImageParameters(databaseId , pl , row , col);
                var plate = imgParms[0].plateData;
                var wellout = imgParms[0].wellData;
                var fields = imgParms[0].fields;
                var channels = imgParms[0].channels;
                var versions = imgParms[0].versions;
                var zcount = imgParms[0].zCount;
                var tcount = imgParms[0].timepointCount;

                if (plate != null)
                {
                    il = new HCS.Shared.DTO.ImageParameters();
                    il.Id = int.Parse(plate.plateId);
                    il.Barcode = plate.barcode;
                    il.DateSrc = plate.timestamp;
                    il.Date = DateTimeUtils.ParseDateTime(il.DateSrc , siteId);

                    int format = plate.format;
                    il.Format = format != 0 ? format : 96;
                    il.Path = plate.path;
                    il.Name = plate.name;
                    if (string.IsNullOrEmpty(il.Name))
                        il.Name = "NoName";

                    if (zcount != 0)
                    {
                        il.ZIndexCount = zcount != 0 ? zcount : 0;
                    }
                    if (tcount != 0)
                    {
                        il.TimepointCount = tcount != 0 ? tcount : 0;
                    }
                    if (wellout != null)
                    {
                        il.Well = new HCS.Shared.DTO.Well
                        {
                            Col = wellout.col ,
                            Row = wellout.row
                        };
                    }
                    if (fields != null)
                    {
                        il.Fields = fields.ConvertAll<string>(delegate (int i)
                        {
                            return i.ToString();
                        }); ;
                    }
                    if (channels != null)
                    {
                        il.Channels = channels;
                    }
                    if (versions != null)
                    {
                        il.Versions = new List<string>();
                        foreach (string ver in versions)
                        {
                            il.Versions.Add(ver);
                        }
                    }
                }
                else
                {
                    il = new HCS.Shared.DTO.ImageParameters();
                }
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetImageParameters, " + eX.Message);
                throw new CException("ConnectHelper:GetImageParameters, " + eX.Message , eX);
            }

            return il;
        }

        /// <summary>
        /// GetCompounds
        /// </summary>
        /// <param name="helios"></param>
        /// <param name="compoundId"></param>
        /// <returns></returns>
        public IList<HCS.Shared.DTO.Compound> GetCompounds(string helios , string compoundId)
        {
            var connect = new Connect();
            var compounds = new List<HCS.Shared.DTO.Compound>();
            CompoundList cmpdList;

            try
            {
                FileLogger.Info("ConnectHelper:GetCompounds, helios = " + helios + " compoundId = " + compoundId);

                cmpdList = connect.GetCompoundInfo(compoundId , helios.Equals("HELIOS_PROD1") ? HCSWebServiceSetting.csSITE_BS : (helios.Equals("HELIOS_PROD2") ? HCSWebServiceSetting.csSITE_CA : ""));

                foreach (HCS.DataAccess.Compound c in cmpdList)
                {
                    var cmpd = new HCS.Shared.DTO.Compound();
                    cmpd.Barcode = c.plateBarcode;
                    cmpd.Assay = c.assay;
                    cmpd.Id = c.compoundId;
                    cmpd.InstrumentId = c.dataSource.id;
                    cmpd.Instrument = c.dataSource.description;
                    cmpd.Row = c.wellData.row;
                    cmpd.Col = c.wellData.col;

                    var siteId = string.Empty;
                    var db = string.Empty;
                    DataService.ResolveSiteAndDB(cmpd.InstrumentId , out siteId , out db);
                    if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(db))
                    {
                        cmpd.DatabaseName = db;
                        cmpd.HasLink = true;
                        cmpd.SiteId = siteId;
                    }

                    compounds.Add(cmpd);
                }
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetCompounds, " + eX.Message);
                throw new CException("ConnectHelper:GetCompounds, " + eX.Message , eX);
            }

            return compounds;
        }

        /// <summary>
        /// GetChannelsByPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public ChannelList GetChannelsByPaths(HCS.Shared.DTO.PathsModel pathsModel)
        {
            var connect = new Connect();
            ChannelList res = new ChannelList();
            var pModel = new HCS.DataAccess.PathsModel
            {
                DatabaseId = pathsModel.DatabaseId ,
                Paths = pathsModel.Paths
            };

            try
            {
                res = connect.GetChannelsByParentPaths(pModel);
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:GetChannelsByParentPath " + eX.Message);
                throw new CException("ConnectHelper:GetChannelsByParentPath, " + eX.Message , eX);
            }

            return res;
        }

        /// <summary>
        /// LoadImage
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string LoadImage(ImageUrlModel imageUrl , string baseUrl , string siteDir , WellImagesModel model)
        {
            var path = string.Empty;
            var plate = new HCS.DataAccess.Plate();

            try
            {
                FileLogger.Debug("ConnectHelper:LoadImage");

                var plateKey = imageUrl.PlateKey;
                var platePath = Path.Combine(siteDir , plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }

                var found = false;
                var fileKey = imageUrl.FileKey;
                var fileName = Path.Combine(platePath , fileKey); //imageUrl.FileName;
                FileLogger.Debug("ConnectHelper:LoadImage-->fileKey: " + fileKey + ", fileName: " + fileName + ", baseURL" + baseUrl + ", siteDir: " + siteDir);

                plate.plateId = model.Id.ToString();

                if (!imageUrl.Overlay)
                {
                    var spec = new ImageSpec();
                    spec.field = Convert.ToInt32(model.Fields[imageUrl.FieldIndex]);
                    spec.color = string.IsNullOrEmpty(model.ChannelParameters[imageUrl.ChannelIndex].Color) ? "" : model.ChannelParameters[imageUrl.ChannelIndex].Color;
                    // Set the channel parameters if specified
                    if (model.ChannelParameters.Count > 0)
                    {
                        spec.channel = model.ChannelParameters[imageUrl.ChannelIndex].Channel;

                        if (!string.IsNullOrEmpty(model.ChannelParameters[imageUrl.ChannelIndex].Brightness))
                        {
                            double brightness;
                            if (double.TryParse(model.ChannelParameters[imageUrl.ChannelIndex].Brightness, out brightness))
                            {
                                spec.brightness = brightness;
                            }
                        }
                        else if (!string.IsNullOrEmpty(model.ChannelParameters[imageUrl.ChannelIndex].Auto))
                        {
                            bool autoContrast;
                            if (Boolean.TryParse(model.ChannelParameters[imageUrl.ChannelIndex].Auto, out autoContrast))
                            {
                                spec.auto = autoContrast;
                            }
                        }
                        else if (!string.IsNullOrEmpty(model.ChannelParameters[imageUrl.ChannelIndex].Level))
                        {
                            bool levels;
                            if (Boolean.TryParse(model.ChannelParameters[imageUrl.ChannelIndex].Level, out levels))
                            {
                                spec.level = levels;
                            }
                            if (levels)
                            {
                                Int64 whitePoint;
                                Int64 blackPoint;

                                if ((Int64.TryParse(model.ChannelParameters[imageUrl.ChannelIndex].WhitePoint, out whitePoint)) && Int64.TryParse(model.ChannelParameters[imageUrl.ChannelIndex].BlackPoint, out blackPoint))
                                {
                                    spec.whitePoint = whitePoint;
                                    spec.blackPoint = blackPoint;
                                }
                            }
                        }
                    }

                    spec.format = ImageUtils.IMAGE_FORMAT;
                    spec.reqImageProcess = true;
                    spec.invert = model.Invert;
                    spec.z_index = model.ZIndex != null ? model.ZIndex.Value : 0;
                    spec.timepoint = model.Timepoint != null ? model.Timepoint.Value : 0;
                    spec.version = model.Version;
                    spec.thumbPixelSize = !string.IsNullOrEmpty(model.SizeId) ? Convert.ToInt32(ImageUtils.GetThumbSize(model.SizeId)) : 0;

                    found = SynchSaveImage(imageUrl.Url , fileName , model.DatabaseId , plate , imageUrl.Row , imageUrl.Col , spec);
                }
                else
                {
                    var spec = new OverlayImageSpec();
                    spec.field = Convert.ToInt32(model.Fields[imageUrl.FieldIndex]);
                    spec.format = ImageUtils.IMAGE_FORMAT;
                    spec.z_index = model.ZIndex != null ? model.ZIndex.Value : 0;
                    spec.timepoint = model.Timepoint != null ? model.Timepoint.Value : 0;
                    spec.version = model.Version;
                    spec.thumbPixelSize = !string.IsNullOrEmpty(model.SizeId) ? Convert.ToInt32(ImageUtils.GetThumbSize(model.SizeId)) : 0;
                    spec.invert = model.Invert;

                    // Set the channel parameters
                    foreach (ChannelImageParameters c in model.ChannelParameters)
                    {
                        var chName = c.Channel;
                        var chColor = c.Color.ToLower();
                        foreach (ImageSpec colorSpec in spec.imageElements)
                        {
                            if (chColor.Equals(colorSpec.color.ToLower()) && !"grayscale".Equals(chColor))
                            {
                                colorSpec.channel = chName;
                                if (!string.IsNullOrEmpty(c.Auto))
                                {
                                    bool autoContrast;
                                    if (Boolean.TryParse(c.Auto, out autoContrast))
                                    {
                                        colorSpec.auto = autoContrast;
                                    }
                                }
                                if (!string.IsNullOrEmpty(c.Level))
                                {
                                    bool levels;
                                    if (Boolean.TryParse(c.Level, out levels))
                                    {
                                        colorSpec.level = levels;
                                    }
                                    if (levels)
                                    {
                                        Int64 whitePoint;
                                        Int64 blackPoint;

                                        if ((Int64.TryParse(c.WhitePoint, out whitePoint)) && Int64.TryParse(c.BlackPoint, out blackPoint))
                                        {
                                            colorSpec.whitePoint = whitePoint;
                                            colorSpec.blackPoint = blackPoint;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(c.Brightness))
                                {
                                    double brightness;
                                    if (double.TryParse(c.Brightness , out brightness))
                                    {
                                        colorSpec.brightness = brightness;
                                    }
                                }
                            }
                        }
                    }

                    found = SynchSaveImage(imageUrl.Url , fileName , model.DatabaseId , plate , imageUrl.Row , imageUrl.Col , spec);
                }
                if (found)
                {
                    path = baseUrl + "/" + plateKey + "/" + fileKey;
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:LoadImage, " + eX.Message);
                throw new CException("ConnectHelper:LoadImage, " + eX.Message , eX);
            }

            return path;
        }

        /// <summary>
        /// LoadImage
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string LoadImage(ImageUrlModel imageUrl, string baseUrl, string siteDir, PlateImagesModel model)
        {
            var path = string.Empty;
            var plate = new HCS.DataAccess.Plate();
            bool found = false;
                
            try
            {
                FileLogger.Debug("ConnectHelper:LoadImage");

                var plateKey = imageUrl.PlateKey;
                var platePath = Path.Combine(siteDir, plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }

                var fileKey = imageUrl.FileKey;
                var fileName = imageUrl.FileName;
                FileLogger.Debug("ConnectHelper:LoadImage-->fileKey: " + fileKey + ", fileName: " + fileName + ", Row:" + imageUrl.Row + ", Col:" + imageUrl.Col + " baseURL" + baseUrl + ", siteDir: " + siteDir);
                plate.plateId = model.Id.ToString();

                if (!imageUrl.Overlay)
                {
                    var spec = new ImageSpec();

                    //parse out plate channel image parameters
                    spec.field = Convert.ToInt32(model.Field);
                    spec.channel = model.PlateChannelParameters.Channel;
                    spec.format = ImageUtils.IMAGE_FORMAT;
                    spec.reqImageProcess = true;
                    spec.invert = model.Invert;
                    spec.z_index = model.ZIndex != null ? model.ZIndex.Value : 0;
                    spec.timepoint = model.Timepoint != null ? model.Timepoint.Value : 0;
                    spec.version = model.Version;
                    spec.thumbPixelSize = !string.IsNullOrEmpty(model.SiteId) ? Convert.ToInt32(ImageUtils.GetThumbSize(model.SiteId)) : 0;
                    spec.color = string.IsNullOrEmpty(model.PlateChannelParameters.Color) ? "" : model.PlateChannelParameters.Color;
                    spec.channel = model.PlateChannelParameters.Channel;

                    if (!string.IsNullOrEmpty(model.PlateChannelParameters.Brightness))
                    {
                        double brightness;
                        if (double.TryParse(model.PlateChannelParameters.Brightness , out brightness))
                        {
                            spec.brightness = brightness;
                        }
                    }
                    else if (!string.IsNullOrEmpty(model.PlateChannelParameters.Auto))
                    {
                        bool autoContrast;
                        if (Boolean.TryParse(model.PlateChannelParameters.Auto , out autoContrast))
                        {
                            spec.auto = autoContrast;
                        }
                    }
                    else if (!string.IsNullOrEmpty(model.PlateChannelParameters.Level))
                    {
                        bool levels;
                        if (Boolean.TryParse(model.PlateChannelParameters.Level , out levels))
                        {
                            spec.level = levels;
                        }
                        if (levels)
                        {
                            Int64 whitePoint;
                            Int64 blackPoint;

                            if ((Int64.TryParse(model.PlateChannelParameters.WhitePoint , out whitePoint)) && Int64.TryParse(model.PlateChannelParameters.BlackPoint , out blackPoint))
                            {
                                spec.whitePoint = whitePoint;
                                spec.blackPoint = blackPoint;
                            }
                        }
                    }
                    found = SynchSaveImage(imageUrl.Url , fileName , model.DatabaseId , plate , imageUrl.Row + 1, imageUrl.Col + 1 , spec);
                }
                else
                {
                    var spec = new OverlayImageSpec();
                    spec.field = Convert.ToInt32(model.Field);
                    spec.format = ImageUtils.IMAGE_FORMAT;
                    spec.z_index = model.ZIndex != null ? model.ZIndex.Value : 0;
                    spec.timepoint = model.Timepoint != null ? model.Timepoint.Value : 0;
                    spec.version = model.Version;
                    spec.thumbPixelSize = !string.IsNullOrEmpty(model.SizeId) ? Convert.ToInt32(ImageUtils.GetThumbSize(model.SizeId)) : 0;
                    spec.invert = model.Invert;

                    // Set the channel parameters
                    foreach (ChannelImageParameters c in model.ChannelParameters)
                    {
                        var chName = c.Channel;
                        var chColor = c.Color.ToLower();
                        foreach (ImageSpec colorSpec in spec.imageElements)
                        {
                            if (chColor.Equals(colorSpec.color.ToLower()) && !"grayscale".Equals(chColor))
                            {
                                colorSpec.channel = chName;
                                if (!string.IsNullOrEmpty(c.Auto))
                                {
                                    bool autoContrast;
                                    if (Boolean.TryParse(c.Auto , out autoContrast))
                                    {
                                        colorSpec.auto = autoContrast;
                                    }
                                }
                                if (!string.IsNullOrEmpty(c.Level))
                                {
                                    bool levels;
                                    if (Boolean.TryParse(c.Level , out levels))
                                    {
                                        colorSpec.level = levels;
                                    }
                                    if (levels)
                                    {
                                        Int64 whitePoint;
                                        Int64 blackPoint;

                                        if ((Int64.TryParse(c.WhitePoint , out whitePoint)) && Int64.TryParse(c.BlackPoint , out blackPoint))
                                        {
                                            colorSpec.whitePoint = whitePoint;
                                            colorSpec.blackPoint = blackPoint;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(c.Brightness))
                                {
                                    double brightness;
                                    if (double.TryParse(c.Brightness , out brightness))
                                    {
                                        colorSpec.brightness = brightness;
                                    }
                                }
                            }
                        }
                    }
                    found = SynchSaveImage(imageUrl.Url , fileName , model.DatabaseId , plate , imageUrl.Row + 1 , imageUrl.Col + 1 , spec);
                }

                if (found)
                {
                    path = baseUrl + "/" + plateKey + "/" + fileKey;
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:LoadImage, " + eX.Message);
                throw new CException("ConnectHelper:LoadImage, " + eX.Message , eX);
            }

            return path;
        }

        /// <summary>
        /// SynchSaveImage
        /// </summary>
        /// <param name="url"></param>
        /// <param name="fileName"></param>
        /// <param name="model"></param>
        /// <param name="fieldIndex"></param>
        /// <param name="channelIndex"></param>
        /// <returns></returns>
        private bool SynchSaveImage(string url , string fileName , string databaseId , HCS.DataAccess.Plate plate , int row , int col , ImageSpec spec)
        {
            var found = false;

            try
            {
                lock (string.Intern(fileName))
                {
                    if (!File.Exists(fileName))
                    {
                        found = SaveImageInternal(url , fileName , databaseId , plate , row , col , spec);
                    }
                    else
                    {
                        found = true;
                    }
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                throw new CException("ConnectHelper:SynchSaveImage, " + eX.Message , eX);
            }
            return found;
        }

        /// <summary>
        /// SynchSaveImage
        /// </summary>
        /// <param name="url"></param>
        /// <param name="fileName"></param>
        /// <param name="model"></param>
        /// <param name="fieldIndex"></param>
        /// <param name="channelIndex"></param>
        /// <returns></returns>
        private bool SynchSaveImage(string url , string fileName , string databaseId , HCS.DataAccess.Plate plate , int row , int col , OverlayImageSpec spec)
        {
            var found = false;

            try
            {
                lock (string.Intern(fileName))
                {
                    if (!File.Exists(fileName))
                    {
                        found = SaveImageInternal(url , fileName , databaseId , plate , row , col , spec);
                    }
                    else
                    {
                        found = true;
                    }
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                throw new CException("ConnectHelper:SynchSaveImage, " + eX.Message , eX);
            }
            return found;
        }

        /// <summary>
        /// SaveImageInternal
        /// </summary>
        /// <param name="serviceUrl"></param>
        /// <param name="filePath"></param>
        /// <param name="databaseId"></param>
        /// <param name="plate"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        private bool SaveImageInternal(string serviceUrl , string filePath , string databaseId , HCS.DataAccess.Plate plate , int row , int col , ImageSpec spec)
        {
            var connect = new Connect();
            var result = false;

            try
            {
                FileLogger.Debug("ConnectHelper:SaveImageInternal");

                Stream strm = connect.GetImageCacheMetadata(databaseId , plate , row , col , spec);

                if (strm != null)
                {
                    using (FileStream writeStream = new FileStream(filePath , FileMode.Create , FileAccess.Write))
                    {
                        int Length = 256;
                        byte[] buffer = new byte[Length];
                        int bytesRead = strm.Read(buffer , 0 , Length);
                        // write the required bytes
                        while (bytesRead > 0)
                        {
                            writeStream.Write(buffer , 0 , bytesRead);
                            bytesRead = strm.Read(buffer , 0 , Length);
                        }
                        writeStream.Close();
                        result = true;
                    }
                    strm.Close();
                }
                else
                {
                    FileLogger.Debug("ConnectHelper:SaveImageInternal, Image not found: " + serviceUrl);
                }
            }
            catch(ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:SaveImageInternal " + eX.Message + " , serviceUrl=" + serviceUrl + ", filePath=" + filePath);
                throw new CException("ConnectHelper:SaveImageInternal, " + eX.Message , eX);
            }
            return result;
        }


        /// <summary>
        /// SaveImageInternal
        /// </summary>
        /// <param name="serviceUrl"></param>
        /// <param name="filePath"></param>
        /// <param name="databaseId"></param>
        /// <param name="plate"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        private bool SaveImageInternal(string serviceUrl , string filePath , string databaseId , HCS.DataAccess.Plate plate , int row , int col , OverlayImageSpec spec)
        {
            var connect = new Connect();
            var result = false;
            var well = new HCS.DataAccess.Well();

            try
            {
                FileLogger.Debug("ConnectHelper:SaveImageInternal");

                well.row = row;
                well.col = col;

                Stream strm = connect.OverlayImage(databaseId , plate , well , spec , serviceUrl);

                if (strm != null)
                {
                    using (FileStream writeStream = new FileStream(filePath , FileMode.Create , FileAccess.Write))
                    {
                        int Length = 256;
                        byte[] buffer = new byte[Length];
                        int bytesRead = strm.Read(buffer , 0 , Length);
                        // write the required bytes
                        while (bytesRead > 0)
                        {
                            writeStream.Write(buffer , 0 , bytesRead);
                            bytesRead = strm.Read(buffer , 0 , Length);
                        }
                        writeStream.Close();
                        result = true;
                    }
                    strm.Close();
                }
                else
                {
                    FileLogger.Debug("ConnectHelper:SaveImageInternal, Image not found: " + serviceUrl);
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                FileLogger.Error("ConnectHelper:SaveImageInternal " + eX.Message + " , serviceUrl=" + serviceUrl + ", filePath=" + filePath);
                throw new CException("ConnectHelper:SaveImageInternal, " + eX.Message , eX);
            }
            return result;
        }
    }
}