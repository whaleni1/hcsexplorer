﻿using System.Collections.Generic;

namespace HCSExplorer.Web.Hubs
{
    public class ConnectionsStorage
    {
        private readonly HashSet<string> _connections = new HashSet<string>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(string connectionId)
        {
            lock (_connections)
            {
                if (!_connections.Contains(connectionId))
                {
                    _connections.Add(connectionId);
                }
            }
        }

        public void Remove(string connectionId)
        {
            lock (_connections)
            {
                if (!_connections.Contains(connectionId))
                {
                    return;
                }
                _connections.Remove(connectionId);
            }
        }

        public bool Contains(string connectionId)
        {
            return _connections.Contains(connectionId);
        }
    }
}