﻿using System;

namespace HCSExplorer.Web.Hubs
{
    public class BrowseFileConfigSettings
    {
        private static int maxLoadingTime;
        private static int maxFilesLoadingCount;

        public static int MaxLoadingTime // in ms
        {
            get
            {
                if (maxLoadingTime == 0)
                {
                    string strValue = System.Configuration.ConfigurationManager.AppSettings["maxDirLoadingTime"];
                    maxLoadingTime = Int32.Parse(strValue);
                }
                return maxLoadingTime;
            }
        }

        public static int MaxFilesLoadingCount
        {
            get
            {
                if (maxFilesLoadingCount == 0)
                {
                    string strValue = System.Configuration.ConfigurationManager.AppSettings["maxFilesLoadingCount"];
                    maxFilesLoadingCount = Int32.Parse(strValue);
                }
                return maxFilesLoadingCount;
            }
        }
    }
}