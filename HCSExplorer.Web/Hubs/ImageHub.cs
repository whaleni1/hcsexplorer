﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.DTO;
using HCS.Shared.Logger;
using HCSExplorer.Services;
using HCSExplorer.Web.Consts;
using HCSExplorer.Web.Helpers;
using HCSExplorer.Web.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace HCSExplorer.Web.Hubs
{
    [HubName("imagesLoader")]
    public class ImagesHub : Hub
    {
        private readonly static ConnectionsStorage _connections = new ConnectionsStorage();
        private ConnectHelper _connectHelper = new ConnectHelper();

        /// <summary>
        /// GetMaxThreadsCount
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private int GetMaxThreadsCount(string siteId)
        {
            string strValue;
            if ("RI".Equals(siteId) || "ZJ".Equals(siteId)) // temp solution
            {
                strValue = System.Configuration.ConfigurationManager.AppSettings["maxThreadsCountSingapore"];
            }
            else
            {
                strValue = System.Configuration.ConfigurationManager.AppSettings["maxThreadsCount"];
            }
            return Int32.Parse(strValue);
        }

        /// <summary>
        /// WebClientTimeout
        /// </summary>
        private int WebClientTimeout
        {
            get
            {
                int res = 0;
                string strValue = System.Configuration.ConfigurationManager.AppSettings["webClientTimeout"];
                Int32.TryParse(strValue , out res);
                return res;
            }
        }

        /// <summary>
        /// OnConnected
        /// </
        /// >
        /// <returns></returns>
        public override Task OnConnected()
        {
            FileLogger.Debug(String.Format("{0}:Hub OnConnected: ConnectionId: {1}" , GetType().Name , Context.ConnectionId));

            _connections.Add(Context.ConnectionId);

            return base.OnConnected();
        }

        /// <summary>
        /// OnDisconnected
        /// </summary>
        /// <returns></returns>
        public override Task OnDisconnected()
        {
            FileLogger.Debug(String.Format("{0}:Hub OnDisconnected: ConnectionId: {1}" , GetType().Name , Context.ConnectionId));

            _connections.Remove(Context.ConnectionId);

            return base.OnDisconnected();
        }

        /// <summary>
        /// OnReconnected
        /// </summary>
        /// <returns></returns>
        public override Task OnReconnected()
        {
            FileLogger.Debug(String.Format("{0}:Hub OnReconnected: ConnectionId: {1}" , GetType().Name , Context.ConnectionId));

            _connections.Add(Context.ConnectionId);

            return base.OnConnected();
        }

        /// <summary>
        /// PlateImagesLoad
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ImageMatrix PlateImagesLoad(PlateImagesModel model)
        {
            ImageMatrix imgMatrix = null;

            try
            {
                FileLogger.Info(GetType().Name + ":PlateImagesLoad");

                var ctx = HttpContext.Current;
                var app = ctx.ApplicationInstance as WebApiApplication;
                var dataService = app.Container.Resolve<IDataService>();
                var executionContext = app.Container.Resolve<IExecutionContext>();
                var siteDir = ctx.Server.MapPath("~/" + WebApp.CachedImagesDir);
                var delimiter = ctx.Request.ApplicationPath.EndsWith("/") ? string.Empty : "/";
                var baseUrl = ctx.Request.ApplicationPath + delimiter + WebApp.CachedImagesUrlPath;
                var appPath = ctx.Request.ApplicationPath + delimiter;
                var testingParams = model.TestingParams;
                var threadsCount = GetMaxThreadsCount(model.SiteId);
                var webClientTimeout = 0;

                if (testingParams.HCSConnectTimeout > 0)
                {
                    webClientTimeout = testingParams.HCSConnectTimeout;
                }
                else
                {
                    // get value from web.config
                    webClientTimeout = WebClientTimeout;
                }

                if (testingParams.ThreadsCount > 0)
                {
                    threadsCount = testingParams.ThreadsCount;
                }
                if (testingParams.ClearCache && !testingParams.IsReconnect)
                {
                    ImageUtils.ClearPlateDir(siteDir , model);
                }

                if (!testingParams.IsReconnect)
                {
                    FileLogger.Debug(GetType().Name + ":PlateImagesLoad Starting to load plate (Submit pressed). Connection ID = " + Context.ConnectionId);
                    FileLogger.Debug(GetType().Name + ":PlateImagesLoad User Cookie = " + executionContext.Cookies);
                }

                var progressCount = 0;
                var matrixSize = model.Rows * model.Cols;
                var countForProgress = matrixSize;
                var matrix = new string[matrixSize];
                var getImgUrls = new string[matrixSize];
                imgMatrix = new ImageMatrix
                {
                    ImagePathes = matrix ,
                    GetImageUrls = getImgUrls ,
                    PdfPath = null
                };

                var imurls = dataService.PlateImageUrls(executionContext , baseUrl , siteDir , model);

                var loaded = imurls.Where(x => x.Loaded);
                foreach (ImageUrlModel item in loaded)
                {
                    int indx = item.Row * model.Cols + item.Col;
                    matrix[indx] = baseUrl + "/" + item.PlateKey + "/" + item.FileKey;
                    getImgUrls[indx] = item.Url;
                    progressCount++;
                }
                imgMatrix.Progress = progressCount.ToString() + "/" + countForProgress;

                if (loaded.Count() > 0)
                {
                    Clients.Caller.PlateProgress(new { Matrix = imgMatrix });
                }

                model.Well = new Well();

                object locObject = new Object();

#if DEBUG
                foreach (ImageUrlModel item in imurls)
                {
#else
                Task.Factory.StartNew(() =>
                {
                    // TODO move pdf download to UI button
                    string pdfpath = null;

                    if (Parallel.ForEach(imurls.Where(x => !x.Loaded) , new ParallelOptions { MaxDegreeOfParallelism = threadsCount } , (item , loopState) =>
                    {
                        if (!_connections.Contains(Context.ConnectionId)) // client has been disconnected
                        {
                            FileLogger.Debug(GetType().Name + ":PlateImagesLoad Plate view loading: Stop Thread");
                            loopState.Stop();
                        }
#endif
                        int indx = item.Row * model.Cols + item.Col;
                        ImageResultModel imgRes = null;
                        bool isWebClientDisconnect = false;
                        model.Well.Row = item.Row + 1;
                        model.Well.Col = item.Col + 1;

                        try
                        {
                            FileLogger.Debug(string.Format("{0}:PlateImagesLoad , Load image started. Thread ID = {1}, ConnectionID = {2}, Index = {3}" , GetType().Name , System.Threading.Thread.CurrentThread.ManagedThreadId , Context.ConnectionId , indx));

                            string res = string.Empty;
#if !DEBUG
                            if (model.SiteId.Equals(ApplicationLayoutHelper.GetLocalSite()))
#endif
                            res = _connectHelper.LoadImage(item , baseUrl , siteDir , model);
#if !DEBUG
                            else
                                res = dataService.LoadImgByUrl(executionContext , item , baseUrl , siteDir , webClientTimeout);
#endif
                        if (res != null) imgRes = new ImageResultModel
                            {
                                Path = res ,
                                GetImgUrl = item.Url
                            };
                        }
                        catch (System.Net.WebException e)
                        {
                            if (e.Status == System.Net.WebExceptionStatus.Timeout)
                            {
                                isWebClientDisconnect = true;
                            }
                            FileLogger.Error(GetType().Name + ":PlateImagesLoad, " + e.Message);
                        }
                        catch (Exception e)
                        {
                            FileLogger.Error(GetType().Name + ":PlateImagesLoad, " + e.Message);
                        }
                        string progress = null;
                        lock (locObject)
                        {
                            progressCount++;
                            progress = progressCount.ToString() + "/" + countForProgress;
                        }

                        if (imgRes == null || imgRes.Path == null)
                        {
                            matrix[indx] = ImageHelper.GetWellImgNotFoundUrl(appPath);
                            imgRes = new ImageResultModel
                            {
                                Path = ImageHelper.GetWellImgNotFoundUrl(appPath) ,
                                IsWebClientDisconnect = isWebClientDisconnect
                            };
                        }
                        else
                        {
                            matrix[indx] = imgRes.Path;
                            getImgUrls[indx] = imgRes.GetImgUrl;
                        }
#if DEBUG
                }
#else
                        Clients.Caller.PlateProgress(new { Index = indx , Image = imgRes , Progress = progress });
                        FileLogger.Debug(string.Format("{0}:PlateImagesLoad Image sent to client. Thread ID = {1}, ConnectionID = {2}, Index = {3}" , GetType().Name , System.Threading.Thread.CurrentThread.ManagedThreadId , Context.ConnectionId , indx));
                    } //close lambda 
                    ).IsCompleted)
                    {
                        //TODO move pdf download to UI button
                        try
                        {
                            string serverUrl = ctx.Request.Url.GetLeftPart(UriPartial.Authority) + ctx.Request.ApplicationPath;
                            if (!serverUrl.EndsWith("/"))
                                serverUrl += "/";
                            pdfpath = ImageUtils.CreatePdf(siteDir , baseUrl , serverUrl , ctx.Server.MapPath("~/") , WebApp.CachedImagesUrlPath , model , imgMatrix.ImagePathes , imgMatrix.GetImageUrls);
                        }
                        catch (Exception ex)
                        {
                            FileLogger.Error(GetType().Name + ":PlateImagesLoad, " + ex.Message);
                            pdfpath = "Error";
                        }
                        Clients.Caller.PlateProgress(new { PdfPath = pdfpath });
                        FileLogger.Debug(string.Format("Pdf file: {0}. ConnectionID = {1}. Path sent to a client." , pdfpath , Context.ConnectionId));
                    }
                });
#endif
            }
            catch (Exception ex)
            {
                FileLogger.Error(GetType().Name + ":PlateImagesLoad, " + ex.Message);
                new CException(this.GetType().Name + ":PlateImagesLoad, " + ex.Message , ex);
            }

            return imgMatrix;
        }

        /// <summary>
        /// WellImagesLoad
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Object WellImagesLoad(WellImagesModel model)
        {
            try
            {
                FileLogger.Info(GetType().Name + ":WellImagesLoad");

                var ctx = HttpContext.Current;
                var app = ctx.ApplicationInstance as WebApiApplication;
                var dataService = app.Container.Resolve<IDataService>();
                var executionContext = app.Container.Resolve<IExecutionContext>();
                string siteDir = ctx.Server.MapPath("~/" + WebApp.CachedImagesDir);
                string delimiter = ctx.Request.ApplicationPath.EndsWith("/") ? string.Empty : "/";
                string baseUrl = ctx.Request.ApplicationPath + delimiter + WebApp.CachedImagesUrlPath;
                string appPath = ctx.Request.ApplicationPath + delimiter;
                int colcnt = (model.Channels.Count > 1) ? (model.Channels.Count + 1) : model.Channels.Count;
                int matrixSize = colcnt * model.Fields.Count;
                int countForProgress = model.EnableOverlay ? matrixSize : (model.Channels.Count * model.Fields.Count);
                int progressCount = 0;
                string[] matrix = new string[matrixSize];
                string[] getImgUrls = new string[matrixSize];
                ImageMatrix imgMatrix = new ImageMatrix
                {
                    ImagePathes = matrix ,
                    GetImageUrls = getImgUrls ,
                    Well = model.Well
                };

                IList<ImageUrlModel> imurls = dataService.WellImageUrls(executionContext , baseUrl , siteDir , model);
                if (model.Channels.Count > 1 && (!model.EnableOverlay))
                {
                    string ovlDis = ImageHelper.GetOverlayDisableImgUrl(appPath);

                    for (int i = 0 ; i < model.Fields.Count ; i++)
                    {
                        int indx = i * colcnt + model.Channels.Count;
                        matrix[indx] = ovlDis;
                        getImgUrls[indx] = null;
                    }
                }

                var loaded = imurls.Where(x => x.Loaded);
                foreach (ImageUrlModel item in loaded)
                {
                    int indx = 0;
                    if (item.Overlay)
                        indx = item.FieldIndex * colcnt + model.Channels.Count;
                    else
                        indx = item.FieldIndex * colcnt + item.ChannelIndex;

                    matrix[indx] = baseUrl + "/" + item.PlateKey + "/" + item.FileKey;
                    getImgUrls[indx] = item.Url;
                    imgMatrix.Progress = progressCount.ToString() + "/" + countForProgress;
                    progressCount++;
                }

                if (loaded.Count() > 0)
                    Clients.Caller.Progress(imgMatrix);

                object locObject = new Object();
#if DEBUG
                foreach (ImageUrlModel item in imurls)
#else
                Parallel.ForEach(imurls.Where(x => !x.Loaded) , new ParallelOptions { MaxDegreeOfParallelism = GetMaxThreadsCount(model.SiteId) } , item =>
#endif
                {
                    int indx = 0;
                    if (item.Overlay)
                        indx = item.FieldIndex * colcnt + model.Channels.Count;
                    else
                        indx = item.FieldIndex * colcnt + item.ChannelIndex;

                    ImageResultModel imgRes = null;
                    try
                    {
                        string res = string.Empty;
#if !DEBUG
                        if (model.SiteId.Equals(ApplicationLayoutHelper.GetLocalSite()))
#endif
                            res = _connectHelper.LoadImage(item , baseUrl , siteDir , model);
#if !DEBUG
                        else
                            res = dataService.LoadImgByUrl(executionContext , item , baseUrl , siteDir , WebClientTimeout);
#endif

                        if (res != null) imgRes = new ImageResultModel
                        {
                            Path = res ,
                            GetImgUrl = item.Url
                        };
                    }
                    catch (Exception e)
                    {
                        FileLogger.Error(GetType().Name + ":WellImagesLoad, " + e.Message);
                    }

                    //lock (locObject)
                    //{
                    progressCount++;
                    imgMatrix.Progress = progressCount.ToString() + "/" + countForProgress;
                    //}

                    if (imgRes == null || imgRes.Path == null)
                        matrix[indx] = ImageHelper.GetWellImgNotFoundUrl(appPath);
                    else
                    {
                        matrix[indx] = imgRes.Path;
                        getImgUrls[indx] = imgRes.GetImgUrl;
                    }

                    Clients.Caller.Progress(imgMatrix);
                } //close lambda 
#if !DEBUG
                );
#endif
                return imgMatrix;
            }
            catch (Exception ex)
            {
                FileLogger.Error(GetType().Name + ":WellImagesLoad, " + ex.Message);
                new CException(this.GetType().Name + ":WellImagesLoad, " + ex.Message , ex);

                return new Error
                {
                    Message = Resources.Application.GetImagesError
                };
            }
        }
    }
}
