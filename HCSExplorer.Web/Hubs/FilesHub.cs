﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using HCS.Shared.Logger;
using HCSExplorer.Web.Helpers;
using HCSExplorer.Web.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace HCSExplorer.Web.Hubs
{
    [HubName("filesLoader")]
    public class FilesHub : Hub
    {
        private const int HubResponseInterval = 1000; // in ms
        private static readonly string[] ExcludeNames = { "." , ".." };

        /// <summary>
        /// FilesLoad
        /// </summary>
        /// <param name="options"></param>
        public void FilesLoad(BrowseOptions options)
        {
            var dir = HttpUtility.UrlDecode(options.RootPath);

            var cts = new CancellationTokenSource();
            Task task = Task.Factory.StartNew(() =>
            {
                List<FileData> list = new List<FileData>();
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                TimeSpan tssend = stopWatch.Elapsed;
                string errorMsg = null;
                string notAllDataMsg = null;
                try
                {
                    using (FileEnumerator enumerator = new FileEnumerator(dir))
                    {
                        while (enumerator.MoveNext())
                        {
                            if (!ExcludeNames.Contains(enumerator.Current.Name))
                            {
                                list.Add(enumerator.Current);
                            }

                            TimeSpan ts = stopWatch.Elapsed;

                            if (ts.TotalMilliseconds > BrowseFileConfigSettings.MaxLoadingTime || list.Count >= BrowseFileConfigSettings.MaxFilesLoadingCount)
                            {
                                if (list.Count >= BrowseFileConfigSettings.MaxFilesLoadingCount)
                                {
                                    notAllDataMsg = string.Format(Resources.Application.ImgBrowseDlgCountLimitMsg , BrowseFileConfigSettings.MaxFilesLoadingCount);
                                }
                                else
                                {
                                    if (list.Count == 0)
                                        notAllDataMsg = string.Format(Resources.Application.ImgBrowseDlgTimeoutMsgNoFiles , BrowseFileConfigSettings.MaxLoadingTime);
                                    else
                                        notAllDataMsg = Resources.Application.ImgBrowseDlgTimeoutMsg;
                                }

                                cts.Cancel();
                                break;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    FileLogger.Error(GetType().Name + ":FileLoad, " + e.Message);
                    errorMsg = e.Message;
                }
                stopWatch.Stop();
                if (errorMsg == null && options.IsRoot && !cts.IsCancellationRequested && list.Count == 0)
                {
                    errorMsg = Resources.Application.ImgBrowseDlgRootIsEmpty;
                }
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    Clients.Caller.FileProgress(new { Message = errorMsg , Options = options , Files = SortFiles(list) });
                }
                else
                {
                    Clients.Caller.FileProgress(new { Options = options , Files = SortFiles(list) , NotAllData = cts.IsCancellationRequested , NotAllDataMsg = notAllDataMsg });
                }

                return;
            } , cts.Token);
        }

        /// <summary>
        /// SortFiles
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<FileData> SortFiles(List<FileData> list)
        {
            FileLogger.Debug(string.Format("Files count: {0}" , list.Count));
            return list.OrderBy(f => !f.IsFolder).ThenBy(n => n.Name);
        }
    }
}
