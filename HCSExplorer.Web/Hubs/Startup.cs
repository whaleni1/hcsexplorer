﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System;
using System.Web.Configuration;
[assembly: OwinStartup(typeof(HCSExplorer.Web.Hubs.Startup))]

namespace HCSExplorer.Web.Hubs
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            var strDisconnectTimeout = WebConfigurationManager.AppSettings["signalRDisconnectTimeout"];
            int disconnectTimeout;
            if (int.TryParse(strDisconnectTimeout, out disconnectTimeout))
            {
                GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(disconnectTimeout);
            }

            var strConnectionTimeout = WebConfigurationManager.AppSettings["signalRConnectionTimeout"];
            if (!string.IsNullOrEmpty(strConnectionTimeout))
            {
                int connectionTimeout;
                if (int.TryParse(strDisconnectTimeout, out connectionTimeout))
                {
                    GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(connectionTimeout);
                }
            }

            app.MapSignalR();
        }
    }
}
