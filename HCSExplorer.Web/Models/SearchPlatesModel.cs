﻿using System.Collections.Generic;
using HCS.Shared.DTO;

namespace HCSExplorer.Web.Models
{
    public class SearchPlatesModel
    {
        public IList<ObjectBase> Sites { get; set; }
        public IList<ObjectBase> Databases { get; set; }
        public string SiteId { get; set; }
        public string SiteName { get; set; }
        public string DatabaseId { get; set; }
        public string DatabaseName { get; set; }
        public string Barcode { get; set; }
        public IList<Plate> Plates { get; set; }
        public string LocalSite { get; set; }
    }
}
