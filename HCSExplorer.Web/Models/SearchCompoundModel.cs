﻿using System.Collections.Generic;
using HCS.Shared.DTO;

namespace HCSExplorer.Web.Models
{
    public class SearchCompoundModel
    {
        public string Helios { get; set; }
        public string CompoundId { get; set; }
        public IList<Compound> Compounds { get; set; }
    }
}
