﻿using System.Collections.Generic;
using HCS.Shared.DTO;

namespace HCSExplorer.Web.Models
{
    public class CreateRunModel : RunDetails
    {
        public string LocalSite { get; set; }
        public IList<ObjectBase> Sites { get; set; }
        public IList<ObjectBase> Databases { get; set; }
        public IList<ObjectBase> Paths { get; set; }

        public string PathId { get; set; }

        public CreateRunModel()
        {
        }

        public CreateRunModel(RunDetails r)
        {
            Id = r.Id;
            CreatedBy = r.CreatedBy;
            CreatedDate = r.CreatedDate;
            Name = r.Name;
            Description = r.Description;
            HeliosId = r.HeliosId;
            AresId = r.AresId;
            AssayDescription = r.AssayDescription;
            Dirs = r.Dirs;
            WavelengthMap = r.WavelengthMap;
            Channels = r.Channels;
            ProtocolName = r.ProtocolName;
            ProtocolRef = r.ProtocolRef;
            ProtocolLocalFileName = r.ProtocolLocalFileName;
            Block = r.Block;
            DataLocation = r.DataLocation;
            DatabaseId = r.DatabaseId;
            InOneFile = r.InOneFile;
            Icp4 = r.Icp4;
            ImageListOnly = r.ImageListOnly;
            ImagesInputType = r.ImagesInputType;
            ImageListName = r.ImageListName;
            ImageListRef = r.ImageListRef;
            ImgListLocalFileName = r.ImgListLocalFileName;

            // Post Processing
            PostProcessingEnabled = r.PostProcessingEnabled;
            PpCountsSummed = r.PpCountsSummed;
            PpMeanMeanIntensity = r.PpMeanMeanIntensity;
            PpMedianMedianIntensity = r.PpMedianMedianIntensity;
            PpStdDeviationMeanIntensity = r.PpStdDeviationMeanIntensity;
        }
    }
}