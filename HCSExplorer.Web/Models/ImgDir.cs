﻿using System.Collections.Generic;

namespace HCSExplorer.Web.Models
{
    public class ImgDir
    {
        public string Key { get; set; }//Path
        public string Title { get; set; }
        public bool IsFolder { get; set; }
        public bool IsLazy { get; set; }
        public bool Expand { get; set; }
        public bool HideCheckbox { get; set; }
        public List<ImgDir> Children { get; set; }
    }

    public class FileData 
    {
        public string Path {get; set; }//Path
        public string Name { get; set; }
        public bool IsFolder { get; set; }
    }

    public class BrowseOptions
    {
        public string SiteId { get; set; }
        public string DatabaseId { get; set; }
        public string RootPath { get; set; }
        public bool IsRoot { get; set; }
        public string ExpandedKeyList { get; set; }
    }
}