﻿using System.Collections.Generic;
using HCS.Shared.DTO;

namespace HCSExplorer.Web.Models
{
    public class RunsListModel
    {
        public IList<Run> Runs { get; set; }
    }
}