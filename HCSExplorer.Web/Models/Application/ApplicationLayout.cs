﻿namespace HCSExplorer.Web.Models.Application
{
    public class ApplicationLayout
    {
        public Footer Footer { get; set; }
        public Menu Menu { get; set; }
        public string UserName { get; set; }
        public string AppHelpContent { get; set; }
        public string NibrFirst{ get; set; }
        public string NibrLast{ get; set; }
        public string Version{ get; set; }
        public bool DebugMode { get; set; }
    }
}
