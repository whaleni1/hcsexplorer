﻿namespace HCSExplorer.Web.Models.Application
{
    public class ScriptPair
    {
        public ScriptPair(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; set; }
        public string Value { get; set; }
    }
}
