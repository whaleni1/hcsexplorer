﻿namespace HCSExplorer.Web.Models.Application
{
    public class Menu
    {
        public Menu(string name, string link)
        {
            Name = name;
            Link = link;
        }

        public string Name { get; set; }
        public string Link { get; set; }
        public Menu[] Items { get; set; }
    }
}
