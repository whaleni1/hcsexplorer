﻿using System;

namespace HCSExplorer.Web.Models.Application
{
    public class Footer
    {
        public bool InitializeVisibility { get; set; }
        public string Version { get; set; } //must be removed
        public string Copyright { get; set; }
        //Version
        public int Build { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Revision { get; set; }
        public DateTime date { get; set; }
    }
}
