﻿namespace HCSExplorer.Web.Models
{
    public class Error
    {
        public class ValidationDescriptor
        {
            public int Level { get; set; }
            public string ModelName { get; set; }
            public string ElementName { get; set; }
            public string[] Description { get; set; }
            public int Row { get; set; }
        }

        public string Message { get; set; }

        public string MessageDetail { get; set; }

        public ValidationDescriptor[] ValidationErrors { get; set; }
        public bool ReloginRequired { get; set; }

        public string PadString { get; set; }
    }
}
