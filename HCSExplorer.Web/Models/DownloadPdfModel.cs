﻿namespace HCSExplorer.Web.Models
{
    public class DownloadPdfModel
    {
        public string PdfPath { get; set; }
    }
}
