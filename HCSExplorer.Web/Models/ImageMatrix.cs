﻿using HCS.Shared.DTO;

namespace HCSExplorer.Web.Models
{
    public class ImageMatrix
    {
        public string[] ImagePathes { get; set; }
        public string[] GetImageUrls { get; set; }
        public string Progress { get; set; }
        public Well Well { get; set; }
        public string PdfPath { get; set; }
    }
}
