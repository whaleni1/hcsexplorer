﻿namespace HCSExplorer.Web.Models
{
    public class DownloadImageModel
    {
        public bool PathChBox { get; set; }
        public bool PlateIdChBox { get; set; }
        public bool ChannelChBox { get; set; }
        public bool FieldChBox { get; set; }
        public bool RowColumnChBox { get; set; }
        public bool VersionChBox { get; set; }
        public bool ZIndexChBox { get; set; }
        public bool TimepointChBox { get; set; }

        public string TextColor { get; set; }
        public string TextSize { get; set; }
        public string CustomText { get; set; }
 
        public string ImgUrl { get; set; }
        public string ImgPath { get; set; }
        public string Barcode { get; set; }
        public string PlatePath { get; set; }
    }
}