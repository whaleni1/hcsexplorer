﻿namespace HCSExplorer.Web.Consts
{
    internal static class Messages
    {
        public const string ErrorOnStop = "WebApp: Errors when stopping WebApp";
        public const string InitialStop = "WebApp: WebApp is stopping";
        public const string SuccessfullyStop = "WebApp: WebApp has successfully stopped";
        public const string WebAppErrorOnStartup = "WebApp: Errors when starting WebApp";
        public const string WebAppInitialStart = "WebApp: WebApp is starting";
        public const string WebAppSuccessfullyStart = "WebApp: WebApp has been successfully started";

        public const string WebAppLoadServicesStart = "WebApp: Load Widget services as separate task is starting";
        public const string WebAppLoadServicesDone = "WebApp: Widget Services has been successfully loaded";
        public const string WebAppErrorLoadingServices = "WebApp: Errors when loading Widget services";
        public const string FailedToGetScriptList = "WebApp: Failed to get scripts from script config file";

        public const string GetLayoutError = "ApplicationController: Error retrieving application layout";
        public const string GetSearchError = "SearchController: Error";
        public const string DictionaryError = "DictionaryController: Failed to get {0}";


        // Common controllers message template
        public const string DALException = "{0}: {1}";
        public const string NotFound = "{0}: {1} not found (Id = {2})";
        public const string FailedToGet = "{0}: Failed to get {1} (Id = {2})";
        public const string FailedToCreate = "{0}: Failed to create a new {1}";
        public const string FailedToUpdate = "{0}: Failed to update {1} (Id= {2})";
        public const string FailedToDelete = "{0}: Failed to delete {1} (Id = {2})";
        public const string FailedToGetManagement = "{0}: Failed to get {1} management model";
        public const string NotFoundInLdap = "{0}: {1} not found in LDAP (Email = {2})";
        public const string AlreadyExist = "{0}: {1} already exist (Email = {2})";
        public const string Missing = "{0}: {1} missing";


        // Object Names for controllers message template
        public const string User = "User";


        // Validate Protocol messages
        public const string NotMimeMultipartContent = "{0}: Not Mime Multipart Content";
        public const string ErrorValidateProtocolFile = "{0}: Failed to validate protocol file";
        public const string XssAttacDetected = "{0}: {1}";
    }
}
