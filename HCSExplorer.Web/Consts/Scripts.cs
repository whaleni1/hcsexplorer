﻿namespace HCSExplorer.Web.Consts
{
    internal static class Scripts
    {
        public const string ScriptsPrefix = "~/Scripts/{0}";
        public const string BundlePrefix = "~/bundles/{0}";
    }
}
