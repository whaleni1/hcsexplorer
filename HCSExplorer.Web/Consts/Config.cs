﻿
namespace HCSExplorer.Web.Consts
{
  static class Config
  {

    public const string InitMode                    = "InitMode";
    public const string MenuFile                    = "MenuFile";
    public const string ScriptFile                  = "ScriptFile";
    public const string DateTimeFormat              = "yyyy-MM-ddTHH:mm:ss.fff";

    public const int    RowsPerPage                = 15;

  }
}