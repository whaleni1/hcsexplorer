﻿namespace HCSExplorer.Web.Consts
{
    internal static class WebApp
    {
        public const string Container = "container";
        public const string Loader = "loader";
        public const string UnhandledErr = "HCSExplorer: Unhandled Error";
        public const string CustomErrorsSection = "system.web/customErrors";
        public const string TypeIsNullErr = "{0} not found";
        public const string TextPlain = @"text/plain";
        public const string TextHTML = @"text/HTML";
        public const string UploadedFile = @"uploadedfile";
        public const string AppDataPath = @"~/App_Data";
        public const string CachedImagesUrlPath = "LoadedImages";

        public static string CachedImagesDir
        {
            get
            {
                string prefix = "../";
#if DEBUG
                prefix = string.Empty;
#endif
                return prefix + CachedImagesUrlPath;
            }
        }
    }
}
