﻿using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Windsor;
using HCS.Shared.Logger;
using log4net.Config;

namespace HCSExplorer.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Container
        /// </summary>
        public IWindsorContainer Container
        {
            get { return (IWindsorContainer) Application[Consts.WebApp.Container]; }
            set { Application[Consts.WebApp.Container] = value; }
        }

        /// <summary>
        /// Application_Start
        /// </summary>
        protected void Application_Start()
        {
            // FIX disable AppDomain restart when deleting subdirectory   
            // This code will turn off monitoring from the root website directory.   
            // Monitoring of Bin, App_Themes and other folders will still be operational, so updated DLLs will still auto deploy.   
            PropertyInfo p = typeof(HttpRuntime).GetProperty("FileChangesMonitor" , BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            object o = p.GetValue(null , null);

            FieldInfo f = o.GetType().GetField("_dirMonSubdirs" , BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.IgnoreCase);

            object monitor = f.GetValue(o);

            MethodInfo m = monitor.GetType().GetMethod("StopMonitoring" , BindingFlags.Instance | BindingFlags.NonPublic); m.Invoke(monitor , new object[] { });

            XmlConfigurator.Configure();
            Container = WindsorContainerConfig.Install();

            FileLogger.Info(Consts.Messages.WebAppInitialStart);

            SerializationConfig.ConfigJsonSerialization();
            MvcFilterConfig.RegisterGlobalMvcFilters(GlobalFilters.Filters);
            WebApiFilterConfig.RegisterGlobalMvcFilters(GlobalConfiguration.Configuration.Filters);

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            FileLogger.Info(Consts.Messages.WebAppSuccessfullyStart);
        }

        /// <summary>
        /// Application_Error
        /// </summary>
        protected void Application_Error()
        {
            var ex = Server.GetLastError();

            FileLogger.Error(GetType().Name + ":Application_Error, " + Consts.WebApp.UnhandledErr , ex);

            var webEx = ex as HttpException;
            if (webEx != null && webEx.GetHttpCode() == 404)
            {
                var configuration = WebConfigurationManager.OpenWebConfiguration(HttpRuntime.AppDomainAppVirtualPath);
                var section = (CustomErrorsSection) configuration.GetSection(Consts.WebApp.CustomErrorsSection);

                if (section.Mode != CustomErrorsMode.Off)
                {
                    Server.ClearError();
                    Response.Redirect(section.DefaultRedirect);
                }
            }
        }

        /// <summary>
        /// Application_End
        /// </summary>
        protected void Application_End()
        {
            HttpRuntime runtime =
               (HttpRuntime) typeof(System.Web.HttpRuntime).InvokeMember("_theRuntime" ,
                  BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.GetField ,
                  null , null , null);

            if (runtime == null)
                return;

            string shutDownMessage =
               (string) runtime.GetType().InvokeMember("_shutDownMessage" ,
                   BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField ,
                   null , runtime , null);

            string shutDownStack =
               (string) runtime.GetType().InvokeMember("_shutDownStack" ,
                   BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField ,
                   null , runtime , null);

            //FileLogger.Debug(string.Format("\r\n\r\n_shutDownMessage={0}\r\n\r\n_shutDownStack={1}" , shutDownMessage , shutDownStack));
        }
    }
}
