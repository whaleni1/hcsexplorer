﻿define(['bootbox', 'backbone', 'validator', 'filedownload'],
  function (bootbox) {
      var utils = {
      viewError: function (view) {
        var viewutils = this;
        var inDialog = (view.options != null) && (view.options.inDialog);
        var this_ = this;
        view.model.on('error', function (par, e, obj) {
         try {
            var result = $.parseJSON(e.responseText);
            this_.setAllErrors(view, result);
          }
          catch (exp) {
            this_.showErrorBox(view, exp, inDialog);
          }
        }, view);
        Backbone.Validation.bind(view, {
          forceUpdate: true,
          valid: function (view, selector, err) {
           
            this_.setFieldValidationErr(view, selector, '');
          },
          invalid: function (view, selector, err, selectedAttr) {

            this_.setFieldValidationErr(view, selector, err);
          }
        });

        return view;
      },
      setFieldValidationErr: function (view, selector, error) {
          //view.$('#' + selector).next().html(error);
          if (error != null && error != '') {
              view.$('#' + selector).closest('.form-group').addClass('has-error');
              view.$('#' + selector).closest('.form-group').find('.help-block').html('<div class="error">'+error+ "</div>");
          }
          else {
              view.$('#' + selector).closest('.form-group').removeClass('has-error');
              view.$('#' + selector).closest('.form-group').find('.help-block').html('');
          }
      },
      setUploadFieldValidationErr: function (view, selector, error) {
          //view.$('#' + selector).parent().next().html(error);
          view.$('#' + selector).closest('.form-group').find('.error').html(error);
      },
      setAllErrors: function (view, result) {
        var this_ = this;
        var inDialog = (view.options != null) && (view.options.inDialog);
        if (result.message != null) {
          this.showErrorBox(view, result, inDialog);
        }
        if (result.message == null) {
          this.hideErrorBox(view, inDialog);
        }
        _.forEach(result.validationErrors, function (err, selectorName) {
          if ($('#' + err.elementName).length > 0) {
              if (err.elementName.indexOf('uploadedfile')<0) {
                  //$('#' + err.elementName).addClass('has-error');
                  $('#' + err.elementName).closest('.form-group').addClass('has-error');
                  this_.setFieldValidationErr(view, err.elementName, err.description);
              } else {
                  this_.setUploadFieldValidationErr(view, err.elementName, err.description);
              }
          } else {
            this_.showErrorBox(view, { message: err.elementName + ":  " + err.description }, inDialog);
          }
        });
      },
      findDlgErrorBox: function (obj) {
        var box = obj.find('.errorBoxDlg');
        if (box.length > 0)
          return box;
        else {
          if (obj.parent().length > 0)
            box = this.findDlgErrorBox(obj.parent());
          else return;
          return box;
        }
      },
      showErrorBox: function (view, result, inDialog) {
        var viewutils = this;
        var error = result.message;
        var shtml = '<span class="nb"></span>' + error;
        if (!inDialog) {
          view.$('.js-errorBox').show();
          view.$('.js-errorBox').html(shtml);
        } else {
          var errDiv = this.findDlgErrorBox(view.$el);
          errDiv.html(shtml);
          errDiv.addClass('errorBoxDlgBck');        
        }
      },
      hideErrorBox: function (view, inDialog) {
        if (!inDialog) {
          view.$('.js-errorBox').hide();
          view.$('.js-errorBox').empty();
        } else {
          view.$('.errorBoxDlg').empty();
          view.$('.errorBoxDlg').removeClass('errorBoxDlgBck');
        }
      },
      errorPopup: function (model) {
        var utils = this;
        model.off('error');
        model.on('error', function (view, e) {
          var result = utils.errorText(e);
          utils.showErrorPopup(result);
        });
      },
      decodeModelFields: function (model) {
          var utils = this;
          _.each(_.keys(model.attributes), function (name) {
              utils.decodeModelField(model, name);
          });
      },
      decodeModelField: function (model, name) {
          var utils = this;
          var val = model.attributes[name];
          if ($.type(val) == "string")
              model.set(name, utils.decodeString(val));
      },
      decodeString: function (val) {
          return $("<div/>").html(val).text();
      },
      showErrorPopupOld: function (result, fnCallback , title) {
        var utils = this;
        var errtext = result.message;
        if(title==null)title = "Error";
        $("#errDialog").html('<br/>' + errtext);
        $('#errDialog').dialog({
          title: title,  
          autoOpen: false,
          width: 470,
          minHeight: 150,
          resizable: false,
          modal: true,
          position: { my: 'center', at: 'center', of: window },
          close: function () {
            if (result.reloginRequired) utils.reload();
          },
          buttons: {
            Ok: function () {
              $(this).dialog("close");
              if (fnCallback != null && $.isFunction(fnCallback)) {
                _.delay(fnCallback, 1);
              }
              return false;
            }
          }

        });
        _.delay(function () { $("#errDialog").dialog('open'); }, 1);
      },
      showErrorPopup: function (result, fnCallback, title) {
          var utils = this;
          var errtext = result.message;
          if (!errtext || errtext.length == 0) {
              errtext = "Unknown error.";
          }

          if (title == null) title = "Error";

          var err = bootbox.dialog({
              title: title,
              message: errtext,
              closeButton: false,
              buttons: {
                  ok: {
                      label: "OK",
                      className: "btn-primary",
                      callback: function () {
                          if (fnCallback != null && $.isFunction(fnCallback)) {
                              _.delay(fnCallback, 1);
                          }
                          if (result.reloginRequired) {
                              utils.reload();
                          }
                      }
                  }
              }
          });

          err[0].style.zIndex = 1061;
          err.next()[0].style.zIndex = 1060;
      },
      showWaitPopupOld: function (msg, title) {
          var utils = this;
          $("#waitDialog").html('<div style="text-align: center; padding: 16px 0 10px;"><span class="spinner"></span>' + msg + "</div>");
          $('#waitDialog').dialog({
              autoOpen: true,
              minHeight: 50,
              resizable: false,
              modal: true,
              position: { my: 'center', at: 'center', of: window },
              open: function (event, ui) {
                  $(this).closest('.ui-dialog').find('.ui-dialog-titlebar').hide();
              }
          });
      },
      showWaitPopup: function (msg, title) {
          var utils = this;
          utils.waitDlg = bootbox.dialog({
              title: title,
              message: '<div style="text-align: center; padding: 16px 0 10px;"><span class="spinner"></span>' + msg + '</div>',
              closeButton: false
          });
      },
      closeWaitPopupOld: function () {
          //$("#waitDialog").dialog('close');
          _.delay(function () { $("#waitDialog").dialog('close'); }, 10);
      },
      closeWaitPopup: function () {
          var utils = this;
          _.delay(function () { utils.waitDlg.modal("hide"); }, 10);
      },
      downloadFile: function (el) {
          var utils = this
          utils.showWaitPopup("Downloading file...", null);
          $.fileDownload($(el).prop('href'), {
              successCallback: function (url) {
                  utils.closeWaitPopup();
              },
              failCallback: function (responseHtml, url) {
                  utils.closeWaitPopup();
                  utils.showErrorPopup({ message: responseHtml }, null, "Download Error");
              }
          });
          return false; //this is critical to stop the click event which will trigger a normal file download!
      },
      errorText: function (response, forValidation) {
        var result = {};
        result.reloginRequired = false;
        if (response == null) return result;
        try {
          if (response.responseText != null) {
            var obj = $.parseJSON(response.responseText);
            result.reloginRequired = (obj.reloginRequired) ? true : false;
            if (obj.message != null) {
              if (obj.messageDetail != null) {
                result.message = obj.message + obj.messageDetail;
                return result;
              }
              result.message = obj.message;
              result.validationErrors = obj.validationErrors;
              return result;
            }
            if (obj.validationErrors != null) {
                if (forValidation) {
                    result.validationErrors = obj.validationErrors;
                } else {
                    var descriptions = [];
                    _.each(obj.validationErrors, function (err) {
                        _.each(err.description, function (el) { descriptions.push(el); });
                    });
                    result.message = _.uniq(descriptions).join("<br/>");
                }
              return result;
            }
          }
        } catch (e) { }
        if (response.status == 200) return result;
        if (response.responseText != null && response.status == 510) {
          result.message = response.responseText;
          return result;
        }
        if (response.statusText != null) {
          result.message = response.statusText;
          return result;
        }
        return result;
      },
      wasError: function (response) {
        if (response.readyState == 4 && response.status != 200)
          return true;
        else
          return false;
      },
      wasDialogFetchErrorOld: function (view, response) {
        if (this.wasError(response)) {
          var result = this.errorText(response);
          this.showErrorPopupOld(result);
          return true;
        }
      },
      wasDialogFetchError: function (view, response) {
          if (this.wasError(response)) {
              var result = this.errorText(response);
              this.showErrorPopup(result);
              return true;
          }
      },
      reload: function () {
        window.location.reload();
      },

      wasFetchError: function (view, response) {
        var ans = false;
        if (response.readyState == 4 && response.status != 200) {
            var result = this.errorText(response);

            this.showErrorBox(view, result, false);
            ans = true;
        }
        return ans;
      },
      createSelect: function (select, list, needEmptyOption , emptyText) {
        var html = "";
        if (needEmptyOption)
            html = "<option value=\"\">"+ emptyText + "</option>";
        if (list != null) {
          for (var i = 0; i < list.length; i++) {
            var val = list[i].objectId != null ? list[i].objectId : list[i].id;
            if (val == null) val = list[i].name;
            if (val)
              html += "<option value=\"" + val + "\">";
            else
              html += "<option>";
            html += list[i].name + "</option>";
          }
        }
        select.html(html);
      },
      createSelectSimpleInt: function (select, intVal, needEmptyOption, emptyText) {
          var utils = this;
          var list = [];
          for (var i = 1; i <= intVal; i++)
              list.push(i);
          var html = utils.optionsForSelect(list, needEmptyOption, emptyText);
          select.html(html);
      },
      createSelectSimple: function (select, list, needEmptyOption, emptyText) {
          var utils = this;
          var html = utils.optionsForSelect(list, needEmptyOption, emptyText);
          select.html(html);
      },
      optionsForSelect: function (list, needEmptyOption, emptyText , val)
      {
          var html = "";
          if (needEmptyOption)
              html = "<option value=\"\">" + emptyText + "</option>";
          if (list != null) {
              for (var i = 0; i < list.length; i++) {
                  if (list[i]) {
                      var sel = '';
                      if (val != null && val.length > 0 && val == list[i])
                          sel = '" selected="selected';
                      html += "<option value=\"" + list[i] + sel + "\">" + list[i] + "</option>";
                  }

              }
          }
          return html;
      },
      multiselect: function (select, defaults) {
        if ($.isArray(defaults) && defaults.length > 0) {
          _.each(defaults, function (el) {
            select.find('option[value=' + el + ']').attr('selected', 'selected');
          });
        }
        select.multiselect({
          noneSelectedText: "None",
          hight: "auto",
          minWidth: '170',
          selectedText: function (numChecked, numTotal, checkedItems) {
            if (numChecked == numTotal) {
              return 'All';
            }
            if (numChecked == 1) {
              return checkedItems[0].attributes['title'].nodeValue;
            }
            else {
              return numChecked + ' of ' + numTotal + ' checked';
            }
          }
        });
      },
      setSelectFunctionality: function (model, select, name, addempty , emptytext) {
        var arr = model.get(name);
        this.createSelect(select, arr, addempty, emptytext);
      },
      save: function (view, beforeSaving, afterSaving, showErrorInDialog, onError, notModelData) {
         var utils = this;
         if (notModelData == null) notModelData = {};
         if (view.model.isValid(true)) {
          utils.disableView(view);
            var wasErr = false;
            if (beforeSaving) {
              beforeSaving(view);
            }
            view.model.save(notModelData, {
                async: true,
                error: function (model, response) {
                    if (showErrorInDialog)
                        utils.wasDialogFetchError(view, response);
                    else
                        utils.wasFetchError(view, response);
                    utils.enableView(view);
                    if (onError) {
                        onError(view);
                    }
                },
                success: function () {
                    if (afterSaving) {
                        afterSaving(view);
                    }
                    utils.enableView(view);
                }
            });
        }
      },
      enableElement: function (view, sel, enabled) {
          if (enabled) {
              view.$(sel).removeAttr('disabled');
          } else {
              view.$(sel).attr('disabled', 'disabled');
          }
      },
      enableByClass: function (view, sel, enabled) {
          if (enabled) {
              view.$(sel).removeClass("disabled");
          } else {
              view.$(sel).addClass("disabled");
          }
      },
      disableView: function (view) {
        var loadingClass = '.loadingImg';
        if (view.loadingImgSelector != null)
          loadingClass = view.loadingImgSelector;
        view.$(loadingClass).show();
        view.$('input, select, textarea, button').attr('disabled', 'disabled');
      },
      enableView: function (view, holdDisabledSelector) {
        var loadingClass = '.loadingImg';
        if (view.loadingImgSelector != null)
          loadingClass = view.loadingImgSelector;
        view.$(loadingClass).hide();
        view.$('input, select, textarea, button').removeAttr('disabled');
        if (holdDisabledSelector) {
            view.$(holdDisabledSelector).attr("disabled", "disabled");
        }
      },
      createDialog: function (view, dlgselector, name, width, height) {
        var title = name ;
        $(dlgselector).show();
        view.dialog = $(dlgselector).dialog({
          title: title,
          autoOpen: false,
          width: (width) ? width : 470,
          height: (height) ? height : 'auto',
          resizable: false,
          modal: true,
          close: function () { view.undelegateEvents(); view.close() },
          position: { my: 'center', at: 'center', of: window },
          buttons: {}
        });
        view.dialog.dialog('open');
      },
      dateFormat: function (d) {
        var month = d.getUTCMonth() + 1;
        var day = d.getUTCDate();
        var hour = d.getUTCHours();
        var minute = d.getUTCMinutes();
        var second = d.getUTCSeconds();
        var output =
            (('' + month).length < 2 ? '0' : '') + month + '/' +
            (('' + day).length < 2 ? '0' : '') + day + '/' +
             d.getFullYear() + ' ' +
            (('' + hour).length < 2 ? '0' : '') + hour + ':' +
            (('' + minute).length < 2 ? '0' : '') + minute + ':' +
            (('' + second).length < 2 ? '0' : '') + second;
        return output;
      },
  
      //shows loading.gif in mainArticle div
      mainArticleLoading: function () {
          var imgload = $('<img>');
          imgload.attr('src', $('.imgLoading').attr('src'));
          var div = $('<div>').append(imgload).addClass('div-loading');

          $('.mainArticle').empty().html(div);
      },
      //plate function
      getRowIdArray : function(){
          return  ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
         "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF"];
      },
      getRowCount: function (format) {

          var row = 0;

          switch (format) {
              case 6:
                  row = 2;
                  break;
              case 16:
                  row = 8;
                  break;
              case 24:
                  row = 4;
                  break;
              case 48:
                  row = 6;
                  break;
              case 96:
                  row = 8;
                  break;
              case 384:
                  row = 16;
                  break;
              case 1536:
                  row = 32;
                  break;
          }
          return row;
      },
      getColCount: function (format) {
          var col = 0;

          switch (format) {
              case 6:
                  col = 3;
                  break;
              case 16:
                  col = 2;
                  break;
              case 24:
                  col = 6;
                  break;
              case 48:
                  col = 8;
                  break;
              case 96:
                  col = 12;
                  break;
              case 384:
                  col = 24;
                  break;
              case 1536:
                  col = 48;
                  break;
          }
          return col;
      },
      closeAndDestroyAllDialogs: function () {
          bootbox.hideAll();
          //$(".ui-dialog-content").dialog("close");
          //$(".ui-dialog-content").dialog('destroy').remove();
      },
      clearTooltips: function () {
          $('[data-toggle="tooltip"]').tooltip("destroy");
          $(".tooltip").remove();
      }
    };
    return utils;
  });