﻿define(['backbone'],
  function () {
    var utils = {
      liLength: 0,
      findInMenu: function (str, items, index) {
        var level0 = false;
        if (index == null) {
          index = -1;
          level0 = true;
        }
        for (var i = 0; i < items.length; i++) {
          if (level0) index = i;
          var item = items[i];
          if (item.link != null && item.link != '') {
            if (item.link == str) {
              if (index == -1) index = i;
              return index;
            }
          }
          if (item.items != null && item.items.length > 0) {
            if (index == -1) index = i;
            var mindex = this.findInMenu(str, item.items, index);
            if (mindex >= 0) return mindex;
          }
        }
        return -1;
      },
      selectMenu: function (parent, level, items, hash) {
          var lis = parent.find('>li');
          lis.removeClass('selected');
          for (var i = 0; i < items.length; i++) {
              var item = items[i];
              var li = $(lis[i]);
              var isit = (item.link) ? item.link : item.id;
              if (hash != null && hash != '' && isit == hash) {
                  li.addClass('selected');
                  break;
              }
              if (hash == null || hash == '') {
                  if (i == 0) {
                      li.addClass('selected');
                      break;
                  }
              }
          }

      },

      renderMenuLevel: function (parent, level, items, hash, isDialog) {
        if (items != null) {
          for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var isit = (item.link) ? item.link : item.id;
            var li = $('<li>').appendTo(parent).addClass('breakWords');
            if (level == 1) li.addClass('mainLevel');
            if (item.name != null) {
              var a = $('<a>').appendTo(li);
              if (isDialog && level > 1) {
                var padding = 18 * (level - 2) + 4;
                if (this.liLength < padding)
                  this.liLength = padding;
                a.attr('style', 'padding-left: ' + padding + 'px;');
              }
              a.text(item.name);

              if (hash != null && isit == hash) {
                  li.addClass('selected');
              }
              if (hash == null) {
                  if(i==0)
                      li.addClass('selected');
              }
              if (item.link != null && item.link != '')
                a.attr('href', item.link);
            }
            if (item.items != null && item.items.length > 0) {
              var ul = $('<ul>').appendTo(li);
              var slevel = level + 1;
              this.renderMenuLevel(ul, slevel, item.items, hash, isDialog);
            }
          }
        }
        return this;
      }
    };
    return utils;
  });