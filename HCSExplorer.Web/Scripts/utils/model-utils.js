﻿define(['backbone'],
  function () {
    var utils = {
      createBaseUrl: function (apiUrl) {
        var loc = window.location;
        var pathName = loc.pathname;
        var length = pathName.length;
        var delimiter = "";

        if(length > 0) {
          if(pathName.charAt(length - 1) != "/") {
            delimiter = "/";
          }
        }

        var url = loc.protocol +
                  "//" +
                  loc.hostname +
                  (loc.port && ":" + loc.port) +
                  loc.pathname +
                  delimiter +
                  apiUrl;
        return url;
      },
      createBaseImageUrl: function (apiUrl) {
        var loc = window.location;
        var pathname = loc.pathname.replace(/image/ig, '');
        var length = pathname.length;
        var delimiter = "";

        if(length > 0) {
            if (pathname.charAt(length - 1) != "/") {
            delimiter = "/";
          }
        }

        var url = loc.protocol +
                  "//" +
                  loc.hostname +
                  (loc.port && ":" + loc.port) +
                  pathname +
                  delimiter +
                  apiUrl;
        return url;
      },
      getURLParameter: function(name) {
        return decodeURI(
            (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
        );
      },
      getURLParameterImg: function (name, src) {
          var srcen = decodeURIComponent(src);
          var val=(RegExp(name + '=' + '(.+?)(&|$)').exec(srcen) || [, null])[1];
          if (val != null)
              return decodeURI(val);
          else
              return null;
      }
    };
    return utils;
  });