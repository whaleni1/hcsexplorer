﻿define(['backbone'],
  function () {
    var utils = {

      getIdByButton: function (btn, table) {
        var apos = table.fnGetPosition($(btn).closest('td')[0]);
        var aData = table.fnGetData(apos[0]);
        return aData;
      },
      yesNoDialog: function (qs, okCallback, noCallback) {
          var buttons = '<div class="buttonpane">' +
        '<button type="button" class="fright btn_cancel">  No  </button>' +
        '<button type="button" class="fright btn_save" >  Yes  </button>' +
        '<div class="loadingImg" />' +
        '</div>';
        qs.append(buttons);
        qs.find('.btn_save').click(function () {
          qs.find('.loadingImg').show();
          _.delay(function () {
            if (okCallback != null && $.isFunction(okCallback)) {
              var result = okCallback();
              if (result != null) {
                if (result.reloginRequired) {
                  qs.find('.needReload').val("1");
                }
                qs.find('.loadingImg').hide();
                if (result.message == '' || result.message == null) {
                  qs.dialog("close");
                } else {

                  var err = qs.find('.errorBoxDlgQ');
                  if (err.length == 0) {
                    qs.find('.quest').before('<div class="errorBoxDlgQ"></div>');
                  }
                  qs.find('.errorBoxDlgQ').text(result.message);
                  qs.find('.errorBoxDlgQ').addClass('errorBoxDlgBck');
                }
              }
              else {
                qs.dialog("close");
              }
            }
            else {
              qs.find('.loadingImg').hide();
              qs.dialog("close");
            }
          }, 100);
          return false;
        });
        qs.find('.btn_cancel').click(function () {
          qs.dialog("close");
          if (noCallback != null && $.isFunction(noCallback)) {
            noCallback();
          }
          return false;
        });
        qs.dialog({
          autoOpen: true,
          width: 470,
          minHeight: 80,
          resizable: false,
          modal: true,
          close: function () {
            if (qs.find('.needReload').val() == "1")
              window.location.reload();
            qs.empty()
          },
          buttons: {}
        });
      },
      deleteRow: function (obj) {
        var rowText = obj.rowText;
        var qs = $('<div title="Delete"><div class="quest" >Are you sure you want to delete the ' + rowText + '?</div><input type="hidden" class="needReload" value=""></div>');
        this.yesNoDialog(qs, obj.fnDelete);
      },
      addManageColumn: function (model, collectionProperty) {
        if (collectionProperty === undefined) {
          collectionProperty = 'rows';
        }

        var rows = model.get(collectionProperty);
        if (rows != null) {
          for (var i = 0; i < rows.length; i++) {
            rows[i]._mng = '';
          }
        }
      },
      paging: function (ar) {
        var colcount = ar['iColumns'];
        var sort = '';
        var order = '';

        for (var j = 0; j < colcount; j++) {
          if (ar['iSortCol_' + j] != null) {
            sort = ar['mDataProp_' + ar['iSortCol_' + j]];
            order = ar['sSortDir_' + j];
            break;
          }
        }
        return {
          sortedBy: sort,
          sortOrder: order,
          from: ar['iDisplayStart'],
          to: ar['iDisplayStart'] + ar['iDisplayLength']
        };
      },
      serverData: function (aoData, model, filter) {
        var ar = [];
        for (var i = 0; i < aoData.length; i++) {
          ar[aoData[i].name] = aoData[i].value;
        }
        var sEcho = 1;
        try {
          sEcho = parseInt(ar["sEcho"]);
        } catch (e) { }
        var tableData = model.get('rows');
        if (sEcho > 1) {
          var response = model.save(
          {
            paging: this.paging(ar),
            filter: (filter) ? filter : {},
            rows: []
          }, { async: false });
          if (response.readyState == 4 && response.status == 200) {
              tableData = model.get('rows');

          }
          else {
            model.set('rows', tableData);
          }
        }
        //this.addManageColumn(model);
        return {
          "sEcho": sEcho,
          "iTotalRecords": model.get('count'),
          "iTotalDisplayRecords": model.get('count'),
          "aaData": tableData
        };
      },
      rightPage: function (table, model, aoData, fnCallback) {
        var res = this.serverData(aoData, model);
        if (res.aaData.length == 0 && res.iTotalRecords != 0)
          table.fnPageChange('previous');
        else
          fnCallback(res);
      },

      selectRowByNum: function (tbl, tblobj, num) {
        var tr = tblobj.fnGetNodes(num);
        this.selectRow(tbl, tr);
      },
      selectRow: function (tbl, tr) {
        tbl.find('.selected_row').removeClass('selected_row');
        if (tr != null)
          $(tr).addClass('selected_row');
      },
      getBaseDtConfig: function () {
        var tableUtils = this;
        return {
          bJQueryUI: true,
          bAutoWidth: true,
          bPaginate: true,
          sPaginationType: 'full_numbers',
          aLengthMenu: [10, 15, 20, 30, 50],
          bFilter: false,
          bProcessing: false,
          bServerSide: true,
          sAjaxSource: '',
          aaSorting: [[2, 'asc']],
          aoColumnDefs: [{ "aTargets": [0], "sClass": "mngColumn" }]
        }
      },

      getSelectedRows: function (oTableLocal) {
          var aReturn = new Array();
          var aTrs = oTableLocal.fnGetNodes();

          for (var i = 0 ; i < aTrs.length ; i++) {
              if ($(aTrs[i]).hasClass('row_selected')) {
                  aReturn.push(aTrs[i]);
              }
          }

          return aReturn;
      },

      hideNumberOfEntriesMenu: function (table) {
          $(table).closest('.dataTables_wrapper').find('.dataTables_length').parent().hide();
      }
    };
    return utils;
  });