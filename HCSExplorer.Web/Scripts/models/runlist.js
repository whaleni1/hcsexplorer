﻿define(['utils/model-utils', 'backbone', 'validator'],
  function (utils) {
      var runsModel = Backbone.Model.extend({
          urlRoot: utils.createBaseUrl("api/run/runs")
      });
      return runsModel;
  });