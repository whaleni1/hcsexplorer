﻿define(['utils/model-utils', 'backbone'],
  function (utils) {
      var chModel = Backbone.Model.extend({
          urlRoot: utils.createBaseUrl("api/run/getchannels")
      });
      return chModel;
  });