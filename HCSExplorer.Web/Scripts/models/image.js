﻿define(['utils/model-utils', 'backbone'],
  function (utils) {
      var imageModel = Backbone.Model.extend({
          defaults: {
          },
          url: utils.createBaseImageUrl("api/image/getSingleImage")
      });
      return imageModel;
  });