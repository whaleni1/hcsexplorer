﻿define(['utils/model-utils', 'backbone'],
  function (utils) {
      var pathsModel = Backbone.Model.extend({
          urlRoot: utils.createBaseUrl("api/browse/paths")
      });
      return pathsModel;
  });