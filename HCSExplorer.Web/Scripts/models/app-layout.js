﻿define(['utils/model-utils', 'backbone'],
  function (utils) {
    var layoutModel = Backbone.Model.extend({
      defaults: {
        menu: {},
        footer: {}

      },
      url: utils.createBaseUrl("api/application/layout")
    });
    return layoutModel;
  });