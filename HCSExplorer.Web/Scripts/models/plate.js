﻿define(['utils/model-utils', 'backbone', 'validator'],
  function (utils) {
      var plateModel = Backbone.Model.extend({
          defaults: {
              wellViewSize:'M'
          },
          validation: {
              brightness: {
                  pattern: 'number',
                  min: 0,
                  max: 100,
                  required: false
              },
              whitePoint: {
                  pattern: 'number',
                  min: 0,
                  max: 100,
                  required: false
              },
              blackPoint: {
                  pattern: 'number',
                  min: 0,
                  //max: 100,
                  required: false
              },
              //level: {
              //    acceptance: false,
              //    required: false
              //},
              auto: {
                  acceptance: false,
                  required: false
              }
          },
          url: utils.createBaseUrl("api/plate/plateParameters")
    });
      return plateModel;
  });