﻿define(['utils/model-utils', 'backbone'],
  function (utils) {
      var downloadImgModel = Backbone.Model.extend({
          defaults: {
              pathChBox: true,
              plateIdChBox: true,
              channelChBox: true,
              rowColumnChBox: true,
              fieldChBox: true,
              
              textColor: 'default',
              textSize: 'default',
              customText: '',
              
              imgUrl: '',
              imgPath: '',
              barcode: '',
              platePath: ''
          },
          //url: utils.createBaseUrl("api/image/getSingleImage")
      });
      return downloadImgModel;
  });
