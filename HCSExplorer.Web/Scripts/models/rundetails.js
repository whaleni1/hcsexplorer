﻿define(['utils/model-utils', 'backbone', 'validator'],
  function (utils) {
      var plateModel = Backbone.Model.extend({
          urlRoot: utils.createBaseUrl("api/run/details")
    });
      return plateModel;
  });