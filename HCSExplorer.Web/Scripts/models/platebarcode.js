﻿define(['utils/model-utils', 'backbone', 'validator'],
  function (utils) {
      var plateModel = Backbone.Model.extend({
          defaults: {
              wellViewSize:'S'
          },
          url: utils.createBaseUrl("api/plate/plateParBarcode")
    });
      return plateModel;
  });