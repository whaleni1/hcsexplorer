﻿define(['utils/model-utils', 'backbone', 'validator'],
  function (utils) {
      var runModel = Backbone.Model.extend({
          validation: {
              name: [
                {
                  required: true,
                  msg: 'Cannot be blank'
                },
                {
                  msg: 'Run Name too long',
                  maxLength: 255
                }
              ],
              description: {
                  required: false,
                  maxLength: 2000
              },
              assayDescription: {
                  required: false,
                  maxLength: 2000
              },
              heliosId: {
                  required: false,
                  maxLength: 255
              },
              aresId: {
                  required: false,
                  maxLength: 255
              }
          },
        
          urlRoot: utils.createBaseUrl("api/run/create")
      });
      return runModel;
  });