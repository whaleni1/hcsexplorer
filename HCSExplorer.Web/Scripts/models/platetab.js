﻿define(['utils/model-utils', 'backbone', 'validator'],
  function (utils) {
      var searchModel = Backbone.Model.extend({
          validation: {
              brightness: {
                  pattern: 'number',
                  min: 0,
                  max: 100,
                  required: false
              },
              whitePoint: {
                  pattern: 'number',
                  min: 0,
                  max: 100,
                  required: false
              },
              blackPoint: {
                  pattern: 'number',
                  min: 0,
                  //max: 100,
                  required: false
              },
              level: {
                  acceptance: false,
                  required: false
              },
              auto: {
                  acceptance: false,
                  required: false
              }
          },
          url: utils.createBaseUrl("api/plate/images")
      });
      return searchModel;
  });