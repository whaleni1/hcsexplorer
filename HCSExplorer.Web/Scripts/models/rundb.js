﻿define(['utils/model-utils', 'backbone'],
  function (utils) {
      var dbModel = Backbone.Model.extend({
          urlRoot: utils.createBaseUrl("api/browse/databases")
      });
      return dbModel;
  });