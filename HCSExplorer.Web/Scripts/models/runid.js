﻿define(['utils/model-utils', 'backbone'],
  function (utils) {
      var runIdModel = Backbone.Model.extend({
          urlRoot: utils.createBaseUrl("api/run/getNextId")
      });
      return runIdModel;
  });