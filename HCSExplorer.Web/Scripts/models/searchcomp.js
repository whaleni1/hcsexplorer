﻿define(['utils/model-utils', 'backbone'],
  function (utils) {
      var searchModel = Backbone.Model.extend({
    validation: {
        compoundId: {
            required: true,
            msg: 'Field is required'
        }
    },
      url: utils.createBaseUrl("api/search/compounds")
    });
    return searchModel;
  });