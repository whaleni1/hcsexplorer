﻿define(['utils/model-utils', 'backbone', 'validator'],
  function (utils) {
      var searchModel = Backbone.Model.extend({
        validation: {
            siteId: {
                required: true,
                msg: 'Field is required'
            },
            databaseId: {
                required: true,
                msg: 'Field is required'
            },
            barcode: {
                required: true,
                minLength: 2
            }
       },

        url: utils.createBaseUrl("api/search/plates")
    });
    return searchModel;
  });