/*if(!window.jQuery || !window.jQuery.ui)
	throw 'SIG has a dependency on jQuery and jQuery UI, but both are not on the page. ' +
	      'Please make sure that jQuery and jQuery UI are imported before the SIG files';

if($.browser.msie && $.browser.version < 9) {
	(function(a,b){function r(a){var b=-1;while(++b<f)a.createElement(e[b])}if(!window.attachEvent||!b.createStyleSheet||!function(){var a=document.createElement("div");return a.innerHTML="<elem></elem>",a.childNodes.length!==1}())return;a.iepp=a.iepp||{};var c=a.iepp,d=c.html5elements||"abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|subline|summary|time|video",e=d.split("|"),f=e.length,g=new RegExp("(^|\\s)("+d+")","gi"),h=new RegExp("<(/*)("+d+")","gi"),i=/^\s*[\{\}]\s*$/,j=new RegExp("(^|[^\\n]*?\\s)("+d+")([^\\n]*)({[\\n\\w\\W]*?})","gi"),k=b.createDocumentFragment(),l=b.documentElement,m=b.getElementsByTagName("script")[0].parentNode,n=b.createElement("body"),o=b.createElement("style"),p=/print|all/,q;c.getCSS=function(a,b){try{if(a+""===undefined)return""}catch(d){return""}var e=-1,f=a.length,g,h=[];while(++e<f){g=a[e];if(g.disabled)continue;b=g.media||b,p.test(b)&&h.push(c.getCSS(g.imports,b),g.cssText),b="all"}return h.join("")},c.parseCSS=function(a){var b=[],c;while((c=j.exec(a))!=null)b.push(((i.exec(c[1])?"\n":c[1])+c[2]+c[3]).replace(g,"$1.iepp-$2")+c[4]);return b.join("\n")},c.writeHTML=function(){var a=-1;q=q||b.body;while(++a<f){var c=b.getElementsByTagName(e[a]),d=c.length,g=-1;while(++g<d)c[g].className.indexOf("iepp-")<0&&(c[g].className+=" iepp-"+e[a])}k.appendChild(q),l.appendChild(n),n.className=q.className,n.id=q.id,n.innerHTML=q.innerHTML.replace(h,"<$1font")},c._beforePrint=function(){if(c.disablePP)return;o.styleSheet.cssText=c.parseCSS(c.getCSS(b.styleSheets,"all")),c.writeHTML()},c.restoreHTML=function(){if(c.disablePP)return;n.swapNode(q)},c._afterPrint=function(){c.restoreHTML(),o.styleSheet.cssText=""},r(b),r(k);if(c.disablePP)return;m.insertBefore(o,m.firstChild),o.media="print",o.className="iepp-printshim",a.attachEvent("onbeforeprint",c._beforePrint),a.attachEvent("onafterprint",c._afterPrint)})(this,document);
}*/

/**
 * Extends jQuery UI Tabs to save the current tab in the hash
 */
/*$.extend($.ui.tabs.prototype, {
	hashify: function() {
		var oldSelect = this.options.select || function() { },
				oldHashChange = window.onhashchange || function() { },
				self = this;

		self._flagged = false;

		this.options.select = function(event, ui) {
			oldSelect.apply(this, [].slice.call(arguments));
			self._flagged = true;
			location.hash = ui.tab.hash;
		}

		window.onhashchange = function() {
			oldHashChange.call(window);

			if(!self._flagged)
				self.select(location.hash);

			self._flagged = false;

			return false;
		}

		return this;
	}
});*/

function getDomainFromSiteCode(siteCode) {

	var mapping = {
		'us' : 'NANET', // USA USA USA
		'ca' : 'NANET', // Canada
		'br' : 'LANET', // Brazil
		'mx' : 'LANET', // Mexico
		'ar' : 'LANET', // Argentina
		'be' : 'EUNET', // Belgium
		'ch' : 'EUNET', // Switzerland
		'it' : 'EUNET', // Italy
		'gb' : 'EUNET', // Great Britain
		'fr' : 'EUNET', // France
		'at' : 'EUNET', // Austria
		'es' : 'EUNET', // Spain
		'hu' : 'EUNET', // Hungary
		'de' : 'EUNET', // Germany (deutchland)
		'dk' : 'EUNET', // Denmark
		'eg' : 'EUNET', // Egypt
		'fi' : 'EUNET', // Finland
		'gr' : 'EUNET', // Greece
		'nl' : 'EUNET', // Netherlands
		'no' : 'EUNET', // Norway
		'pl' : 'EUNET', // Poland
		'pt' : 'EUNET', // Portugal
		'ru' : 'EUNET', // Russia
		'se' : 'EUNET', // Sweden
		'tr' : 'EUNET', // Turkey
		'ke' : 'EUNET', // Turkey
		'au' : 'APNET', // Australia
		'ph' : 'APNET', // Philippines
		'bd' : 'APNET', // Bangladesh
		'sg' : 'APNET', // Singapore
		'cn' : 'APNET', // China
		'id' : 'APNET', // Indonesia
		'jp' : 'APNET', // Japan
		'kr' : 'APNET', // Korea
		'in' : 'APNET', // India
		'vn' : 'APNET', // Vietnam
		'tw' : 'APNET'	// Taiwan
	}

	// Default to NANET if site code doesn't exist
	return mapping[siteCode.substring(0, 2)] ? mapping[siteCode.substring(0, 2)] : 'NANET';
}

/*$(function() {
	// Custom styles for Internet Explorer
	if ($.browser.msie) {
		$(".sig-button, .sig-button-icon").addClass("ie-shadowed");
		$(".sig-button-icon").css("background-position", "6px 45%");
		$(".sig-button-calendar, .sig-button-grid").css("background-position", "-1px 30%");
	}

	// Apply jQuery UI Accordion
	$(".sig-accordion").accordion();
	$(".sig-accordion-noheight").accordion({autoHeight:false,clearStyle:true,collapsible:true});
	// Apply jQuery UI Tabs
	$(".sig-tabs").tabs();

	if($.fn.placehold) // Apply input placeholders
		$("input, textarea").placehold();

	// Apply jQuery UI Progress Bar
	$(".sig-progressbar").progressbar();

	if($.fn.multiselect) // Apply jQuery UI Selects
		$('.sig-content select[multiple]:not(.simple)').multiselect({header:false, selectedList:3});

	if($.fn.validate) // Apply jQuery Validation plugin
		$('form.validate').validate({
			errorElement: 'div',
			errorClass: 'error',
			validClass: 'field-valid'
		});

	// Set datepicker defaults
	$.datepicker.setDefaults({
		dateFormat: 'd M yy',
		showOn: 'both',
		buttonImage: 'http://nebulacdn.na.novartis.net/images/calendar_small_off.png',
		buttonImageOnly: true
	});

	// Apply the three-states events for clickable icons
	$('.icon-three-states').mousedown(function() {
		if (/_on/.test(this.src)) {
			this.src = this.src.replace("_on", "_click");
		} else if (/_off/.test(this.src)) {
			this.src = this.src.replace("_off", "_click");
		}
	}).mouseenter(function() {
		if (/_off/.test(this.src)) {
			this.src = this.src.replace("_off", "_on"); 
		} else if (/_click/.test(this.src)) {
			this.src = this.src.replace("_click","_on");
		}
	}).mouseleave(function() {
		if (/_on/.test(this.src)){
			this.src = this.src.replace ("_on", "_off");
		} else if (/_click/.test(this.src)) {
			this.src = this.src.replace("_click", "_off");
		}
	}).mouseup(function() {
		if (/_click/.test(this.src)){
			this.src = this.src.replace ("_click", "_off");
		} else if (/_on/.test(this.src)) {
			this.src = this.src.replace("_on", "_off");
		}
	});


	// Multi-select component - remove
	$(".sig-selections div").live('click', function() {
		$(this).remove();
	})
	
	// Multi-select component - add
	$(".sig-multiselect-add").click(function() {
		var container = $(this).closest(".sig-multiselect");
		var input = $(container).find(".sig-multiselect-input");
		if ($(input).val() != '') {
			$(container).find(".sig-selections").append("<div>"+$(input).val()+"</div>");
			$(input).val('');
			$(input).focus();
		}
	});
	
	// Page navigation default interactivity
	// TODO: Convert this to a "Navigation API"
	$(".sig-page-nav ul ul").parent("li").addClass("has-submenu");
	$('.sig-page-nav-content:first').show();
	$('.sig-page-nav-inline ul li:first').addClass('selected');
	$('.sig-page-nav-inline ul li a[href^="#"]').click(function(){
		$('.sig-page-nav-content').hide();
		var targetTab = $(this).attr('href');
		$(targetTab).show();
		$('.sig-page-nav ul li').removeClass('selected');
		$(this).parent().addClass('selected');
		return false;
	});*/

/*
	// http://css3pie.com/about/
	// We don't use the PIE.htc approach since it can't be load cross-domain (i.e. from NebulaCDN)
	if (window.PIE) {
		
		// Array of elements to PIE
		var pies = [
			'header.sig .search input',
			'.sig-content input[type="text"]',
			'.sig-content input[type="password"]',
			'.sig-content input[type="number"]',
			'.sig-content input[type="tel"]',
			'.sig-content input[type="url"]',
			'.sig-content input[type="email"]',
			'.sig-content textarea',
			'.sig-content select',
			'.sig-button',
			'div.nibr-login',
			'.sig-rounded-panel'
		]
		
		$(pies.join(',')).each(function() {
			PIE.attach(this);
		});
	}
*/

/*});

$(function(){
	$.extend($.fn.disableTextSelect = function() {
		return this.each(function() {
			if($.browser.mozilla) { // Firefox
				$(this).css('MozUserSelect','none');
			}
			else if($.browser.msie) { // IE
				$(this).bind('selectstart',function(){return false;});
			}else{//Opera, etc.
				$(this).mousedown(function(){return false;});
			}
		});
	});
});*/
