(function($){var methods={init:function(options){logPostGetUser521='';logPostGetUserFirstLastName='';if(typeof(NIBRIam)!="undefined"){logPostGetUser521=NIBRIam.NIBR521;logPostGetUserFirstLastName=NIBRIam.NIBRFull;}else if($('.nibr-current-user-name')[0]){var matches=/[^\\]+$/.exec($('.nibr-current-user-name').text());if(matches&&matches.length>0){logPostGetUser521=matches[0];logPostGetUserFirstLastName='unavailable';}else{logPostGetUser521="unavailable";logPostGetUserFirstLastName="unavailable";}}
var feedbackLoaded=false;var ticketsLoaded=false;var getHelpPanel='<div id="sig-help-dropdown" class="sig-help-panel sig-help-dropdown">\
    			<div class="close"><a href="javascript:void(0);">&times;</a></div>\
    			<div class="section">\
    				<h3>GetHelp <span class="sig-help-appname">with </span></h3>\
    				<div class="sig-help-app-content sig-content sig-content-text"><p>The application team is currently building a guide for this app.  In the meantime, please feel free to contact us through the feedback link.</p></div>\
    			</div>\
    			<div class="section">\
    				<ul class="sig-help-list">\
						<li class="bullet"><a href="http://communities.nibr.novartis.net/welcome" target="_blank" class="sig-help-link-techtalk"><strong>Ask the community</strong> in TechTalk</a></li>\
						<li class="group bullet">\
	    					<a href="javascript:void(0);" class="sig-help-link-livechat bubble push-right">\
	    						<span class="count"><img id="sig-help-livechat-status-img" src="http://nebulacdn.na.novartis.net/images/presence_unknown.png" alt=""></span> <span id="sig-help-livechat-status-text">Offline</span>\
	    						<img src="http://nebulacdn.na.novartis.net/images/pointy_small_off.png" class="pointy" />\
	    					</a>\
	    					<div><a href="javascript:void(0);" class="sig-help-link-livechat"><strong>Live Chat</strong> with the Service Desk</a></div>\
						</li>\
						<li class="group bullet">\
	    					<a href="javascript:void(0);" class="sig-help-link-tickets bubble push-right">\
	    						<span class="count sig-help-tickets-count">&hellip;</span> tickets\
	    						<img src="http://nebulacdn.na.novartis.net/images/pointy_small_off.png" class="pointy" />\
	    					</a>\
	    					<div><a href="javascript:void(0);" class="sig-help-link-tickets"><strong>Report a problem or make a request</strong> to the Service Desk</a></div>\
						</li>\
						<li class="group bullet">\
							<a href="javascript:void(0);" class="sig-help-link-feedback bubble push-right">\
								<span class="count sig-help-feedback-count">&hellip;</span> feedback\
								<img src="http://nebulacdn.na.novartis.net/images/pointy_small_off.png" class="pointy" />\
							</a>\
	    					<div><a href="javascript:void(0);" class="sig-help-link-feedback"><strong>Send feedback</strong> to the <span class="sig-help-appname"></span> application team</a></div>\
						</li>\
						<li class="bullet"><a href="javascript:void(0);" class="sig-help-link-servicedesk"><strong>Call</strong> your local Service Desk</a></li>\
    				</ul>\
    			</div>\
    		</div>';var feedbackPanel='<div id="sig-feedback-dropdown" class="sig-help-panel sig-help-sidepanel">\
				<div class="close"><a href="javascript:void(0)">&times;</a></div>\
				<h2><span class="sig-help-appname"></span> Feedback</h2>\
    			<div class="section">\
    				<h3>Send feedback</h3>\
    				<p>Contact the <span class="sig-help-appname"></span> team with your comments, suggestions, or feature requests.</p>\
    				<div id="sig-feedback-form-error" class="form-error"></div>\
    				<ul class="list-unstyled">\
    					<li class="sig-content group">\
    						<form role="form" id="sig-help-feedback-form" class="sig-help-form" onsubmit="return false;">\
	    						<textarea class="form-control" name="sig-feedback-description" id="sig-feedback-description" placeholder="Describe your feedback..."></textarea>\
	    						<input class="form-control" type="text" name="sig-feedback-title" id="sig-feedback-title" placeholder="Sum it up with a short title" />\
	    						<img src="http://nebulacdn.na.novartis.net/images/spinner.gif" id="sig-feedback-loading" class="spinner" />\
	    						<input class="btn" type="submit" id="sig-feedback-button" value="Send feedback" />\
    						</form>\
    					</li>\
    				</ul>\
    				<div id="sig-feedback-form-message" class="form-message"></div>\
    			</div>\
    			<div class="section">\
    				<h3>Your recent feedback</h3>\
    				<table class="ticket-list table-list">\
    					<tr><td>No feedback items.</td></tr>\
    				</table>\
    				<a href="javascript:void(0);" target="_blank" class="sig-help-feedback-link-all">See all <span class="sig-help-feedback-count"></span> feedback tickets...</a>\
    			</div>\
    		</div>';var getHelpTechTalkPanel='<div id="sig-help-panel-techtalk" class="sig-help-panel sig-help-sidepanel">\
    			<h2>TechTalk (coming soon!)</h2>\
				<div class="close"><a href="javascript:void(0)">&times;</a></div>\
    			<div class="section sig-content group">\
    				<p>A new version of TechTalk is coming soon, so you can connect with your colleagues to share knowledge and solve problems.</p>\
					<p>For more information, <a href="http://techtalk.nibr.novartis.intra" target="_blank">click here</a>.</p>\
    			</div>\
    		</div>';var getHelpLiveChatPanelOnline='<div id="sig-help-panel-livechat" class="sig-help-panel sig-help-sidepanel">\
				<h2>Live Chat</h2>\
						<div class="close"><a href="javascript:void(0)">&times;</a></div>\
				<div class="section sig-content group">\
						<form class="sig-help-livechat-form">\
								<div class="form-group">\
									<label for="sig-help-livechat-message">How can we help?</label>\
									<input class="form-control" type="text" id="sig-help-livechat-message" placeholder="Describe your issue in a few words">\
								</div>\
								<br>\
								<input class="btn" id="sig-help-livechat-button" type="submit" value="Start Chat">\
						</form>\
				</div>\
			</div>';var getHelpLiveChatPanelOffline='<div id="sig-help-panel-livechat" class="sig-help-panel sig-help-sidepanel">\
    			<h2>Live Chat</h2>\
				<div class="close"><a href="javascript:void(0)">&times;</a></div>\
    			<div class="section sig-content group">\
    				<p>Live Chat operators are not available at the moment. Please select "Report a problem", "Send feedback", or call the service desk directly if this is a critical issue.</p>\
    			</div>\
    		</div>';var getHelpServiceDesksPanel='<div id="sig-help-panel-servicedesk" class="sig-help-panel sig-help-sidepanel">\
    			<h2>Contact Information</h2>\
    			<div class="close"><a href="javascript:void(0)">&times;</a></div>\
    			<div class="section">\
		    		<table class="help-contacts table-list">\
						<tr><td>Basel<br />Horsham</td><td>x61000 / +41 61 696 1000<br /><a href="mailto:nibreurope.servicedesk@novartis.com"><img src="http://nebulacdn.na.novartis.net/images/email_small_off.png"> Local E-mail</a></td></tr>\
						<tr><td>Cambridge<br />East Hanover</td><td>x43333 / +1 617 871 3333<br /><a href="mailto:nibrus.servicedesk@novartis.com"><img src="http://nebulacdn.na.novartis.net/images/email_small_off.png"> Local E-mail</a></td></tr>\
						<tr><td>Emeryville</td><td>x8000 / +1 510 923 8000<br /><a href="mailto:nibrus.servicedesk@novartis.com"><img src="http://nebulacdn.na.novartis.net/images/email_small_off.png"> Local E-mail</a></td></tr>\
						<tr><td>Shanghai</td><td>x6666 / +86 21 6160 6666<br /><a href="mailto:servicedesk.cnibr@novartis.com"><img src="http://nebulacdn.na.novartis.net/images/email_small_off.png"> Local E-mail</a></td></tr>\
						<tr><td>Singapore</td><td>x3000 / +65 6722 3000<br /><a href="mailto:nitd.servicedesk@novartis.com"><img src="http://nebulacdn.na.novartis.net/images/email_small_off.png"> Local E-mail</a></td></tr>\
                        <tr><td>Vienna</td><td>x3040 / +43 80 166 3040<br /><a href="mailto:nibreurope.servicedesk@novartis.com"><img src="http://nebulacdn.na.novartis.net/images/email_small_off.png"> Local E-mail</a></td></tr>\
					</table>\
				</div>\
		    </div>';var singleTicketPanel='<div id="sig-help-panel-single" class="sig-help-panel sig-help-sidepanel">\
		    	<h2></h2>\
		    	<div class="close"><a href="javascript:void(0)">&times;</a></div>\
		    	<div class="section">\
		    		<h3>Description</h3>\
		    		<div class="sig-single-ticket-description"></div>\
		    	</div>\
		    	<div class="section">\
		    		<h3>Response</h3>\
		    		<div class="sig-single-ticket-response"></div>\
		    	</div>\
		    	<div class="section sig-single-ticket-link"></div>';var getHelpTicketsPanel='<div id="sig-help-panel-tickets" class="sig-help-panel sig-help-sidepanel">\
    			<h2>How can we help?</h2>\
    			<div class="close"><a href="javascript:void(0)">&times;</a></div>\
    			<div class="section">\
    				<h3>Report a problem / make a request</h3>\
    				<p>We\'ll create a ticket to track your problem or request, and get back to you within 24 hours.</p>\
    				<div id="sig-ticket-form-error" class="form-error"></div>\
    				<ul class="list-unstyled">\
    					<li class="sig-content group">\
    						<form role="form" id="sig-help-ticket-form" class="sig-help-form" onsubmit="return false;">\
	    						<textarea class="form-control" id="sig-ticket-description" name="sig-ticket-description" placeholder="Describe your issue..."></textarea>\
	    						<input class="form-control" type="text" id="sig-ticket-title" name="sig-ticket-title" placeholder="Sum it up with a short title" />\
	    						<img src="http://nebulacdn.na.novartis.net/images/spinner.gif" id="sig-ticket-loading" class="spinner" />\
	    						<input class="btn" type="submit" id="sig-ticket-button" value="Send to Service Desk" />\
    						</form>\
    					</li>\
		    		</ul>\
		    		<div id="sig-ticket-form-message" class="form-message"></div>\
		    	</div>\
		    	<div class="section">\
		    		<h3>Your recent tickets</h3>\
		    		<table class="ticket-list table-list">\
		    			<tr>\
		    				<td>No tickets.</td>\
		    			</tr>\
		    		</table>\
    				<a id="tePortalLink" target="_blank" href="http://techexcelweb/portal/">See all Service Desk tickets in TechExcel</a>\
		    	</div>\
		    </div>';jQuery.cachedScript=function(url,options){options=$.extend(options||{},{dataType:"script",cache:true,url:url});return jQuery.ajax(options);};$.cachedScript("http://nebulacdn.na.novartis.net/component/TechExcel/1.0.7/iso8601.min.js");$.cachedScript("http://nebulacdn.na.novartis.net/component/TechExcel/1.0.7/TechExcel.js");if($("meta[name='GENERATOR'][content='Microsoft SharePoint']")[0]){$("html").addClass("sharepoint");}
var opt={nebulaWebRoot:'http://nebula.na.novartis.net/',projectId:null,assignee:null,issueType:1,appTitle:null,appHelpContent:null,feedbackNotes:[]};opt=$.extend(opt,options);opt.feedbackNotes.push({name:"Screen Resolution",value:screen.width+'x'+screen.height});if(opt.appTitle==null||opt.appTitle==''){alert("SIG GetHelp Plugin Error\nPlease provide the name of your application through the plugin's appTitle option.\nVisit http://sig.nibr.novartis.net/ for full instructions.");}
if(opt.projectId==null||opt.projectId==''){alert("SIG GetHelp Plugin Error\nPlease provide your application's JIRA Project ID through the plugin's projectId option.\nVisit http://sig.nibr.novartis.net/ for full instructions.");}
var $obj=$(this);var te=null;$(getHelpPanel).appendTo("body");$(feedbackPanel).appendTo("body");$(getHelpTechTalkPanel).appendTo("body");$(getHelpLiveChatPanelOffline).appendTo("body");$(getHelpServiceDesksPanel).appendTo("body");$(getHelpTicketsPanel).appendTo("body");$(singleTicketPanel).appendTo("body");$(document).on("click",".sig-help-ticket-link",function(){var id=$(this).attr("data-id");var ticket=te.incidentArray[id];$("#sig-help-panel-single h2").html(fixLongWords(ticket.subject));$("#sig-help-panel-single .sig-single-ticket-description").text($.trim(ticket.description));$("#sig-help-panel-single .sig-single-ticket-response").text($.trim(ticket.closeddescription));$("#sig-help-panel-single .sig-single-ticket-link").hide();var coords=$("#sig-help-panel-tickets").offset();var dropdownheight=$("#sig-help-panel-tickets").outerHeight();var height=$('#sig-help-panel-single').outerHeight();$('#sig-help-panel-single').css("top",coords.top+dropdownheight-height).show();});$(document).on('click',".sig-feedback-ticket-link",function(){var $row=$(this).closest("tr");$("#sig-help-panel-single h2").html($row.data("title"));$("#sig-help-panel-single .sig-single-ticket-description").html($row.data("description"));$("#sig-help-panel-single .sig-single-ticket-link").html('<a href="'+$row.data("link")+'" target="_blank">See full ticket details...</a>').show();if($row.data("comment")!=null){$("#sig-help-panel-single .sig-single-ticket-response").text($row.data("comment"));}else{$("#sig-help-panel-single .sig-single-ticket-response").text('');}
var coords=$("#sig-feedback-dropdown").offset();var dropdownheight=$("#sig-feedback-dropdown").outerHeight();var height=$('#sig-help-panel-single').outerHeight();$('#sig-help-panel-single').css("left",coords.left-357).css("top",coords.top+dropdownheight-height).show();});$(".sig-help-appname").append(opt.appTitle);if(opt.appHelpContent!=null){$(".sig-help-app-content").html(opt.appHelpContent);}
if($.fn.placehold){$(".sig-help-panel input, .sig-help-panel textarea").placehold();}else{$.cachedScript("http://nebulacdn.na.novartis.net/plugins/jquery.placehold.js").done(function(script,textStatus){$(".sig-help-panel input, .sig-help-panel textarea").placehold();});}
$obj=$('#getHelpLink');$obj.text('GetHelp');$obj.click(function(){$(".sig-help-sidepanel").fadeOut();var bottom=$obj.offset().top+25;$('#sig-help-dropdown').css({"top":bottom});$('#sig-help-panel-tickets').css({"top":bottom});$('#sig-feedback-dropdown').css({"top":bottom});if(!feedbackLoaded){if(typeof(NIBRIam)!="undefined"){var userEmail=NIBRIam.NIBREMAIL;jiraTicketRequest(NIBRIam.NIBR521,opt.projectId);feedbackLoaded=true;}else if($('.nibr-current-user-name')[0]){var userEmail=$('.nibr-current-user-email').text();var matches=/[^\\]+$/.exec($('.nibr-current-user-name').text());if(matches&&matches.length>0){var user521=matches[0];jiraTicketRequest(user521,opt.projectId);feedbackLoaded=true;}else{}}}
if(te==null){var userEmail;te=new TechExcel({'LinkedSystemID':'NIBRGlobal','LinkedProjectID':'NIBRGlobal','ProjectID':'48','stringEmail':userEmail,'maxIncidents':5,'onSuccess':function(){te.getTickets({onSuccess:function(){var tix=te.incidentArray;$(".sig-help-tickets-count").text(te.incidentCountAll);if(te.incidentCountAll>0){$('#sig-help-panel-tickets .ticket-list').empty();for(var i=0;i<tix.length;i++){var row=tix[i];$('#sig-help-panel-tickets .ticket-list').append('<tr><th>'+(i+1)+'</th><td><a href="javascript:void(0);" class="sig-help-ticket-link" data-id="'+i+'">'+fixLongWords(row.subject)+'</a></td><td class="meta">'+row.status+'</td></tr>');}}else{$("#tePortalLink").hide();}}});},'onError':function(){$(".sig-help-link-tickets.bubble").remove();$("#sig-help-panel-tickets table.ticket-list").after('<div style="margin-bottom:0.5em;"><em>Sorry, there was a problem retrieving your tickets. Use the link below to see all of your tickets.</em></div>').remove();}});}
if(window.XDomainRequest){var xdr=new XDomainRequest();xdr.open('get','http://uscalx1399.nibr.novartis.net/status.php');xdr.onload=function(){if(xdr.responseText==="online"){$('#sig-help-livechat-status-text').text('online');$('#sig-help-livechat-status-img').attr('src','http://nebulacdn.na.novartis.net/images/presence_online.png');$('#sig-help-panel-livechat').html($(getHelpLiveChatPanelOnline).closest('#sig-help-panel-livechat').html());}};xdr.send();}else{$.ajax({type:'GET',cache:false,url:'http://uscalx1399.nibr.novartis.net/status.php',success:function(data){if(data==="online"){$('#sig-help-livechat-status-text').text('online');$('#sig-help-livechat-status-img').attr('src','http://nebulacdn.na.novartis.net/images/presence_online.png');$('#sig-help-panel-livechat').html($(getHelpLiveChatPanelOnline).closest('#sig-help-panel-livechat').html());}}});}
$("#sig-help-dropdown").fadeToggle();});$(document).on('click',".sig-help-panel .close",function(){$(this).closest(".sig-help-panel").fadeToggle();$("#sig-help-panel-single").fadeOut();if($(this).closest(".sig-help-panel").attr("id")=="sig-help-dropdown"){$(".sig-help-sidepanel").fadeOut();}});$(".sig-help-link-feedback").click(function(){logPostOrGet('http://web.global.nibr.novartis.net/services/logpostorget/GetHelpFeedbackLinkClick');$('.sig-help-sidepanel').not('#sig-feedback-dropdown').fadeOut();$("#sig-feedback-form-message").empty();var coords=$("#sig-help-dropdown").offset();$('#sig-feedback-dropdown').fadeToggle();});$(document).on('click',".sig-help-link-techtalk",function(){logPostOrGet('http://web.global.nibr.novartis.net/services/logpostorget/GetHelpTechTalkLinkClick');});$(".sig-help-link-livechat").click(function(){logPostOrGet('http://web.global.nibr.novartis.net/services/logpostorget/GetHelpLiveChatLinkClick');$(".sig-help-sidepanel").not('#sig-help-panel-livechat').fadeOut();var linkCoords=$(this).offset();var coords=$("#sig-help-dropdown").offset();var height=$('#sig-help-panel-livechat').outerHeight();$('#sig-help-panel-livechat').css("top",linkCoords.top-20).fadeToggle();});$(document).on('submit',".sig-help-livechat-form",function(){var name=$('.nibr-current-user-displayname').text()||(typeof(NIBRIam)!="undefined"&&NIBRIam.NIBRFull)||'';var email=$('.nibr-current-user-email').text()||(typeof(NIBRIam)!="undefined"&&NIBRIam.NIBREMAIL)||'';var url="http://uscalx1399.nibr.novartis.net/client.php?&name="+name+"&email="+email+"&message="+$("#sig-help-livechat-message").val()+"&url="+window.location;window.open(url,"LiveChat","toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=0");$(".sig-help-sidepanel").fadeOut();$("#sig-help-livechat-message").val("");return false;});$(document).on('click',".sig-help-link-servicedesk",function(){logPostOrGet('http://web.global.nibr.novartis.net/services/logpostorget/GetHelpServiceDeskLinkClick');$(".sig-help-sidepanel").not('#sig-help-panel-servicedesk').fadeOut();$('#sig-ticket-form-message').empty();var linkCoords=$(this).offset();var coords=$("#sig-help-dropdown").offset();var dropdownheight=$("#sig-help-dropdown").outerHeight();var height=$('#sig-help-panel-servicedesk').outerHeight();$('#sig-help-panel-servicedesk').css("top",coords.top+dropdownheight-height).fadeToggle();});$(".sig-help-link-tickets").click(function(){logPostOrGet('http://web.global.nibr.novartis.net/services/logpostorget/GetHelpTechExcelTicketskLinkClick');$(".sig-help-sidepanel").not('#sig-help-panel-tickets').fadeOut();var coords=$("#sig-help-dropdown").offset();$('#sig-help-panel-tickets').fadeToggle();});$("#sig-help-feedback-form").submit(function(){$("#sig-feedback-form-error").empty();});$(document).on('click','#techTalk',function(){logPostOrGet('http://web.global.nibr.novartis.net/services/logpostorget/GetHelpTechTalkLinkClick');});$("#sig-help-ticket-form").submit(function(){$("#sig-ticket-form-message").empty();$("#sig-ticket-form-error").empty();var formValidated=true;var description=$("#sig-ticket-description").val()+'  FROM APPLICATION: '+opt.appTitle+' URL ENCODED: '+encodeURIComponent(window.location);var title=$("#sig-ticket-title").val();if(description==''||description==$("#sig-ticket-description").attr('placeholder')){formValidated=false;$("#sig-ticket-form-error").append('<strong>Please provide a description.</strong><br />');}
if(title==''||title==$("#sig-ticket-title").attr('placeholder')){formValidated=false;$("#sig-ticket-form-error").append('<strong>Please provide a title.</strong>');}
if(formValidated){$("#sig-ticket-button").attr("disabled","disabled");$("#sig-ticket-loading").show();te.addTicket({'IncidentTitle':title,'IncidentDescription':description,'onSuccess':function(){$("#sig-ticket-description").val('');$("#sig-ticket-title").val('');$("#sig-ticket-form-message").html('<strong>Thank you for contacting the Service Desk.</strong> You will receive an e-mail shortly to track the status of your request.');$("#sig-ticket-button").removeAttr("disabled");$("#sig-ticket-loading").hide();},'onError':function(){$("#sig-ticket-form-error").html('<strong>Sorry, your request could not be submitted.</strong> Please try again. If the problem persists, please <a href="javascript:void(0);" class="sig-help-link-servicedesk">call or e-mail</a> your local Service Desk.');$("#sig-ticket-button").removeAttr("disabled");$("#sig-ticket-loading").hide();}});}
return false;});$("#sig-help-feedback-form").submit(function(){$("#sig-feedback-form-error").empty();$("#sig-feedback-form-message").empty();var formValidated=true;var description=$("#sig-feedback-description").val();var title=$("#sig-feedback-title").val();if(description==''||description==$("#sig-feedback-description").attr('placeholder')){formValidated=false;$("#sig-feedback-form-error").append('<strong>Please provide a description.</strong><br />');}
if(title==''||title==$("#sig-feedback-title").attr('placeholder')){formValidated=false;$("#sig-feedback-form-error").append('<strong>Please provide a title.</strong>');}
if(formValidated){$("#sig-feedback-button").attr("disabled","disabled");$("#sig-feedback-loading").show();if(opt.feedbackNotes.length>0){description+="\n\n";for(i=0;i<opt.feedbackNotes.length;i++){description+=opt.feedbackNotes[i].name+": "+opt.feedbackNotes[i].value+"\n";};}
if(typeof opt.fnAddJiraNotes!='undefined'){addNotes=opt.fnAddJiraNotes.call();description+="\n";for(i=0;i<addNotes.length;i++){description+=addNotes[i].name+": "+addNotes[i].value+"\n";};}
var user521;if(typeof(NIBRIam)!="undefined"){user521=NIBRIam.NIBR521;}else if($('.nibr-current-user-name')[0]){var matches=/[^\\]+$/.exec($('.nibr-current-user-name').text());if(matches&&matches.length>0){user521=matches[0];}else{}}
var data={project:opt.projectId,summary:title,description:description,issuetype:opt.issueType,user521:user521};if(opt.assignee!=null){data.assignee=opt.assignee;}
$.ajax({type:"GET",url:opt.nebulaWebRoot+"feedback",dataType:"jsonp",processData:true,data:data,success:function(resp){if(resp.response.errors.length){$.each(resp.response.errors,function(){if(this.field=='jira'){$("#sig-feedback-form-error").html("<strong>Sorry, your feedback could not be submitted.</strong> Please try again. (JIRA, our issue tracker, reported this error: '"+this.error+"')");}else{$("#sig-feedback-form-error").append('<strong>'+this.error+'</strong><br />');}});}else{$("#sig-help-feedback-form")[0].reset();$("#sig-feedback-form-message").html('<strong>Thank you for your feedback.</strong> You will receive an e-mail shortly to track the status of your request.');}
$("#sig-feedback-button").removeAttr("disabled");$("#sig-feedback-loading").hide();}});}
return false;});},addPanel:function(options){var emptyPanel='<div class="sig-help-panel sig-help-sidepanel sig-help-userpanel">\
    				<h2></h2>\
					<div class="close"><a href="javascript:void(0)">&times;</a></div>\
					<section class="sig-content sig-content-text"></section>\
				</div>';var opt={panelTitle:null,panelContent:null,panelId:null};opt=$.extend(opt,options);$p=$(emptyPanel).attr("id",opt.panelId).appendTo("body");$p.find("h2").text(opt.panelTitle);$p.find("section").html(opt.panelContent);}};$.fn.extend({sigGetHelp:function(method){if(methods[method]){return methods[method].apply(this,Array.prototype.slice.call(arguments,1));}else if(typeof method==='object'||!method){return methods.init.apply(this,arguments);}else{$.error('Method '+method+' does not exist on jQuery.sigGetHelp');}}});ticketDetails=new Array();function jiraTicketRequest(fiveTwoOne,appId){var jiraUrl="http://web.global.nibr.novartis."+(location.hostname.indexOf('novartis.intra')>-1?'intra':'net')+"/apps/jira/rest/api/2/search?jql=(reporter="+fiveTwoOne+"+OR+cf[10030]~"+fiveTwoOne+")+AND+project="+appId+"+order+by+created&maxResults=50&startAt=0";$.support.cors=true;$.ajax(jiraUrl,{type:"get",dataType:"json",xhrFields:{withCredentials:true},success:function(data){total=data.total;$(".sig-help-feedback-count").text(total);if(total=='0'){$(".sig-help-feedback-link-all").remove();return;}
if(total>6){var limit=6;}else{var limit=total;}
for(i=0;i<limit;i++){var jiraTicketDetailsUrl=data.issues[i].self;$.ajax(jiraTicketDetailsUrl,{type:"get",dataType:"json",xhrFields:{withCredentials:true},success:function(datas){ticketDetails.push(datas);if(ticketDetails.length==total||ticketDetails.length==5){$('#sig-feedback-dropdown .table-list').empty();for(i=0;i<ticketDetails.length;i++){var $row=$('<tr><th>'+(i+1)+'</th><td><a class="sig-feedback-ticket-link" href="javascript:void(0);">'+fixLongWords(ticketDetails[i].fields.summary)+'</a></td><td class="meta">'+ticketDetails[i].fields.status.name+'</td></tr>');$row.data('title',fixLongWords(ticketDetails[i].fields.summary));$row.data('description',fixLongWords($.trim(ticketDetails[i].fields.description)));$row.data('link','http://web.global.nibr.novartis.net/apps/jira/browse/'+ticketDetails[i].key);if(ticketDetails[i].fields.comment.length>0){$row.data('comment',fixLongWords($.trim(ticketDetails[i].fields.comment[ticketDetails[i].fields.comment.length-1].body)));}
$row.appendTo('#sig-feedback-dropdown .table-list');}
var url="http://web.global.nibr.novartis.net/apps/jira/secure/IssueNavigator.jspa?reset=true&jqlQuery=(reporter="+fiveTwoOne+"+OR+cf[10030]~"+fiveTwoOne+")+AND+project="+appId+"+order+by+created";$(".sig-help-feedback-link-all").attr("href",url);}},error:handleJiraError});}},error:handleJiraError});}
function handleJiraError(xdr,errorText,error){$(".sig-help-link-feedback.bubble").remove();$("#sig-feedback-dropdown table.ticket-list").after('<div style="margin-bottom:0.5em;"><em>Sorry, there was a problem retrieving your tickets. We cannot display them at this time.</em></div>').remove();}
function logPostOrGet(url){$.ajax(url,{type:"POST",data:'user521='+logPostGetUser521+'&fullName='+logPostGetUserFirstLastName+'',success:function(data){}});}
function fixLongWords(str){var MAX_CHARS=15;return str.replace(RegExp("(\\w{"+MAX_CHARS+"})(\\w)","g"),function(all,text,char){return text+"&#8203;"+char;});}})(jQuery);