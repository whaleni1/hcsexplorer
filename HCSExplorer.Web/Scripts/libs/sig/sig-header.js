// Get width of navigation bar
function getNavBarWidth() {
    return $("header.sig nav").width(); 
}

// Get maximum width of navigation content
function getNavContentWidth(attribute) {
    if (attribute == undefined) {
        attribute = '';
    }
    var ncw = 0;
    $("header.sig nav > ul > li" + attribute).each(function() {
        ncw += $(this).width() + 1;
    });

    return (ncw == 0 ? 0 : ncw + 1);
}

function appendTimeZero(value) {
	return (value < 10 ? '0' + value : value);
}

function prettySigDateTime(date) {
	return prettySigDate(date) +  " " + appendTimeZero(date.getHours()) + ":" + appendTimeZero(date.getMinutes()) + ":" + appendTimeZero(date.getSeconds()) + " GMT-" + (date.getTimezoneOffset() / 60);
}

function prettySigDate(date) {
	
	var month_names = new Array();
	month_names[month_names.length] = "Jan";
	month_names[month_names.length] = "Feb";
	month_names[month_names.length] = "Mar";
	month_names[month_names.length] = "Apr";
	month_names[month_names.length] = "May";
	month_names[month_names.length] = "Jun";
	month_names[month_names.length] = "Jul";
	month_names[month_names.length] = "Aug";
	month_names[month_names.length] = "Sep";
	month_names[month_names.length] = "Oct";
	month_names[month_names.length] = "Nov";
	month_names[month_names.length] = "Dec";
	
	var day_names = new Array();
	day_names[day_names.length] = "Sunday";
	day_names[day_names.length] = "Monday";
	day_names[day_names.length] = "Tuesday";
	day_names[day_names.length] = "Wednesday";
	day_names[day_names.length] = "Thursday";
	day_names[day_names.length] = "Friday";
	day_names[day_names.length] = "Saturday";
	
	return day_names[date.getDay()] + ", " + date.getDate() + " " + month_names[date.getMonth()] + " " + date.getFullYear();
	
}

var NIBRIam = {};

function sigGetCurrentUserName() {
    if (typeof (NIBRIam) != "undefined") {
        return NIBRIam.NIBRFirst + " " + NIBRIam.NIBRLast;
    }
    return '';
}

function initSigContent() {
    // append date/time to footer
    $("footer.sig .timestamp").append(prettySigDateTime(new Date()) + " &middot; ");

    /* USER IDENTITY */

    if (typeof (NIBRIam) != "undefined") {
        var identityHtml =
            '<span class="current-user" id="current-user"><a href="http://mysite.na.novartis.net/">' +
            	NIBRIam.NIBRFirst + " " + NIBRIam.NIBRLast + '</a></span>';
        $("header.sig .identity").html(identityHtml);
    }
    

    $(".sig-alpha header.sig hgroup").append('<img src="http://nebulacdn.na.novartis.net/images/alpha.png" alt="alpha" class="sig-alpha-beta" />');
    $(".sig-beta header.sig hgroup").append('<img src="http://nebulacdn.na.novartis.net/images/beta.png" alt="beta" class="sig-alpha-beta" />');

    $("header.sig .current-user").click(function () {
        $(this).siblings(".current-user-dropdown").width($(this).outerWidth() + 20);
        $(this).siblings(".current-user-dropdown").toggle();
    });

    /* PRIMARY NAVIGATION SUB MENUS */
    $("header.sig nav > ul > li").live({
        mouseenter: function () {
            if ($(this).not(".disabled")) {
                $(this).children("a").css("z-index", "402");
                $(this).find("> ul").show();
            }
        },
        mouseleave: function () {
            $(this).find("> ul").fadeOut(200);
            $(this).children("a").css("z-index", "");
        }
    });

    /* INNER NAVIGATION SUB MENUS */
    $("header.sig nav > ul > li > ul > li").live({
        mouseenter: function () {
            $(this).find("> ul").css("top", $(this).position().top).show();
            //$(this).children("a").css("z-index", "402");
        },
        mouseleave: function () {
            $(this).find("> ul").fadeOut(200);
        }
    });

    $("header.sig nav ul ul").parent("li").addClass("has-submenu");

    // ADAPT NAVIGATION ON WINDOW RESIZE    
    $(window).resize(function () {

        // Show all nav items
        $("header.sig nav > ul > li").show();

        while (getNavBarWidth() < getNavContentWidth(":visible")) {
            // Hide last visible nav item
            $("header.sig nav > ul > li:visible").not(".extended").last().hide();
        }

        if (getNavBarWidth() < getNavContentWidth()) {
            if ($("header.sig nav .extended").length == 0) {
                $("header.sig nav > ul").append('<li class="extended"><a href="javascript:void(0);">&raquo;</a><ul></ul></li>')
            } else {
                $("header.sig nav .extended").show();
            }
            $("header.sig nav .extended ul").empty();
            $("header.sig nav > ul > li").not(":visible").not(".extended").clone().show().appendTo("header.sig nav .extended > ul");

        } else {
            $("header.sig nav .extended").hide();
        }

    }).resize().resize();
}
