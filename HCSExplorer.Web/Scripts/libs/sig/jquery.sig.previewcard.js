
var previewCardTimeout,
    hideTimeout,
    cache = {},
    namesCache = {};



(function ($) {
	

	
	var entities = {
		'person' : function(element) {
			var rel = $(element).attr('rel'),
			    phtml;
			if(phtml = cache[rel]) {
				$('#sig-preview-card').html(phtml);
			}
			else {
				$.ajax({
					url : 'http://uscalx1214.na.novartis.net/people/get.json',
					dataType : "jsonp",
					data : {
						id : rel
					},
					success: function (data) {
						var personHtml;

						if(data.response && data.response.errors && data.response.errors.length) {
							personHtml = '<div class="persion-preview">'+data.response.errors[0]+'</div>';
						}
						else {
							var person = data.response.people[0];
							
							personHtml = '<div class="person-preview"><div class="photo"><img src="http://mysite.na.novartis.net/User%20Photos/Profile%20Pictures/'+getDomainFromSiteCode(person.site_code)+'_'+person.unique_id+'_MThumb.jpg" onerror="personImageError(this);" /></div>' +
								'<div class="person-details">' + '<a target="_blank" href="http://mysite.na.novartis.net/profile/' + person.firstname + '.' + person.lastname+'">'+person.firstname + ' ' + person.lastname+'</a>' +
								'<div class="location">'+person.site_name + ' &middot; ' + person.workplace+'</div>' + 
								'<div class="email"><a href="mailto:'+person.e_mail+'">'+person.e_mail+'</a></div>' +
								(person.phone
								    ? '<div class="phone">'+ person.phone +'</div>'
								    : '<div class="phone unavailable">Phone number unavailable</div>') +
								'</div></div>';
						}

						cache[rel] = personHtml;

						$("#sig-preview-card").html(personHtml);
					}
				});
			}
		},
		'target' : function(element) {
			
			window.fn = function(data) {
				
				var targetHtml = '';
				var terms = [];
				
				for (var i = 0; i < data.preferredTerm.length; i++) {
					if ($.inArray(data.preferredTerm[i].value, terms) == -1) {
						targetHtml += '<li>' + data.preferredTerm[i].value + '</li>';
						terms.push(data.preferredTerm[i].value); 
					}
				}
				
				$("#sig-preview-card").html('<h4><strong>'+$(element).attr('rel')+'</strong></h4><ul class="sig">' + targetHtml + '</ul>');
			}
			
			$.ajax({
				url : 'http://www.metastoreservices.novartis.intra:8021/msrest/api/preferredterms',
				dataType : 'jsonp',
				data : {
					text: $(element).attr('rel'),
					types : 'TARGETS',
					format : 'jsonp'
				}
			});
			
			//$("#sig-preview-card").html('<h4>Target Preferred Name</h4><ul class="sig"><li>Synonym</li><li>Synonym</li><li>Synonym</li><li>Synonym</li></ul><a href="#">See all synonyms</a>');
		},
		'compound' : function(element) {
			$("#sig-preview-card").html('<h4>Sorry!</h4><p>Compound card not defined yet.</p>');
		},
		'gene' : function(element) {
			$("#sig-preview-card").html('<h4>Sorry!</h4><p>Gene card not defined yet.</p>');
		},
		'study' : function(element) {
			
			// ACZ885B2204
			$.ajax({
				url: 'http://tsnapshot.na.novartis.net/ppm/novsp',
				dataType: 'jsonp',
				data: {
					storedfunction: 'ui_study_pkg.getstudies',
					i_snum: $(element).attr('rel')
				},
				cache: true,
				success: function(data) {
					var study = data.data[0];
					$("#sig-preview-card").html('<h4><strong>'+study.study_no+':</strong> '+study.project+' in '+study.indication+'</h4><div><strong>Therapeutic Area:</strong> '+study.ta+'</div><div><strong>Status:</strong> '+study.ss_status+'</div><div><strong>CTL:</strong> '+study.ctl+'</div><div><strong>TME:</strong> '+study.tme+'</div>');
				}
			})
//"NetworkError: 500 Internal Server Error - http://tsnapshot.na.novartis.net/ppm/novsp?callback=jQuery17106939172321150398_1324483525128&storedfunction=ui_study_pkg.getstudies&i_snum=ACZ885B2204&_=1324483527776"
		},
		'assay' : function(element) {
			
			$.ajax({
				url: 'http://uscalx4017.na.novartis.net/api/assay/get',
				dataType: 'jsonp',
				data: {
					id: $(element).attr('rel'),
					summary: 'true',
					includeTargets: 'true',
					includePeople: 'true',
					format: 'json'
				},
				success: function(data) {
					var assay = data.assay;
					var technologies = '';
					var target = 'None specified';
					
					for (var i = 0; i < assay.technologies.length; i++) {
						if (technologies != '') {
							technologies += ' &middot; ';
						} 
						technologies += assay.technologies[i].name;
					}
					
					dance:
					for (var i = 0; i < assay.components.length; i++) {
						for (var j = 0; j < assay.components[i].roles.length; j++) {
							if (assay.components[i].roles[j].roleCode == 'TARGET') {
								target = assay.components[i].name;
								break dance;
							}
						}
						
					}
					
					$("#sig-preview-card").html('<h4>'+assay.name+'</h4><p style="font-size:10px;color:#666;">'+technologies+'</p><p style="font-size:12px;"><strong>Target:</strong> '+target+'</p>');					
				}
			});
			
			//http://uscalx4017.na.novartis.net/api/assay/get?id=13144&summary=true&includeTargets=true&includePeople=true
			
			
		},
		'project' : function(element) {
			$("#sig-preview-card").html('<h4>Sorry!</h4><p>Project card not defined yet.</p>');
		}
	}

	var methods = {

		init : function(options) {
			
			var opt = { 
				width: 350,
				height: 'auto',
				content: function() { },
				entity: null,
				delay: 150
			};
			
			var htmlCard = '<div id="sig-preview-card" class="sig-preview-card sig-content sig-content-text sig-rounded-panel"></div>';

			opt = $.extend(opt, options);
			
			// Create and Initialize the preview card element
			$("body").append(htmlCard);
			
			$("#sig-preview-card").on('mouseenter', function(e) {
				clearTimeout(hideTimeout);
			});
		
			$("#sig-preview-card").on('mouseleave', function(e) {
				hideTimeout = setTimeout(function() {
					$("#sig-preview-card").fadeOut();
				}, opt.delay);
			});
			
			return this.each(function() {
				
				var $obj = $(this);
					
				$obj.on('mouseover',
					function(e){
						previewCardTimeout = setTimeout(function() {
							clearTimeout(hideTimeout);

							var $spc = $("#sig-preview-card");
							
							$spc.empty();
							
							// run the method that generates the content
							if (opt.entity != null) {
								entities[opt.entity]($obj);
							} else {
								opt.content($obj);
							}
							
							$spc.width(opt.width)
								.height(opt.height)
								.show();
							
							// get hovered element's position
							var pos = $(this).offset();
							// get the document's scroll position
							var vScrollPosition = $(document).scrollTop();
							// get the right-most width of the page
							var limitRight = $(window).width() - opt.width;
							// get the bottom-most height of the page
							var limitBottom = (vScrollPosition + $(window).height()) - $spc.outerHeight();
							// mouse's horizontal position
							var posHorizontal = e.pageX;
							// mouse's vertical position
							var posVertical = e.pageY;
							// these offsets give the cursor some breathing room
							var offsetHorizontal = 10;
							var offsetVertical = 10;
							
							// if cursor is less than opt.width pixels away from right window border, show card to left of cursor
							if(posHorizontal > limitRight) {
								posHorizontal = e.pageX - $spc.outerWidth();
								offsetHorizontal = -10;
							}
				
							// if cursor is less than opt.height pixels away from bottom window border, show card above cursor
							if(posVertical > limitBottom) {
								posVertical = posVertical - $spc.outerHeight();
								offsetVertical = -10;
							}
							
							// show it with the spinner
							$spc.css('top', posVertical + offsetVertical)
								.css('left', posHorizontal + offsetHorizontal);
						}, opt.delay);
					}
				).on('mouseout', function(e) {
					clearTimeout(previewCardTimeout);

					hideTimeout = setTimeout(function() {
						$('#sig-preview-card').fadeOut();
					}, 300);
				});
				
			});
		}
	}

	$.fn.extend({

		sigPreviewCard : function(method) {

			// Method calling logic
			if (methods[method]) {
				return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
			} else if (typeof method === 'object' || !method) {
				return methods.init.apply(this, arguments);
			} else {
				$.error('Method ' + method + ' does not exist on jQuery.sigPreviewCard');
			}

		},
		sigConvert521toNames: function () {
		    $(this).sigPreviewCard({ entity: 'person' });
		    var uniqueRels = {};
		    for (var i = 0; i < this.length; i++) {
		        var user521 = $(this[i]).attr('rel');
		        if (!uniqueRels[user521]) {
		            uniqueRels[user521] = user521;
		        }
		    }
		    for (var key in uniqueRels) {
		        if (!namesCache[key]) {
		            namesCache[key] = { fname: key }; // to prevent searching for this user in next iterations
		            getPersonData(key, namesCache);
		        } else {
		            $('.person-name[rel="' + key + '"]').html(namesCache[key].fname);
		        }
		    }
		}
	});

})(jQuery);

function personImageError(img) {
	img.src = 'http://nebulacdn.na.novartis.net/images/anonymous-large.png';
	img.onerror = '';
	return true;
}
function getPersonData(user521, namesCache) {
    $.ajax({
        url: 'http://uscalx1214.na.novartis.net/people/get.json',
        dataType: "jsonp",
        data: {
            id: user521
        },
        success: function (data) {
            var userName = user521;
            var url = '';
            if (data.response && data.response.errors && data.response.errors.length) {
                // do nothing
            }
            else {
                var person = data.response.people[0];
                userName = person.firstname + ' ' + person.lastname;
                url = person.firstname + '.' + person.lastname;
            }

            namesCache[user521] = { fname: userName, urlpart: url };
            $('.person-name[rel="' + user521 + '"]').html(userName);
        }
    });
}

function getPersonsFullName(tabledata, namesCache, curindex, onFinish) {

    if (namesCache == null)
        namesCache = {};
    if (curindex == null) curindex = 0;
    var finish = function() {
        if (onFinish != null && $.isFunction(onFinish))
            onFinish();
    };

    if (curindex == tabledata.length) {
        curindex++;
        finish();
        return;
    }

    var user521 = tabledata[curindex].createdBy;

    if (namesCache[user521] != null) {
        tabledata[curindex].createdByFullName = namesCache[user521].fname;
        tabledata[curindex].createdByUrlPart = namesCache[user521].urlpart;
        curindex++;
        getPersonsFullName(tabledata, namesCache, curindex, onFinish);
    } else {
        $.ajax({
            url: 'http://uscalx1214.na.novartis.net/people/get.json',
            dataType: "jsonp",
            data: {
                id: user521
            },
            timeout:5000,
            error: function (xhr, status, error) {
                
                namesCache[user521] = { fname: user521 };
                tabledata[curindex].createdByFullName = namesCache[user521].fname;
                tabledata[curindex].createdByUrlPart = null;
                curindex++;
                getPersonsFullName(tabledata, namesCache, curindex, onFinish);

            },
            success: function (data) {
                var userName = user521;
                var url = null;
                if (data.response && data.response.errors && data.response.errors.length) {
                    personHtml = '<div class="persion-preview">' + data.response.errors[0] + '</div>';
                }
                else {
                    var person = data.response.people[0];
                    userName = person.firstname + ' ' + person.lastname;
                    var urlPart;
                    //urlPart = person.firstname + '.' + person.lastname;
                    var email = person.e_mail;
                    if (email && email.indexOf('@') > 0) {
                        urlPart = email.substring(0, email.indexOf('@'));
                    } else {
                        urlPart = person.firstname + '.' + person.lastname;
                    }

                    personHtml = '<div class="person-preview"><div class="photo"><img src="http://mysite.na.novartis.net/User%20Photos/Profile%20Pictures/' + person.unique_id + '_MThumb.jpg" onerror="personImageError(this);" /></div>' +
                            '<div class="person-details">' + '<a target="_blank" href="http://mysite.na.novartis.net/profile/' + urlPart + '">' + person.firstname + ' ' + person.lastname + '</a>' +
                            '<div class="location">' + person.site_name + ' &middot; ' + person.workplace + '</div>' +
                            '<div class="email"><a href="mailto:' + person.e_mail + '">' + person.e_mail + '</a></div>' +
                            (person.phone
                                ? '<div class="phone">' + person.phone + '</div>'
                                : '<div class="phone unavailable">Phone number unavailable</div>') +
                            '</div></div>';
                }
                namesCache[user521] = { fname: userName, urlpart: urlPart };
                cache[user521] = personHtml;
                tabledata[curindex].createdByFullName = namesCache[user521].fname;
                tabledata[curindex].createdByUrlPart = namesCache[user521].urlpart;
                curindex++;
                getPersonsFullName(tabledata, namesCache, curindex, onFinish);
            }
        });
    }
}