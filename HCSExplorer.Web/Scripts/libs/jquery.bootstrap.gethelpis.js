/* version v 1.0.2*/

var serviceUrlPrefix = "//online-support-services.nibr.novartis.net/apps/OSS/rest/";

var logPostGetUser521;
var logPostGetUserFirstLastName;
var widgetPopupState = "closed";
var successMessage = "Your message has been delivered, and we will respond as soon as possible.";
var appNameLengthRe = /^.{13,}/;
var otherResourceLengthRe = /^.{900,}/;
var subHeadingRegEx = /^.{36,}/;
var subHeadingTextRegEx = /^.{117,}/;
var htmlRegEx = /[<>]/;
var otherResourcesRegEx = /<[^>]*script/;
var numRegEx = /^[0-9]+$/;
var alpRegEx = /^[ a-zA-Z0-9 ,-./ ]+$/;
//var emailRegEx = /^\w.+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
var emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var testGlobalrequestSupportChannel;
var testGlobalgeneralFeedbackSupportChannel;
var testGlobalissueSupportChannel;
var issueDetails = new Object();
var appName = "";

$(document).ready(function () {
    $.ajaxSetup({cache: false});
});

(function ($) {

    var methods = {
        init: function (options) {

            /* Get the user details from auth.js*/
            logPostGetUser521 = '';
            logPostGetUserFirstLastName = '';

            if (typeof (NIBRIam) !== "undefined") {
                logPostGetUser521 = NIBRIam.NIBR521;
                logPostGetUserFirstLastName = NIBRIam.NIBRFull;

            } else {
                logPostGetUser521 = "unavailable";
                logPostGetUserFirstLastName = "unavailable";
            }
            /*GetHelp Widget Header section*/
            var getHelpISHeader = '<div>	\
                                    <span class="gethelpis-close"><a href="javascript:void(0);">&times;</a></span>\
                                    <img src="//nebulacdn.na.novartis.net/component/gethelp/1.0.1/assets/images/gethelp-26x26.png" />\
                                    <span class="gethelpis-heading" id="appname"></span>\
                                <br>\<br>\
				<span class="gethelpis-subheader"><span class="gethelpis-subheader" id="gethelp-subheader">How can we help you?</span></span>\<br>\
                                <span class="gethelpis-subheadertext"><span class="gethelpis-subheadertext" id="gethelp-subheadertext">Report an issue or make a request below. We will respond as soon as possible, or choose Live Chat if available.</span></span>\
                                <div id="gethelp-message"><p class="gethelp-successtext"> <span id="gethelp-successmessage">  </span> </p></div>\
                                </div>';
            /*GetHelp Widget Report issue section*/
            var getHelpISReportIssue = '<div id="gethelp-formbody">\
                                        <div id="gh-incident-form-message" style="display:none;">\
                                                <button id="error-msg" type="button" class="close" aria-hidden="true" >&times;</button>\
                                                <p id="warning"></p>\
                                        </div>\<div class="form-group">\
                                            <span class="gethelpis-h4"> Select a category:</span>\
                                            <br>\<div class="form-group">\
                                            <span class="gethelp-category-errorMsg" style="display:none">CATEGORY REQUIRED</span>\
                                             </div><select id=gethelp-issueType class="gethelpis-select" style="width:345px;">\
                                                <option value="default" disabled selected hidden>-- Select from this list --</option>\
                                                <option value="Issue">Issue / Bug</option>\
                                                <option value="Request">Request</option>  \
                                                <option value="GeneralQuestions">General Question / Feedback</option></select></div>\
                                                <span style="display:none" class="gethelp-title-erroMsg">TITLE REQUIRED</span>\
                                            <div  class = "form-group">\<input  class="form-control gethelpis-input-fields" type="text" id="gethelp-ticket-title" name="gethelp-ticket-title" placeholder="Create a short title.">\<br>\
                                            </div>\<form name="gethelp" method="post" id="gethelp" enctype="multipart/form-data">\
                                                <span style="display:none" class="gethelp-desc-erroMsg">DESCRIPTION REQUIRED</span>\<div  class = "form-group">\
                                                <textarea wrap="soft" class="form-control gethelpis-input-fields gethelpis-textarea-field" id="gethelp-ticket-description" name="gethelp-description" placeholder="Please describe the issue or request."></textarea>\
                                                </div>\
                                                <span class="gethelp-file-errorMsg" style="display:none"></span>\
                                                <input type="text" id="uploadFile" placeholder="Attach file or screenshot (optional)" disabled="disabled" class="gethelp-file-sm gethelp-file-border" /><span style="display:none" class="close-icon">&times;</span>\
                                                <div class="fileUpload btn gethelp-btn-upload">\
                                                <input id="attachfile" type="file" name="file" class="upload" placeholder="" />\
                                                <span>Browse</span>\
                                            </div><br/><br>\
                                            <div id="gethelp-buttons" align="left">\
                                                <input type="hidden" id="gethelp-issueDetails" name="issueDetails">\
												<div id="wait" style="display:none;position:absolute;top:50%;left:50%;"><img src="//nebulacdn.na.novartis.net/component/getHelpISEng/1.0.2/assets/images/spinner.gif" alt="Please Wait..."></div>\
                                                <div id="gethelp-buttons"><input type="button" id="gethelp-submit-request" class="btn gethelp-btn-submit" value="&nbsp&nbsp&nbsp Submit &nbsp&nbsp&nbsp;"></div>\
                                                </div></form></div><p class="gethelp-hr"/>';
            /*GetHelp Widget Other Resources section*/
            var getHelpISOtherResources = '<div class="gethelpis-h4" id="gethelp-other-resources">Other Resources<div class="gethelpis-otherResources-content" id=otherResource></div></div>';

            var getHelpISPanel = '<div id="gethelp-widget" class="gethelp-panel">\
                                    <div class="gethelp-panel-form">'
                    + getHelpISHeader
                    + getHelpISReportIssue

            if (typeof options.otherResource !== 'undefined' && options.otherResource !== null && options.otherResource.trim() !== "")
                getHelpISPanel = getHelpISPanel + getHelpISOtherResources

                        + '</div>\
                        </div>';

            /*Create object and set positions for GetHelp Widget Panel*/
            var $obj = $(this);
            $obj.click(function () {
                // Calculate the proper position
                var bottom = $obj.offset().top + 25;
                // Create main panel and set position
                $('#gethelp-widget').css({
                    "top": bottom
                });
                $("#gethelp-widget").fadeToggle();
                //});
                /* End for setting position for Get Help Widget */


                /* Onclick of gethelp icon popup the widget*/
                //$("#" + getHelpid).click(function () {
                if (widgetPopupState === "closed") {

                    /*Set place holders for form elements*/
                    $('#gethelp-ticket-title').placeHolder();
                    $('#gethelp-ticket-description').placeHolder();
                    $('#uploadFile').placeHolder();
                    $('#gethelp-file-attachment').replaceWith($('#gethelp-file-attachment').val('').clone(true));
                    /*Validate the user configuration input details*/
                    var validName = validateAppName(options.appName);
                    validateSubHeading(options.subHeading);
                    validateSubHeadingText(options.subHeadingText);
                    if (typeof options.otherResource !== 'undefined' && options.otherResource !== null && options.otherResource.trim() !== "")
                        validateOtherResource(options.otherResource);
                    var validateRequest = validateSupportChannel("request", options.requestSupportChannel);
                    var validateIssue = validateSupportChannel("issue", options.issueSupportChannel);
                    var validateFeedback = validateSupportChannel("generalFeedback", options.generalFeedbackSupportChannel);
                    testGlobalrequestSupportChannel = options.requestSupportChannel;
                    testGlobalgeneralFeedbackSupportChannel = options.generalFeedbackSupportChannel;
                    testGlobalissueSupportChannel = options.issueSupportChannel;
                    if ((validateRequest) && (validateIssue) && (validateFeedback) && (validName)) {
                        widgetPopupState = "open";
                        $('#gethelp-message').hide();
                        $("#uploadFile").css('height', '25%');
                        $("#uploadFile").css('width', '70%');
                    } else {
                        $('#gethelp-widget').hide();
                    }
                } else {
                    resetMessageDiv();
                    widgetPopupState = "closed";
                    $("#gethelp-successmessage").empty();
                    $('#gethelp-formbody').show();
                }

            });
            $(getHelpISPanel).appendTo("body");

            $("#attachfile").on("change", function () {
                logPostOrGet('//web.global.nibr.novartis.net/services/logpostorget/getHelpISAttachFileButtonClick');
                var filePath = $("#attachfile").val();
                var result = filePath;
                if (filePath.length >= 40) {
                    result = filePath.substr(0, 40) + '..';
                }
                $("#uploadFile").val(result);
                $("#uploadFile").css('width', '98%');
                $('.fileUpload').hide();
                $('.close-icon').show();
            });
            $('.close-icon').click(function () {
                $("#uploadFile").val("");
                $('#attachfile').replaceWith($('#attachfile').val('').clone(true));
                $("#uploadFile").placeHolder();
                $("#uploadFile").css('width', '70%');
                $('#gethelp-buttons').removeClass('btn-submit');
                $('.gethelp-file-errorMsg').text("");
                $('#uploadFile').removeClass('gethelp-input-error');
                $('#uploadFile').addClass('gethelp-file-border');
                $('.fileUpload').show();
                $('.close-icon').hide();
            });


            /* Onclick of Report Issue button call the service to report issue to JIRA/ServiceDesk */
            $("#gethelp-submit-request").click(function () {
                $("#gethelp-successmessage").empty();
                $('#gethelp-message').show();

                var browsercheck;
                /* Check the browser version for IE9 and below */
                if (document.all && !window.atob) {
                    browsercheck = true;
                } else {
                    browsercheck = false;
                }

                if (typeof (NIBRIam) !== "undefined") {
                    issueDetails.userEmail = NIBRIam.NIBREMAIL;
                    issueDetails.user521 = NIBRIam.NIBR521;
                }
                if (validateFormBeforeSubmit()) {
                    $("#gethelp-submit-request").attr('disabled', true);
                    var description = $("#gethelp-ticket-description").val();
                    issueDetails.description = description + "\n\n\nFROM APPLICATION: " + appName + "\nURL ENCODED: " + encodeURIComponent(window.location) + "\nBrowser Version: " + browserVersion();
                    issueDetails.title = $("#gethelp-ticket-title").val();
                    issueDetails.requestSource = $("#appheader").text();
                    issueDetails.issueType = $("#gethelp-issueType").val();
                    // $("#gethelp-issueDetails").val(JSON.stringify(issueDetails));

                    /* Call the service depending on the issue type selected*/
                    var supportChannel;
                    if (issueDetails.issueType === "Request") {
                        supportChannel = issueDetails.requestSupportChannel;
                        if (supportChannel !== undefined)
                        {
                            if (supportChannel === "JIRA")
                            {
                                var jiraJsonInput = testGlobalrequestSupportChannel.substring((testGlobalrequestSupportChannel.indexOf(",")) + 1, (testGlobalrequestSupportChannel.length));
                                try {
                                    var jiravalues = JSON.parse(jiraJsonInput);
                                    issueDetails.jiraIssueTypeName = jiravalues.issueTypeName;
                                    //alert("jiraIssueTypeName"+ issueDetails.jiraIssueTypeName+"jiravalues.issueTypeName"+jiravalues.issueTypeName);
                                } catch (e) {
                                    alert("Invalid JSON Syntax in " + testGlobalrequestSupportChannel + "GlobalrequestSupportChannel");
                                    return false;
                                }

                            }
                        }
                    } else if (issueDetails.issueType === "Issue") {
                        supportChannel = issueDetails.issueSupportChannel;
                        if (supportChannel !== undefined)
                        {
                            if (supportChannel === "JIRA")
                            {
                                var jiraJsonInput = testGlobalissueSupportChannel.substring((testGlobalissueSupportChannel.indexOf(",")) + 1, (testGlobalissueSupportChannel.length));
                                try {
                                    var jiravalues = JSON.parse(jiraJsonInput);
                                    issueDetails.jiraIssueTypeName = jiravalues.issueTypeName;
                                    //alert("jiraIssueTypeName"+ issueDetails.jiraIssueTypeName+"jiravalues.issueTypeName"+jiravalues.issueTypeName);
                                } catch (e) {
                                    alert("Invalid JSON Syntax in " + testGlobalissueSupportChannel + "GlobalissueSupportChannel");
                                    return false;
                                }

                            }
                        }
                    } else {
                        supportChannel = issueDetails.generalFeedbackSupportChannel;
                        if (supportChannel !== undefined)
                        {
                            if (supportChannel === "JIRA")
                            {
                                var jiraJsonInput = testGlobalgeneralFeedbackSupportChannel.substring((testGlobalgeneralFeedbackSupportChannel.indexOf(",")) + 1, (testGlobalgeneralFeedbackSupportChannel.length));
                                try {
                                    var jiravalues = JSON.parse(jiraJsonInput);
                                    issueDetails.jiraIssueTypeName = jiravalues.issueTypeName;
                                    //alert("jiraIssueTypeName"+ issueDetails.jiraIssueTypeName+"jiravalues.issueTypeName"+jiravalues.issueTypeName);                               
                                } catch (e) {
                                    alert("Invalid JSON Syntax in " + testGlobalgeneralFeedbackSupportChannel + "GlobalgeneralFeedbackSupportChannel");
                                    return false;
                                }

                            }
                        }
                    }
                    var serviceName;
                    if (supportChannel !== undefined) {
                        if (supportChannel === "JIRA") {
                            serviceName = "IncidentManagement/createJiraIssue";
                        } else if (supportChannel === "ServiceDesk") {
                            serviceName = "IncidentManagement/createTETicket";
                        } else if (supportChannel === "Email") {
                            serviceName = "IncidentManagement/sendEmail";
                        }
                    }
                    $("#gethelp-issueDetails").val(JSON.stringify(issueDetails));
                    var options = {
                        iframe: browsercheck,
                        async: true,
                        ContentType: "multipart/form-data",
                        xhrFields: {
                            withCredentials: true
                        },
                        url: serviceUrlPrefix + serviceName + "?" + Math.random(),
						beforeSend: function() {
                        	$('#wait').show();
                         },
                         complete: function(){
                        	 $('#wait').hide();
                         },
                        success: function (response) {
                            //logPostOrGet('//web.global.nibr.novartis.net/services/logpostorget/GetHelpISIncidentSubmitClick');
                            $('#gethelp-formbody').hide();
                            $('#gethelp-subheadertext').hide();
                            $('#gethelp-subheader').css({color: '#4c4c4c'});
                            $('#gethelp-subheader').html("<strong>Thank you</strong>");
                            $('#gethelp-successmessage').html("" + successMessage + "");
                            $('#gethelp-successmessage').show();
                            $("#gethelp-window").height(280);
                            $("#gethelp-submit-request").removeAttr('disabled');
                        }, error: function (response) {
                            if (response.status === 422) {
                                if (response.responseText.indexOf('file type') !== -1) {
                                    $('.gethelp-file-errorMsg').show();
                                    $("#uploadFile").css('width', '98%');
                                    $('.gethelp-file-errorMsg').text(response.responseText);
                                    $('#uploadFile').addClass('gethelp-input-error');
                                    $('#uploadFile').removeClass('gethelp-file-border');
                                } else {
                                    setFormMessage("Error while processing your request. Please contact the service desk.", "alert alert-danger");
                                }
                            } else {
                                setFormMessage("Error while processing your request. Please contact the service desk.", "alert alert-danger");
                            }
                            $("#gethelp-submit-request").removeAttr('disabled');
                        }
                    };
                    $("#gethelp").ajaxSubmit(options);
                }
            });

            /* Close the widget on click of close icon */
            $(".gethelpis-close").click(function () {
                widgetPopupState = "closed";
                $(this).closest(".gethelp-panel").hide();
                $('#gethelp-formbody').show();
                $('#gethelp-successmessage').hide();
                resetMessageDiv();
            });


            /* Close function for Error Message */
            $("#error-msg").click(function () {
                $("#gh-incident-form-message").hide();
            });

            $("#gethelp-ticket-title").keyup(function () {
                $('.gethelp-title-erroMsg').hide();
                $("#gethelp-ticket-title").removeClass('gethelp-input-error');
            }).click(function () {
                $('.gethelp-title-erroMsg').hide();
                $("#gethelp-ticket-title").removeClass('gethelp-input-error');
            });

            $("#gethelp-ticket-description").keyup(function () {
                $('.gethelp-desc-erroMsg').hide();
                $("#gethelp-ticket-description").removeClass('gethelp-input-error');
            }).click(function () {
                $('.gethelp-desc-erroMsg').hide();
                $("#gethelp-ticket-description").removeClass('gethelp-input-error');
            });

            $("#gethelp-issueType").change(function () {
                $('.gethelp-category-errorMsg').hide();
                $("#gethelp-issueType").removeClass('gethelp-input-error');
            });

        }// End of init

    };// END of methods



    /*Validate  form fields*/
    function validateFormBeforeSubmit() {

        var isValid = true;

        if (($("#gethelp-ticket-title").val() === "") || ($("#gethelp-ticket-title").val() === 'Create a short title.')) {
            $('.gethelp-title-erroMsg').show();
            $("#gethelp-ticket-title").addClass('gethelp-input-error');
            isValid = false;
        }
        if (($("#gethelp-ticket-description").val() === "") || ($("#gethelp-ticket-description").val() === 'Please describe the issue or request.')) {
            $('.gethelp-desc-erroMsg').show();
            $("#gethelp-ticket-description").addClass('gethelp-input-error');
            isValid = false;
        }
        if ($("#gethelp-issueType :selected").val() === "default") {
            $('.gethelp-category-errorMsg').show();
            $("#gethelp-issueType").addClass('gethelp-input-error');
            isValid = false;
        }

        if (isValid) {
            /* Validate the file extension for browser version IE9 and below*/
            if (document.all && !window.atob) {
                var fileName = $("#attachfile").val();
                var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
                if (ext === "exe" || ext === "js") {
                    isValid = false;
                    $('.gethelp-file-errorMsg').text("Sorry, that is not an acceptable file type.");
                    $('#uploadFile').addClass('gethelp-input-error');
                    $('#uploadFile').removeClass('gethelp-file-border');
                }
            }
        }
        return isValid;
    }

    /*Setting error message */
    function setFormMessage(message, cssClass) {
        $("#gh-incident-form-message").removeClass();
        $("#gh-incident-form-message").addClass(cssClass);
        $("#warning").html(message);
        $('#gh-incident-form-message').show();
    }

    /*Reset error message*/
    function resetMessageDiv() {
        $("#gh-incident-form-message").hide();
        $('.gethelp-title-erroMsg').hide();
        $("#gethelp-ticket-title").removeClass('gethelp-input-error');
        $('.gethelp-desc-erroMsg').hide();
        $("#gethelp-ticket-description").removeClass('gethelp-input-error');
        $('.gethelp-category-errorMsg').hide();
        $("#gethelp-issueType").removeClass('gethelp-input-error');
        $('.gethelp-file-errorMsg').text("");
        $('#uploadFile').removeClass('gethelp-input-error');
        $('#uploadFile').addClass('gethelp-file-border');
        $("#warning").text('');
        $("#gh-incident-form-message").removeClass();
        $("#gethelp-ticket-title").val('');
        $("#gethelp-ticket-description").val('');
        $("#gethelp-file-attachment").val('');
        $('#attachfile').replaceWith($('#attachfile').val('').clone(true));
        $("#uploadfile").val('');
        $("#uploadFile").css('width', '70%');
        $('.fileUpload').show();
        $('.close-icon').hide();
        $('#gethelp-subheadertext').show();
        $('#gethelp-successmessage').hide();
        $('#gethelp-issueType').val('default');
        $('#gethelp-subheader').css({color: '#ec8026'});
        $('#gethelpis-subheader').html("<strong>How can we help you? </strong>");
    }

    /* Capture usage metrics */
    function logPostOrGet(url) {

        var browsercheck;
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0) {
            browsercheck = true;
        } else {
            browsercheck = false;
        }
        var data = 'user521=' + logPostGetUser521 + '&fullName=' + logPostGetUserFirstLastName + '';
        if (document.all && !window.atob) {
            serviceRequest('POST', url, data);
        } else {
            $.ajax(url, {
                type: "POST",
                data: data,
                success: function (data) {
                }
            });

        }
    }

    /*Plug-in override function for GetHelp*/
    $.fn.extend({
        nibrGetHelp: function (method) {
            getHelpid = $(this).attr("id");
            // Method calling logic
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('Method ' + method + ' does not exist on this file');
            }
        }
    });
    /* Function to display place holder for input fields */
    $.fn.placeHolder = function () {
        var input = this;
        var text = input.attr('placeholder');
        if (text)
            input.val(text).css({color: 'grey'});
        input.focus(function () {
            if (input.val() === text)
                input.css({color: 'lightGrey'}).selectRange(0, 0).one('keydown', function () {
                    input.val("").css({color: 'black'});
                });
        });
        input.blur(function () {
            if (input.val() === "" || input.val() === text)
                input.val(text).css({color: 'grey'});
        });
        input.keyup(function () {
            if (input.val() === "")
                input.val(text).css({color: 'lightGrey'}).selectRange(0, 0).one('keydown', function () {
                    input.val("").css({color: 'black'});
                });
        });
        input.mouseup(function () {
            if (input.val() === text)
                input.selectRange(0, 0);
        });
    };

    $.fn.selectRange = function (start, end) {
        return this.each(function () {
            if (this.setSelectionRange) {
                this.setSelectionRange(start, end);
            } else if (this.createTextRange) {
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }

        });

    };
    /*END of plug-in override function for gethelp*/

    /* Validate the App name configured */
    function validateAppName(header) {
        if ((typeof header !== 'undefined') && (htmlRegEx.test(header))) {
            alert("appName must not contain html tags");
            return false;
        } else if ((typeof header !== 'undefined') && appNameLengthRe.test(header)) {
            alert("appName can have a maximum length of 12");
            return false;
        } else if (typeof header === 'undefined') {
            alert("appName must be defined");
            return false;
        } else {
            appName = header;
            $("#appname").text("GetHelp for " + header);
            return true;
        }
    }
    /* Validate the subheading configured */
    function validateSubHeading(subHeading) {
        if ((typeof subHeading !== 'undefined') && (htmlRegEx.test(subHeading))) {
            alert("subHeading must not contain html tags");
        } else if ((typeof subHeading !== 'undefined') && subHeadingRegEx.test(subHeading)) {
            alert("subHeading can have a maximum length of 35");
        } else {
            $("#gethelp-subheader").text(subHeading);
        }
    }
    /* Validate the subheadingtext configured */
    function validateSubHeadingText(subHeadingText) {
        if ((typeof subHeadingText !== 'undefined') && (htmlRegEx.test(subHeadingText))) {
            alert("subHeadingText must not contain html tags");
        } else if ((typeof subHeadingText !== 'undefined') && subHeadingTextRegEx.test(subHeadingText)) {
            alert("subHeadingText can have a maximum length of 116");
        } else {
            $("#gethelp-subheadertext").text(subHeadingText);
        }
    }
    /* Validate the Other resources section configured */
    function validateOtherResource(otherResource) {
        if ((typeof otherResource !== 'undefined') && (otherResourcesRegEx.test(otherResource))) {
            alert("otherResource should not contain script tag");
        } else {
            $("#otherResource").html(otherResource);
            $("#otherResource").find("a").attr("onclick", "window.open(this.href); return false;");
        }

    }
    /* Validate the support channels configured */
    function validateSupportChannel(type, supportchannel) {
        if (typeof supportchannel !== "undefined") {
            if (supportchannel.indexOf(",") > -1) {
                var supportchanneloption = (supportchannel.substring(0, supportchannel.indexOf(",")));
            } else {
                var supportchanneloption = supportchannel;
            }
            if ((supportchanneloption === 'ServiceDesk') || (supportchanneloption === 'JIRA') || (supportchanneloption === 'Email')) {

                /* Set the support channel values as input to service */
                if (type === "request") {
                    issueDetails.requestSupportChannel = supportchanneloption;
                } else if (type === "issue") {
                    issueDetails.issueSupportChannel = supportchanneloption;
                } else {
                    issueDetails.generalFeedbackSupportChannel = supportchanneloption;
                }
                if (supportchanneloption === 'Email') {
                    var emailidlist = supportchannel.substring((supportchannel.indexOf("{")) + 1, (supportchannel.length) - 1);
                    var emailids = emailidlist.split(",");
                    var i;
                    for (i = 0; i < emailids.length; i++) {
                        if ((!emailRegEx.test(emailids[i])) || (emailids[i] === null)) {
                            alert("In " + type + "SupportChannel Email Address " + emailids[i] + " must be defined in the format name@email.com");
                            return false;
                        }
                    }
                    issueDetails.toEmailAddress = emailidlist;
                }
                if (supportchanneloption === 'JIRA') {
                    var jiraJsonInput = supportchannel.substring((supportchannel.indexOf(",")) + 1, (supportchannel.length));
                    try {
                        var jiravalues = JSON.parse(jiraJsonInput);
                        /*if ((!numRegEx.test(jiravalues.issueTypeId)) || (jiravalues.issueTypeId === null)) {
                         alert("In " + type + "SupportChannel issueTypeId must be defined as a number");
                         return false;
                         } else {
                         issueDetails.jiraIssueTypeID = jiravalues.issueTypeId;
                         }*/
                        if (jiravalues.issueTypeName === null) {
                            alert("In " + type + "SupportChannel issueTypeName is mandatory ");
                            return false;
                        } else if ((!alpRegEx.test(jiravalues.issueTypeName)) || (jiravalues.issueTypeName === null)) {
                            alert("In " + type + "SupportChannel issueTypeName accepts only aplhanumeric, spaces and special charcters ',' or '.' or '/' or '-'");
                            return false;
                        } else {
                            issueDetails.jiraIssueTypeName = jiravalues.issueTypeName;
                        }

                        if ((typeof jiravalues.projectCode === 'undefined') || (jiravalues.projectCode === "")) {
                            alert("In " + type + "SupportChannel projectCode is mandatory");
                            return false;
                        } else {
                            issueDetails.jiraProjectCode = jiravalues.projectCode;
                        }
                        if (!(typeof jiravalues.component === 'undefined') && !(jiravalues.component === "")) {
                            issueDetails.jiraComponent = jiravalues.component;
                        }
                        if (!(typeof jiravalues.defaultAssignee === 'undefined') && !(jiravalues.defaultAssignee === "")) {
                            issueDetails.defaultAssignee = jiravalues.defaultAssignee;
                        }
                    } catch (e) {
                        alert("Invalid JSON Syntax in " + type + " supportchannel");
                        return false;
                    }
                }
                return true;
            } else {
                alert(type + "SupportChannel must be defined as 'ServiceDesk' or 'JIRA' or 'Email'");
                return false;
            }
        } else {
            alert(type + "SupportChannel must be defined");
            return false;
        }
    }


})(jQuery);


/* function*/
function browserVersion() {

    var nVer = navigator.appVersion;
    var nAgt = navigator.userAgent;
    var browserName = navigator.appName;
    var fullVersion = '' + parseFloat(navigator.appVersion);
    var majorVersion = parseInt(navigator.appVersion, 10);
    var nameOffset, verOffset, ix;

    if ((verOffset = nAgt.indexOf("OPR/")) !== -1) {
        browserName = "Opera";
        fullVersion = nAgt.substring(verOffset + 4);
    } else if ((verOffset = nAgt.indexOf("Opera")) !== -1) {
        browserName = "Opera";
        fullVersion = nAgt.substring(verOffset + 6);
        if ((verOffset = nAgt.indexOf("Version")) !== -1)
            fullVersion = nAgt.substring(verOffset + 8);
    } else if ((verOffset = nAgt.indexOf("MSIE")) !== -1) {
        browserName = "Microsoft Internet Explorer";
        fullVersion = nAgt.substring(verOffset + 5);
    } else if ((verOffset = nAgt.indexOf("Chrome")) !== -1) {
        browserName = "Chrome";
        fullVersion = nAgt.substring(verOffset + 7);
    } else if ((verOffset = nAgt.indexOf("Safari")) !== -1) {
        browserName = "Safari";
        fullVersion = nAgt.substring(verOffset + 7);
        if ((verOffset = nAgt.indexOf("Version")) !== -1)
            fullVersion = nAgt.substring(verOffset + 8);
    } else if ((verOffset = nAgt.indexOf("Firefox")) !== -1) {
        browserName = "Firefox";
        fullVersion = nAgt.substring(verOffset + 8);
    } else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) <
            (verOffset = nAgt.lastIndexOf('/')))
    {
        browserName = nAgt.substring(nameOffset, verOffset);
        fullVersion = nAgt.substring(verOffset + 1);
        if (browserName.toLowerCase() === browserName.toUpperCase()) {
            browserName = navigator.appName;
        }
    }
    if ((ix = fullVersion.indexOf(";")) !== -1)
        fullVersion = fullVersion.substring(0, ix);
    if ((ix = fullVersion.indexOf(" ")) !== -1)
        fullVersion = fullVersion.substring(0, ix);

    majorVersion = parseInt('' + fullVersion, 10);
    if (isNaN(majorVersion)) {
        fullVersion = '' + parseFloat(navigator.appVersion);
        majorVersion = parseInt(navigator.appVersion, 10);
    }
    return browserName + " V " + fullVersion;
}

/* Generic method for AJAX request*/
function serviceRequest(method, url, data) {

    var xmlhttp = new XMLHttpRequest();
    if (window.XDomainRequest) {
        xmlhttp = new XDomainRequest();
    } else if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open(method, url, true);
    xmlhttp.withCredentials = true;
    xmlhttp.onprogress = function () {
    };
    xmlhttp.ontimeout = function () {
    };
    xmlhttp.onerror = function () {
    };
    setTimeout(function () {
        xmlhttp.send(data);
    }, 0);
    return xmlhttp.responseText;
}