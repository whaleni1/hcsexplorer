﻿define([
  'text!templates/detailspopup.htm',
  'utils/view-utils', 'bootbox', 'backbone'
],
  function (template, utils, bootbox) {
      var view = Backbone.View.extend({

          Initialize: function (options) {
              this.compiledTemplate = _.template(template);
              this.options = options;
              this.render();
          },
          render: function () {
              var view = this;

              view.$el.html(view.compiledTemplate);
              //$('#detailsPopupDlg').empty().html(view.$el);

              view.$('#fullText').html(utils.decodeString(view.options.text));

              //utils.createDialog(view, '#detailsPopupDlg', 'Details', '50%', 'auto');
              view.showModal('Details', view.$el);

              return this;
          },

          showModal: function (title, html) {
              var view = this;
              view.dialog = bootbox.dialog({
                  title: title,
                  message: html,
                  show: true,
                  buttons: {
                      ok: {
                          label: "OK",
                          className: "btn-primary",
                          callback: function () {
                          }
                      }
                  }
              }).addClass("modal-details");
          },

          onclose: function () {
          }
      });
      return view;
  });
