﻿define([
  'text!templates/runlist.htm',
  'models/runlist', 'utils/view-utils', 'utils/model-utils', 'utils/date-utils', 'utils/datatable-utils', 'ifvisible',
  'backbone', 'mbinder', 'datatables', 'datatablesbootstrap', 'datatablesscroller'
],
  function (template, model, utils, modelUtils, dateUtils, tableUtils , ifvisible) {

      
      var view = Backbone.View.extend({

          Initialize: function () {
              this.compiledTemplate = _.template(template);
              this.options = {
                  myTblMaxHeight: 174,
                  allTblMaxHeight: 261,
                  myexecuting : false,
                  allexecuting : false,               
                  updateInterval: 30000, //ms
                  myScrollPos: 0,
                  allScrollPos: 0,
                  fixedRowHeight: 0,
                  minWindowWidth: 1000,
                  buffering: true
              };
              var _doAdjustColumns = _.debounce(this.adjustColumns, 300);
              $(window).bind("resize.app", _.bind(_doAdjustColumns, this));

              this.oldW = $('body').css('min-width');
              $('body').css('min-width', this.options.minWindowWidth + 'px');
              var cH = this.calcHeight();

              this.options.myTblMaxHeight = cH.myTblMaxHeight;
              this.options.allTblMaxHeight = cH.allTblMaxHeight;
              this.allVisible = true;
              this.myVisible = true;
              this.render();
          },
          calcHeight: function () {

              var pageH = $(window).height();
              if (pageH < 500) pageH = 500;
              var tblsH = pageH - 340;
              if ($(window).width() < this.options.minWindowWidth)//horiz scroll bar
                  tblsH -= 30;
              return {
                  myTblMaxHeight: tblsH / 5 * 2,
                  allTblMaxHeight: tblsH / 5 * 3
              }
          },
          render: function () {
              var view = this;

              view.$el.html(view.compiledTemplate);
              
              var pageW = $(window).width();
              view.options.fixedRowHeight = 22;


              //view.options.fixedRowHeight = pageW >= 1300 ? 46 : 64;
              //view.options.fixedRowHeight = pageW >= 1300 ? 39 : 57; // (n*rowH + padding-top + padding-bottom)

              var tmodel = new model();
              var response = tmodel.fetch({
                  async: false
              });
              if (utils.wasFetchError(view, response)) {
                  $('.mainArticle').empty().html(view.$el);
                  return;
              }

              var runs = tmodel.get('runs').rows;
              var myruns = tmodel.get('myRuns').rows;
              view.namesCache = {};

              $('.mainArticle').empty().html(view.$el);

              var rmodel = new model();
              rmodel.set('id', tmodel.get('runs').id);
              rmodel.set('filter', tmodel.get('runs').filter);
              var mymodel = new model();
              mymodel.set('id', tmodel.get('myRuns').id);
              mymodel.set('filter', tmodel.get('myRuns').filter);
              view.rmodel = rmodel;
              view.mymodel = mymodel;


              view.initRunListColumns();
              view.headerTable = view.initHeaderTable();
              view.table = view.initTable(rmodel);
              view.myTable = view.initTable(mymodel);

              $(view.myTable.fnSettings().nTableWrapper).find('.dataTables_scrollBody').on('scroll', function () { view.scrollTables(view.myTable, view.table, view.headerTable); });
              $(view.table.fnSettings().nTableWrapper).find('.dataTables_scrollBody').on('scroll', function () { view.scrollTables(view.table, view.myTable, view.headerTable); });

              //view.adjustColumns();

              //view.$('#tblFilter').placehold();

              view.delegateEvents({
                  'click .js-download-link': 'downloadFile',
                  'click .js-download-menu-link': 'showDownloadMenu',
                  'click  #createNewBtn': 'showCreateRunDlg',
                  'keyup  #tblFilter': 'doFilter',
                  'hide.bs.collapse #my-runs-target,#all-runs-target': 'hideGroup',
                  'shown.bs.collapse #my-runs-target,#all-runs-target': 'showGroup'
              });

              ifvisible.on('statusChanged', function (e) {
                  if (e.status == 'active')
                      view.updateRuns();
              });

              $('body').on("click", function (e) {
                  // close dowload menu if any
                  view.hideDownloadMenu();
              });

              return this;
          },
          scrollTables: function (t, t1, t2) {
              var view = this;
              view.hideDownloadMenu();
              var s = $(t.fnSettings().nTableWrapper).find('.dataTables_scrollBody').scrollLeft();
              $(t1.fnSettings().nTableWrapper).find('.dataTables_scrollBody').scrollLeft(s);
              $(t2.fnSettings().nTableWrapper).find('.dataTables_scrollHead').scrollLeft(s);
          },
          downloadFile: function (ev) {
              var view = this;
              if ($(ev.currentTarget).hasClass("js-popup-link")) { // close popup download menu
                  view.hideDownloadMenu();
              }
              utils.downloadFile(ev.currentTarget);
              return false; //this is critical to stop the click event which will trigger a normal file download!
          },
          hideDownloadMenu: function () {
              $(".download-popup-menu").remove();
              $(".js-menu-opened").removeClass("js-menu-opened");
          },
          showDownloadMenu: function (ev) {
              var view = this;
              var $tagertLink = $(ev.currentTarget);

              $(".download-popup-menu").remove();
              if ($tagertLink.hasClass("js-menu-opened")) {
                  $tagertLink.removeClass("js-menu-opened");
                  return false;
              }
              $(".js-menu-opened").removeClass("js-menu-opened");

              var links = $tagertLink.next(".js-links-holder").find(".js-download-link");
              var $dpm = $("<div class='download-popup-menu'>")
              for (var i = 0; i < links.length; i++) {
                  $dpm.append(links[i].outerHTML).append("</br>");
              }
              //$("body").append($dpm);
              view.$el.append($dpm);
              $tagertLink.addClass("js-menu-opened");

              var pos = $tagertLink.offset();
              var h = $tagertLink.height();
              $dpm.css('top', pos.top + h).css('left', pos.left);
              $dpm.show();
              return false;
          },
          hideGroup: function (ev) {
              var view = this;
              var scrollPos = $(ev.currentTarget).find(".dataTables_scrollBody").scrollTop();
              if (ev.currentTarget.id == "my-runs-target") {
                  view.options.myScrollPos = scrollPos;
                  view.myVisible = false;
              } else {
                  view.options.allScrollPos = scrollPos;
                  view.allVisible = false;
              }

              _.delay(function () { view.adjustColumns(); }, 200);

          },
          showGroup: function (ev) {
              var view = this;
              var scrollPos;
              var my;
              if (ev.currentTarget.id == "my-runs-target") {
                  scrollPos = view.options.myScrollPos;
                  view.myVisible = true;
              } else {
                  scrollPos = view.options.allScrollPos;
                  view.allVisible = true;
              }
              $(ev.currentTarget).find(".dataTables_scrollBody").scrollTop(scrollPos);
              _.delay(function () { view.adjustColumns(); }, 200);
          },
          doFilter: function (ev) {
              return;
              //var view = this;
              //var oTable = view.table;
              //oTable.fnFilter(ev.target.value, null, true);
          },
          updateRuns: function () {
              var view = this;
              view.stopAutoUpdate();
              if (ifvisible.now()) {
                  view.cache = [];//clear cache
                  view.myCache = [];
                  utils.clearTooltips();
                  var myRunsVisible = view.$('#tblRunsMy').is(":visible");
                  var otherRunsVisible = view.$('#tblRuns').is(":visible");

                  if (myRunsVisible) {
                      view.myTable.fnDraw({ iInitDisplayStart: -1 });
                  }
                  if (otherRunsVisible) {
                      view.table.fnDraw({ iInitDisplayStart: -1 });

                  }
              }
                           
          },
          setTableHeight: function(){
              var view = this;
              var cH = view.calcHeight();
              var myRunsVisible = view.myVisible;
              var otherRunsVisible = view.allVisible;
              if (myRunsVisible && otherRunsVisible) {
                  $(view.myTable.fnSettings().nTableWrapper).find('.dataTables_scrollBody').height(cH.myTblMaxHeight);
                  $(view.table.fnSettings().nTableWrapper).find('.dataTables_scrollBody').height(cH.allTblMaxHeight);
              } else if (myRunsVisible) {
                  $(view.myTable.fnSettings().nTableWrapper).find('.dataTables_scrollBody').height(cH.myTblMaxHeight + cH.allTblMaxHeight);
              } else if (otherRunsVisible) {
                  $(view.table.fnSettings().nTableWrapper).find('.dataTables_scrollBody').height(cH.myTblMaxHeight + cH.allTblMaxHeight);
              }
          },
          adjustColumns: function ( showopt) {
              
              var view = this;

              var myRunsVisible = view.myVisible;
              var otherRunsVisible = view.allVisible;



              if (!view.table || !view.headerTable || !view.myTable)
                  return;
              view.setTableHeight();
              

              view.headerTable.fnAdjustColumnSizing();
              if (otherRunsVisible) view.table.fnAdjustColumnSizing();
              if (myRunsVisible) view.myTable.fnAdjustColumnSizing();

              //var myScrollDiv = $(view.myTable.fnSettings().nTableWrapper).find('.dataTables_scrollBody');

              //var cellsHdrs = view.table.fnSettings().aoHeader[0].nTr.cells;
              
             
          },
          initHeaderTable: function () {
              var view = this;
              var headCols = [];
              _.each(view.runListColumns, function (el, index) {
                 
                  var col = { mDataProp: el.mDataProp, minWidth: el.minWidth, bSortable: false, bVisible: el.bVisible };
                 
                  var w = el.sWidth;
                  if (w !== null) {

                      if (index == 7) //last vis col
                          col.sWidth = parseInt(w) + 15 + "";//scrollbar
                      else
                          col.sWidth = w;
                  }
                  headCols.push(col);
              });
              var dtConfig = {
                  bJQueryUI: true,
                  bAutoWidth: false,
                  aaSorting: [[6, 'desc']],
                  sScrollY: "0px",
                  sDom: "rt",
                  aoColumns: headCols
              };
              var tbl = view.$('#tblHeader').dataTable(dtConfig);
              return tbl;
          },
          initRunListColumns: function() {
              var view = this;
              view.runListColumns = [
                      { mDataProp: 'status', minWidth: 60, sWidth: "60", bSortable: false, bVisible: true, bSearchable: true, mRender: function (v, type, oData) {
                          if (oData.imageListOnly && oData.statusCode == 2) { // show more correct message instead of "On Cluster" for the ImageListOnly runs in "processing" state
                              return "Generating";
                          }
                          return v;
                      }},
                      { mDataProp: 'idStr', minWidth: 100, sWidth: "100", iDataSort: 12, bSortable: false, bVisible: true, bSearchable: true, mRender: function (v, type, oData) {
                          if (v == null) return v;
                          if (type == "display")
                              return '<a href="#' + view.makeRunDetailsUrl(oData.calcId) + '">' + v + '</a>';
                          else return v;
                      }
                      },
                      {
                          mDataProp: 'createdByFullName', sClass: 'ellipsis', minWidth: 120, sWidth: "120", bSortable: false, bVisible: true, bSearchable: true, mRender: function (v, type, oData) {
                              if (type == "display") {
                                  if (oData.createdByUrlPart != null) {
                                      //var siteCode = $.ajax({
                                      //    url: 'http://web.global.nibr.novartis.net/services/ps/v1/person/' + oData.createdBy,
                                      //    type: 'GET',
                                      //    //contentType: "application/json",
                                      //    dataType: "json",
                                      //    data: {
                                      //        siteCode: siteCode
                                      //    },
                                      //    success: function (data) {
                                      //        console.log('NIBR personService call for ' + oData.createdBy + ' was successful.');
                                      //    },
                                      //    error: function (jqXHR, textStatus, errorThrown) {
                                      //        console.log(errorThrown);
                                      //    }
                                      //});
                                      //console.log('runlist.js: siteCode ='+siteCode);
                                      //var raw = siteCode.site_code;
                                      //console.log('runlist.js: siteCode.site_code ='+raw);
                                      ////drive the domain based on user's site code
                                      //var userDomain = getDomainFromSiteCode(siteCode);

                                      //return '<a class="person-name" target="_blank" href="http://mysite.nibr.novartis.net/Person.aspx?accountname=' + userDomain + '\\' + oData.createdBy + '" rel="' + oData.createdBy + '">' + v + '</a>';
                                      //return '<a class="person-name" target="_blank" href="http://mysite.na.novartis.net/profile/' + oData.createdByUrlPart + '" rel="' + oData.createdBy + '">' + v + '</a>';
                                      return '<a class="person-name" target="_blank" href="http://mysite.nibr.novartis.net/Person.aspx?accountname=NANET\\' + oData.createdBy + '" rel="' + oData.createdBy + '">' + v + '</a>';
                                  }
                                  else if (oData.createdBy != null)
                                      return '<a class="person-name" target="_blank" rel="' + oData.createdBy + '">' + v + '</a>';
                                  else
                                      return '';

                              }
                              else return v;
                          }
                      },
                      { mDataProp: 'name', sClass: 'ellipsis', minWidth: 50, bSortable: false, bVisible: true, bSearchable: true, mRender: function (v, type, oData) {
                          if (v == null) return v;
                          var text = v;
                         /* if (v.length > 65) {
                              text = v.substr(0, 65) + ' ...';
                          } else {
                              // disable tooltip if full text is visible
                              v = "";
                          }*/
                          if (type == "display")
                              return '<a href="#' + view.makeRunDetailsUrl(oData.calcId) + '" data-toggle="tooltip" data-placement="left" title="' + v + '">' + text + '</a>';
                          else return v;
                      }
                      },
                      { mDataProp: 'hasResults', minWidth: 120, sWidth: "120", bVisible: true, bSortable: false, bSearchable: false, mRender: function (v, type, oData) {
                          if (!v) return 'N/A';
                          if (oData.imageListOnly) {
                              return '<a class="js-download-link" href="' + view.makeDownloadImglistUrl(oData.calcId) + '" style="white-space:nowrap;"><span class="glyphicon glyphicon-download"></span> Download</a>';
                          } else {
                              //return '<a class="js-download-link" href="' + view.makeDownloadResultsUrl(oData.calcId) + '" style="white-space:nowrap;"><span class="glyphicon glyphicon-download"></span> Download</a>';
                              var html = '<a class="js-download-menu-link" href="#" style="white-space:nowrap;">Download <span class="caret"></span></a>';

                              html += '<div class="js-links-holder">';
                              if (oData.inOneFile) {
                                  html += '<a class="js-download-link js-popup-link" href="' + view.makeDownloadResultsUrl(oData.calcId, "inOneFile") + '" style="white-space:nowrap;"><span class="glyphicon glyphicon-download"></span> In One File</a>';
                              }
                              if (oData.icp4) {
                                  html += '<a class="js-download-link js-popup-link" href="' + view.makeDownloadResultsUrl(oData.calcId, "icp4") + '" style="white-space:nowrap;"><span class="glyphicon glyphicon-download"></span> ICP4</a>';
                              }
                              html += '</div>';
                                     
                              return html;
                          }
                      }
                      },
                      {
                          mDataProp: 'protocolName', sClass: 'ellipsis', minWidth: 50, bVisible: true, bSortable: false, bSearchable: true, mRender: function (v, type, oData) {
                          if (type == "display") {
                              if (oData.imageListOnly) {
                                  return "&lt;Image list only&gt;";
                              } else if (v) {
                                  return '<a class="js-download-link" href="' + view.makeDownloadProtocolUrl(oData.calcId) + '"><span class="glyphicon glyphicon-download"></span> ' + v + '</a>';
                              } else {
                                  return v;
                              }
                          } else
                              return v;
                      }
                      },
                      { mDataProp: 'createdDateStr', minWidth: 135, sWidth: "140", bSortable: false, bVisible: true, bSearchable: true },
                      { mDataProp: 'lastUpdatedStr', minWidth: 135, sWidth: "140", bVisible: true, bSortable: false, bSearchable: true },
                      { mDataProp: 'id', bVisible: false, bSearchable: false }
              ];
          },
          initTable: function (runmod) {
              var view = this;
              var runs = runmod.get('rows');
              var tid = runmod.get('filter').my ? '#tblRunsMy' : '#tblRuns';
              var isMyTable = runmod.get('filter').my;
             
              var tHeight = isMyTable ? view.options.myTblMaxHeight + 'px' : view.options.allTblMaxHeight + 'px';

              var dtConfig = {
                  bJQueryUI: true,
                  bAutoWidth: false,
                  bFilter: false,
                  bSort:false,
                  bDeferRender: false,
                  sDom: "rtiS",
                  oScroller: {
                      trace: false,
                      displayBuffer: 3,
                      loadingIndicator: true,
                      boundaryScale: 0.5,
                      rowHeight: view.options.fixedRowHeight,
                      serverWait:0
                  },

                  bProcessing: true,
                  bServerSide: true,
                  sScrollY: tHeight,
                  bScrollCollapse: true,
                  //bLengthChange: true,
                  sAjaxSource: '',
                  fnServerData: //_.throttle( 
                      function (sSource, aoData, fnCallback, oSettings) {
                      view.stopAutoUpdate();
                      var thisid = this.selector;
                      var pmodel = null;

                      if (thisid == '#tblRuns') {
                          pmodel = view.rmodel;
                          var tbl = view.table;
                      } else {
                          pmodel = view.mymodel;
                          tbl = view.myTable;
                      }
                      view.rightPage(this, pmodel, aoData, fnCallback,  oSettings);
                      },
                  //1000),

                  fnDrawCallback: function () {
                      //view.adjustColumns();
                      utils.clearTooltips();
                  },
                  oLanguage: { sSearch: '' , sProcessing: 'Loading...'},
                  aoColumns: view.runListColumns,
                  fnRowCallback: function (nRow, aaData, iDisplayIndex) {
                      $(nRow).addClass('fix-tr-height');
                      var tdClass = '';
                      switch (aaData['statusCode']) {
                          case 1:
                              tdClass = 'run-status-validating';
                              break;
                          case 2:
                              tdClass = 'run-status-submit';
                              break;
                          case 3:
                              tdClass = 'run-status-error';
                              break;
                          case 4:
                              tdClass = 'run-status-completed';
                              break;
                      }
                      $('td:eq(0)', nRow).addClass(tdClass);
                      $(".person-name", nRow).sigPreviewCard({ entity: 'person' });
                      return nRow;
                  },
              };
              if(isMyTable){
                  dtConfig.iDisplayLength = view.options.myDisplayLength;
                  dtConfig.aLengthMenu = view.options.myLengthMenu;
                  
              }else{
                  dtConfig.iDisplayLength = view.options.allDisplayLength;
                  dtConfig.aLengthMenu = view.options.allLengthMenu;
              }
              var oTable = view.$(tid).dataTable(dtConfig);
              return oTable;
          },
          makeRunDetailsUrl: function (id) {
              return "run/" + id;
          },
          makeDownloadResultsUrl: function (id, fileType) {
              if (!fileType) {
                  fileType = "inOneFile";
              }
              return modelUtils.createBaseUrl("api/run/downloadresults") + "?id=" + id + "&type=" + fileType;
          },
          makeDownloadImglistUrl: function (id) {
              return modelUtils.createBaseUrl("api/run/downloadimglist") + "/" + id;
          },
          makeDownloadProtocolUrl: function (id) {
              return  modelUtils.createBaseUrl("api/run/downloadprotocol") + "/" + id; 
          },
          showCreateRunDlg: function () {
              var view = this;
              view.stopAutoUpdate();
              require(['views/createrun'],
                function (createRunView) {
                    view.createRunView = new createRunView({parentView: view});
                },
                function (error) {
                });
          },
          rightPage: function (table, model, aoData, fnCallback, oSettings) {
              var view = this;
              var bmy = false;
              if (table.selector == "#tblRunsMy") {
                  bmy = true;
                  view.options.myexecuting = true;
              } else {
                  bmy = false;
                  view.options.allexecuting = true;
              }

              var ar = [];
              var ind0 = 0;
              var ind1 = -1;
              var ind2 = 0;
              for (var i = 0; i < aoData.length; i++) {
                  
                    if (aoData[i].name == "iDisplayStart" || aoData[i].name == 'start') {
                        ind0 = i;
                        ar["iDisplayStart"] = aoData[i].value;
                    }
                    else if (aoData[i].name == "iDisplayEnd") {
                        ar["iDisplayEnd"] = aoData[i].value;
                        ind1 = i;
                    }
                    else if (aoData[i].name == 'iDisplayLength' || aoData[i].name == 'length') {
                        ind2 = i;
                        ar["iDisplayLength"] = aoData[i].value;

                    } else if (aoData[i].name == 'sEcho' || aoData[i].name == 'draw') {
                        ar['sEcho'] = aoData[i].value;
                    } else {
                    ar[aoData[i].name] = aoData[i].value;
                    }
              }

              var sEcho = 1;
              try {
                  sEcho = parseInt(ar["sEcho"]);
              } catch (e) {
              }
             
              var pagingModel = tableUtils.paging(ar);
              var cache;
              if (bmy) {
                  cache = view.myCache;
              } else {
                  cache = view.cache;
              }
              var needload = true;
              var olddata = [];
              
              if (view.options.buffering  && cache != null) {
                  needload = false;
                  for (var k = pagingModel.from; k < pagingModel.to; k++) {
                      if (cache[k] == null) {
                          needload = true;
                          break;
                      } else {
                          olddata.push(cache[k]);
                      }

                  }
              }
              var oldcount = model.get('count');
              if (needload) {
                  var tableData = model.get('rows');
                 
                  //if (sEcho > 1) {
                  var response = model.save(
                  {
                      paging: pagingModel,
                      rows: []
                     
                  }, {
                      success: function (m, response) {
                          tableData = m.get('rows');

                          getPersonsFullName(tableData, view.namesCache, 0, function () {

                              var res = {
                                  "sEcho": sEcho,
                                  "iDisplayStart": m.get('paging').from,
                                  "iDisplayLength": m.get('paging').from + tableData.length,
                                  "iTotalRecords": m.get('count'),
                                  "iTotalDisplayRecords": m.get('count'),
                                  "aaData": tableData
                              };
                              //if (res.aaData.length == 0 && res.iTotalRecords != 0)
                              //    table.fnPageChange('previous');
                              //else

                              if (bmy)
                                  view.$('#my-runs-target  .dataTables_processing').css('visibility', 'hidden');
                              else
                                  view.$('#all-runs-target  .dataTables_processing').css('visibility', 'hidden');

                              fnCallback(res);

                              if (bmy) {
                                  view.options.myexecuting = false;
                              } else {
                                  view.options.allexecuting = false;
                              }

                              if (view.options.buffering) {
                                  if (bmy) {
                                      if (view.myCache == null) {
                                          view.myCache = [];
                                      }
                                      var from = m.get('paging').from;
                                      for (var kk = 0; kk < tableData.length; kk++)
                                          view.myCache[from + kk] = tableData[kk];
                                  } else {
                                      if (view.cache == null) {
                                          view.cache = [];
                                      }
                                      var from = m.get('paging').from;
                                      for (var kk = 0; kk < tableData.length; kk++)
                                          view.cache[from + kk] = tableData[kk];
                                  }
                              }
                              if (!view.options.myexecuting && !view.options.allexecuting)
                                  view.startAutoUpdate();

                          });
                      },
                      error: function (m, response) {
                          utils.wasFetchError(view, response);
                          m.set('rows', tableData);
                          if (bmy) {
                              view.options.myexecuting = false;
                          } else {
                              view.options.allexecuting = false;
                          }
                          if (!view.options.myexecuting && !view.options.allexecuting)
                              view.startAutoUpdate();
                      }
                  });
              } else {

                  var res = {
                      "sEcho": sEcho,
                      "iDisplayStart": pagingModel.from,
                      "iDisplayLength": pagingModel.to - pagingModel.from,
                      "iTotalRecords": oldcount,
                      "iTotalDisplayRecords": oldcount,
                      "aaData": olddata
                  };
                  //if (res.aaData.length == 0 && res.iTotalRecords != 0)
                  //    table.fnPageChange('previous');
                  //else

                  if (bmy)
                      view.$('#my-runs-target  .dataTables_processing').css('visibility', 'hidden');
                  else
                      view.$('#all-runs-target  .dataTables_processing').css('visibility', 'hidden');

                  fnCallback(res);

                  if (!view.options.myexecuting && !view.options.allexecuting)
                      view.startAutoUpdate();


              }
                 
          },
          startAutoUpdate: function () {
              var view = this;
              view.stopAutoUpdate();
              view.intervalId = setTimeout(function () { view.updateRuns(); }, view.options.updateInterval);
          },
          stopAutoUpdate: function () {
              var view = this;
              if (view.intervalId) {
                  clearTimeout(view.intervalId);
                  view.intervalId = null;
              }
          },
          onclose: function () {
              var view = this;
              view.stopAutoUpdate();
              $(window).unbind("resize.app");
              if (this._modelBinder)
                  this._modelBinder.unbind();
              Backbone.Validation.unbind(this);
              $('body').css('min-width', view.oldW);
          }
      });
      return view;
  });
