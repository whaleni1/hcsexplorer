define(['utils/view-utils', 'backbone',
        'datatables', 'iframetransport'],
function (utils) {

    var msgSize0 = 'Yours is ';

  var file_upload = Backbone.View.extend({
    Initialize: function(options) {
      _.bindAll(this, 'fail', 'uploading', 'done');
      this.model =Backbone.Model.extend({});
      options = options || {};
      this.url = options.url || '/upload';
      this.data = {};
      this.dataselector = options.dataselector;
      this._accept = options.accept;
      this._name = options.name || 'uploadedfile';
      this._id = options.id || this._name;
      this._enabled = options.hasOwnProperty('enabled') ? options.enabled : true;
      this.beforeupload = options.beforeupload;
      this.uploadonchangeurl = options.uploadonchangeurl;
      this.clearondone = options.clearondone;
      this.maxSize = options.maxSize;
      this.msgUpload = options.msgUpload;
      this.msgSize = options.msgSize;
      return this;
    },
    tagName: 'input',
    events: {
      'change': 'change'
    },
    render: function() {
        this.$el.attr('type', 'file').attr('name', this._name).attr('id', this._id);
      this.$el.addClass('file-upload');
      /*if (this._multiple) {
        this.$el.attr('multiple', true);
      }*/
      if (!this._enabled) {
        this.$el.attr('disabled', 'disabled');
      }
      if (this._accept!=null) {
          this.$el.attr('accept', this._accept);
      }
      this.$input = this.$el;
      return this;
    },
    enable: function(enable) {
      if (this._enabled != enable) {
        this._enabled = enable;
        if (enable) {
          this.$input.removeAttr('disabled');
        } else {
          this.$input.attr('disabled', 'disabled');
        }
        this.trigger('enabled', enable);
      }
    },
    toggle_enable: function() {
      this.enable(!this._enabled);
    },
    clearValidationError: function () {
        var diverr = this.$input.parent().next();
        diverr.html('');
    },
    setValidationError: function (err) {
        //var diverr = this.$input.parent().next();
        var diverr = this.$input.closest('.form-group').find('.error');
        diverr.html(err);
    },
    change: function (ev) {
        var diverr = this.$input.parent().next();
        this.clearValidationError();

        if (this._enabled) {
            //check size for FF
            if (this.$input[0].files != null) {

                if (this.$input[0].files.length > 0) {

                    var size = -1;
                    try{
                        size = this.$input[0].files[0].size;
                    } catch (e) { }

                    if (size == 0 ) {
                        this.clearFile();
                        diverr.html(this.msgUpload);
                        var result = {};
                        result.validationErrors = [{ elementName: this._id, description: "The file is empty" }];
                        this.trigger('fail', result, null);
                        return;
                    }
                    if ( size > this.maxSize) {
                        this.clearFile();
                        diverr.html(this.msgSize + msgSize0 + (size / 1048576).toPrecision(4) + " MB.");
                        var result = {};
                        result.validationErrors = [{ elementName: this._id, description: this.msgSize + msgSize0 + (size / 1048576).toPrecision(4) + " MB." }];
                        this.trigger('fail', result, null);
                        return;
                    }
                }
            }
            this.upload(true);
        }
    },
    upload: function (validatefile) {
        var this_ = this;
        if (this.$input.val() == '')
            return;
      if (this_._enabled) {
        //???var modeldata =  $(this_.dataselector + " :input").serializeArray();
        if ($.isFunction(this_.beforeupload)) {
          this_.beforeupload();
        }
        this.$input.removeAttr('disabled');
        var url = this_.url;
        if (validatefile) url = this_.uploadonchangeurl;
        _.delay(function (view) {
          $.ajax(url, {
            files: view.$input,
            iframe: true,
            dataType: 'json',
            //???data: modeldata,
            processData: false,
            type: 'POST'
          }).always(view.uploading)
            .done(view.done)
            .fail(view.fail);
        }, 100, this_);
      }        
    },
    done: function(data, textStatus, jqXHR) {
        var result = utils.errorText(jqXHR, true);
        var bcleared = false;
        if (this.clearondone) {
            bcleared = true;
            this.clearFile();
        }
        if ((result.message != null && result.message != '') || (result.validationErrors != null && result.validationErrors.length > 0)) {
            if (!bcleared)
                this.clearFile();
            this.trigger('fail', result, jqXHR);
        }
        else
            this.trigger('done', data, jqXHR);
    },
    fail: function (jqXHR) {
      var result = utils.errorText(jqXHR);
      this.clearFile();
      if (jqXHR.status == 510) {
          result.message = null;
          result.validationErrors = [{ elementName: this._id, description: this.msgSize }];
      }
      this.trigger('fail', result, jqXHR);

    },
    uploading: function(data, textStatus, jqXHR) {   
      this.trigger('uploading');
    },
    clearFile: function () {
      return; // disable clearing
      if ($.browser.msie) {
          //var id = this._id;
          var inp = this.$input.clone(true);
          this.$input.next("input").remove();
          this.$input.replaceWith(inp);
          this.$input = inp;
          //inp.atrr('id', this._id);
      } else {
        this.$input.val('');
      }
    }
  });
  return file_upload;
});
