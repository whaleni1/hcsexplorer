﻿define([
  'text!templates/rundetails.htm',
  'models/rundetails',
  'utils/view-utils', 'utils/model-utils', 'utils/date-utils', 'utils/datatable-utils',  'ifvisible',
  'backbone', 'mbinder', 'datatables', 'signalR', 'signalRHubs'
],
  function (dTemplate, model, utils, modelUtils, dateUtils, dtUtils, ifvisible) {

      var rowId = utils.getRowIdArray();
      var view = Backbone.View.extend({ 

          Initialize: function (par) {
              this.compiledTemplate = _.template(dTemplate);
              this.options = par.options;
              this.options.updateInterval = 30000; // ms
              $(window).bind("resize.app", _.bind(this.adjustFieldsWidth, this));
              this.render();
          },
          render: function () {
              utils.clearTooltips();
              var view = this;
              view.$el.html(view.compiledTemplate);

              var tmodel = new model();
              tmodel.set('id', view.options.id);
              var response = tmodel.fetch({
                  async: false
              });
              $('.mainArticle').empty().html(view.$el);
              if (utils.wasFetchError(view, response)) {
                  return;
              }

              view.$('.rundetails').show();
              view.adjustFieldsWidth();

              this.model = tmodel;
              utils.decodeModelFields(this.model); // bug fix for "Run Description" field containing '\n' characters

              view.updateFields(tmodel);

              var user521 = tmodel.get('createdBy');

              var userNames = [];
              userNames.push({ createdBy: user521 });

              getPersonsFullName(userNames, view.namesCache, 0, function () {
                  var urlPart = userNames[0].createdByUrlPart;
                  var fName = userNames[0].createdByFullName;
                  if (urlPart != null) {
                      //view.$('#createdBy').html('<a class="person-name" target="_blank" href="http://mysite.na.novartis.net/profile/' + urlPart + '" rel="' + user521 + '">' + fName + '</a>');
                      //call the person service to get the site code from 
                      //var personService = 'http://web.global.nibr.novartis.net/services/ps/v1/person/'+ user521;
                      //var siteCode = $.ajax({
                      //    url: 'http://web.global.nibr.novartis.net/services/ps/v1/person/'+ user521,
                      //    type: 'GET',
                      //    //contentType: "application/json",
                      //    dataType: "json",
                      //    data: {
                      //        siteCode: siteCode
                      //    },
                      //    success: function (data) {
                      //        console.log('NIBR personService call for '+user521 +' was successful.');
                      //    },
                      //    error: function (jqXHR, textStatus, errorThrown) {
                      //        console.log(errorThrown);
                      //    }
                      //});
                      //console.log('rundetails.js: siteCode =' + siteCode);
                      //var raw = siteCode.site_code;
                      //console.log('rundetails.js: siteCode.site_code =' + raw);
                      ////drive the domain based on user's site code
                      ////drive the domain based on user's site code
                      //var userDomain = getDomainFromSiteCode(siteCode);
                      view.$('#createdBy').html('<a class="person-name" target="_blank" href="http://mysite.nibr.novartis.net/Person.aspx?accountname=NANET\\' + user521 + '" rel="' + user521 + '">' + fName + '</a>');
                      //view.$('#createdBy').html('<a class="person-name" target="_blank" href="http://mysite.nibr.novartis.net/Person.aspx?accountname='+ userDomain + '\\' + user521 + '" rel="' + user521 + '">' + fName + '</a>');
                  } else {
                      view.$('#createdBy').html('<a class="person-name" target="_blank" rel="' + user521 + '">' + user521 + '</a>');
                  }
                  $(".person-name").sigPreviewCard({ entity: 'person' });
              });

              utils.viewError(view);

              view.$('#directories').dataTable({
                "bSort": false,
                "bJQueryUI": false,
                "bInfo": false,
                "bFilter": false,
                "sScrollY": "207px",
                "bScrollCollapse": true,
                "bPaginate": false,
                "aoColumns": [
                    { mDataProp: 'path', sWidth: "100%"}
                ],
                "oLanguage": {
                    "sEmptyTable": "No image directories selected. An image list was uploaded."
                },
                "aaData": view.model.get("dirs")
              });

              view.$('#wavemap').dataTable({
                "bSort": false,
                "bJQueryUI": false,
                "bInfo": false,
                "bFilter": false,
                "bPaginate": false,
                //"sScrollY": "200px",
                "bScrollCollapse": false,
                "bPaginate": false,
                "aoColumns": [
                    { mDataProp: 'biologicalConcept', sWidth: "50%"},
                    { mDataProp: 'channel', sWidth: "50%"}
                ],
                "aaData": view.model.get("wavelengthMap")
              });

              view.platesTable = view.$('#plates').dataTable({
                  "bJQueryUI": true,
                  "sDom": 'tpl',
                  "bInfo": false,
                  "bFilter": true,
                  "aaSorting":[[1,'asc']],
                  "bPaginate": false,
                  "aoColumns": [
                      { mDataProp: 'progress', bSearchable:false, bSortable: true , sType:'numeric'},
                      { mDataProp: 'barcode', bSortable: true },
                      { mDataProp: 'detail',bSearchable:false, bSortable: true, mRender: function (v, type, oData) {
                              if (v == null) return v;
                              var text = v;
                              if (v.length > 100) {
                                  text = v.substr(0, 100) + ' ...';
                              }
                              if (type == "display")
                                  return '<span class="link js-popup" title="Click to show full content" full-text="' + v + '">' + text + '</span>';
                              else return v;
                          }
                      },
                      { mDataProp: 'status',bSearchable:false, bSortable: true },
                      { mDataProp: 'lastUpdatedStr', bSortable: true , iDataSort:5},
                      { mDataProp: 'lastUpdated', bVisible: false, bSearchable: false }
                  ],
                  "aaData": view.model.get("plates"),
                  "fnRowCallback": function (nRow, aaData, iDisplayIndex) {
                      var tdClass = '';
                      switch (aaData['statusCode']) {
                          case 1:
                              tdClass = 'run-status-validating';
                              break;
                          case 2:
                              tdClass = 'run-status-submit';
                              break;
                          case 3:
                              tdClass = 'run-status-error';
                              break;
                          case 4:
                              tdClass = 'run-status-completed';
                              break;
                          case 5:
                              tdClass = 'run-status-warning';
                              break;
                          case 6:
                              tdClass = 'run-status-timeout';
                              break;
                      }
                      $('td:eq(0)', nRow).addClass(tdClass);
                      return nRow;
                  },
              });


              //view.$('#tblFilter').placehold();

              view._modelBinder = new Backbone.ModelBinder();
              view._modelBinder.bind(view.model, view.$('.rundetails'));

              // download links                 
              var downloadProtocolUrl = modelUtils.createBaseUrl("api/run/downloadprotocol") + "/" + view.options.id;
              var downloadResultsUrlInOneFile = modelUtils.createBaseUrl("api/run/downloadresults") + "?id=" + view.options.id + "&type=inOneFile";
              var downloadResultsUrlIcp4 = modelUtils.createBaseUrl("api/run/downloadresults") + "?id=" + view.options.id + "&type=icp4";
              var downloadImgListUrl = modelUtils.createBaseUrl("api/run/downloadimglist") + "/" + view.options.id;
              view.$('#btnDownloadProt').attr('href', downloadProtocolUrl);
              view.$('#btnDownloadResInOneFile').attr('href', downloadResultsUrlInOneFile);
              view.$('#btnDownloadResIcp4').attr('href', downloadResultsUrlIcp4);
              view.$('#btnDownloadImgList').attr('href', downloadImgListUrl);

              this.delegateEvents({
                'click  .js-download-link': 'downloadFile',
                'keyup  #tblFilter': 'doFilter',
                'click  #stopRunBtn': 'stopRun',
                'click  #copyRunBtn': 'showCopyRunDlg',
                'click  .js-popup': 'showErrorDetailsPopup'
              });

              var isRunning = view.model.get('isRunning');
              if (isRunning) {
                  view.startAutoUpdate();
              }

              return this;
          },
          downloadFile: function (ev) {
              utils.downloadFile(ev.currentTarget);
              return false; //this is critical to stop the click event which will trigger a normal file download!
          },
          showErrorDetailsPopup: function (ev) {
              var str = $(ev.target).attr('full-text');

              require(['views/detailspopup'],
                  function (popupView) {
                      view.popupView = new popupView({text: str});
                  },
                  function (error) {
                  });
          },
          doFilter: function (ev) {
              var view = this;
              var oTable = view.platesTable;
              oTable.fnFilter(ev.target.value, null, true);
          },

          updateFields: function (model) {
              var view = this;

              if (model.get("imageListOnly")) {
                  view.$(".hide-for-image-list-only").hide();
              }

              if (model.get("postProcessingEnabled")) {
                  view.$("#postProcessingSection").show();
              }

              var isRunning = model.get('isRunning');
              if (isRunning == true) {
                  view.$('#stopRunBtn').removeAttr("disabled", "disabled");;
              } else {
                  view.$('#stopRunBtn').attr("disabled", "disabled");;
              }
              var hasImageList = model.get('hasImageList');
              if (hasImageList == true) {
                  view.$('#btnDownloadImgList').show();
              } else {
                  view.$('#btnDownloadImgList').hide();
              }
              view.$('#btnDownloadResInOneFile,#btnDownloadResIcp4').hide();
              var hasResults = model.get('hasResults');
              if (hasResults == true) {
                  var inOneFile = model.get('inOneFile');
                  var icp4 = model.get('icp4');
                  if (inOneFile) {
                      view.$('#btnDownloadResInOneFile').show();
                  }
                  if (icp4) {
                      view.$('#btnDownloadResIcp4').show();
                  }
              }
              var outputFolder = model.get('outputFolderUNC');
              if (outputFolder && outputFolder.length > 0) {
                  view.$('#btnOutputDir').attr('href', outputFolder);
                  view.$('#btnOutputDir').show();
              } else {
                  view.$('#btnOutputDir').hide();
              }
              if (!model.get('protocolName')) {
                  view.$('#btnDownloadProt').hide();
              } else {
                  view.$('#btnDownloadProt').show();
              }
              var note = model.get('note');
              if (!note) {
                  view.$('#noteDiv').hide();
              } else {
                  view.$('#noteDiv').show();
                  var text = note;
                  if (note.length > 250) {
                      text = note.substr(0, 250) + ' ...';
                  }
                  view.$('#noteField').html(text).attr('full-text', note);
              }

              //view.$('#status').progressbar({ value: model.get('progress') });
              var progressValue = model.get('progress');
              if (!progressValue) {
                  progressValue = 0;
              }
              view.$('#status').css('width', progressValue + '%').attr('aria-valuenow', progressValue).text(progressValue + '%');
          },

          updateRun: function () {
              var view = this;
              
              var tmodel = new model();
              tmodel.set('id', view.options.id);
              var response = tmodel.fetch({
                  async: false
              });

              if (utils.wasFetchError(view, response)) {
                  return;
              }

              view.model.set("lastUpdatedStr", tmodel.get('lastUpdatedStr'));
              view.model.set("completePlatesCount", tmodel.get('completePlatesCount'));
              view.model.set("totalPlatesCount", tmodel.get('totalPlatesCount'));

              view.updateFields(tmodel);

              view.platesTable.fnClearTable();
              var plates = tmodel.get("plates");
              if (plates.length > 0) {
                  view.platesTable.fnAddData(plates);
              }
              view.platesTable.fnDraw();

              var isRunning = tmodel.get("isRunning");
              if (!isRunning) {
                  view.stopAutoUpdate();
              }
          },

          adjustFieldsWidth: function () {
              var view = this;
              var w = view.$('.col-md-9').width();
              view.$('.fixWidthField').width(w / 2 - 20);
              view.$('#wavemap').width(w);
          },

          stopRun: function () {
              var view = this;
              var qs = $('<div title="Stop Run"><div class="quest" >Are you sure you want to stop the run? This action cannot be reversed.</div><input type="hidden" class="needReload" value=""></div>');
              dtUtils.yesNoDialog(qs, function () {
                  var dmodel = new model();
                  dmodel.set("id", view.model.get("id"));
                  dmodel.destroy();
              }, null);
          },

          showCopyRunDlg: function () {
              var view = this;
              view.stopAutoUpdate();
              require(['views/createrun'],
                function (createRunView) {
                    view.createRunView = new createRunView({ parentView: view, isCopyRun: true, copyingRunId: view.options.id });
                },
                function (error) {
                });
          },
          startAutoUpdate: function () {
              var view = this;
              view.intervalId = setInterval(function () {
                  if (ifvisible.now()) {
                      $('[data-toggle="tooltip"]').tooltip("destroy");
                      $(".tooltip").remove();
                      view.updateRun();
                  }
              }, view.options.updateInterval);
          },
          stopAutoUpdate: function () {
              var view = this;
              if (view.intervalId) {
                  clearInterval(view.intervalId);
                  view.intervalId = null;
              }
          },
          onclose: function () {
              var view = this;
              view.stopAutoUpdate();
              $(window).unbind("resize.app");
              if (this._modelBinder)
                  this._modelBinder.unbind();
              Backbone.Validation.unbind(this);
          }

      });
      return view;
  });
