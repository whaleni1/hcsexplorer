﻿define([
  'text!templates/dirbrowse.htm',
  'utils/view-utils', 'utils/model-utils',
  'bootbox', 'backbone', 'mbinder', 'dynatree', 'signalR', 'signalRHubs'
], 
  function (template, utils, modelUtils, bootbox) {

      //var errLoadMsg = 'Loading this directory is taking a long time.It may be a plate directory with many thousands of image files.';
      var view = Backbone.View.extend({

          Initialize: function (options) {
              this.compiledTemplate = _.template(template);
              this.options = options;
              this.render();
          },
          render: function () {
              var view = this;

              view.$el.html(view.compiledTemplate);
              view.showModal('Browse Image Directories', view.$el);
              //$('#fileTreeDlg').empty().html(view.$el);

              //utils.createDialog(view, '#fileTreeDlg', 'Browse Image Directories', 600);
              
              view.initSignalR();
              this.delegateEvents({
                  //'click #selectBtn': 'selectDirectories',
                  'click #scanBtn': 'scanDirectories',
                  'keypress #currentPath': 'checkEnter'
              });

              return this;
          },
          showModal: function (title, html) {
                var view = this;
              view.dialog = bootbox.dialog({
                  title: title,
                  message: html,
                  show: false,
                  buttons: {
                      select: {
                          label: "Select",
                          className: "btn-primary",
                          callback: function () {
                              view.selectDirectories();
                              return false;
                          }
                      }
                  }
              }).addClass("modal-image-browser");
              
              view.dialog.on('hide.bs.modal', function () {
                  view.$('#fileTreeDiv').dynatree('destroy');
                  view.$('#fileTreeDiv').empty().html('<ul></ul>');
                  view.close()
              });
              view.dialog.modal('show');
              view.dialog.next()[0].style.zIndex = 1051;
          },
          checkEnter: function (e) {
              var view = this;
              if (e.keyCode != 13) {
                  return;
              }
              e.preventDefault();
              view.scanDirectories();
          },

          setNodeNotAllChilds: function (node, errmsg) {
              var view = this;
              var showErr = false;
              var statusNode = false;
              if (node.childList != null && node.childList.length === 1 && node.childList[0].isStatusNode())
                  statusNode = true;
              if (statusNode) {
                  showErr = true;
                  node._isLoading = false;
                  node._setStatusNode({
                      title: '<span class="tree-load-error">' + errmsg + '</span>',
                      addClass: view.tree.options.classNames.nodeError
                  });
                  $(node.ul).find('.dynatree-icon').hide();
              } else {
                  var t = node.data.title + '<span class="tree-load-error">' + errmsg + '</span>';
                  node.setTitle(t);
              }
              return showErr;
          },

          getNameFromTitle: function(title){
              var k = title.indexOf('<span');
              if (k >= 0) {
                  return title.substring(0, k);
              }
              return title;
          },
          initSignalR: function () {
              var view = this;
              view.filesLoader = $.connection.filesLoader;

              // Add client-side hub methods that the server will call
              $.extend(view.filesLoader.client, {
                  fileProgress: function (result) {
                      var options = result.Options;
                      var path = options.RootPath;
                      var node = null;
                      var files = result.Files;
                      var tree = view.tree;
                      if (tree == null)
                          return;

                      if (options.IsRoot)
                          node = tree.getRoot();
                      else
                          node = tree.getNodeByKey("0" + path);


                      if (node != null) {
                          /* var hchld = true;
                           if (node.childList === null || node.childList === undefined) {
                               // Not yet loaded
                               hchld = false;
                           } else if (node.childList.length === 0) {
                               // Loaded, but response was empty
                               hchld = false;
                           } else if (node.childList.length === 1 && node.childList[0].isStatusNode()) {
                               // Currently loading or load error
                               hchld = false;
                               
                           }*/
 
                          var showErr= false;

                              if (result.NotAllData) {
                                      showErr = view.setNodeNotAllChilds(node, result.NotAllDataMsg);
                              } else if (result.Message != null) {
                                      showErr= view.setNodeNotAllChilds(node, result.Message);
                              }
                         
                              if (files.length > 0)
                              {
                                  node.removeChildren();
                                  var tdata = [];
                                  for (var i = 0; i < files.length; i++) {
                                      var f = files[i];
                                      var ndata = {
                                          title: f.Name,
                                          key: (f.IsFolder ? "0" : "1") + f.Path + "\\" + f.Name,
                                          isFolder: f.IsFolder,
                                          isLazy: f.IsFolder,
                                          hideCheckbox: !f.IsFolder,
                                          select: f.IsFolder && node.bSelected,
                                          unselectable: !f.IsFolder
                                      };
                                      tdata.push(ndata)
                                  }
                                  node.addChild(tdata);

                              }
                              if (!showErr)
                                  _.delay(function () { node.setLazyNodeStatus(DTNodeStatus_Ok); }, 1);
                      } 

                  },
                  error: function (errormessage) {

                  }
              });
              $.connection.filesLoader.logging = true;
              //$.connection.hub.start().pipe(function () {
              $.connection.hub.start({ transport: ['webSockets', 'longPolling'] }).pipe(function () {
                  view.loadTree();
              });
              $.connection.hub.stateChanged(function (change) {
                 
              });

          },

          loadTree: function () {
              var view = this;
              var mfilter = null;
              var mroot = view.options.data.pathId;
              if (mroot.lastIndexOf('\\') == mroot.length - 1)
                  mroot = mroot.substr(0, mroot.length - 1);
              var path = $.trim(view.$('#currentPath').val());

              var err = false;
              if (path != ''){
                  
                  if (path.indexOf('\\\\') == 0) {
                      err = true;
                  } else if (path.indexOf('\\') != 0) {
                      view.$('#currentPath').val('\\' + path);
                      path = $.trim(view.$('#currentPath').val());

                  }
              }


              var model = {
                  siteId: view.options.data.siteId,
                  databaseId: view.options.data.databaseId,
                  pathId: mroot + path,
                  rootPath: mroot + path,
                  isRoot: view.options.data.isRoot ? true : false
                  //showFiles: $("input[name=rbshow]:checked").val()
              }; 

              var view = this;
              utils.disableView(view);

              view.$('#fileTreeDiv').dynatree({
                  checkbox: true,
                  selectMode: 3,
                  clickFolderMode: 3,
                  //persist: true,
                  //noLink:true,
                  children: [],
                  onLazyRead: function (node) {
                      //node.tree.disable();

                      var cpath = node.data.key.substring(1);
                      var cmodel = {
                          siteId: view.options.data.siteId,
                          databaseId: view.options.data.databaseId,
                          pathId: view.options.data.pathId,
                          rootPath: cpath
                          //showFiles: $("input[name=rbshow]:checked").val()
                      };
                      node.setLazyNodeStatus(DTNodeStatus_Loading);
                      view.filesLoader.server.filesLoad(cmodel)
                          .fail(function (err) {
                              alert(err);
                              //node.tree.enable();
                          });
                  }
              });
              view.tree = view.$('#fileTreeDiv').dynatree("getTree");
              
              if (!err) {
                  view.tree.getRoot().setLazyNodeStatus(DTNodeStatus_Loading);
                  view.filesLoader.server.filesLoad(model)
                                .done(function () {
                                    utils.enableView(view);
                                    view.inRequest = false;

                                }).fail(function (err) {
                                    alert(err);
                                    utils.enableView(view);
                                });
              } else {


                  node = view.tree.getRoot();
                  var errmsg = "Invalid path";
                  node._setStatusNode({
                              title: '<span class="tree-load-error">' + errmsg + '</span>',
                              addClass: view.tree.options.classNames.nodeError
                  });
                  $(node.ul).find('.dynatree-icon').hide();
                  utils.enableView(view);
              }
 
          },

          getChecked: function () {
              var view = this;
              var dynatr = view.$('#fileTreeDiv').dynatree('getTree');
              var selNodes = dynatr.getSelectedNodes(true);
              return selNodes;
          },
          scanDirectories: function () {
              var view = this;
              var path = $.trim(view.$('#currentPath').val());
              if (path.lastIndexOf('\\') == path.length - 1) {
                  path = path.substr(0, path.length - 1);
                  view.$('#currentPath').val(path);
              }
              if (path.indexOf(view.options.data.pathId) == 0) {
                  path = path.substr(view.options.data.pathId.length);
                  view.$('#currentPath').val(path);
              } 
              view.$('#fileTreeDiv').dynatree('destroy');
              view.$('#fileTreeDiv').empty().html('<ul></ul>');
              this.loadTree();
          },
          selectDirectories: function() {
              var view = this;
              //var selectedChbxs = $('input[type=checkbox]:checked', $('#fileTreeDiv'));
           
              var selectedChbxs = view.getChecked();
               if (selectedChbxs.length == 0) {
                  utils.showErrorPopup({ message: "Please select at least one folder to continue" }, null, "Warning");
              } else {
                  var res = [];
                  for (var i = 0; i < selectedChbxs.length; i++) {

                      var node = selectedChbxs[i];
                      if (node.data.key.indexOf("0")==0)
                        res.push({ path: node.data.key.substring(1), name: view.getNameFromTitle( node.data.title ), id: i });
                  }
                  //$('#fileTreeDlg').dialog('close');
                  view.dialog.modal("hide"); // close bootbox dialog
                  view.options.parentView.addDirectories(res);
              }
          },

          onclose: function () {
              var view = this;
              try{
                  $.connection.hub.stop();
              } catch (e) { }

          }

      });
      return view;
  });
