﻿define([
  'text!templates/platetab.htm',
  'models/platetab',
  'utils/view-utils', 'utils/model-utils', 'backbone', 'mbinder','signalR', 'signalRHubs'
],
  function (ptemplate, model, utils, modelUtils) {
      //brightness variables
      var autoContrast = '';
      var bPoint = '';
      var wPoint = '';
      var levelsVal = '';
      var brightnessOpt = '';

      var rowId = utils.getRowIdArray();
      var strOverlay = 'Overlay';
      var view = Backbone.View.extend({

          Initialize: function (options) {
              this.compiledTemplate = _.template(ptemplate);
              this.options = options;
              this.testingParams = options.testingParams;
              this.loadingImgSelector = '.loadingImgPlate';
              $(window).bind("resize.app", _.bind(this.agjustGridHeight, this));
              this.render();
          },
          render: function () {

              var view = this;
              view.$el = this.options.el;
              view.$el.html(view.compiledTemplate);

              this.model = new model();
              var fields = view.options.fields;
              if (fields != null && fields.length > 0) {
                  utils.createSelectSimple(view.$('#field'), fields, false);
                  view.model.set('field', fields[0]);
              }
              var channels = view.options.channels;
              if (channels != null && channels.length > 0) {
                  utils.createSelectSimple(view.$('#channel'), channels, false);
                  if (channels.length > 1)
                      view.$('#channel').append('<option value="' + strOverlay + '">' + strOverlay + '</option>');

                  view.model.set('channel', channels[0]);
              }

              var versions = view.options.versions;
              if (versions != null && versions.length > 0) {
                  utils.createSelectSimple(view.$('#pversion'), versions, false);
                  if (view.$('#pversion option[value=raw]').length > 0) {
                      view.$('#pversion').val("raw");
                      view.model.set('version', "raw");

                  } else {
                      view.model.set('version', versions[0]);
                  }
              } else {
                  view.$('.js-version').hide();
              }
              //brightness controls
              $('.p-options').bind('change', function () {
                  var selected = this.id;
                  var option = $('#' + selected + " option:selected").val();
                  if (option == 'brightness') {
                      $('#bDiv').empty();
                      var optionHTML = '<input type="text" name="brightness" id="brightness" class="form-control input-sm" style="width:40px;" />';
                      $('#bDiv').append(optionHTML);
                  }
                  if (option == 'auto') {
                      $('#bDiv').empty();
                      var optionHTML = '<div name="auto" id="auto" data-id="selected"></div>';
                      $('#bDiv').append(optionHTML);
                  }
                  //if (option == 'level') {
                  //    $('#bDiv').empty();
                  //    var optionHTML = '<p>White <input type="text" name="whitepoint" id="whitepoint" class="form-control input-sm" style="width:40px;" />' +
                  //        ' Black <input type="text" name="blackpoint" id="blackpoint" class="form-control input-sm" style="width:40px;" /></p>';
                  //    $('#bDiv').append(optionHTML);
                  //}
              })
              $('.p-options').trigger('change'); //load event gets triggered on change
              $('#bDiv').empty(); // don't show the textbox underneath the Brightness dropdown on page load
              var tcount = view.options.timepointCount;
              if (tcount != null && tcount > 0) {
                  utils.createSelectSimpleInt(view.$('#ptimepoint'), tcount, false);
                  view.model.set('timepoint', 1);
              } else {
                  view.$('.js-timepoint').hide();

              }

              var zcount = view.options.zIndexCount;
              if (zcount != null && zcount > 0) {
                  utils.createSelectSimpleInt(view.$('#pzIndex'), zcount, false);
                  view.model.set('zIndex', 1);
              } else {
                  view.$('.js-zindex').hide();
              }

              view.model.set('color', view.$('#color option:first').attr('value'));
              view.model.set('sizeId', view.$('#sizeId option:first').attr('value'));
              var view = this;
              view.rows = utils.getRowCount(view.options.format);
              view.cols = utils.getColCount(view.options.format);
              view.generatePlateView();

              utils.viewError(view);

              view._modelBinder = new Backbone.ModelBinder();
              view._modelBinder.bind(view.model, view.$('.js-form-fields'));
              view.inRequest = false;
              this.delegateEvents({
                  'click .js-submit': 'loadPlateImages',
                  'click .js-export-pdf': 'downloadPdf',
                  'change #channel': 'checkChannel'
              });

              if (IS_DEBUG_MODE) {
                  view.$('#testMetricSpan').show();
              }

              return this;
          },

          checkChannel: function (e) {
              var view = this;
              view.checkChannelWithPopup(e, true);
          },
          checkChannelWithPopup: function (e , showpopup) {
              var view = this;
              var selectedChannel = view.$('#channel option:selected').attr('value');

              if (selectedChannel == "Overlay") {
                  // disable brightness textbox
                  //view.$("#blackpoint").attr('disabled', 'disabled');
                  // disable brightness textbox
                  //view.$("#whitepoint").attr('disabled', 'disabled');
                  view.$("#brightnessOptions").attr('disabled', 'disabled');
                  // disable brightness textbox
                  view.$("#auto").attr('disabled', 'disabled');
                  // disable brightness textbox
                  view.$("#brightness").attr('disabled', 'disabled');
                  // disable color selection drop down menu
                  view.$("#color").attr('disabled', 'disabled');
                  // disable check box to invert
                  view.$("#invert").attr('disabled', 'disabled');
                  if (showpopup) utils.showErrorPopup(
                  { message: "The overlay images will be generated based on the channel color and brightness settings in the 'Well View' tab.  Please make sure they are configured properly before clicking on 'Submit'." },
                  null,
                  "Information"
                  );
              } else {
                  // enable whitepoint textbox
                  //view.$("#blackpoint").removeAttr('disabled');
                  // enable whitepoint textbox
                  //view.$("#whitepoint").removeAttr('disabled');
                  view.$("#brightnessOptions").removeAttr('disabled');
                  // enable auto checkbox
                  view.$("#auto").removeAttr('disabled');
                  // enable brightness textbox
                  view.$("#brightness").removeAttr('disabled');
                  // disable color selection drop down menu
                  view.$("#color").removeAttr('disabled');
                  // disable check box to invert
                  view.$("#invert").removeAttr('disabled');
              }
          },
          updateExportPdfButton: function () {
              var hdnPdfPath = $('#pdfPath').val();
              $('.js-export-pdf').prop("disabled", !(hdnPdfPath != null && hdnPdfPath.length > 0));
          },
         
          downloadPdf: function () {
              var action = modelUtils.createBaseUrl("api/plate/downloadpdf");
              $('#pdfForm')[0].setAttribute('action', action);
              $('#pdfForm').submit();
          },

          formatTimeSpan: function (ms) {
              var obj = {};
              obj._milliseconds = ms;
              obj.milliseconds = obj._milliseconds % 1000;
              obj._seconds = (obj._milliseconds - obj.milliseconds) / 1000;
              obj.seconds = obj._seconds % 60;
              obj._minutes = (obj._seconds - obj.seconds) / 60;
              obj.minutes = obj._minutes % 60;
              obj._hours = (obj._minutes - obj.minutes) / 60;
              obj.hours = obj._hours % 24;

              return obj.hours + ':' + obj.minutes + ':' + obj.seconds + ':' + obj.milliseconds + 'ms';
          },

          progress: function (matrix) {
              var view = this;
              if (view.inRequest) {
                  if (matrix.Progress!=null)
                    view.$('#imgPlateNotification').text(matrix.Progress);
                  if (matrix.PdfPath != null) {
                      var endTime = new Date().getTime();
                      var duration = endTime - view.startLoadingTime;
                      view.$('#loadingTime').html('Loading time: ' + view.formatTimeSpan(duration));
                      view.$('#loadingTimeTicks').html(duration);
                      view.$('#disconnectsCount').html(view.disconnectsCount);
                      view.$('#hcsConnectDisconnectsCount').html(view.$('img.trumb.disconnect').length);
                      // plate loading completed
                      view.enableView();
                      view.hideProgress();
                      view.inRequest = false;
                      if (matrix.PdfPath != "Error") {
                          $('#pdfPath').val(matrix.PdfPath);
                          $('.js-export-pdf').prop("disabled", false);
                      }
                      return;
                  }
                  if (matrix.Matrix != null) {
                      view.$('#imgPlateNotification').text(matrix.Matrix.Progress);
                      view.updateImageMatrix(matrix.Matrix.ImagePathes, matrix.Matrix.GetImageUrls);
                  } else if (matrix.Index != null) {
                      view.updateImageInMatrix(matrix);
                  }
              }
          },

          updateImageMatrix:  function( matrix, GetImageUrls ){
                var view = this;
                var tds = view.$('#imagePlateLayout .pwithimg');
                if (tds.length != matrix.length) {
                    alert(tds.length + "!=" + length);
                }
                for (var i = 0; i < matrix.length; i++) {
                    if (matrix[i] != null && $(tds[i]).find('img').length == 0) {
                        $(tds[i]).append('<img class="trumb" src="' + matrix[i] + '" />');
                        if (GetImageUrls[i] != null) {
                            $(tds[i]).append('<input type="hidden" class="getImgUrl" value="' + GetImageUrls[i] + '" />');
                        }
                    }
                }
          },

          updateImageInMatrix: function (paramObj) {
              var view = this;
              var tds = view.$('#imagePlateLayout .pwithimg');
              var serviceDisconnect = (paramObj.Image.IsWebClientDisconnect == true);
              var i = paramObj.Index;
              if (paramObj.Image != null) {
                  if (paramObj.Image.Path != null && $(tds[i]).find('img').length == 0) {
                      var imgClass = 'trumb';
                      if (serviceDisconnect) {
                          imgClass += ' disconnect';
                      }
                      $(tds[i]).append('<img class="' + imgClass + '" src="' + paramObj.Image.Path + '" />');
                      if (paramObj.Image.GetImgUrl != null) {
                          $(tds[i]).append('<input type="hidden" class="getImgUrl" value="' + paramObj.Image.GetImgUrl + '" />');
                      }
                  }
              }
          },
          /*adjustBodyWidth: function () {
              var view = this;
              var padW = 58;
              var bodyW = $('body').width();
              var tableW = view.$('#imagePlateLayout').width();
              if (bodyW < (tableW + padW)) {
                  $('body').css({'min-width' : (tableW + padW) + 'px'});
              }
          },
          clearBodyWidth: function () {
              $('body').removeAttr('style');
          },*/
          agjustGridHeight: function () {
              var view = this;
              var gridH = view.$('#imagePlateLayout').height();
              var windowH = $(window).height();
              var shift = 320;
              if (gridH > (windowH - shift)) {
                  view.$('#imagePlatesDiv').height(windowH - shift);
              }
          },

          loadPlateImages: function(isReconnect) {

              var view = this;
              if (view.model.isValid(true)) {
                  var chs = view.options.channels;

                  var fields = view.options.fields;
                  if (isReconnect != true) {
                      view.$('#imagePlateLayout img').remove();
                      view.$('#imagePlateLayout .getImgUrl').remove();
                      view.$('#imagePlateLayout').show();
                      var imgsize = view.$('#sizeId option:selected').attr('value');
                      view.$('#imagePlateLayout').removeClass('S').removeClass('M').addClass(imgsize);
                      //view.adjustBodyWidth();
                      view.agjustGridHeight();
                      view.startLoadingTime = new Date().getTime();
                      view.disconnectsCount = 0;
                  } else {
                      view.disconnectsCount++;
                  }
                  view.testingParams.isReconnect = (isReconnect == true);
                  //get brightness values
                 
                  function parseBrightnessOptions() {
                      //get the value from the dropdown
                      var option = '';
                      //var autoVal = $('#auto').val();
                      var autoVal = $('#auto').data("id");
                      //var blackPointVal = $('#blackpoint').val();
                      //var whitePointVal = $('#whitepoint').val();
                      var brightness = $('#brightness').val();
                      if (autoVal == 'selected') {
                          autoContrast = 'true';
                          brightnessOpt = '';
                          //levelsVal = 'false';
                          return;
                      }
                      //if (blackPointVal != 'undefined' && whitePointVal != 'undefined') {
                      //    bPoint = blackPointVal;
                      //    wPoint = whitePointVal;
                      //    levels = 'true';
                      //    return;
                      //}
                      if (brightness != 'undefined') {
                          brightnessOpt = brightness;
                          autoContrast = '';
                          return;
                      }
                  }
                  parseBrightnessOptions();
                  var model = {
                      siteId: view.options.plateOptions.siteId,
                      databaseId: view.options.plateOptions.databaseId,
                      id: view.options.plateOptions.id,
                      sizeId: view.$('#sizeId option:selected').attr('value'),
                      field: view.$('#field option:selected').attr('value'),
                    
                      plateChannelParameters: {
                          //TODO: look into levels value
                          //levels: function getLevels() { },
                          //whitepoint:  view.model.get('whitepoint'),
                          //blackpoint: view.model.get('blackpoint'),
                          auto: autoContrast,//view.model.get('auto'),
                          brightness: brightnessOpt,//view.model.get('brightness'),
                          color: view.model.get('color'),
                          channel: view.model.get('channel')
                      },
                      testingParams: view.testingParams,
                      channels: chs,
                      fields: fields,
                      rows: view.rows,
                      cols: view.cols,
                      format: view.options.format,
                      invert: (view.$('#invert:checked').length > 0),
                      channelParameters: [], //for overlay

                      barcode: view.options.barcode,
                      path: view.options.path,
                      name: view.options.name,
                      date: view.options.date,

                      version: view.model.get('version'),
                      timepoint: view.model.get('timepoint'),
                      zIndex: view.model.get('zIndex')
                  };

                  if (model.plateChannelParameters.channel == strOverlay) {
                      view.options.channelParametersFunction(model);
                      var enableOverlay = view.options.enableOverlayFunction();
                      //check enableOverlay
                      if (!enableOverlay) {
                          utils.showErrorPopup({message:'Overlay is not enabled'});
                          return;
                      }
                  }

                  // Start the connection

                  utils.disableView(view);
                  view.inRequest = true;
                  view.imagesLoader.server.plateImagesLoad(model)
                   .done(function (matrix) {
                      /* view.updateImageMatrix(matrix.ImagePathes, matrix.GetImageUrls);
                       view.enableView();
                       view.hideProgress();
                       view.inRequest = false;
                       $('#pdfPath').val(matrix.PdfPath);
                       $('.btn_pdf').prop("disabled", false);*/
                   }).fail(function (err) {
                       alert(err);
                       view.enableView();
                   });
              }

          },

          enableView: function () {
              var view = this;
              utils.enableView(view);
              var selectedChannel = view.$('#channel option:selected').attr('value');
              if (selectedChannel == "Overlay") {
                  // disable auto textbox
                  view.$("#auto").attr('disabled', 'disabled');
                  // disable brightness options dropdown
                  view.$("#brightnessOptions").attr('disabled', 'disabled');
                  // disable brightness textbox
                  view.$("#brightness").attr('disabled', 'disabled');
                  // disable color selection drop down menu
                  view.$("#color").attr('disabled', 'disabled');
                  // disable check box to invert
                  view.$("#invert").attr('disabled', 'disabled');
                  // disable whitepoint textbox
                  //view.$("#whitepoint").attr('disabled', 'disabled');
                  // disable blackpoint textbox
                  //view.$("#blackpoint").attr('disabled', 'disabled');
                  // disable brightness textbox
              };
          },

          hideProgress: function () {
              this.$('#imgPlateNotification').text('');
          },
          //plate view
          generatePlateView: function () {
              var view = this;
              var numrows = view.rows + 1; // Add extra row for column number
              var numcols = view.cols + 1; // Add extra column for row number

              var tbl = view.$('#imagePlateLayout');
              tbl.find('img').remove();
              var imgsize = view.$('#sizeId option:selected').attr('value');
              tbl.removeClass('S').removeClass('M').addClass(imgsize);

              for (var r = 0; r < numrows; r++) {
                  var tr = $('<tr></tr>');
                  for (var c = 0; c < numcols; c++) {
                      var tc = $('<td></td>');
                      tc.attr('id', r + "-" + c);
                      if (r == 0 && c == 0) {
                          tc.html("<div>&nbsp</div>");
                          tr.append(tc);
                          continue;
                      }
                      if (r == 0 && c != 0) {
                          tc.html("<div class='divwidth'>" + c + "</div>");
                          tc.attr('align', 'center');
                          tr.append(tc);
                          continue;
                      }
                      if (c == 0 && r != 0) {
                          tc.html( rowId[r - 1]);
                          tr.append(tc);
                          continue;
                      }
                      tc.addClass('pwithimg');
                      tc.attr('title', rowId[r - 1] + c);
                      tc.attr('data-toggle', 'tooltip');
                      tr.append(tc);
                  }
                  tbl.append(tr);
              }
          },

          onclose: function () {
              $('#tempTestDiv').hide();
              //this.clearBodyWidth();
              $(window).unbind("resize.app");
              this._modelBinder.unbind();
              Backbone.Validation.unbind(this);
          }
      });
      return view;
  });
