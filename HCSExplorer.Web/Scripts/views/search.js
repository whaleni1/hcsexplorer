﻿define([
  'text!templates/search.htm',
  'models/search',
  'models/databases',
  'utils/view-utils', 'utils/datatable-utils', 'utils/date-utils',
   'backbone', 'mbinder', 'datatables', 'datatablesbootstrap', 'datatablesscroller'
],
  function (searchTemplate, model, dbModel, utils, dtUtils, dateUtils) {
    var view = Backbone.View.extend({

      Initialize: function (param) {
          this.compiledTemplate = _.template(searchTemplate);
          this.options = param.options;
          this.layout = param.layout;
          this.render();
      },
      render: function () {
       
        var view = this;
        var tmodel = new model();
        var response = tmodel.fetch({ async: false });
        if (utils.wasFetchError(view, response))
            return;

        view.$el.html(view.compiledTemplate);
        $('.mainArticle').empty().html(view.$el);

        utils.setSelectFunctionality(tmodel, view.$('#siteId'), 'sites', true, "Select...");

        this.model = new model();
        utils.viewError(view);

        view._modelBinder = new Backbone.ModelBinder();
        view._modelBinder.bind(view.model, view.$('.js-searchFields'));

 

        this.delegateEvents({
            'change #siteId': 'siteSelected',
            'click .js-search': 'searchPlates',
            //'click  .viewplate': 'viewPlate',
            'keypress #barcode': 'checkEnter'
        });

        if (this.options != null) {
            view.model.set('siteId', view.options.siteId);
            view.siteSelected(null, true);
        } else {
            var localSite = tmodel.get('localSite');
            if (localSite) {
            view.$('#siteId').val(localSite);
                view.model.set('siteId', localSite);
                view.siteSelected(null, false);
            }
        }
        return this;
      },

      checkEnter: function (e) {
          var view = this;
          if (e.keyCode != 13) {
              return;
          }
          e.preventDefault();
          e.stopPropagation();
          view.model.set('barcode', view.$('#barcode').val());
          view.searchPlates();
      },


      makePlateUrl: function (id, siteId, databaseId) {
          var view = this;
          var siteId = view.lastSearch.get("siteId");
          var databaseId = view.lastSearch.get("databaseId");
          return "plate/" + id + "/" + siteId + "/" + databaseId;
      },
      /*viewPlate: function (e) {
          var view = this;
          var el = e.currentTarget;
          var view = this;
          var id = dtUtils.getIdByButton(el, view.table).id;
          Backbone.history.navigate(view.makePlateUrl(id), true);
      },*/
      clearDatabases : function (){
          var view = this;
          view.$('select#databaseId options').remove();
      },
      siteSelected: function (e, filltable) {
          var view = this;
          var siteId = view.$('#siteId option:selected').attr("value");
          if (siteId == '') {
              view.clearDatabases();
          }
          else {
             
              var dmodel = new dbModel();
              dmodel.set('id', siteId);
              utils.disableView(view);
              var response = dmodel.fetch({
                  success: function () {
                      utils.setSelectFunctionality(dmodel, view.$('#databaseId'), 'databases', true, "Select...");
                      var dataSources = dmodel.get('databases');
                      if (dataSources.length == 1) {
                          var dsId = dataSources[0].id;
                          view.model.set('databaseId', dsId);
                          view.$('#databaseId').val(dsId);
                      }
                      utils.enableView(view);

                      if (filltable) {
                          var bc = utils.decodeString(view.options.barcode);
                          var did = view.options.databaseId;
                          view.model.set('barcode', bc);
                          view.model.set('databaseId', did);
                          _.delay(function () {
                              if (view.layout.lastResult == null || siteId != view.layout.lastSiteId
                                  || did != view.layout.lastDatabaseId || bc != view.layout.lastBarcode) {
                                  view.searchPlates();
                              } else {
                                  view.lastSearch = view.model.clone();
                                  view.initTable(view.layout.lastResult);
                              }
                          }, 10);

                      }
                  },
                  error: function (model, response) {
                      utils.enableView(view);
                      utils.wasFetchError(view, response);
                  }
              });

          }

      },
      calculateTableHeight: function () {
          var h = $(document).height();
          var minScrollH = 250, otherUsedH = 380;
          var scrollH = minScrollH;
          if (h > (minScrollH + otherUsedH)) {
              scrollH = h - otherUsedH;
          }
          return scrollH;
      },
      initTable: function(plates){
          var view = this;
          var tableHeight = view.calculateTableHeight();
          if (plates == null) plates = view.model.get('plates');
          if (view.table == null) {
              var dt_config = {

                  bDeferRender: true,
                  sDom: "frtiS",
                  sScrollY: tableHeight,
                  scrollCollapse: true,
                  oScroller: {
                      trace: true,
                      displayBuffer:20
                  },
                  
                  bJQueryUI: true,
                  bAutoWidth: false,
                  bPaginate: true,
                  bFilter: true,
                  bProcessing: false,
                  bServerSide: false,
                  bRetrieve: true,
                  aaSorting: [[4, 'desc']],
                  aaData: plates,
                  oLanguage: {
                      sSearch: '',
                      sInfo: "_TOTAL_ entries to show"
                  },
                  aoColumns: [
                      {
                          mDataProp: 'id',
                          asSorting: ['asc', 'desc'],
                          bVisible: true,
                          bSearchable: true,
                          sType: 'numeric' ,
                          mRender: function (v, type, oData) {
                              if (type == 'display')
                                  return '<span class="viewplate"><a href="#' + view.makePlateUrl(oData.id) + '">' + v + '</a></span>';
                              else
                                  return v;
                          }
                      },
                      { mDataProp: 'barcode', asSorting: ['asc', 'desc'], bVisible: true, bSearchable: true },
                      { mDataProp: 'name', asSorting: ['asc', 'desc'], bVisible: true, bSearchable: true },
                      { mDataProp: 'format', asSorting: ['asc', 'desc'], bVisible: true, sType:'numeric', bSearchable: true },
                      {
                          mDataProp: 'date', sWidth: "150", iDataSort: 6, asSorting: ['asc', 'desc'], bVisible: true, bSearchable: true, mRender: function (v, type, oData) {
                              if (v != null) return dateUtils.toUserFormatDateTimeIgnoreZone(v);
                              return oData.dateSrc;
                          }
                      },
                      { mDataProp: 'path', asSorting: ['asc', 'desc'], bVisible: true, bSearchable: true },
                      { mDataProp: 'date', bVisible: false, bSearchable: false }
                  ]
              };
              view.$('#tblPlates').show();
              view.table = view.$('#tblPlates').dataTable(dt_config);
          } else {
              view.table.fnClearTable();
              if (plates != null && plates.length>0)
                view.table.fnAddData(plates);
              view.table.fnDraw();
          }

          _.delay(function () { view.table.fnAdjustColumnSizing(); }, 10);

          view.$('.dataTables_length').parent().hide();
          if (plates != null && plates.length > 0) {
              view.$('#spanRowCount').text(plates.length);
              view.$('.rowcount').show();
          } else {
              view.$('.hide').show();

              view.$('.norecords').show();
              view.$('#tblPlates').hide();
              view.$('.fg-toolbar').hide();
          }
      },
      searchPlates: function () {

          var beforeSaving = function(view) {
              view.model.set('sites', []);
              view.model.set('databases', []);
              view.model.set('plates', []);
              var barcode = $.trim(view.model.get('barcode'));
              view.model.set('barcode', barcode);
              utils.hideErrorBox(view, false);
          };
          var afterSaving = function (view) {
              view.layout.lastResult = view.model.get('plates');
              view.layout.lastSiteId = view.model.get('siteId');
              view.layout.lastDatabaseId = view.model.get('databaseId');
              view.layout.lastBarcode = view.model.get('barcode');
              view.layout.lastCompResult = null;
              view.layout.lastCompoundId = null;
              view.layout.lastHelios = null;
              utils.decodeModelField(view.model, 'barcode');
              var navstr = "search/" + view.model.get('siteId') + "/" + view.model.get('databaseId') + "/" + view.model.get('barcode');
              if (location.hash == "#" + navstr) {
                  view.lastSearch = view.model.clone();
                  view.initTable(view.layout.lastResult);
              }else
                  Backbone.history.navigate(navstr, true);
          };
          utils.save(this, beforeSaving, afterSaving);
      },

      onclose: function () {
          this._modelBinder.unbind();
          Backbone.Validation.unbind(this);
      }
          
    });
    return view;
  });
