﻿define([
  'text!templates/downloadimg.htm',
  'models/downloadimg', 'utils/view-utils', 'utils/model-utils', 'bootbox',
  'bootstrap', 'backbone', 'mbinder', 'jqueryui'
],
  function (template, model, utils, modelUtils, bootbox) {
      var view = Backbone.View.extend({

          Initialize: function (options) {
              this.compiledTemplate = _.template(template);
              this.options = options;
              this.render();
          },
          render: function () {
              var view = this;

              view.$el.html(view.compiledTemplate);
              //$('#downloadImgContent').empty().html(view.$el);
              
              //utils.createDialog(view, '#downloadImgContent', 'Options to Embed Texts in Image:', 'auto');
              view.showModal('Options to Embed Texts in Image:', view.$el);

              var dModel = new model();
              dModel.set("imgUrl", view.options.imgUrl);
              dModel.set("imgPath", encodeURIComponent(view.options.path));
              dModel.set("barcode", view.options.barcode);
              dModel.set("platePath", view.options.platePath);
              if (view.options.showVersion) {
                  dModel.set("versionChBox", true);
              } else {
                  dModel.set("versionChBox", false);
                  view.$('#sp_version').hide();
              }
              if (view.options.showTimepoint) {
                  dModel.set("timepointChBox", true);
              } else {
                  dModel.set("timepointChBox", false);
                  view.$('#sp_timepoint').hide();
              }
              if (view.options.showZIndex) {
                  dModel.set("zIndexChBox", true);
              } else {
                  dModel.set("zIndexChBox", false);
                  view.$('#sp_zIndex').hide();
              }

              this.model = dModel;

              view._modelBinder = new Backbone.ModelBinder();
              view._modelBinder.bind(view.model, view.$('.databinding'));

              this.delegateEvents({
                  'click #submitBtn': 'submit'
              });
              
              return this;
          },

          showModal: function (title, html) {
              var view = this;
              view.dialog = bootbox.dialog({
                  title: title,
                  message: html,
                  show: true
              });
          },

          submit: function () {
              var view = this;
              _.delay(function () { view.dialog.modal("hide"); }, 100);
          }
      });
      return view;
  });
