﻿define(['text!templates/error-page.htm', 'backbone'],
  function (pageTemplate) {
    var pageView = Backbone.View.extend({
      Initialize: function (error) {
        this.compiledTemplate = _.template(pageTemplate);
        this.render(error);
      },
      render: function (error) {
        this.$el.html(this.compiledTemplate({error:error}));
        $('.mainArticle').html(this.$el);
        this.$el.addClass('pageContent');
      }
    });
    return pageView;
  });
