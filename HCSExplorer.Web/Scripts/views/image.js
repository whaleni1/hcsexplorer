﻿define([
  'text!templates/image.htm',
  'models/image', 'utils/view-utils', 'utils/model-utils',
  'backbone', 'mbinder'
],
  function (template, model, utils, modelUtils) {
      var view = Backbone.View.extend({

          Initialize: function () {
              this.compiledTemplate = _.template(template);
              this.loadingImgSelector = '.loadingImg';
              this.render();
          },
          render: function () {
              var view = this;

              view.$el.html(view.compiledTemplate);
              $('#imgPageContent').empty().html(view.$el);
              
              utils.disableView(view);
              4
              var imgModel = new model();
              this.model = imgModel;
              this.getImgUrl = modelUtils.getURLParameter('src');
              this.barcode = modelUtils.getURLParameter('barcode');
              this.platePath = modelUtils.getURLParameter('platePath');

              var col = modelUtils.getURLParameterImg('col', this.getImgUrl);
              var row = modelUtils.getURLParameterImg('row', this.getImgUrl);
              var channel = modelUtils.getURLParameterImg('channel', this.getImgUrl);
              var field = modelUtils.getURLParameterImg('field', this.getImgUrl);
              var timepoint = null;
              try{
                  timepoint = modelUtils.getURLParameterImg('timepoint_index', this.getImgUrl);
              } catch (e) { };

              var zIndex = null;
              try {
                  zIndex = modelUtils.getURLParameterImg('z_index', this.getImgUrl);
              } catch (e) { };

              var version = null;
              try{
                  version = modelUtils.getURLParameterImg('version', this.getImgUrl);
              }catch (e) { };
              var rowId = utils.getRowIdArray();
              var cellId = rowId[parseInt(row) - 1] + col.toString();

              this.model.set('cellId', cellId).set('channel', channel).set('field', field).set('timepoint', timepoint).set('version', version).set('zIndex',zIndex);
              if (imgModel.get("version") == null || imgModel.get("version") == '')
                  view.$('#tr_version').hide();
              if (imgModel.get("timepoint") == null || imgModel.get("timepoint") == '')
                  view.$('#tr_timepoint').hide();
              if (imgModel.get("zIndex") == null || imgModel.get("zIndex") == '')
                  view.$('#tr_zIndex').hide();
             
              view._modelBinder = new Backbone.ModelBinder();
              view._modelBinder.bind(view.model, view.$('.databinding'));

              var response = imgModel.fetch({
                  async: true,
                  data: { getImgUrl: this.getImgUrl },
                  error: function (model, response) {
                      utils.wasFetchError(view, response);
                      utils.enableView(view);
                  },
                  success: function () {
                      view.$("#wellImg").attr("src", imgModel.get("path"));


                      view.$("#wellImg").show();
                      view._modelBinder = new Backbone.ModelBinder();
                      view._modelBinder.bind(view.model, view.$('.databinding'));

                      view.delegateEvents({
                          'click  #downLoadFullSizeImgBtn': 'showDownloadImgPopup'
                      });

                      utils.enableView(view);

                  }
              });
  
              return this;
          },
          
          showDownloadImgPopup: function () {
              var view = this;
              var path = view.model.get('path');
              var imgUrl = view.getImgUrl;
              var barcode = view.barcode;
              var platePath = view.platePath;
              var bVersion = view.model.get('version') != null && view.model.get('version') != '';
              var bZIndex = view.model.get('zIndex') != null && view.model.get('zIndex') > 0;
              var bTimepoint = view.model.get('timepoint') != null && view.model.get('timepoint') > 0;

              require(['views/downloadimg'],
                function (popupView) {
                    view.downloadImgView = new popupView({
                        path: path, imgUrl: imgUrl, barcode: barcode, platePath: platePath,
                        showVersion: bVersion, showTimepoint: bTimepoint, showZIndex:bZIndex
                    });
                },
                function (error) {
                    // TODO: handle
                });
          }
      });
      return view;
  });
