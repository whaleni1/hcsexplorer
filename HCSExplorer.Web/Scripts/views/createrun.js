﻿define([
  'text!templates/createrun.htm',
  'models/createrun', 'models/rundb', 'models/paths', 'models/getchannels', 'models/runid', 'utils/view-utils',
  'views/file-upload', 'utils/model-utils', 'utils/date-utils',
  'bootbox', 'backbone', 'mbinder', 'bootstrap', 'datatables', 'filetree', 'inputfile'
],
  function (template, model, dbModel, pathModel, cModel, runidModel, utils, uploadView, modelUtils, dateUtils, bootbox) {

      var plclass = '.spanUpload';
      var plclassImList = '.spanUploadList';

      var view = Backbone.View.extend({

          Initialize: function (options) {
              this.compiledTemplate = _.template(template);
              this.options = options;
              this.render();
          },
          render: function () {
              var view = this;
              view.submitPressed = false;

              view.$el.html(view.compiledTemplate);

              var plimageclass = '.divUploadImg';
              var beforeupload = function () {
                  if (view.$('.uploadImg').length == 0)
                      view.$(plimageclass).append('<div class="loadingImg uploadImg"></div>');
                  view.$('.uploadImg').show();
              };


              utils.disableView(view);
              beforeupload();

              view.dirtable = view.$('#imgDirectories').dataTable({
                  bSort: false,
                  bJQueryUI: false,
                  bInfo: false,
                  bFilter: false,
                  bPaginate: false,
                  //"sScrollY": "100px",
                  //"bScrollCollapse": true,
                  aaData: [],
                  aoColumns: [
                      { mDataProp: 'name' },
                      { mDataProp: 'path' },
                      {
                          mDataProp: 'id',  sWidth: "16px",
                          mRender: function (v, type, oData) {
                              return '<span title="Delete" class="glyphicon glyphicon-btn glyphicon-remove text-danger button-delete" >';
                          }
                      }
                  ]
              });

              var title = view.options.isCopyRun == true ? 'Copy Run' : 'Create New Run';
              view.showModal(title, view.$el);

              var tmodel = new model();
              if (view.options.isCopyRun) {
                  tmodel.set('id', view.options.copyingRunId);
              }
              var response = tmodel.fetch({
                  async: true,
                  success: function () {
                      if (view.options.isCopyRun) {
                          view.model = tmodel;
                          utils.decodeModelFields(view.model); // bug fix for "Run Description" field containing '\n' characters
                      } else { // create new run
                          view.model = new model();
                          view.model.set('sites', tmodel.get('sites'));
                          view.model.set('id', tmodel.get('id'));
                          view.model.set('createdDate', tmodel.get('createdDate'));
                          view.model.set('createdBy', tmodel.get('createdBy'));
                          view.model.set('inOneFile', tmodel.get('inOneFile'));
                          view.model.set('icp4', tmodel.get('icp4'));
                          view.model.set('imageListOnly', tmodel.get('imageListOnly'));
                          // post processing
                          view.model.set('postProcessingEnabled', tmodel.get('postProcessingEnabled'));
                          view.model.set('ppCountsSummed', tmodel.get('ppCountsSummed'));
                          view.model.set('ppMeanMeanIntensity', tmodel.get('ppMeanMeanIntensity'));
                          view.model.set('ppMedianMeanIntensity', tmodel.get('ppMedianMeanIntensity'));
                          view.model.set('ppStdDeviationMeanIntensity', tmodel.get('ppStdDeviationMeanIntensity'));
                      }
                      utils.viewError(view);


                      view.$('.uploadImg').hide();

                      view.restoreOutputFiles();
                      view.restorePostProcessing();

                      view.wavetable = view.$('#wavemap').dataTable({
                          "bSort": false,
                          "bJQueryUI": false,
                          "bInfo": false,
                          "bFilter": false,
                          "bPaginate": false,
                          "bScrollCollapse": true,
                          "sScrollY": "100px",
                          aoColumns: [
                              {
                                  mDataProp: 'key', sWidth: "50%"
                              },
                              {
                                  mDataProp: 'value', sWidth: "50%", mRender: function (v, type, oData) {
                                      var chls =  view.model.get('channels');
                                      if (chls == null || chls.length == 0)
                                          return '';
                                      return '<select class="sChan">' + utils.optionsForSelect(chls, true, '', v) + '</select>';
                                  }
                              }
                          ]
                          //"sScrollY": "200px",
                      });
                      _.delay(function () { view.wavetable.fnAdjustColumnSizing(); }, 10);

                      if (view.options.isCopyRun) {
                          // fill directories table
                          var dirs = view.model.get('dirs');
                          if (dirs && dirs.length > 0) {
                              for (var i = 0; i < dirs.length; i++) {
                                  dirs[i].id = i;
                              }

                              view.fillDirsTable(dirs);
                          }


                          // fill wavelength map
                          var wMap = view.model.get('wavelengthMap');
                          view.restoreWavelengthMap(wMap);

                          //channels
                          var chls = view.model.get('channels');
                          if (chls == null || chls.length == 0) {
                              utils.setFieldValidationErr(view, "lblWaveMap", "Channels are required");
                          }
                      }

                      var createdDate = tmodel.get('createdDate');
                      var createdBy521 = tmodel.get('createdBy');

                      view.$('#createdDate').text(dateUtils.toUserFormatDate(createdDate));
                      var userName = sigGetCurrentUserName(); //TODO: change this!
                      if ($.trim(userName).length == 0) {
                          userName = createdBy521;
                      }
                      view.$('#createdBy').text(userName);
                      var msgUpload = ['Not a valid Cell Profiler protocol file. Refer to GetHelp for support.', 'Not a valid .'];
                      var msgSize = ['Only protocols < 1 MB supported.', 'Only files < 100 MB supported.'];


                      view.upload_view = new uploadView({
                          url: modelUtils.createBaseUrl('api/run/uploadprotocol'),
                          uploadonchangeurl: modelUtils.createBaseUrl('api/run/uploadprotocol'),
                          // dataselector: '.formFields',
                          beforeupload: beforeupload,
                          clearondone: false,
                          accept: '.cppipe',
                          msgUpload: 'Not a valid Cell Profiler protocol file. Refer to GetHelp for support.',
                          msgSize: 'Only protocols < 1 MB supported.',
                          maxSize: 1048576
                      });
                      view.upload_view.on('uploading', view.uploading, view);
                      view.upload_view.on('done', view.done, view);
                      view.upload_view.on('fail', view.fail, view);
                      view.upload_view.render();
                      view.upload_view.$el.insertAfter(view.$(plclass));


                      view.$('#uploadedfile').customFileInput();
                      view.$(plclass).closest('.form-group').find('.customfile-feedback').attr('name', 'protocolName');


                      view.upload_image_list = new uploadView({
                          url: modelUtils.createBaseUrl('api/run/uploadimagelist'),
                          uploadonchangeurl: modelUtils.createBaseUrl('api/run/uploadimagelist'),
                          name: 'uploadedfilelist',
                          beforeupload: beforeupload,
                          clearondone: false,
                          accept: '.csv',
                          msgUpload: 'Not a valid .',
                          //msgSize: 'Only files < 4 MB supported.',
                          //maxSize: 4194304
                          //msgSize: 'Only files < 100 MB supported.',
                          //maxSize: 104857600
                          msgSize: 'Only files < 200 MB supported.',
                          maxSize: 209715200

                      });
                      view.upload_image_list.on('uploading', view.uploading, view);
                      view.upload_image_list.on('done', view.doneImList, view);
                      view.upload_image_list.on('fail', view.failImList, view);
                      view.upload_image_list.render();
                      view.upload_image_list.$el.insertAfter(view.$(plclassImList));

                      view.$('#uploadedfilelist').customFileInput();
                      view.$(plclassImList).closest('.form-group').find('.customfile-feedback').attr('name', 'imageListName');


                      var imgListOnly = view.model.get("imageListOnly");
                      view.$("#imageListOnlyChbx").prop('checked', imgListOnly);
                      utils.enableByClass(view, "#uploadImageListLi", !imgListOnly);
                      view.enableView();

                      view._modelBinder = new Backbone.ModelBinder();
                      view._modelBinder.bind(view.model, view.$('.form-group'));

                      utils.setSelectFunctionality(tmodel, view.$('#siteIdSelect'), 'sites', true, "Select Site");
                      if (view.options.isCopyRun) {
                          view.$('#siteIdSelect').val(tmodel.get('siteId'));

                          if (view.model.get('dataLocation') != null) {
                              utils.setSelectFunctionality(tmodel, view.$('#databaseIdSelect'), 'databases', true, "Select Data Source");
                              view.$('#databaseIdSelect').val(tmodel.get('databaseId'));
                              utils.setSelectFunctionality(tmodel, view.$('#pathIdSelect'), 'paths', true, "Select Network Drive");
                              view.$('#pathIdSelect').val(tmodel.get('dataLocation'));

                          }

                          view.model.set('name', view.model.get('name'));//if validation error


                          var ilName = view.model.get('imageListName');
                          if (ilName != null && ilName != '') {
                              $('ul#plateTab a[href="#uploadImageList"]').tab('show')
                          }


                      } else { // set localSite
                          var localSite = tmodel.get('localSite');
                          if (localSite) {
                              view.model.set('siteId', localSite);
                              view.siteSelected();
                          }

                          //for new run set focus to name
                          //if validation error
                          //_.delay(function () { view.$('#name').focus();}, 500);
                      }
                      utils.createSelectSimpleInt(view.$('#blockSelect'), 50, false);
                      view.$('#blockSelect').val(tmodel.get('block'));
                      view.updateSubmitBtn();
                      if (!view.options.isCopyRun) {
                          _.delay(function () { view.$('#name').focus(); }, 500);
                      }
                  },
                  error: function () {
                      view.model = tmodel;
                      view.enableView();
                      view.$('.uploadImg').hide();
                      utils.errorPopup(tmodel);
                      _.delay( function(){view.cancelBtnClicked();},1000);//close
                  }
              });

              this.delegateEvents({
                  'blur #name': function () {
                      if (this.model.get('name') == null) {
                          this.model.set('name', '');
                      }
                  },
                  'keyup.input[type=text]': 'inputTextKeyUp',
                  'click #cancelBtn': 'cancelBtnClicked',
                  'click #btnBrowseDirs': 'showFileTreeDialog',
                  'change #siteIdSelect': 'siteSelected',
                  'change #databaseIdSelect': 'databaseSelected',
                  'change #pathIdSelect': 'pathSelected',
                  'keyup textarea[maxlength]': 'taOnEdit',
                  'keydown textarea[maxlength]': 'taOnEdit',
                  'focus textarea[maxlength]': 'taOnEdit',
                  'paste textarea[maxlength]': 'taOnEdit',
                  'input textarea[maxlength]': 'taOnEdit',
                  'change #wavemap select': 'waveMapChanged',
                  'click #imgDirectories .button-delete': 'deleteDir',
                  'mouseenter #imgDirectories tr': 'showDelImage',
                  'mouseleave #imgDirectories tr': 'hideDelImage',
                  'click .output-files': 'outputFilesChbxClicked',
                  'click #imageListOnlyChbx': 'imageListOnlyChbxClicked',
                  'click #chkbxDoPostProcessing': 'postProcessingChbxClicked',
                  'click .chkbx-pp-type': 'postProcessingTypeChbxClicked',
                  'shown.bs.tab a[data-toggle="tab"]': 'tabChanged',
                  'show.bs.tab a[data-toggle="tab"]': 'tabClicked'
                  //,'change .bConc': 'waveMapChanged'
              });

              return this;
          },
          tabChanged: function (e) {
              var view = this;
              if (view.model == null) return;
              view.model.set('imagesInputType', e.target.name);
          },
          tabClicked: function (e) {
              if ($(e.target).closest("li").hasClass("disabled")) {
                  return false;
              }
          },
          showModal: function (title, html) {
              var view = this;
              var d = bootbox.dialog({
                  title: title,
                  message: html,
                  show: false,
                  buttons: {
                      cancel: {
                          label: "Cancel",
                          className: "btn-default",
                          callback: function () {
                              // TODO:
                          }
                      },
                      submit: {
                          label: "SUBMIT",
                          className: "btn-primary js-btn-submit",
                          callback: function () {
                              view.submitRun();
                              return false; // prevent auto closing
                          }
                      }
                  }
              }).addClass("modal-create-run");

              view.dialog = d;
              d.on("show.bs.modal", function () {
                  view.enableSubmitBtn(false);
              });
              d.modal("show");
          },
          restoreOutputFiles: function () {
              var view = this;
              view.$('.output-files').each(function (indx) {
                  var name = $(this).attr('name');
                  var value = view.model.get(name);
                  if (value) {
                      $(this).prop('checked', value);
                  }
              });
          },
          imageListOnlyChbxClicked: function (ev) {
              var view = this;
              var isChecked = ev.currentTarget.checked;
              this.model.set("imageListOnly", isChecked);
              this.enablePostProcessingSection(!isChecked);
              /*this.enableProtocolSectionAndWave(!isChecked);
              if (isChecked) {
                  utils.setFieldValidationErr(view, "lblWaveMap", "");
              } else {
                
                  var cmbs = view.$('#wavemap select')
                  if (cmbs.length > 0) {
                      map = view.validateMap(null, true);
                  }
              }*/
              if (isChecked) {
                  $('.nav-tabs a[href=#selectImgDirsGroup]').tab('show');
                  this.upload_view.setValidationError('');
              }
              utils.enableByClass(view, "#uploadImageListLi", !isChecked);
              this.updateSubmitBtn();
          },
          enableProtocolSectionAndWave: function (enabled) {
              utils.enableElement(view, "#analysisProtocolGroup .form-control, #analysisProtocolGroup input,#wavemap select", enabled);
              utils.enableByClass(view, "#analysisProtocolGroup .customfile", enabled);
          },
          enablePostProcessingSection: function (enabled) {
              if (enabled) {
                  view.$("#postProcessingSection").show();
              } else {
                  view.$("#postProcessingSection").hide();
              }
          },
          outputFilesChbxClicked: function (ev) {
              var isChecked = ev.currentTarget.checked;
              var name = ev.currentTarget.name;
              
              var checkedCount = 0;
              view.$('.output-files').each(function (indx) {
                  if ($(this).prop('checked')) {
                      checkedCount++;
                  }
              });
              if (checkedCount == 0)
                  return false; // do not allow uncheck both items

              this.model.set(name, isChecked);
          },
          postProcessingChbxClicked: function (ev) {
              var view = this;
              var checked = ev.currentTarget.checked;
              utils.enableElement(view, "#postProcessingFieldset", checked);

              view.model.set("postProcessingEnabled", checked);
          },
          postProcessingTypeChbxClicked: function (ev) {
              var view = this;
              var checked = ev.currentTarget.checked;
              if (checked && ev.currentTarget.id == 'chkbxStdDeviationMeanIntensity') {
                  view.$('#chkbxMeanMeanIntensity').prop('checked', true);
                  view.model.set("ppMeanMeanIntensity", true);
              } else if (!checked && ev.currentTarget.id == 'chkbxMeanMeanIntensity') {
                  view.$('#chkbxStdDeviationMeanIntensity').prop('checked', false);
                  view.model.set("ppStdDeviationMeanIntensity", false);
              }

              var name = ev.currentTarget.name;
              view.model.set(name, checked);
          },
          restorePostProcessing: function () {
              var view = this;
              var ppEnabled = view.model.get("postProcessingEnabled");
              view.$('#chkbxDoPostProcessing').prop('checked', ppEnabled);
              utils.enableElement(view, "#postProcessingFieldset", ppEnabled);
              view.$('.chkbx-pp-type').each(function (indx) {
                  var name = $(this).attr('name');
                  var value = view.model.get(name);
                  if (value) {
                      $(this).prop('checked', value);
                  }
              });
          },
          channels: function () {
              var res = this.model.get('channels');
              if (!res)
                  res = [];
              return res;
          },
          taOnEdit: function (e) {
              var ta = $(e.currentTarget);
              var ml = ta.attr('maxlength');
              var iml = parseInt(ml);
              if (ta.val().length > iml) {
                  ta.val(ta.val().substr(0, iml));
              }
          },

          cancelBtnClicked: function () {
              $('#createRunDlg').dialog('close');
          },
          //upload
          uploading: function () {
              utils.hideErrorBox(this, true);
          },

          enableView: function () {
              view = this;
              var sid = view.model.get('siteId'); if (sid == '') sid = null;
              var did = view.model.get('databaseId'); if (did == '') did = null;
              var pid = view.model.get('dataLocation'); if (pid == '') pid = null;

              var imgListOnly = view.model.get("imageListOnly");

              if (sid != null && did != null && pid != null) {
                  utils.enableView(view );
              }else if (sid != null && did != null) {
                  utils.enableView(view, '#btnBrowseDirs');
              } else if (sid != null) {
                  utils.enableView(view, '#pathIdSelect,#btnBrowseDirs');
              } else {
                  utils.enableView(view, '#databaseIdSelect,#pathIdSelect,#btnBrowseDirs');
              }

              //view.enableProtocolSectionAndWave(!imgListOnly);
              view.enablePostProcessingSection(!imgListOnly);

          },
          fail: function (data) {
              var view = this;
              utils.setAllErrors(this, data);
              this.$('.uploadImg').hide();
              view.enableView();
              view.model.set('bioConcepts', null);
              view.model.set('protocolName', null);
              view.model.set('protocolRef', null);
              view.model.set('protocolLocalFileName', null);
              view.$('#protocolWarning').text("");
              view.$(plclass).closest('.form-group').find('.customfile-feedback').removeClass('customfile-feedback-populated');
              view.$(plclass).closest('.form-group').find('.customfile-feedback').text('No file selected...');
              view.redrawWMapTable(0);
              view.updateSubmitBtn();
          },
          done: function (data) {
              var view = this;
              view.$('.uploadImg').hide();
              view.enableView();
              this.upload_view.setValidationError('');
              view.model.set('bioConcepts', data.BiologicalConcepts);
              view.model.set('protocolName', data.FileName);
              view.model.set('protocolRef', null);
              view.model.set('protocolLocalFileName', data.LocalFileName);
              var warning = data.WarningMessage ? data.WarningMessage : "";
              view.$('#protocolWarning').text(warning);
              view.redrawWMapTable(0);
              view.updateSubmitBtn();
          },
          failImList: function (data) {
              var view = this;
              utils.setAllErrors(this, data);
              this.$('.uploadImg').hide();
              //clear 1-st tab
              view.clearTab1();

              view.enableView();
              view.disabled = false;
              view.$('#uploadListWarning').text("");

              view.model.set('imageListName', null);
              view.model.set('imageListRef', null);
              view.model.set('imgListLocalFileName', null);

              _.delay(function () {
                  view.$(plclassImList).closest('.form-group').find('.customfile-feedback').removeClass('customfile-feedback-populated');
                  view.$(plclassImList).closest('.form-group').find('.customfile-feedback').text('No file selected...');
              }, 10);

              view.ilData = null;
              //clear wavemap
              view.model.set('channels', []);
              view.redrawWMapTable();
              view.updateSubmitBtn();
          },
          doneImList: function (data) {
              var view = this;
              view.ilData = data;
              view.$('.uploadImg').hide();
              //clear 1-st tab
              view.clearTab1();

              view.enableView();
              this.upload_image_list.setValidationError('');
              view.model.set('imageListName', data.FileName);
              view.model.set('imageListRef', null);
              view.model.set('imgListLocalFileName', data.LocalFileName);
              

              var warning = data.WarningMessage ? data.WarningMessage : "";
              view.$('#uploadListWarning').text(warning);
              //fill channels
              view.model.set('channels', data.Channels);
              if (view.model.get('channels')==null || view.model.get('channels').length == 0)
                  utils.setFieldValidationErr(view, "lblWaveMap", data.message);
              view.redrawWMapTable();
              view.enableView();
              view.disabled = false;
              view.updateSubmitBtn();

          },
   
          redrawWMapTable: function ( op) {
              var view = this;
              var tbldata = [];
              var chls = view.model.get("channels");
              var biologicalConcepts = view.model.get("bioConcepts");

              var map = view.model.get("bioConcepts");
              if (chls != null && chls.length > 0) {
                  var wavelengthMap = view.model.get("wavelengthMap");

                  var chNotChanged = true;
                  if(wavelengthMap !=null && chls.length==wavelengthMap.length)
                  {
                     
                      for (var j = 0; j < wavelengthMap.length; j++) {
                          var fnd = false;
                          for (var i = 0; i < chls.length; i++) {                        
                              if (chls[i] == wavelengthMap[j].channel) {
                                  fnd=true;
                                  break;
                              }
                          }
                          if(!fnd){
                              chNotChanged = false;
                              break;
                          }
                      }
                  }else{
                      chNotChanged = false;
                  }


                  if (biologicalConcepts != null && biologicalConcepts.length > 0) {
                      var cnt = biologicalConcepts.length;
                      if (cnt > chls.length) {
                          utils.setFieldValidationErr(view, "lblWaveMap", "Did not find sufficient number of wavelengths in the image directories to satisfy the protocol. Update your selection before continuing.");
                          for (var i = 0; i < cnt; i++) {
                              tbldata.push({ 'key': biologicalConcepts[i], 'value': '' });
                          }

                      } else {
                          utils.setFieldValidationErr(view, "lblWaveMap", "");
                         
                          for (var i = 0; i < cnt; i++) {
                              var chcur = chls[i];

                              if (chNotChanged)
                                  chcur = wavelengthMap[i].channel;
                              else
                                  chcur = ''; //reset selection

                              if (op == 0)
                                  chcur = ''; //reset selection

                              tbldata.push({ 'key': biologicalConcepts[i], 'value': chcur });

                          }
                      }
                  } else {
                      utils.setFieldValidationErr(view, "lblWaveMap", "");
                      for (var i = 0; i < chls.length; i++) {
                          var chcur = chls[i];
                          if (chNotChanged)
                              chcur = wavelengthMap[i].channel;
                          tbldata.push({ 'key': '', 'value': chcur });
                      }
                  }
              } else if (biologicalConcepts != null && biologicalConcepts.length > 0) {
                  for (var i = 0; i < biologicalConcepts.length; i++) {
                          tbldata.push({ 'key': biologicalConcepts[i], 'value': null });
                  }
              }

              view.wavetable.fnClearTable();
              if (tbldata.length > 0) view.wavetable.fnAddData(tbldata);
              view.wavetable.fnDraw();
          },
          restoreWavelengthMap: function (data) {
              var view = this;
              var tbldata = [];
              if (data != null && data.length > 0) {
                  utils.setFieldValidationErr(view, "lblWaveMap", "");
                  for (var i = 0; i < data.length; i++) {
                      tbldata.push({ 'key': data[i].biologicalConcept, 'value': data[i].channel });
                  }
              }

              view.wavetable.fnClearTable();
              if (tbldata.length > 0) view.wavetable.fnAddData(tbldata);
              view.wavetable.fnDraw();
          },
          siteSelected: function () {
              var view = this;
              var siteId = view.$('#siteIdSelect option:selected').attr("value");
              if (siteId == '') {
                  view.$('#databaseIdSelect,#pathIdSelect,#btnBrowseDirs').attr("disabled", "disabled");
                  view.model.set('siteId', null);
              } else {
                  var dmodel = new dbModel();
                  dmodel.set('id', siteId);
                  view.clearTab2();
                  utils.disableView(view);
                  var response = dmodel.fetch({
                      success: function () {
                          view.model.set('siteId', siteId);
                          utils.setSelectFunctionality(dmodel, view.$('#databaseIdSelect'), 'databases', true, "Select Data Source");
                          var dataSources = dmodel.get('databases');
                          if (dataSources.length == 1) {
                              var dsId = dataSources[0].id;
                              view.model.set('databaseId', dsId);
                              view.$('#databaseIdSelect').val(dsId);
                              view.enableView();
                              view.databaseSelected();
                          } else {
                              view.enableView();
                          }
                      },
                      error: function (model, response) {
                          view.enableView();
                          utils.wasDialogFetchError(view, response);
                      }
                  });
              }
              var drs = view.model.get('dirs');
              if (drs != null && drs.length>0)
                view.emptyDirsTable();
          },
          databaseSelected: function () {
              var view = this;
              var dbId = view.$('#databaseIdSelect option:selected').attr("value");

              if (dbId == '') {
                  view.$('#pathIdSelect,#btnBrowseDirs').attr("disabled", "disabled");
                  view.model.set('databaseId', null);
              } else {
                  var pmodel = new pathModel();
                  pmodel.set('siteId', view.model.get('siteId'));
                  pmodel.set('databaseId', view.model.get('databaseId'));
                  utils.disableView(view);
                  pmodel.save({}, {
                      success: function () {
                          utils.setSelectFunctionality(pmodel, view.$('#pathIdSelect'), 'paths', true, "Select Network Drive");
                          var paths = pmodel.get('paths');
                          if (paths.length == 1) {
                              var dataLocation = paths[0].id;
                              var pathId = paths[0].name;
                              view.model.set('dataLocation', dataLocation);
                              view.model.set('pathId', pathId);
                              view.$('#pathIdSelect').val(dataLocation);
                              view.enableView();
                              view.pathSelected(); 
                          } else {
                              view.model.set('pathId', null);
                              view.model.set('dataLocation', null);
                              view.enableView();
                          }
                      },
                      error: function (model, response) {
                          view.model.set('pathId', null);
                          view.model.set('dataLocation', null);
                          view.enableView();
                          utils.wasDialogFetchError(view, response);
                      }
                  });
              }
              var drs = view.model.get('dirs');
              if (drs != null && drs.length > 0)
                  view.emptyDirsTable();
          },
          pathSelected: function () {
              var view = this;
              var pathId = view.$('#pathIdSelect option:selected').text();
              view.model.set('pathId', pathId);
              if (pathId == '') {
                  view.$('#btnBrowseDirs').attr("disabled", "disabled");
              } else {
                  view.$('#btnBrowseDirs').removeAttr('disabled');
              }
              var drs = view.model.get('dirs');
              if (drs != null && drs.length > 0)
                  view.emptyDirsTable();
          },

          clearTab1: function (notRedrawMap) {
              var view = this;
              view.model.set('siteId', null);
              view.model.set('databaseId', null);
              view.model.set('pathId', null);
              view.model.set('dirs', []);
              $(view.$('#siteIdSelect option')[0]).attr('selected', 'selected');
              $(view.$('#databaseIdSelect option')).not('first').remove();
              $(view.$('#pathIdSelect option')).not('first').remove();
              view.$('#databaseIdSelect,#pathIdSelect,#btnBrowseDirs').attr("disabled", "disabled");
              view.emptyDirsTable(notRedrawMap);
          },
          clearTab2: function () {
              var view = this;

              view.model.set('channels', []);
              view.model.set('imageListName', null);
              view.model.set('imageListRef', null);
              view.model.set('imgListLocalFileName', null);

              view.$(plclassImList).closest('.form-group').find('.customfile-feedback').removeClass('customfile-feedback-populated');
              view.$(plclassImList).closest('.form-group').find('.customfile-feedback').text('No file selected...');
              view.ilData = null;
              view.redrawWMapTable();
          },

          showFileTreeDialog: function () {
              var view = this;

              var data = {
                  siteId: view.model.get('siteId'),
                  databaseId: view.model.get('databaseId'),
                  pathId: view.model.get('pathId'),
                  isRoot: true
              };
              require(['views/dirbrowse'],
                  function (browseView) {
                      view.browseView = new browseView({ parentView: view, data: data });
                  },
                  function (error) {
                  });
          },
          fillDirsTable: function (data) {
              var view = this;
              view.dirtable.fnClearTable();
              view.dirtable.fnAddData(data);
              view.dirtable.fnDraw();
              
          },
          deleteDir: function (e) {
              var view = this;
              if (!view.disabled) {
                  var span = $(e.currentTarget);
                  var tr = span.closest('tr');
                  var rowIndex = view.dirtable.fnGetPosition(tr[0]);
                  var data = view.model.get('dirs');
                  data.splice(rowIndex, 1);
                  if (data.length > 0) {
                      view.model.set('dirs', data);
                      this.addDirectories();
                  }
                  else {
                      view.emptyDirsTable();
                  }

              }
              view.updateSubmitBtn();
          },

          emptyDirsTable: function(notRedrawMap){
              var view = this;
              view.model.set('dirs', []);
              view.dirtable.fnClearTable();
              utils.setFieldValidationErr(view, "imgDirectories_wrapper", "");
              if (notRedrawMap == null) {
                  view.model.set('channels', []);
                  view.redrawWMapTable();
              }
          },
          showDelImage: function(e)
          {
              var view = this;
              if (!view.disabled) {
                  var tr = $(e.currentTarget);
                  tr.find('.button-delete').addClass('btn-delete');
                  
              }
          },
          hideDelImage: function (e) {
                  var tr = $(e.currentTarget);
                  tr.find('.btn-delete').removeClass('btn-delete');

          },
          addDirectories: function (datanew) {
              var view = this;
              var data = view.model.get('dirs');
              if (data == null || data.length == 0) {
                  view.model.set('dirs', datanew);
                  data = datanew;
              }
              else {
                  if (datanew != null) {
                      for (var k = 0; k < datanew.length; k++) {
                          var fnd = false;
                          for (var i = 0; i < data.length; i++) {
                              var old = data[i];
                              if (old.path == datanew[k].path) {
                                  fnd = true;
                                  break;
                              }
                          }
                          if (!fnd)
                            data.push(datanew[k]);
                      }
                      view.model.set('dirs', data);
                  }
               
              }
              if ( data != null && data.length > 0)
                  utils.setFieldValidationErr(view, "imgDirectories_wrapper", "");
              view.disabled = true;
              this.fillDirsTable(data);

              var cmodel = new cModel();
              var ph = [];
              for (var i = 0; i < data.length; i++)
                  ph.push(data[i].path)
              cmodel.set('paths', ph);
              cmodel.set('siteId', view.model.get('siteId'));
              cmodel.set('databaseId', view.model.get('databaseId'));
              utils.disableView(view);

              view.$('#channelsSpinner').show();
              var response = cmodel.save({}, {
                  success: function () {

                      view.model.set('channels', cmodel.get('channels'));
                      if (cmodel.get('channels').length == 0)
                          utils.setFieldValidationErr(view, "lblWaveMap", cmodel.get('message'));
                      view.redrawWMapTable();
                      view.enableView();
                      view.disabled = false;
                      view.$('#channelsSpinner').hide();
                      view.updateSubmitBtn();
                  },
                  error: function (model, response) {
                      view.$('#channelsSpinner').hide();
                      var result = null;
                      
                      try {
                          result = $.parseJSON(response.responseText);
                      } catch (e) { };

                      if (result != null && result.message != null && result.channels != null) {

                          view.model.set('channels', result.channels);
                          view.redrawWMapTable();
                          utils.setFieldValidationErr(view, "imgDirectories_wrapper", result.message);
                          view.enableView();
                          view.disabled = false;

                      } else {
                          view.model.set('channels', []);
                          view.redrawWMapTable();
                          view.enableView();
                          view.disabled = false;
                          utils.wasDialogFetchError(view, response);
                      }
                      view.updateSubmitBtn();
                  }
              });

          },
          submitRun: function () {
              var view = this;
              view.submitPressed = true;
              if (view.validateModel()) {
                  var afterSaving = function (view) {
                      view.$('.uploadImg').hide();
                      view.dialog.modal("hide"); // close bootbox dialog

                      if (view.options.isCopyRun) {
                          Backbone.history.navigate('runs', true);
                      } else {
                          view.options.parentView.updateRuns();
                          view.options.parentView.startAutoUpdate();
                      }
                  };
                  var onError = function (view) {
                      view.$('.uploadImg').hide();
                      view.enableSubmitBtn(true);
                      var idModel = new runidModel();
                      var response = idModel.fetch({
                          async: false
                      });
                      view.model.set("id", idModel.get("id"));
                      if (view.ilData != null  &&  view.model.get('imageListName')!= null) {
                          view.clearTab1(true);
                      }
                  };
                  view.$('.uploadImg').show();
                  view.enableSubmitBtn(false);

                  var notModelData = null;

                  if (view.ilData!=null) {
                      notModelData = {
                          siteId: view.ilData.SiteId,
                          databaseId: view.ilData.DatabaseId,
                          dataLocation: view.ilData.DataLocation
                      };
                  }
                  utils.save(view, null, afterSaving, true, onError, notModelData);
              }
          },
          waveMapChanged: function (e) {
              var view = this;
              view.validateMap(e, true);
              view.updateSubmitBtn();
          },
          validateMap: function (e, userChanged) {
              var samechannels = false;
              var view = this;
              var map = true;
              var wavelengthMap =  [];
              var alltr = view.wavetable.fnGetNodes();
              for (var i = 0; i < alltr.length; i++) {

                  var txt = $($(alltr[i]).find('td')[0]).text();
                  var cmb = $(alltr[i]).find('select');
                  var sel = cmb.find('option:selected');
                  if (cmb.length == 0)
                      map = false;
                  if (txt == '')
                      map = false;
                  if (sel.length == 0)
                      map = false;
                  else {
                      if (map) {
                          for (var j = i + 1; j < alltr.length; j++) {
                              var cmbj = $(alltr[j]).find('select');
                              var selj = cmbj.find('option:selected');
                              if (selj.length > 0) {
                                  if (sel[0].text == selj[0].text && sel[0].text!='') {
                                      map = false;
                                      samechannels = true;
                                  }
                              }
                          }
                      }

                  }

                  if (map && sel[0].text == '')
                      map = false;
                  wavelengthMap.push({ biologicalConcept: txt, channel: ((sel.length >0)?sel[0].text:null) });
              }
              view.model.set("wavelengthMap", wavelengthMap);
              if (userChanged) { // input select change
                  if (map == true) {
                      utils.setFieldValidationErr(view, "lblWaveMap", "");
                  } else {
                      if(!samechannels)
                        utils.setFieldValidationErr(view, "lblWaveMap", "Wavelength Map is required");
                      else
                        utils.setFieldValidationErr(view, "lblWaveMap", "The same channel is selected in multiple rows");
                  }
              }
              return map;
          },
          inputTextKeyUp: _.debounce(function (ev) {
              var view = this;
              if (ev.target.name == 'description')
                  return;
              if (ev.target.className == 'bConc') {
                  var map = view.validateMap(null, true);
                  if (!map) {
                      utils.setFieldValidationErr(view, "lblWaveMap", "Use the wavelength map to associate wavelengths with named biological concepts.");
                  } else {
                      utils.setFieldValidationErr(view, "lblWaveMap", "");
                  }
              }
              view.updateSubmitBtn(true);
          }, 500),
          updateSubmitBtn: function (inputTextChanging) {
              var view = this;
              var valid = view.validateModelDoNotShowErros(inputTextChanging);
              view.enableSubmitBtn(valid);
          },
          enableSubmitBtn: function (enable) {
              var view = this;
              var btn = view.dialog.find('.js-btn-submit');
              if (enable) {
                  btn.removeAttr('disabled');
              } else {
                  btn.attr("disabled", "disabled");
              }
          },
          validateModelDoNotShowErros: function (inputTextChanging) {
              var view = this;

               var runName = view.$("#name").val();
              if (inputTextChanging == true) {
                  var el = view.$("#name")[0];
                  var start = el.selectionStart, end = el.selectionEnd;
                  view.model.set("name", runName);
                  el.setSelectionRange(start, end);
              }
              
              if (runName == null || runName.trim().length == 0)
                  return false;

              var vname = view.model.get("name");
              if (vname != null && vname.length > 255)
                  return false;

              if (view.model.get("protocolName") == null) {
                  return false;
              }

              var tab1 = view.$('ul#plateTab li.active [href="#selectImgDirsGroup"]');
              if (tab1.length > 0) {

                  var dirs = view.model.get("dirs");
                  if (dirs == null || dirs.length == 0) {
                      return false;
                  }
              }

              var map = true;
              //if (!isChecked) {

                  var biologicalConcepts = view.model.get("bioConcepts");
                  if (biologicalConcepts == null || biologicalConcepts.length == 0)
                      map = false;
                  var cmbs = view.$('#wavemap select')
                  if (cmbs.length == 0)
                      map = false;
                  if (map) {
                      map = view.validateMap(null, true);
                  }
             // }

              return map;
          },
          validateModel: function () {
              var view = this;
              var result = view.model.isValid(true);


              if (view.model.get("protocolName") == null)
                  this.upload_view.setValidationError("Protocol is required");

              var tab1 = view.$('ul#plateTab li.active [href="#selectImgDirsGroup"]');
              if (tab1.length > 0) {

                  var dirs = view.model.get("dirs");
                  if (dirs == null || dirs.length == 0) {
                      utils.setFieldValidationErr(view, "imgDirectories_wrapper", "Image Directories are required");
                      result = false;
                  }
              }

              var map = true;

              var biologicalConcepts = view.model.get("bioConcepts");
              if (biologicalConcepts == null || biologicalConcepts.length == 0)
                  map = false;
              var cmbs = view.$('#wavemap select')
              if (cmbs.length == 0)
                  map = false;
              if (map) {
                  map = view.validateMap(null, true);
              }
              result = result && map;


              return result;
          },

          onclose: function () {
              if (view.submitPressed == false) {
                  view.options.parentView.startAutoUpdate();
              }
              if (this._modelBinder)
                this._modelBinder.unbind();
              Backbone.Validation.unbind(this);
          }
      });
      return view;
  });
