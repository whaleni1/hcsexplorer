﻿define(['text!templates/application.htm',
        'utils/view-utils',
        'utils/menu-utils',
        'jquery',
        'bootstrap',
        'backbone',
        'bootbox',
        'menubar',
        'sig',
],
  function (applicationTemplate, utils, menuUtils) {
    var applicationView = Backbone.View.extend({

      Initialize: function (layout) {
        this.compiledTemplate = _.template(applicationTemplate);
        this.model.on('change', this.render, this);
        this.render();
      },
      render: function () {
        var view = this;
        this.undelegateEvents();

        this.$el.empty().html(this.compiledTemplate());
        $('#pagecontent').empty().html(this.$el);


        $('body').nibrheader({
            name: 'HCS EXPLORER',
            description: 'High Content Image Visualization',
            deploymentState: 'Prod v4.0',
            appNameUrl: 'http://web-dev.global.nibr.novartis.net/apps/HCSExplorer/'
        });


        NIBRIam = {};
        NIBRIam.NIBRFirst = view.model.get('nibrFirst');
        NIBRIam.NIBRLast = view.model.get('nibrLast');
        NIBRIam.NIBR521 = view.model.get('userName');

        IS_DEBUG_MODE = view.model.get('debugMode');
        if (NIBRIam.NIBR521 != null && NIBRIam.NIBR521!='')
        {
            $("#getHelpLink").nibrGetHelp({
                appName: "HCS Explorer",
                subHeading: "How can we help you?",
                subHeadingText: "Please fill out the form to contact the HCS Explorer Support Team about issues or feature requests.",
                otherResource: "<a href='http://communities.nibr.novartis.net/groups/hcs'>HCS on NIBR Talk</a>",
                requestSupportChannel: "Email, {hcse.support@novartis.com}",
                generalFeedbackSupportChannel: "Email, {hcse.support@novartis.com}", //'JIRA, {"issueTypeName": "Bug", "defaultAssignee": "whaleni1", "projectCode": "HCSE", "component": "HCS Explorer"}',
                issueSupportChannel: "Email, {hcse.support@novartis.com}"
            });

        }

        view.$('.copyright-year').text(view.model.get('footer').copyright);
        this.renderMenu();
        view.$('.version').text(view.model.get('footer').vers);
        /*this.loginInfo(true);
        this.footerInfo();
        */
        //this.footerInfo();
        //initSigContent();
        return this;
      },
      //footerInfo: function () {
      //  var footer = this.model.get('footer');
      //  var InitializeVisibility = footer.InitializeVisibility;
      //  this.$('#copyright').html(footer.copyright);
      //  var vers = footer.major + '.' + footer.minor;
      //  var build = vers + '.' + footer.build + '.' + footer.revision;
      //  var versStr = ' Version: ' + vers + ', Build: ' + build + ', Build Date: ' + footer.date.substring(0, 10);
      //  this.$('#version').html(versStr);
      //},
      //loginInfo: function (bshow) {
      //  if (bshow) {
      //    this.$('.login').text(this.model.get('userName'));

      //    this.$('.pageName').addClass('pageNameAuth');
      //  } else {
      //    this.$('.login').empty();

      //    this.$('.pageName').removeClass('pageNameAuth');
      //  }
      //  return this;
      //},
      renderMenu: function () {
        this.$('nav ul').empty();
        var items = null;
        var menu = this.model.get('menu');
        if (menu != null && menu.items != null)
            items = this.model.get('menu').items;
        menuUtils.renderMenuLevel($('nav ul'), 1, items);
        this.$('nav ul').show();

        return this;
      },

      selectMenu: function () {
          var items = null;
          var menu = this.model.get('menu');
          if (menu != null && menu.items != null)
              items = this.model.get('menu').items;
          menuUtils.selectMenu($('nav ul'), 1, items, location.hash);
          return this;
      }
    });
    return applicationView;
  });
