﻿define([
  'text!templates/searchcomp.htm',
  'models/searchcomp',
  'utils/view-utils', 'utils/datatable-utils',
   'backbone', 'mbinder', 'datatables'
],
  function (searchTemplate, model, utils , dtUtils) {
    var view = Backbone.View.extend({

        Initialize: function (param) {
            this.compiledTemplate = _.template(searchTemplate);
            this.options = param.options;
            this.layout = param.layout;
          this.render();
        
      },
      render: function () {
          var view = this;
         
          view.$el.html(view.compiledTemplate);
          $('.mainArticle').empty().html(view.$el);
          view.model = new model();
          view.model.set('helios', view.$('#helios option:selected').attr('value'))
          utils.viewError(view);

          view._modelBinder = new Backbone.ModelBinder();
          view._modelBinder.bind(view.model, view.$('.js-searchFields'));

          this.delegateEvents({
              'click .js-search': 'searchCompounds',
              //'click .viewplate': 'viewPlate',
              'keypress #compoundId': 'checkEnter'
          });

          if (this.options != null) {
              view.model.set('compoundId', utils.decodeString(view.options.compoundId));
              view.model.set('helios', view.options.helios);
              view.$('#helios').val(view.options.helios);
              _.delay(function () {
                  if (view.layout.lastCompResult == null ||
                      view.layout.lastCompoundId != view.model.get('compoundId') ||
                      view.layout.lastHelios != view.model.get('helios')
                      ) {
                      view.searchCompounds();
                  } else {
                      view.lastSearch = view.model.clone();
                      view.initTable(view.layout.lastCompResult);
                  }
              }, 10);
          }


        return this;
      },

      makePlateUrl: function (oData) {
          var view = this;
          var barcode = oData.barcode;
          var siteId = oData.siteId;;
          var databaseId = oData.databaseName;
          var col = oData.col;
          var row = oData.row;
          return "well/" + encodeURIComponent(barcode) + "/" + siteId + "/" + databaseId + "/" + row + "/" + col;
      },

      checkEnter: function (e) {
          var view = this;
          if (e.keyCode != 13) {
              return;
          }
          view.model.set('compoundId', view.$('#compoundId').val());
          view.searchCompounds();
      },

      /*viewPlate: function (e) {
          var view = this;
          var el = e.currentTarget;
          var view = this;
          var oData = dtUtils.getIdByButton(el, view.table);
          Backbone.history.navigate(view.makePlateUrl(oData), true);
      },*/
      calculateTableHeight: function () {
          var h = $(document).height();
          var minScrollH = 250, otherUsedH = 380;
          var scrollH = minScrollH;
          if (h > (minScrollH + otherUsedH)) {
              scrollH = h - otherUsedH;
          }
          return scrollH;
      },

      initTable: function(plates){
          var view = this;
          var tableHeight = view.calculateTableHeight();
          if (plates == null) plates = view.model.get('compounds');
          if (view.table == null) {
              var dt_config = {
                  bDeferRender: true,
                  sDom: "frtiS",
                  sScrollY: tableHeight,
                  scrollCollapse: true,
                  oScroller: {
                      trace: true,
                      displayBuffer: 20
                  },

                  bJQueryUI: true,
                  bAutoWidth: false,
                  bPaginate: true,
                  bFilter: true,
                  bProcessing: false,
                  bServerSide: false,
                  bRetrieve: true,
                  aaSorting: [ [0,'asc']],
                  aaData: plates,
                  oLanguage: {
                      sSearch: '',
                      sInfo: "_TOTAL_ entries to show"
                  },
                  aoColumns: [
                      {
                          mDataProp: 'barcode',
                          asSorting: ['asc', 'desc'],
                          bVisible: true,
                          bSearchable: true,
                          mRender: function (v, type, oData) {
                              if (oData.hasLink)
                                  return '<span class="viewplate"><a href="#' + view.makePlateUrl(oData) + '">' + v + '</a></span>';
                              else
                                  return v;
                          }
                      },
                      { mDataProp: 'id', asSorting: ['asc', 'desc'], bVisible: true, bSearchable: false },
                      { mDataProp: 'assay', asSorting: ['asc', 'desc'], bVisible: true, bSearchable: false },
                      { mDataProp: 'instrument', asSorting: ['asc', 'desc'], bVisible: true, bSearchable: false },
                      { mDataProp: 'row', asSorting: ['asc', 'desc'], bVisible: true, bSearchable: false, sType: 'numeric' },
                      { mDataProp: 'col', asSorting: ['asc', 'desc'], bVisible: true, bSearchable: false, sType: 'numeric' }
                  ]
              };
              view.$('#tblPlates').show();

              view.table = view.$('#tblPlates').dataTable(dt_config);
          } else {
              view.table.fnClearTable();
              view.table.fnAddData(plates);
              view.table.fnDraw();

          }
          _.delay(function () { view.table.fnAdjustColumnSizing(); }, 10);
          view.$('.dataTables_length').parent().hide();
          //view.$('.fg-toolbar').hide();
          if (plates != null && plates.length > 0) {
              view.$('#spanRowCount').text(plates.length);
              view.$('.rowcount').show();
              view.$('#spanRowCount').show();
              view.$('.norecords').hide();
              view.$('#tblPlates').show();
              view.$('#comoundResultWrapper').show();
              view.$('.alert.alert-danger.text-center.js-errorBox').hide();
          } else {
           
              view.$('.hide').show();
              view.$('.norecords').show();
              view.$('#tblPlates').hide();
              view.$('.fg-toolbar').hide();
          }


      },

      searchCompounds: function () {
          var view = this;
          view.$('#tblPlates').hide();
          view.$('#spanRowCount').hide();
          view.$('#comoundResultWrapper').hide();
          
          view.$('.hide').hide();
          view.$('.norecords').show();
     
          view.$('.fg-toolbar').hide();
          var beforeSaving = function(view) {
              var cid = $.trim(view.model.get('compoundId'));
              view.model.set('compoundId', cid);
          };
          var afterSaving = function(view) {

              view.layout.lastCompResult = view.model.get('compounds');
              view.layout.lastCompoundId = view.model.get('compoundId');
              view.layout.lastHelios = view.model.get('helios');
              view.layout.lastResult = null;
              view.layout.lastSiteId = null;
              view.layout.lastDatabaseId = null;
              view.layout.lastBarcode = null;
              utils.decodeModelField(view.model, 'compoundId');
              var navstr = "scomp/" + view.model.get('compoundId') + "/" + view.model.get('helios') ;
              if (location.hash == "#" + navstr) {
                  view.lastSearch = view.model.clone();
                  view.initTable(view.layout.lastCompResult);
              } else
                  Backbone.history.navigate(navstr, true);
          };
          utils.save(this, beforeSaving, afterSaving);
      },

      onclose: function () {
          this._modelBinder.unbind();
          Backbone.Validation.unbind(this);
      }
          
    });
    return view;
  });
