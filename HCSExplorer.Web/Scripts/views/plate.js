﻿define([
  'text!templates/plate.htm',
  'models/plate', 'models/platebarcode', 'views/platetab',
  'utils/view-utils', 'utils/date-utils', 'utils/model-utils',
   'backbone', 'mbinder', 'datatables', 'signalR', 'signalRHubs'
],
  function (searchTemplate, model, modelForBarcode, platetabView, utils, dateUtils, modelUtils) {

      var rowId = utils.getRowIdArray();
      var view = Backbone.View.extend({ 

          Initialize: function (par) {
              this.compiledTemplate = _.template(searchTemplate);
              this.options = par.options;
              this.readTestingParams();
              this.loadingImgSelector = '.loadingImgShort';
              this.render();
          },
          readTestingParams: function () {
              var params = this.options.testingParams;
              var obj = {};
              if (params) {
                  var threadsCount = modelUtils.getURLParameterImg('threads', params);
                  if (threadsCount) obj.threadsCount = threadsCount;
                  var hcsConnectTimeout = modelUtils.getURLParameterImg('hcsconnect_timeout', params);
                  if (hcsConnectTimeout) obj.hcsConnectTimeout = hcsConnectTimeout;
                  var clearCache = modelUtils.getURLParameterImg('clearcache', params);
                  if (clearCache) obj.clearCache = clearCache;
              }
              this.testingParams = obj;
          },
          render: function () {

              var view = this;
              view.changeStateLog = '';
              view.$el.html(view.compiledTemplate);

              if (IS_DEBUG_MODE) {
                  $('#tempTestDiv').show();
              }

              var tmodel = null;

              var fdata = null;
              if (view.options.barcode != null) {
                  tmodel = new modelForBarcode();
                  fdata = {
                      barcode: view.options.barcode,
                      siteId: view.options.siteId,
                      databaseId: view.options.databaseId
                  };
              } else {
                  tmodel = new model();
                  fdata = view.options;
              }
              view.ok = false;
              var response = tmodel.fetch({
                  async: true, data: fdata,
                  error: function (fmodel, response) {
                      view.showBegin(fmodel, response);
                  },
                  success: function (fmodel,response) {

                      view.showBegin(fmodel, response);
                      view.$('#date').text(dateUtils.toUserFormatDateTime(fmodel.get('date')));
                      var well = tmodel.get('well');
                      if (well != null) {
                          if (view.options.id == null) view.options.id = tmodel.get('id');
                          view.platetab = new platetabView({
                              el: view.$('#plate-form'),
                              fields: tmodel.get('fields'),
                              channels: tmodel.get('channels'),
                              plateOptions: view.options,
                              testingParams: view.testingParams,
                              format: tmodel.get('format'),
                              clearChannelparametersFunction: view.clearChannelParameters,
                              channelParametersFunction: view.setChannelParameters,
                              enableOverlayFunction: view.checkOverlayImageCondition,
                              barcode: tmodel.get('barcode'),
                              path: tmodel.get('path'),
                              name: tmodel.get('name'),
                              date: tmodel.get('date'),
                              versions: tmodel.get('versions'),
                              zIndexCount: tmodel.get('zIndexCount'),
                              timepointCount: tmodel.get('timepointCount'),
                          });


                          if (well.row == 0 && well.col == 0) {
                              if (view.options.row == null) {
                                  well.row = 1;
                                  well.col = 1;
                              } else {
                                  well.row = parseInt(view.options.row);
                                  well.col = parseInt(view.options.col);

                              }
                          }

                          view.model = tmodel;
                          view.loadPlateLayout(well.row, well.col);
                          view.highlightSelectWell(well.row, well.col);

                          var versions = view.model.get('versions');
                          if (versions != null && versions.length > 0) {
                              utils.createSelectSimple(view.$('#version'), versions, false);
                              if (view.$('#version option[value=raw]').length > 0) {
                                  view.$('#version').val("raw");
                                  view.model.set('version', "raw");

                              } else {
                                  view.model.set('version', versions[0]);
                              }
                          } else {
                              view.$('.js-version').hide();
                          }

                          var tcount = view.model.get('timepointCount');
                          if (tcount != null && tcount > 0) {
                              utils.createSelectSimpleInt(view.$('#timepoint'), tcount, false);
                              view.model.set('timepoint', 1);
                          } else {
                              view.$('.js-timepoint').hide();
                          }

                          var zcount = view.model.get('zIndexCount');
                          if (zcount != null && zcount > 0) {
                              utils.createSelectSimpleInt(view.$('#zIndex'), zcount, false);
                              view.model.set('zIndex', 1);
                          } else {
                              view.$('.js-zindex').hide();
                          }

                          utils.viewError(view);

                          view.loadImageLayout();
                          view._modelBinder = new Backbone.ModelBinder();
                          view._modelBinder.bind(view.model, view.$('.js-databinding'));
                          view.inRequest = false;

                          //init SignalR  && get images for 1,1
                          view.initSignalR();
                          view.delegateEvents({
                              'click  #plateLayout td': 'wellClick',
                              'click  #well_up': 'wellUp',
                              'click  #well_down': 'wellDown',
                              'click  #well_left': 'wellLeft',
                              'click  #well_right': 'wellRight',
                              'click #imageUpdate': 'wellImagesUpdate',
                              'click img.trumb': 'imageClick'
                          });

                          $(document).on('keydown', function (e) { view.checkArrows(e) });

                          view.ok = true;
                      }
                  }
              });
              return this;
          },

          showBegin: function (fmodel, response) {
              var view = this;
              $('.mainArticle').empty().html(view.$el);
              utils.wasFetchError(view, response);

              // TODO: handle "change tab" event
              /*$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                  // TODO: check if "on" is binded only one time.
                  var selectedTabIndex = e.target.hash == "#plate-form" ? 1 : 0;
                  view.selectedTabIndex = selectedTabIndex;
                  if (view.platetab && selectedTabIndex == 1) {
                      //view.platetab.adjustBodyWidth();
                      view.platetab.updateExportPdfButton();
                  } else if (view.platetab) {
                      //view.platetab.clearBodyWidth();
                  }
              });*/
          },

          checkArrows: function (e) {
              var view = this;
              if (view.selectedTabIndex == 1) return;
              if (!view.inRequest && view.platetab!=null && !view.platetab.inRequest) {
                  if (e.keyCode == 37) { view.wellLeft(); e.preventDefault(); } //prevent page scrolling by key events
                  if (e.keyCode == 38) { view.wellUp(); e.preventDefault(); }
                  if (e.keyCode == 39) { view.wellRight(); e.preventDefault(); }
                  if (e.keyCode == 40) { view.wellDown(); e.preventDefault(); }
              }
          },

          imageClick: function (ev) {
              var view = this;
              var barcode = view.model.get('barcode');
              var path = view.model.get('path');
              var hdnUrls = $(ev.target).siblings(".getImgUrl");
              if (hdnUrls.length == 1) {
                  var imgUrl = hdnUrls[0].value;
                  var pathname = window.location.pathname;
                  var length = pathname.length;
                  var delimiter = "";
                  if (length > 0) {
                      if (pathname.charAt(length - 1) != "/") {
                          delimiter = "/";
                      }
                  }
                  window.open(pathname + delimiter + "image?src=" + encodeURIComponent(imgUrl.replace(/&thumb=\d+/i, "")) + "&barcode=" + encodeURIComponent(barcode) + "&platePath=" + encodeURIComponent(path));
              }
          },

          wellImagesUpdate: function () {
              var view = this;
              view.clearChannelParameters(model);
             view.loadWellImages(view.curRow, view.curCol);
          },
         
          initSignalR: function () {
              var view = this;
              view.imagesLoader = $.connection.imagesLoader;

              // Add client-side hub methods that the server will call
              $.extend(view.imagesLoader.client, {
                  progress: function (matrix) {
                      if (view.inRequest) {
                          view.$('#imgLayoutNotification').text(matrix.Progress);
                          view.updateImageMatrix(matrix.Well.Row, matrix.Well.Col, matrix.ImagePathes, matrix.GetImageUrls);
                      }
                  },
                  plateProgress: function (matrix) {
                      view.platetab.progress(matrix);
                  },
                  error: function (errormessage) {

                  }
              });
              $.connection.imagesLoader.logging = true;

              $.connection.hub.start({ transport: ['longPolling'] }).pipe(function () {
                  view.loadWellImages(view.curRow, view.curCol);
                  $('#tempConnectionId').html($.connection.hub.id);
              });
              $.connection.hub.stateChanged(function (change) {
                  view.changeStateLog += ' ' + change.newState;
                  $('#tempConnectionId').html($('#tempConnectionId').html() + view.changeStateLog);
                  if (change.newState === $.signalR.connectionState.disconnected) { // disconnected (due to long polling timeout issue)
                    if (view.pageClosed == true) {
                        return;
                    }
                    setTimeout(function () {
                        $.connection.hub.start({ transport: ['longPolling'] }).pipe(function () {
                            if (view.platetab && view.platetab.inRequest == true) { // plate loading is in progress
                                view.platetab.loadPlateImages(true);
                            }
                            $('#tempConnectionId').html('reconnected, ' + $.connection.hub.id + view.changeStateLog);
                        });
                    }, 1000);
                  }
              });

              $.connection.hub.reconnected(function () { // app pool recycling
                  if (view.platetab && view.platetab.inRequest == true) {
                      view.platetab.loadPlateImages(true);
                  }
              });
              view.platetab.imagesLoader = view.imagesLoader;
          },
          hideProgress: function(){
              this.$('#imgLayoutNotification').text('');
          },
          setChannelParameters: function(model)
          {
              var view = this;
              var chs = model.channels;
              var htds = $('#imageLayout tr:first td');
              for (var i = 0; i < chs.length; i++) {
                  var bOptions = $(htds[i + 1]).find('[data-id="b_options"]');
                  var bOptionsValue = bOptions.find('option:selected').attr('value');
                  var auto = '';
                  var levelValue;
                  var wPoint = $('#whitepoint').val();
                  var bPoint = $('#blackpoint').val();
                  var bright = '';//$(htds[i + 1]).find('input').val();
                  if (bOptionsValue == 'auto') {
                      auto = 'true';
                      //is it checked?
                      //auto = $('#auto:checkbox:checked').length > 0;
                        }
                  else if (bOptionsValue == 'brightness') { //parse out the levels from the input boxes
                      bright = $(htds[i + 1]).find('input').val();
                      if (bright == 'on') {
                          bright = '';
                      }
                      auto = 'false';
                      //levelValue = 'true';
                      //bright = '';
                  }
                  //else if (bOptionsValue == 'level') { //parse out the levels from the input boxes
                  //    var wPoint = $('#whitepoint').val();
                  //    var bPoint = $('#blackpoint').val();
                  //    auto = 'false';
                  //    //levelValue = 'true';
                  //    bright = '';
                  //}

                     
                  model.channelParameters.push(
                      {
                          auto: auto,
                          whitepoint: wPoint,
                          blackpoint: bPoint,
                          level: levelValue,
                          channel: chs[i],
                          brightness:  bright,//$(htds[i + 1]).find('input').val(),
                          color: $(htds[i + 1]).find('select option:selected').attr('value'),
                      }
                  );
              }

              return model;
          },
          clearChannelParameters: function () {
              
              var view = this;
              var chs = view.model.get('channels');
              var model = {
                  well: { row: view.curRow, col: view.curCol },
                  siteId: view.options.siteId,
                  databaseId: view.options.databaseId,
                  id: view.model.get('id'),
                  sizeId: view.$('#wellViewSize option:selected').attr('value'),
                  channels: chs,
                  enableOverlay: view.checkOverlayImageCondition(),
                  fields: view.model.get('fields'),
                  channelParameters: [],
                  invert: (view.$('#wellViewInvert:checked').length > 0),
                  version: view.model.get('version'),
                  timepoint: view.model.get('timepoint'),
                  zIndex: view.model.get('zIndex')
              };
              //for (var i = 0; i < chs.length; i++) {
              //    model.channelParameters.push(
              //        {
              //            auto: '',
              //            whitepoint: '',
              //            blackpoint: '',
              //            level: '',
              //            channel: chs[i],
              //            brightness: '', 
              //            color: 'Grayscale',
              //        }
              //    );
              //}

              //return model;
          },
          loadWellImages: function (r, c) {
              var view = this;
              var chs = view.model.get('channels');

              view.$('#imageLayout img').remove();
              view.$('#imageLayout .getImgUrl').remove();
              var imgsize = view.$('#wellViewSize option:selected').attr('value');
              view.$('#imageLayout').removeClass('S').removeClass('M').removeClass('L').addClass(imgsize);
     
              var model = {
                  well: { row: r, col: c },
                  siteId: view.options.siteId,
                  databaseId: view.options.databaseId,
                  id: view.model.get('id'),
                  sizeId: view.$('#wellViewSize option:selected').attr('value'),
                  channels: chs,
                  enableOverlay: view.checkOverlayImageCondition(),
                  fields: view.model.get('fields'),
                  channelParameters: [],
                  invert: (view.$('#wellViewInvert:checked').length > 0),
                  version: view.model.get('version'),
                  timepoint: view.model.get('timepoint'),
                  zIndex: view.model.get('zIndex')
              };
              view.setChannelParameters(model);

              // Start the connection
              utils.disableView(view);
              view.inRequest = true;
              view.imagesLoader.server.wellImagesLoad(model)
               .done(function (matrix) {
                   view.updateImageMatrix(matrix.Well.Row, matrix.Well.Col, matrix.ImagePathes, matrix.GetImageUrls);
                   utils.enableView(view);
                   view.platetab.updateExportPdfButton();
                   view.platetab.checkChannelWithPopup(null, false);
                   view.hideProgress();
                   view.inRequest = false;
               }).fail(function (err) {
                   if (!view.pageClosed) {
                       alert(err);
                       utils.enableView(view);
                       view.platetab.updateExportPdfButton();
                       view.platetab.checkChannelWithPopup(null, false);
                   }
               });

          },

          updateImageMatrix: function (r, c, imgPathes, GetImageUrls) {
              var view = this;
              if (r == view.curRow && c == view.curCol) {
                  var tds = view.$('#imageLayout .withimg');
                  if (tds.length != imgPathes.length) {
                      alert(tds.length + "!=" + length);
                  }
                  for (var i = 0; i < imgPathes.length; i++) {
                      if (imgPathes[i] != null && $(tds[i]).find('img').length == 0) {
                          $(tds[i]).append('<img class="trumb" src="' + imgPathes[i] + '" />');
                          if (GetImageUrls[i] != null) {
                              $(tds[i]).append('<input type="hidden" class="getImgUrl" value="' + GetImageUrls[i] + '" />');
                          }
                      }
                  }
              }
          },
          /*
          *  According to overlay image's conditons set overlay column enable or disable (M.I.)
          */
          checkOverlayImageCondition: function () {
              var view = this;
              var imagetbl = $("#imageLayout");
              var imageColorDropDownArray = imagetbl.find("select");

              // count number channels for overlay image requirements
              var channelCount = imageColorDropDownArray.length;
              if (channelCount < 2) {

                  //return;
                  return false;
              }

              // conditions for overlay images
              var grayscale = 0;
              var red = 0;
              var green = 0;
              var blue = 0;
              var cyan = 0;
              var magenta = 0;
              var yellow = 0;

              for (var i = 0; i < channelCount; ++i) {

                  if (imageColorDropDownArray[i].value == "Grayscale") {

                      grayscale++;
                  }
                  if (imageColorDropDownArray[i].value == "Red") {

                      red++;
                  }
                  if (imageColorDropDownArray[i].value == "Green") {

                      green++;
                  }
                  if (imageColorDropDownArray[i].value == "Blue") {

                      blue++;
                  }
                  if (imageColorDropDownArray[i].value == "Cyan") {

                      cyan++;
                  }
                  if (imageColorDropDownArray[i].value == "Magenta") {

                      magenta++;
                  }
                  if (imageColorDropDownArray[i].value == "Yellow") {

                      yellow++;
                  }
              }

              // if all color options have selected grayscale then disable overlay option
              if ((grayscale == channelCount || grayscale == (channelCount - 1)) ||
                  (blue > 1 || green > 1 || red > 1 || cyan > 1 || magenta > 1 || yellow > 1)) {
                  return false;
              }

              return true;
          },


          wellUp: function (e) {
              var view = this;
              if (!view.inRequest && view.platetab != null && !view.platetab.inRequest) {
                  var r = view.curRow;
                  var c = view.curCol;
                  if (r > 1) {
                      view.highlightSelectWell(r - 1, c);
                      view.wellImagesUpdate();
                  }
              }
          },
          wellDown: function (e) {
              var view = this;
              if (!view.inRequest && view.platetab != null && !view.platetab.inRequest) {
                  var r = view.curRow;
                  var c = view.curCol;
                  if (r < view.rows) {
                      view.highlightSelectWell(r + 1, c);
                      view.wellImagesUpdate();
                  }
              }
          },
          wellLeft: function (e) {
              var view = this;
              if (!view.inRequest && view.platetab != null && !view.platetab.inRequest) {
                  var r = view.curRow;
                  var c = view.curCol;
                  if (c > 1) {
                      view.highlightSelectWell(r, c - 1);
                      view.wellImagesUpdate();
                  }
              }
          },
          wellRight: function (e) {
              var view = this;
              if (!view.inRequest && view.platetab != null && !view.platetab.inRequest) {
                  var r = view.curRow;
                  var c = view.curCol;
                  if (c < view.cols) {
                      view.highlightSelectWell(r, c + 1);
                      view.wellImagesUpdate();
                  }
              }
          },
          wellClick: function (e) {
              var view = this;
              if (!view.inRequest && view.platetab != null && !view.platetab.inRequest) {
                  var td = $(e.currentTarget);
                  var c = td[0].cellIndex;
                  var r = td.parent()[0].rowIndex;
                  if (c == 0 || r == 0) return;
                  if (view.curRow == r && view.curCol == c) return;
                  view.highlightSelectWell(r, c);
                  view.wellImagesUpdate();
              }
          },

          highlightSelectWell: function (r, c) {

              var view = this;

              view.curRow = r;
              view.curCol = c;

              // update current row and col

              var tbl = view.$("#plateLayout");
              tbl.find('.selected_well').html('').removeClass('selected_well');
              tbl.find('.row_col').removeClass('row_col');

              // hightlight row for positioning (M.I.)
              for (var j = 0; j < r; ++j) {

                  view.$("#plateLayout #" + j + "-" + c).addClass('row_col');
                 
              }
              // hightlight column for positioning (M.I.)
              for (var k = 0; k < c; ++k) {
                  view.$("#plateLayout #" + r + "-" + k).addClass('row_col');
              }

              // hightlight selected well
              var selectedWell = view.$("#plateLayout #" + r + "-" + c);
              selectedWell.html(rowId[r - 1] + c);
              selectedWell.addClass('selected_well');
              view.model.set('selectedWell', rowId[r - 1] + c);
  
          },

          loadPlateLayout: function (selrow, selcol) {
              var view = this;
              view.rows = utils.getRowCount(view.model.get("format"));
              view.cols = utils.getColCount(view.model.get("format"));
              var numrows = view.rows + 1; // Add extra row for column number
              var numcols = view.cols + 1; // Add extra column for row number
              var format = view.model.get("format");
              var tbl = view.$('#plateLayout');

              // configure table cell size based on plate format
              var tclass = 'tdw20';
              if (format == 96) {
                  tclass = 'tdw15';
              }
              else if (format == 384) {
                  tclass = 'tdw7';
              }
              else {
                  if (format == 1536) {
                      tclass = 'tdw7';
                  }
              }
              tbl.addClass(tclass);

              for (var r = 0; r < numrows; r++) {
                  var tr = $('<tr></tr>');
                  for (var c = 0; c < numcols; c++) {
                      var tc = $('<td></td>');
                      tc.attr('id', r + "-" + c);
                      if (r == 0 && c == 0) {
                          tc.html("&nbsp");
                          tr.append(tc);
                          continue;
                      }
                      if (r == 0 && c != 0) {
                          tc.html(c);
                          tr.append(tc);
                          continue;
                      }
                      if (c == 0 && r != 0) {
                          tc.html(rowId[r - 1]);
                          tr.append(tc);
                          continue;
                      }

                      if (r == selrow && c == selcol) {
                          tc.addClass("selected_well");
                      }

                      tc.attr('title', rowId[r - 1] + c);
                      tc.attr('data-toggle', 'tooltip');
                      tr.append(tc);
                  }
                  tbl.append(tr);
              }
          },
          loadImageLayout: function () {

              var view = this;

              var imagetbl = view.$('#imageLayout');
              var chRow = $('<tr></tr>');
              var channels = view.model.get('channels');
              for (var i = 0; i <= channels.length; i++) {
                  var ch = '';
                  var tc = $('<td></td>');
                  if (i == 0) {
                      tc.text('');
                  }
                  else {
                      ch = "Ch:" + channels[i - 1];
                      var chColorDropDownId = '';
                      if (/\S/.test(channels[i - 1])) {
                          //white space in the channel name
                          // replace whitespace with "_" to prevent weird behavior
                          var res = channels[i - 1].replace(" ", "_");
                          chColorDropDownId = res + "Color";
                      } else {
                          chColorDropDownId = channels[i - 1] + "Color";
                      }
                      
                      // td holds channel name and drop down menu for color and brightness selections
                      var strch = '<p>' + ch + '</p><div class="form-inline"><p><select class="form-control input-sm">' +
                        '<option value="Grayscale" selected="selected">Grayscale</option>' +
                        '<option value="Red">Red</option>' +
                        '<option value="Green">Green</option>' +
                        '<option value="Blue">Blue</option>' +
                        '<option value="Cyan">Cyan</option>' +
                        '<option value="Magenta">Magenta</option>' +
                        '<option value="Yellow">Yellow</option></select></p>' +
                        '<p>'+
                        '<select id="' + chColorDropDownId + '" class="form-control input-sm b-options" data-id="b_options">' +
                        '<option value="brightness">Brightness X</option>' +
                        '<option value="auto">Auto-Contrast</option>' +
                        //'<option value="level">Levels</option>'+
                        '</select>' +
                        '</p><div id="options' + chColorDropDownId + '"></div>' +
                        '</div>';
                      tc.append(strch);
                  }
                  chRow.append(tc);
              }

              if (channels.length > 1) {
                  var tableCellForOverlayHeader = $('<td></td>');
                  tableCellForOverlayHeader.text("Ch:Overlay");
                  chRow.append(tableCellForOverlayHeader);
              }

              imagetbl.append(chRow);

              // hidden field with the number of images
              var fields = view.model.get('fields');
              var numOfImages = fields.length * channels.length;
              view.imgCount = numOfImages;

              // arrange images in matrix according to image parameters
              for (var r = 0; r < fields.length; r++) {
                  var field = fields[r];
                  var tr = $('<tr></tr>');

                  // create a table cell to label field
                  var tcLabel = $('<td>Field ' + field + '</td>');
                  tr.append(tcLabel);

                  for (var c = 0; c < channels.length; c++) {
                      var tc = $('<td class="withimg"></td>');
                      tr.append(tc);
                  }

                  if (channels.length > 1) {
                      var tableCellForOverlayImage = $('<td class="withimg"></td>');
                      tr.append(tableCellForOverlayImage);
                  }

                  imagetbl.append(tr);
              }
              //create the brightness option beneath the dropdown according to selection
              $('.b-options').bind('change', function () {
                  //console.log('clicked: ' + this.id);
                  var selected = this.id;
                  var select = $('#brightnessOptions');
                  var option = $('#'+selected + " option:selected").val();
                  if (option == 'brightness') {
                      $('#options' + this.id).empty();
                      var optionHTML = '<input type="text" id="brightnessCh_' + selected + '" class="form-control input-sm" style="width:40px;" />';
                      $('#options' + this.id).append(optionHTML);
                  }
                  if (option == 'auto') {
                      $('#options' + this.id).empty();
                      var optionHTML = '<div id="autoCh_' + selected + '" style="height:22px;"></div>';
                      $('#options' + this.id).append(optionHTML);
                  }
                  //if (option == 'level') {
                  //    $('#options' + this.id).empty();
                  //    var optionHTML = '<p>White <input type="text" name="whitepoint" id="whitepoint" class="form-control input-sm" style="width:40px;" />' +
                  //        ' Black <input type="text" name="blackpoint" id="blackpoint" class="form-control input-sm" style="width:40px;" /></p>';
                  //    $('#options' + this.id).append(optionHTML);
                  //}
              })
              $('.b-options').trigger('change'); //load event gets triggered on change
          },
          onclose: function () {
              $('#tempTestDiv').hide();
              var view = this;
              view.pageClosed = true;
              if (view.ok) {
                  this._modelBinder.unbind();
                  Backbone.Validation.unbind(this);
                  this.platetab.close();
                  $.connection.hub.stop();
                  $(document).off('keydown');
              }
          }

      });
      return view;
  });
