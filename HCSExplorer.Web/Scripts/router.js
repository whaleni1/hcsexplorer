﻿define(['views/app-layout', 'models/app-layout', 'utils/view-utils', 'backbone'],
function (appView, appModel, viewUtils) {
  var appRouter = Backbone.Router.extend({
    routes: {
        '': 'searchPlate',
        'search/:site/:db/*barcode': 'searchPlateParams',
        'plate/:id/:site/:db(/:testingParams)': 'plateView',
        'well/:barcode/:site/:db/:row/:col': 'wellView',
        'scomp': 'searchCompound',
        'scomp/:compoundId/:helios': 'searchCompoundParams',
        'run/*id': 'runDetails',
        'runs': 'runList',
        '*actions': 'errorView'
    },
    routeController: function (router, name, args) {     
      viewUtils.mainArticleLoading();

      _.delay(function () {
        switch(name) {         
            case 'searchPlate':
            require(['views/search'],
                   function (searchPage) {
                       router.loadView(searchPage, {layout:router.layout});
                   });
            break;
            case 'searchPlateParams':

                var options = {
                    barcode: decodeURIComponent(args[2]),
                    siteId: args[0],    
                    databaseId: args[1]

                };
            require(['views/search'],
                   function (searchPage) {
                       router.loadView(searchPage, { layout: router.layout, options: options });
                   });
            break;
     
            case 'plateView':
                var options = {
                    id: args[0],
                    siteId: args[1],
                    databaseId: args[2],
                    testingParams: null
                };
                if (args.length > 3 && args[3]) {
                    options.testingParams = args[3];
                }
                require(['views/plate'],
                       function (platePage) {
                           router.createView(platePage, { options: options });
                       });
                break;
            case 'runDetails':
                var options = {
                    id: args[0]
                };

                require(['views/rundetails'],
                       function (runPage) {
                           router.createView(runPage, { options: options });
                       });
                break;
            case 'wellView':
                var options = {
                    barcode: args[0],
                    siteId: args[1],
                    databaseId: args[2],
                    row: args[3],
                    col: args[4]

                };
                require(['views/plate'],
                       function (platePage) {
                           router.createView(platePage, { options: options });
                       });
                break;
            case 'searchCompound':
                require(['views/searchcomp'],
                       function (searchPage) {
                           router.createView(searchPage, { layout: router.layout});
                       });
                break;
            case 'searchCompoundParams':

                var options = {
                    compoundId: args[0],
                    helios: args[1]
                };

                require(['views/searchcomp'],
                       function (searchPage) {
                           router.createView(searchPage, { layout: router.layout, options: options });
                       });
                break;
            case 'runList':
                require(['views/runlist'],
                       function (runPage) {
                           router.createView(runPage, { layout: router.layout });
                       });
                break;
            default:
            require(['views/error-page'],
                    function (errorPage) {
                      var error = { header: '404 Error Page not found', message: 'Address was typed incorrectly, you don\'t have access to this page or the page no longer exists.' };
                      router.createView(errorPage, error);
                    });
            break;
        }
      }, 1, [router, name, args]);
    },
   
    loadView: function (view, params) {
        return this.createView(view, params);
    },
    createView: function (view, param) {

      this.layoutView.selectMenu();
      if(this.currentView) {
        this.currentView.close();
      }
      this.currentView = new view(param);
      return this.currentView;
    },
    
    updateLayout: function (formToken, callback) {
      this.layout.off('error');
      var router = this;
      this.layout.fetch({ async: false }).error(function (e) {
        var result = viewUtils.errorText(e);
        require(['views/error-page'],
                function (errorPage) {
                  var error = { header: 'Error', message: result.message + '.' };
                  router.createView(errorPage, error);
                });
      }).success(function () { callback(); });
    }
  });

  var Initialize = function () {
    var router = new appRouter;

    // creating layout data object
    router.layout = new appModel;
    viewUtils.errorPopup(router.layout);
    router.layout.fetch({ async: false });

    router.bind("all", function (route, router) {
        viewUtils.closeAndDestroyAllDialogs();
    });

    // creating layout view
    router.layoutView = new appView({ model: router.layout });

    // binding router events to properly render layout view
    Backbone.history.on('route', router.layoutView.renderSimple, router.layoutView);
    Backbone.history.on('route', router.routeController, router);

    Backbone.history.start();
  };
  return {
    Initialize: Initialize
  };
});
