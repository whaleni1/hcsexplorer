﻿define(['router', 'backbone'],
  function (router) {
    var Initialize = function () {
      Backbone.View.prototype.close = function () {
        this.remove();
        this.unbind();
        if (this.onclose) {
          var param = (this.oncloseParam) ? this.oncloseParam : null;
          this.onclose(param);
        }
      };
      router.Initialize();

      // Initialize tootlips
      $('body').tooltip({
          selector: '[data-toggle="tooltip"]',
          container: 'body'
      });
    };
    return {
      Initialize: Initialize
    };
  });
