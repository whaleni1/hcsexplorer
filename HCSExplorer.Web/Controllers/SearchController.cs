﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.Logger;
using HCSExplorer.Services;
using HCSExplorer.Web.Helpers;
using HCSExplorer.Web.Models;

namespace HCSExplorer.Web.Controllers
{
    /// <summary>
    /// SearchController
    /// </summary>
    public class SearchController : ApiController
    {
        private IExecutionContext executionContext;
        private IDataService dataService;
        private ConnectHelper _connectHelper = new ConnectHelper();

        /// <summary>
        /// SearchController
        /// </summary>
        /// <param name="executionContext"></param>
        /// <param name="dataService"></param>
        public SearchController(IExecutionContext executionContext , IDataService dataService)
        {
            this.executionContext = executionContext;
            this.dataService = dataService;
        }

        /// <summary>
        /// Plates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Plates")]
        public HttpResponseMessage Plates()
        {
            try
            {
                FileLogger.Info(GetType().Name + ":Plates");

                var model = new SearchPlatesModel();
                model.Sites = dataService.GetSites();
                model.LocalSite = ApplicationLayoutHelper.GetLocalSite();

                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":Plates, " + ex.Message);
                new CException(this.GetType().Name + ":Plates, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// Databases
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Databases")]
        public HttpResponseMessage Databases(string id)
        {
            try
            {
                FileLogger.Info(GetType().Name + ":GetDatabases");

                return Request.CreateResponse(HttpStatusCode.OK , new { Id = id , Databases = _connectHelper.GetDatabases(id) });
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":Databases, " + ex.Message);
                new CException(this.GetType().Name + ":Databases, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// SearchPlates
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Plates")]
        public HttpResponseMessage SearchPlates(SearchPlatesModel model)
        {
            FileLogger.Info(GetType().Name + ":SearchPlates");

            if (model.Barcode != null && model.Barcode.Length < 2)
            {
                Error error = new Error();
                error.ValidationErrors = new Error.ValidationDescriptor[]{
                      new  Error.ValidationDescriptor{
                       ElementName="barcode",
                       Description = new string[]{Resources.Application.SearchPlatesError}
                      }
                    };

                return Request.CreateResponse(HttpStatusCode.InternalServerError , error);
            }

            try
            {
#if !DEBUG
                if (model.SiteId.Equals(ApplicationLayoutHelper.GetLocalSite()))
                {
#endif
                    model.Plates = _connectHelper.GetPlates(model);
#if !DEBUG
                }
                else
                {
                    model.Plates = dataService.GetPlates(executionContext , model.SiteId , model.DatabaseId ,
                                                         HttpContext.Current.Server.UrlEncode(model.Barcode));
                }
#endif
                // if barcode search finds no results, search by path
                if (model.Plates == null || (model.Plates.Count == 0))
                {
#if !DEBUG
                    if (model.SiteId.Equals(ApplicationLayoutHelper.GetLocalSite()))
                    {
#endif
                        model.Plates = _connectHelper.GetPlatesByPath(model);
#if !DEBUG
                    }
                    else
                    {
                        model.Plates = dataService.GetPlatesByPath(executionContext , model.SiteId , model.DatabaseId ,
                                                             HttpContext.Current.Server.UrlEncode(model.Barcode));
                    }
#endif
                }

                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":SearchPlates, " + ex.Message);
                new CException(this.GetType().Name + ":SearchPlates, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// SearchCompounds
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Compounds")]
        public HttpResponseMessage SearchCompounds(SearchCompoundModel model)
        {
            try
            {
                FileLogger.Info(GetType().Name + ":SearchCompounds");

                // Run compound search through local library
                model.Compounds = _connectHelper.GetCompounds(model.Helios , model.CompoundId);

                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":SearchCompounds, " + ex.Message);
                new CException(this.GetType().Name + ":SearchCompounds, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }
    }
}