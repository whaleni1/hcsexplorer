﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.DTO;
using HCS.Shared.Logger;
using HCSExplorer.Services;
using HCSExplorer.Web.Helpers;
using HCSExplorer.Web.Models;

namespace HCSExplorer.Web.Controllers
{
    /// <summary>
    /// BrowseController
    /// </summary>
    public class BrowseController : ApiController
    {
        private IExecutionContext executionContext;
        private IDataService dataService;
        private static PathsConfigSection _pathsConfig = null;
        private ConnectHelper _connectHelper = new ConnectHelper();

        /// <summary>
        /// PathsConfig
        /// </summary>
        public static PathsConfigSection PathsConfig
        {
            get
            {
                if (_pathsConfig == null)
                {
                    _pathsConfig = (PathsConfigSection) WebConfigurationManager.GetSection("pathsConfigSection");
                }
                return _pathsConfig;
            }
        }

        /// <summary>
        /// BrowseController
        /// </summary>
        /// <param name="executionContext"></param>
        /// <param name="dataService"></param>
        public BrowseController(IExecutionContext executionContext , IDataService dataService)
        {
            this.executionContext = executionContext;
            this.dataService = dataService;
        }

        /// <summary>
        /// Databases
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Databases")]
        public HttpResponseMessage Databases(string id)
        {
            try
            {
                FileLogger.Info(GetType().Name + ":Databases id = " + id);

                //IList<ObjectBase> databases = dataService.GetDatabases(executionContext , id);
                IList<ObjectBase> filtered = _connectHelper.GetDatabases(id);

                return Request.CreateResponse(HttpStatusCode.OK , new { Id = id , Databases = filtered });
            }
            catch (Exception eX)
            {
                FileLogger.Error(this.GetType().Name + ":Databases, " + eX.Message , eX);
                new CException(this.GetType().Name + ":Databases, " + eX.Message , eX);

                return Request.CreateResponse(HttpStatusCode.InternalServerError ,
                                              new Error
                                              {
                                                  Message = eX.Message
                                              });
            }
        }

        /// <summary>
        /// GetPaths
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Paths")]
        public HttpResponseMessage GetPaths(SearchPlatesModel model)
        {
            try
            {
                string siteId = model.SiteId;
                string dbId = model.DatabaseId;
                var ds = PathsConfig.Sites[siteId].DataSources[dbId];

                FileLogger.Info(GetType().Name + ":GetPaths site = " + siteId + " dbId = " + dbId);

                if (ds == null)
                {
                    throw new ApplicationException(string.Format("Unable to find paths for Site = {0} and DataSource = {1}" , siteId , dbId));
                }
                PathsConfigSection.PathsConfigPathsCollection availablePaths = ds.PathsObjects;

                List<ObjectBase> res = (from PathsConfigSection.PathsConfigPathElement pathObj in availablePaths select new ObjectBase { Id = pathObj.DataLocation , Name = pathObj.Path }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK , new { Paths = res });
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":GetPaths, " + ex.Message , ex);
                new CException(this.GetType().Name + ":GetPaths, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError ,
                                              new Error
                                              {
                                                  Message = ex.Message
                                              });
            }
        }
    }
}