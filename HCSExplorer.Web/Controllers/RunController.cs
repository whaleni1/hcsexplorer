﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using HCS.DataAccess.Databases;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.DTO;
using HCS.Shared.Filtering;
using HCS.Shared.Logger;
using HCSExplorer.Services;
using HCSExplorer.Services.MC;
using HCSExplorer.Web.Consts;
using HCSExplorer.Web.Filters;
using HCSExplorer.Web.Helpers;
using HCSExplorer.Web.Models;
using Newtonsoft.Json;

namespace HCSExplorer.Web.Controllers
{
    public class RunController : ApiController
    {
        #region Member Variables and Constants

        const string csSITE_BS = "BS";
        const string csSITE_CA = "CA";
        const string csSITE_ZJ = "ZJ";

        private IExecutionContext executionContext;
        private IDataService dataService;
        private IMCService mcService;
        private ConnectHelper _connectHelper = new ConnectHelper();

        private const int RowsPerPage = 30;
        private const int RowsPerPageMy = 10;

        #endregion

        /// <summary>
        /// ProtocolsShareFolder
        /// </summary>
        private static string ProtocolsShareFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["ProtocolFilesStorage"];
            }
        }

        /// <summary>
        /// GetRunningInputShareFolder
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private static string GetRunningInputShareFolder(string siteId)
        {
            if (csSITE_CA.Equals(siteId))
            {
                return ConfigurationManager.AppSettings["CARunningInputShareFolder"];
            }
            else if (csSITE_BS.Equals(siteId))
            {
                return ConfigurationManager.AppSettings["BSRunningInputShareFolder"];
            }
            else if (csSITE_ZJ.Equals(siteId))
            {
                return ConfigurationManager.AppSettings["ZJRunningInputShareFolder"];
            }
            else
            {
                throw new ApplicationException(string.Format("SiteID = {0} is incorrect." , siteId));
            }
        }

        private static PathsConfigSection _pathsConfig = null;

        /// <summary>
        /// PathsConfig
        /// </summary>
        private static PathsConfigSection PathsConfig
        {
            get
            {
                if (null == _pathsConfig)
                {
                    _pathsConfig = (PathsConfigSection) WebConfigurationManager.GetSection("pathsConfigSection");
                }
                return _pathsConfig;
            }
        }

        /// <summary>
        /// TmpFilesPath
        /// </summary>
        private static string TmpFilesPath
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/" + WebApp.CachedImagesDir);
            }
        }

        /// <summary>
        /// RunController
        /// </summary>
        /// <param name="executionContext"></param>
        /// <param name="dataService"></param>
        /// <param name="mcService"></param>
        public RunController(IExecutionContext executionContext , IDataService dataService , IMCService mcService)
        {
            this.executionContext = executionContext;
            this.dataService = dataService;
            this.mcService = mcService;
        }

        /// <summary>
        /// GetRuns
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Runs")]
        public HttpResponseMessage GetRuns()
        {
            try
            {
                FileLogger.Debug("RunController: getRuns()");

                RunListPagingModel myRunsPaging = new RunListPagingModel
                {
                    Id = Guid.NewGuid() ,
                    Paging = new Paging
                    {
                        From = 0 ,
                        To = RowsPerPageMy ,
                        SortedBy = "lastUpdated" ,
                        SortOrder = "desc"
                    } ,
                    Filter = new RunFilter { My = true }
                };

                RunListPagingModel othersRunsPaging = new RunListPagingModel
                {
                    Id = Guid.NewGuid() ,
                    Paging = new Paging
                    {
                        From = 0 ,
                        To = RowsPerPage ,
                        SortedBy = "lastUpdated" ,
                        SortOrder = "desc"
                    } ,
                    Filter = new RunFilter { My = false }
                };

                var model = new
                {
                    MyRuns = myRunsPaging ,
                    Runs = othersRunsPaging
                };

                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":GetRuns, " + ex.Message , ex);
                new CException(this.GetType().Name + ":GetRuns, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// RunPage
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ActionName("Runs")]
        public HttpResponseMessage RunPage(RunListPagingModel model)
        {
            try
            {
                model = mcService.GetRuns(executionContext , model);
                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":RunPage, " + ex.Message , ex);
                new CException(this.GetType().Name + ":RunPage, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// GetRun
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Details")]
        public HttpResponseMessage GetRun(string id)
        {
            try
            {
                RunDetails ob = mcService.GetRun(executionContext , id);

                if (ob == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = string.Format("Run with id = {0} doesn't exist." , id) });
                }

                if (!string.IsNullOrEmpty(ob.OutputFolder))
                {
                    string relPath = ob.GetOutputFolderRelativePath();
                    string sharePath = GetSharePathByDataLocation(ob.DataLocation);
                    string uncPath = Path.Combine(sharePath , relPath);
                    ob.OutputFolderUNC = "file:///" + uncPath.Replace('\\' , '/');

                    FileLogger.Debug(GetType().Name + ":GetRun, OutputFolderUN=" + ob.OutputFolderUNC);
                }

                //ob.DatabaseTitle = dataService.GetDatabaseTitle(executionContext , ob.DatabaseId);
                ob.DatabaseTitle = _connectHelper.GetDatabaseTitle(ob.DatabaseId);

                return Request.CreateResponse(HttpStatusCode.OK , ob);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":GetRun, " + ex.Message , ex);
                new CException(this.GetType().Name + ":GetRun, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// GetNextRunId
        ///  - get new unique runId
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetNextId")]
        public HttpResponseMessage GetNextRunId()
        {
            GenericDb objDB = new GenericDb();
            try
            {
                CreateRunModel model = new CreateRunModel
                {
                    Id = objDB.GetNextId()
                };
                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":GetNextRunId, " + ex.Message , ex);
                new CException(this.GetType().Name + ":GetNextRunId, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// CreateRun
        ///  - create new run
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Create")]
        public HttpResponseMessage CreateRun()
        {
            try
            {
                GenericDb objDB = new GenericDb();
                int nextId = objDB.GetNextId();
                RunDetails r = new RunDetails
                {
                    Id = nextId ,
                    CreatedDate = DateTime.UtcNow ,
                    CreatedBy = executionContext.UserName ,
                    InOneFile = true
                };
                CreateRunModel model = new CreateRunModel(r)
                {
                    Sites = dataService.GetSites(true) ,
                    LocalSite = ApplicationLayoutHelper.GetLocalSite()
                };

                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":CreateRun, " + ex.Message , ex);
                new CException(this.GetType().Name + ":CreateRun, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// CreateRun
        ///  - copy run
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Create")]
        public HttpResponseMessage CreateRun(string id)
        {
            try
            {
                RunDetails r = mcService.GetRun(executionContext , id);

                string siteId = GetSiteByDataLocation(r.DataLocation);

                GenericDb objDB = new GenericDb();
                r.Id = objDB.GetNextId();
                r.CreatedDate = DateTime.UtcNow;
                r.CreatedBy = executionContext.UserName;
                r.SiteId = siteId;

                if (r.ImagesInputType == ImagesInputType.UploadFromDesktop)
                {
                    UploadImageListModel imgList = null;

                    try
                    {
                        using (StreamReader reader = new StreamReader(GetFillFilePath(r.ImageListRef , siteId)))
                        {
                            imgList = ImageListReader.Read(reader);
                        }
                    }
                    catch (Exception ex)
                    {
                        FileLogger.Error(string.Format("{0}Error reading image list file {1}: {2} " , GetType().Name , r.ImageListRef , ex.Message));
                    }
                    if (imgList != null && imgList.Channels != null)
                        r.Channels = imgList.Channels.ToArray();
                }
                else
                {
                    PathsModel pathsModel = new PathsModel
                    {
                        DatabaseId = r.DatabaseId ,
                        Paths = r.Dirs.Select(d => d.Path).ToArray()
                    };
                    //List<string> channels = dataService.GetChannelsByPaths(executionContext , pathsModel);
                    List<string> channels = _connectHelper.GetChannelsByPaths(pathsModel);
                    if (channels != null)
                    {
                        r.Channels = channels.ToArray();
                    }
                    else
                    {
                        r.Channels = new string[0];
                    }
                }

                CreateRunModel model = new CreateRunModel(r)
                {
                    Sites = dataService.GetSites(true) ,
                };
                model.Name = "Copy of " + model.Name;

                SetDataSourceByPath(model);

                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":CreateRun " + ex.Message , ex);
                new CException(this.GetType().Name + ":CreateRun " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// SubmitRun
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ActionName("Create")]
        public HttpResponseMessage SubmitRun(RunDetails model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ProtocolRef) && (string.IsNullOrEmpty(model.ProtocolName) || string.IsNullOrEmpty(model.ProtocolLocalFileName)))
                {
                    throw new ApplicationException("Protocol file is required.");
                }

                if (string.IsNullOrEmpty(model.ProtocolRef))
                {
                    model.ProtocolRef = CopyProtocolIntoShareFolder(model.ProtocolLocalFileName);
                }

                if (model.ImagesInputType == ImagesInputType.UploadFromDesktop)
                {
                    if (string.IsNullOrEmpty(model.ImageListRef) && (string.IsNullOrEmpty(model.ImageListName) || string.IsNullOrEmpty(model.ImgListLocalFileName)))
                    {
                        throw new ApplicationException("Image list file is required.");
                    }

                    if (string.IsNullOrEmpty(model.ImageListRef))
                    {
                        model.ImageListRef = CopyImgListIntoShareFolder(model.ImgListLocalFileName , model.SiteId);
                    }
                }
                else
                {
                    // file doesn't exist yet and will be created by CPCP wrapper, but set here so it is saved in params by MCR
                    model.ImageListRef = GenerateUniqueFileName("csv");
                }

                //clear
                if (model.ImageListOnly)
                {
                    model.ImageListName = null;
                }
                else
                {
                    if (model.ImagesInputType == ImagesInputType.UploadFromDesktop)
                    {
                        model.Dirs = new Dir[0];
                    }
                    else
                    {
                        model.ImageListName = null;
                    }
                }

                mcService.SubmitRun(executionContext , model);

                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":Sub,mitRun " + ex.Message , ex);
                new CException(this.GetType().Name + ":Sub,mitRun " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// StopRun
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ActionName("Details")]
        public HttpResponseMessage StopRun(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":StopRun, " + ex.Message , ex);
                new CException(this.GetType().Name + ":StopRun, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = Resources.Application.ApplicationError });
            }
        }

        /// <summary>
        /// GetChannels
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("GetChannels")]
        public HttpResponseMessage GetChannels(PathsModel model)
        {
            try
            {
                //List<string> channels = dataService.GetChannelsByPaths(executionContext , model);
                List<string> channels = _connectHelper.GetChannelsByPaths(model);

                if (channels == null || channels.Count == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError , new
                    {
                        Message = Resources.Application.NoImageDirectoriesFound ,
                        Channels = new string[] { }
                    });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK , new { Channels = channels });
                }
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":GetChannels, " + ex.Message , ex);
                new CException(this.GetType().Name + ":GetChannels, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// CopyProtocolIntoShareFolder
        /// </summary>
        /// <param name="tmpFileName"></param>
        /// <returns></returns>
        private static string CopyProtocolIntoShareFolder(string tmpFileName)
        {
            string uniqueName = GenerateUniqueFileName("cp");
            string pipelinePath = Path.Combine(ProtocolsShareFolder , uniqueName);

            string tmpPath = Path.Combine(TmpFilesPath , tmpFileName);

            File.Copy(tmpPath , pipelinePath , true);

            return uniqueName;
        }

        /// <summary>
        /// CopyImgListIntoShareFolder
        /// </summary>
        /// <param name="tmpFileName"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private static string CopyImgListIntoShareFolder(string tmpFileName , string siteId)
        {
            string uniqueName = GenerateUniqueFileName("csv");
            string inputFilesPath = Path.Combine(GetRunningInputShareFolder(siteId) , uniqueName);

            string tmpPath = Path.Combine(TmpFilesPath , tmpFileName);

            File.Copy(tmpPath , inputFilesPath , true);

            return uniqueName;
        }

        /// <summary>
        /// GenerateUniqueFileName
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        private static string GenerateUniqueFileName(string extension)
        {
            string uniqueName = Guid.NewGuid().ToString();
            if (!string.IsNullOrEmpty(extension))
            {
                uniqueName += "." + extension;
            }
            return uniqueName;
        }

        /// <summary>
        /// GetFillFilePath
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private static string GetFillFilePath(string fileName , string siteId)
        {
            return Path.Combine(GetRunningInputShareFolder(siteId) , fileName);
        }

        /// <summary>
        /// GetSharePathByDataLocation
        /// </summary>
        /// <param name="dataLocation"></param>
        /// <returns></returns>
        private static string GetSharePathByDataLocation(string dataLocation)
        {
            if (string.IsNullOrEmpty(dataLocation))
                return null;

            string path = null;

            foreach (PathsConfigSection.PathsConfigSiteElement site in PathsConfig.Sites)
            {
                foreach (PathsConfigSection.PathsConfigDataSourceElement ds in site.DataSources)
                {
                    foreach (PathsConfigSection.PathsConfigPathElement pathItem in ds.PathsObjects)
                    {
                        if (dataLocation.Equals(pathItem.DataLocation))
                        {
                            return pathItem.Path;
                        }
                    }
                }
            }
            return path;
        }

        /// <summary>
        /// GetSiteByDataLocation
        /// </summary>
        /// <param name="dataLocation"></param>
        /// <returns></returns>
        private static string GetSiteByDataLocation(string dataLocation)
        {
            if (string.IsNullOrEmpty(dataLocation))
                return null;

            foreach (PathsConfigSection.PathsConfigSiteElement site in PathsConfig.Sites)
            {
                foreach (PathsConfigSection.PathsConfigDataSourceElement ds in site.DataSources)
                {
                    foreach (PathsConfigSection.PathsConfigPathElement pathItem in ds.PathsObjects)
                    {
                        if (dataLocation.Equals(pathItem.DataLocation))
                        {
                            return site.Site;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// SetDataSourceByPath
        /// </summary>
        /// <param name="model"></param>
        private void SetDataSourceByPath(CreateRunModel model)
        {
            if (model.Dirs == null || model.Dirs.Length == 0)
                return;

            bool found = false;

            string path = model.Dirs[0].Path + "\\";
            foreach (ObjectBase site in dataService.GetSites())
            {
                //var siteDatabases = dataService.GetDatabases(executionContext , site.Id);
                var siteDatabases = _connectHelper.GetDatabases(site.Id);

                var databases = DatasourceHelper.ExcludeNotUsedDBs(siteDatabases);

                foreach (ObjectBase db in databases)
                {

                    var ds = PathsConfig.Sites[site.Id].DataSources[db.Id];
                    if (ds == null || ds.Paths == null)
                    {
                        continue;
                    }
                    PathsConfigSection.PathsConfigPathsCollection availablePaths = ds.PathsObjects;
                    foreach (PathsConfigSection.PathsConfigPathElement pathItem in availablePaths)
                    {
                        if (path.StartsWith(pathItem.Path , StringComparison.OrdinalIgnoreCase))
                        {
                            model.SiteId = site.Id;
                            model.DatabaseId = db.Id;
                            model.PathId = pathItem.Path;
                            model.DataLocation = pathItem.DataLocation;
                            model.Databases = databases;

                            List<ObjectBase> paths = (from PathsConfigSection.PathsConfigPathElement pathObj in availablePaths select new ObjectBase { Id = pathObj.DataLocation , Name = pathObj.Path }).ToList();
                            model.Paths = paths;

                            found = true;
                            break;
                        }
                    }
                    if (found) break;
                }
                if (found) break;
            }
        }

        /// <summary>
        /// DownloadResults
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet, FileDownload]
        [ActionName("Downloadresults")]
        public HttpResponseMessage DownloadResults(string id , ResultFileType type)
        {
            HttpResponseMessage res = null;

            try
            {
                RunDetails r = mcService.GetRun(executionContext , id);

                string sharePath = GetSharePathByDataLocation(r.DataLocation);
                if (string.IsNullOrEmpty(sharePath))
                    throw new ApplicationException("Unable to get share path for DataLocation = " + r.DataLocation);

                string resultsRelativePath = r.GetResultsRelativePath(type);
                string filePath = Path.Combine(sharePath , resultsRelativePath);

                FileInfo fileInfo = new FileInfo(filePath);

                FileLogger.Debug(GetType().Name + ":DownloadResults, filePath=" + filePath);

                res = Request.CreateResponse(HttpStatusCode.OK);
                res.Content = new PushStreamContent(
                        (outputStream , httpContent , transportContext) =>
                        {
                            try
                            {
                                int len = (int) fileInfo.Length, bytes;
                                byte[] buffer = new byte[1024];
                                using (Stream stream = File.OpenRead(filePath))
                                {
                                    while (len > 0 && (bytes = stream.Read(buffer , 0 , buffer.Length)) > 0)
                                    {
                                        outputStream.Write(buffer , 0 , bytes);
                                        len -= bytes;
                                    }
                                }
                            }
                            catch (HttpException ex)
                            {
                                if (ex.ErrorCode == -2147023667) // The remote host closed the connection. 
                                {
                                    return;
                                }
                            }
                            finally
                            {
                                // Close output stream as we are done
                                outputStream.Close();
                            }
                        });

                res.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = string.Format("{0}_{1}_{2}" , r.LastUpdated.ToString("yyyyMMdd" , CultureInfo.CreateSpecificCulture("en-US")) , r.Id , fileInfo.Name)
                };
                res.Content.Headers.ContentLength = fileInfo.Length;
                res.Headers.CacheControl = new CacheControlHeaderValue();
                res.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-zip-compressed");
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":DownloadResults, " + ex.Message , ex);
                new CException(this.GetType().Name + ":DownloadResults, " + ex.Message , ex);
            }

            return res;
        }

        /// <summary>
        /// DownloadImgList
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, FileDownload]
        [ActionName("Downloadimglist")]
        public HttpResponseMessage DownloadImgList(string id)
        {
            HttpResponseMessage res = null;

            try
            {
                RunDetails r = mcService.GetRun(executionContext , id);
                string siteId = GetSiteByDataLocation(r.DataLocation);

                string sharePath = GetRunningInputShareFolder(siteId);
                string fileName;

                // before ImageListRef is introduced the image list files were created with name: <calc_id>.csv
                fileName = string.IsNullOrEmpty(r.ImageListRef) ? string.Format("{0}.csv" , id) : r.ImageListRef;

                string filePath = Path.Combine(sharePath , fileName);

                FileInfo fileInfo = new FileInfo(filePath);

                FileLogger.Debug(GetType().Name + ":DownloadImgList, filePath=" + filePath);

                res = Request.CreateResponse(HttpStatusCode.OK);
                res.Content = new PushStreamContent(
                        (outputStream , httpContent , transportContext) =>
                        {
                            try
                            {
                                int len = (int) fileInfo.Length, bytes;
                                byte[] buffer = new byte[1024];
                                using (Stream stream = File.OpenRead(filePath))
                                {
                                    while (len > 0 && (bytes = stream.Read(buffer , 0 , buffer.Length)) > 0)
                                    {
                                        outputStream.Write(buffer , 0 , bytes);
                                        len -= bytes;
                                    }
                                }
                            }
                            catch (HttpException ex)
                            {
                                if (ex.ErrorCode == -2147023667) // The remote host closed the connection. 
                                {
                                    return;
                                }
                            }
                            finally
                            {
                                // Close output stream as we are done
                                outputStream.Close();
                            }
                        });

                res.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = string.IsNullOrEmpty(r.ImageListName) ?
                                    string.Format("{0}_{1}_ImageList.csv" , r.LastUpdated.ToString("yyyyMMdd" , CultureInfo.CreateSpecificCulture("en-US")) , r.Id) :
                                    r.ImageListName
                };
                res.Content.Headers.ContentLength = fileInfo.Length;
                res.Headers.CacheControl = new CacheControlHeaderValue();
                res.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":DownloadImgList, " + ex.Message , ex);
                new CException(this.GetType().Name + ":DownloadImgList, " + ex.Message , ex);
            }

            return res;
        }

        /// <summary>
        /// DownloadProtocol
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, FileDownload]
        [ActionName("Downloadprotocol")]
        public HttpResponseMessage DownloadProtocol(string id)
        {
            HttpResponseMessage res = null;

            try
            {
                RunDetails r = mcService.GetRun(executionContext , id);

                string filePath = Path.Combine(ProtocolsShareFolder , r.ProtocolRef);
                FileInfo fileInfo = new FileInfo(filePath);
                int fileLength = (int) fileInfo.Length;

                FileLogger.Debug(GetType().Name + ":DownloadProtocol, filePath=" + filePath);

                res = Request.CreateResponse(HttpStatusCode.OK);
                res.Content = new PushStreamContent(
                        (outputStream , httpContent , transportContext) =>
                        {
                            try
                            {
                                int len = fileLength, bytes;
                                byte[] buffer = new byte[1024];
                                using (Stream stream = File.OpenRead(filePath))
                                {
                                    while (len > 0 && (bytes = stream.Read(buffer , 0 , buffer.Length)) > 0)
                                    {
                                        outputStream.Write(buffer , 0 , bytes);
                                        len -= bytes;
                                    }
                                }
                            }
                            catch (HttpException ex)
                            {
                                if (ex.ErrorCode == -2147023667) // The remote host closed the connection. 
                                {
                                    return;
                                }
                            }
                            finally
                            {
                                // Close output stream as we are done
                                outputStream.Close();
                            }
                        });

                res.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = r.ProtocolName
                };
                res.Content.Headers.ContentLength = fileLength;
                res.Headers.CacheControl = new CacheControlHeaderValue();
                res.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":Download, " + ex.Message , ex);
                new CException(this.GetType().Name + ":Download, " + ex.Message , ex);
            }

            return res;
        }

        /// <summary>
        /// UploadProtocol
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("UploadProtocol")]
        public async Task<HttpResponseMessage> UploadProtocol()
        {
            FileLogger.Debug(string.Format("UploadProtocol. User: {0}, Cookies: {1}" , executionContext.UserName , executionContext.Cookies));
            string elname = "uploadedfile";

            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                return MimeMultipartError();
            }
            HttpResponseMessage response;

            string fileName = null, localFilePath = null, localFileName = null;
            try
            {
                long size = 0;

                MultipartFormDataStreamProvider streamProvider = new MultipartFormDataStreamProvider(TmpFilesPath);
                await Request.Content.ReadAsMultipartAsync(streamProvider);

                foreach (MultipartFileData file in streamProvider.FileData)
                {
                    localFilePath = file.LocalFileName;
                    fileName = file.Headers.ContentDisposition.FileName;
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        fileName = fileName.Replace("\"" , string.Empty);
                        fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                        localFileName = localFilePath.Substring(localFilePath.LastIndexOf('\\') + 1);

                        size = new FileInfo(localFilePath).Length;
                        break; // actually streamProvider.FileData collection should contain only one file
                    }
                }

                if (size > 1048576) // > 1 Mb
                {
                    string msg = string.Format(Resources.Application.UploadProtocolSizeError , ((double) size) / 1048576);
                    FileLogger.Error(GetType().Name + "UploadProtocal, File " + fileName + ". " + msg);
                    return CreateUploadError(msg , elname);
                }
                if (string.IsNullOrEmpty(localFilePath))
                {
                    throw new Exception("Error occurs during file uploading");
                }

                Protocol protocol;
                using (StreamReader reader = new StreamReader(localFilePath))
                {
                    protocol = AnalysisProtocolReader.Read(reader);
                }
                protocol.FileName = fileName;
                protocol.LocalFileName = localFileName;

                StringBuilder sb = new StringBuilder("<textarea data-type=\"application/json\">");
                string output = JsonConvert.SerializeObject(protocol);
                sb.Append(output);
                sb.Append("</textarea>");
                response = new HttpResponseMessage();
                response.Content = new StringContent(sb.ToString());
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(WebApp.TextHTML);
                return response;
            }
            catch (ProtocolFormatException ex)
            {
                FileLogger.Error(GetType().Name + "UploadProtocal, File " + fileName + ". " + Resources.Application.UploadProtocolError + " " + ex.Message);
                return CreateUploadError(Resources.Application.UploadProtocolError , elname);

            }
            catch (ProtocolModuleException)
            {
                FileLogger.Error(GetType().Name + "UploadProtocal, File " + fileName + ". " + Resources.Application.UploadProtocolModuleError);
                return CreateUploadError(Resources.Application.UploadProtocolModuleError , elname);
            }
            catch (Exception ex)
            {
                FileLogger.Error(GetType().Name + "UploadProtocal, " + ex.Message);
                new CException(this.GetType().Name + ":UploadProtocal, " + ex.Message , ex);
                return CreateUploadError(Resources.Application.UploadProtocolError , elname);
            }
        }

        /// <summary>
        /// UploadImageList
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("UploadImageList")]
        public async Task<HttpResponseMessage> UploadImageList()
        {
            FileLogger.Debug(string.Format("UploadImageList. User: {0}, Cookies: {1}" , executionContext.UserName , executionContext.Cookies));
            string elname = "uploadedfilelist";


            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                return MimeMultipartError();
            }
            HttpResponseMessage response;

            string fileName = null, localFilePath = null, localFileName = null;
            try
            {
                long size = 0;

                MultipartFormDataStreamProvider streamProvider = new MultipartFormDataStreamProvider(TmpFilesPath);
                await Request.Content.ReadAsMultipartAsync(streamProvider);

                foreach (MultipartFileData file in streamProvider.FileData)
                {
                    localFilePath = file.LocalFileName;
                    fileName = file.Headers.ContentDisposition.FileName;
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        fileName = fileName.Replace("\"" , string.Empty);
                        fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                        localFileName = localFilePath.Substring(localFilePath.LastIndexOf('\\') + 1);

                        size = new FileInfo(localFilePath).Length;
                        break; // actually streamProvider.FileData collection should contain only one file
                    }
                }

                //if (size > 4194304) // > 4 Mb
                //if (size > 104857600) // > 100 Mb
                if (size > 209715200) // > 200 Mb
                {
                    string msg = string.Format(Resources.Application.UploadImageListSizeError , ((double) size) / 1048576);
                    FileLogger.Error(GetType().Name + ":UploadImageList, File " + fileName + ". " + msg);
                    return CreateUploadError(msg , elname);//!!!!!!!!!!!!!!!!!
                }
                if (string.IsNullOrEmpty(localFilePath))
                {

                    return CreateUploadError("Error occurs during file uploading" , elname);
                }

                UploadImageListModel imgList;
                using (StreamReader reader = new StreamReader(localFilePath))
                {
                    imgList = ImageListReader.Read(reader);
                }

                imgList.FileName = fileName;
                imgList.LocalFileName = localFileName;

                StringBuilder sb = new StringBuilder("<textarea data-type=\"application/json\">");
                string output = JsonConvert.SerializeObject(imgList);
                sb.Append(output);
                sb.Append("</textarea>");
                response = new HttpResponseMessage();
                response.Content = new StringContent(sb.ToString());
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(WebApp.TextHTML);
                return response;
            }
            catch (Exception ex)
            {
                FileLogger.Error(GetType().Name + ":UploadImageList, " + ex.Message);
                new CException(this.GetType().Name + ":UploadImageList, " + ex.Message , ex);
                return CreateUploadError(ex.Message , elname);
            }
        }

        /// <summary>
        /// MimeMultipartError
        /// </summary>
        /// <returns></returns>
        private HttpResponseMessage MimeMultipartError()
        {
            FileLogger.Error(GetType().Name + ":MimeMultipartError, " + Resources.Application.NotMimeMultipartContent);

            var error = new Error { Message = Resources.Application.NotMimeMultipartContent };
            error.PadErrorText();

            var response = Request.CreateResponse(HttpStatusCode.UnsupportedMediaType , error);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(WebApp.TextPlain);

            return response;
        }

        /// <summary>
        /// CreateUploadError
        /// </summary>
        /// <param name="message"></param>
        /// <param name="elname"></param>
        /// <returns></returns>
        private HttpResponseMessage CreateUploadError(string message , string elname)
        {
            HttpResponseMessage response;
            var error = new Error();
            error.PadErrorText();
            error.ValidationErrors = new Error.ValidationDescriptor[]{
                      new  Error.ValidationDescriptor{
                       ElementName = elname,
                       Description = new string[]{message}
                      }
                    };
            response = Request.CreateResponse(HttpStatusCode.InternalServerError , error);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(WebApp.TextPlain);
            return response;

        }
    }
}