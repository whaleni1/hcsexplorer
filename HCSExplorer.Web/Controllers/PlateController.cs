﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.DTO;
using HCS.Shared.Logger;
using HCSExplorer.Services;
using HCSExplorer.Web.Consts;
using HCSExplorer.Web.Helpers;
using HCSExplorer.Web.Models;

namespace HCSExplorer.Web.Controllers
{
    public class PlateController : ApiController
    {
        private IExecutionContext executionContext;
        private IDataService dataService;
        private ConnectHelper _connectHelper = new ConnectHelper();

        /// <summary>
        /// PlateController
        /// </summary>
        /// <param name="executionContext"></param>
        /// <param name="dataService"></param>
        public PlateController(IExecutionContext executionContext , IDataService dataService)
        {
            this.executionContext = executionContext;
            this.dataService = dataService;
        }

        /// <summary>
        /// PlateById
        /// </summary>
        /// <param name="id"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("PlateParameters")]
        public HttpResponseMessage PlateById(string id , string siteId , string databaseId)
        {
            try
            {
                ImageParameters il = null;
                if (siteId.Equals(ApplicationLayoutHelper.GetLocalSite()))
                    il = _connectHelper.GetImageLayoutByPlateId(siteId , databaseId , id);
                else
                    il = dataService.GetImageLayoutByPlateId(executionContext , siteId , databaseId , id);


                if (il != null && il.Barcode == null)
                    return Request.CreateResponse(HttpStatusCode.InternalServerError ,
                                             new Error
                                             {
                                                 Message = String.Format(Resources.Application.PlateNotFoundError , "id" , id , siteId , databaseId)
                                             });

                return Request.CreateResponse(HttpStatusCode.OK , il);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":PlateById, " + Consts.Messages.GetLayoutError , ex);
                new CException(this.GetType().Name + ":PlateById, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// PlateByBarcode
        /// </summary>
        /// <param name="barcode"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("PlateParBarcode")]
        public HttpResponseMessage PlateByBarcode(string barcode , string siteId , string databaseId)
        {
            try
            {
                ImageParameters il = null;
                if (siteId.Equals(ApplicationLayoutHelper.GetLocalSite()))
                    il = _connectHelper.GetImageLayoutByBarcode(siteId , databaseId , barcode);
                else
                    il = dataService.GetImageLayoutByBarcode(executionContext , siteId , databaseId , barcode);

                if (il != null && il.Barcode == null)
                    return Request.CreateResponse(HttpStatusCode.InternalServerError ,
                                             new Error
                                             {
                                                 Message = String.Format(Resources.Application.PlateNotFoundError , "barcode" , barcode , siteId , databaseId)
                                             });

                return Request.CreateResponse(HttpStatusCode.OK , il);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":PlateByBarcode, " +
                    Consts.Messages.GetLayoutError ,
                    ex);
                new CException(this.GetType().Name + ":PlateByBarcode, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// Download
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("DownloadPdf")]
        public HttpResponseMessage Download(DownloadPdfModel obj)
        {
            HttpResponseMessage res = null;

            try
            {
                var ctx = HttpContext.Current;
                string filePath = Path.Combine(ctx.Server.MapPath("~/" + WebApp.CachedImagesDir) , obj.PdfPath);

                FileLogger.Info(GetType().Name + ":Download From filePath" + filePath);

                FileInfo fileInfo = new FileInfo(filePath);

                res = Request.CreateResponse(HttpStatusCode.OK);
                res.Content = new PushStreamContent(
                        (outputStream , httpContent , transportContext) =>
                        {
                            try
                            {
                                int len = (int) fileInfo.Length, bytes;
                                byte[] buffer = new byte[1024];
                                using (Stream stream = File.OpenRead(filePath))
                                {
                                    while (len > 0 && (bytes = stream.Read(buffer , 0 , buffer.Length)) > 0)
                                    {
                                        outputStream.Write(buffer , 0 , bytes);
                                        len -= bytes;
                                    }
                                }
                            }
                            catch (HttpException ex)
                            {
                                if (ex.ErrorCode == -2147023667) // The remote host closed the connection. 
                                {
                                    return;
                                }
                            }
                            finally
                            {
                                // Close output stream as we are done
                                outputStream.Close();
                            }
                        });

                res.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                res.Content.Headers.ContentDisposition.FileName = fileInfo.Name;
                res.Content.Headers.ContentLength = fileInfo.Length;
                res.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

            }
            catch (Exception ex)
            {
                FileLogger.Error(GetType().Name + ":Download, " + ex.Message);
                new CException(this.GetType().Name + ":Download, " + ex.Message , ex);
                //throw;
            }
            return res;
        }
    }
}
