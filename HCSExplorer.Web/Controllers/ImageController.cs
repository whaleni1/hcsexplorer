﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.DTO;
using HCS.Shared.Logger;
using HCSExplorer.Services;
using HCSExplorer.Web.Consts;
using HCSExplorer.Web.Models;

namespace HCSExplorer.Web.Controllers
{
    public class ImageController : ApiController
    {
        private const float NumberOfLine = 70;

        private IExecutionContext executionContext;
        private IDataService dataService;

        /// <summary>
        /// ImageController
        /// </summary>
        /// <param name="executionContext"></param>
        /// <param name="dataService"></param>
        public ImageController(IExecutionContext executionContext , IDataService dataService)
        {
            this.executionContext = executionContext;
            this.dataService = dataService;
        }

        /// <summary>
        /// GetImage
        /// </summary>
        /// <param name="getImgUrl"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetSingleImage")]
        public HttpResponseMessage GetImage(string getImgUrl)
        {
            ConnectHelper connectHelper = new ConnectHelper();

            try
            {
                FileLogger.Info(GetType().Name + ":GetImage getImgUrl = " + getImgUrl);
                int iItemIndex = 0;

                string imageUrl = HttpUtility.UrlDecode(getImgUrl);
                string overlay = ResolveImageUrl(imageUrl , "overlay" , ref iItemIndex);
                string db = ResolveImageUrl(imageUrl , "db" , ref iItemIndex);
                string plateId = ResolveImageUrl(imageUrl , "plateid" , ref iItemIndex);
                string row = ResolveImageUrl(imageUrl , "row" , ref iItemIndex);
                string col = ResolveImageUrl(imageUrl , "col" , ref iItemIndex);
                string field = ResolveImageUrl(imageUrl , "field" , ref iItemIndex);
                string format = ResolveImageUrl(imageUrl , "format" , ref iItemIndex);
                string zIndexStr = ResolveImageUrl(imageUrl , "z_index" , ref iItemIndex);
                string timepointStr = ResolveImageUrl(imageUrl , "timepoint_index" , ref iItemIndex);
                string version = ResolveImageUrl(imageUrl , "version" , ref iItemIndex);
                string path = string.Empty;
                string invert = ResolveImageUrl(imageUrl , "invert" , ref iItemIndex);

                int? timepoint = null;
                int? zIndex = null;
                if (!string.IsNullOrEmpty(zIndexStr))
                {
                    int intValue;
                    bool parsed = int.TryParse(zIndexStr , out intValue);
                    if (parsed)
                        zIndex = intValue;
                }
                if (!string.IsNullOrEmpty(timepointStr))
                {
                    int intValue;
                    bool parsed = int.TryParse(timepointStr , out intValue);
                    if (parsed)
                        timepoint = intValue;
                }

                string cellId = null;
                int rowIndex = int.Parse(row);
                int colIndex = int.Parse(col);

                // Decrease by one because of the array notation
                rowIndex = rowIndex - 1;

                cellId = ImageUtils.GetCellId(rowIndex , colIndex);

                var ctx = HttpContext.Current;
                string delimiter = ctx.Request.ApplicationPath.EndsWith("/") ? string.Empty : "/";
                string baseUrl = ctx.Request.ApplicationPath + delimiter + WebApp.CachedImagesUrlPath;
                string siteDir = ctx.Server.MapPath("~/" + WebApp.CachedImagesDir);

                ImageUrlModel item = new ImageUrlModel();
                item.PlateKey = ImageUtils.GetPlateDir(db , plateId);
                item.Row = Convert.ToInt16(row);
                item.Col = Convert.ToInt16(col);
                item.FieldIndex = 0;
                item.ZIndex = zIndex;
                item.Timepoint = timepoint;
                item.Version = version;
                item.FileKey = ImageUtils.GetFileKey(imageUrl , ImageUtils.IMAGE_FORMAT);

                WellImagesModel imageModel = new WellImagesModel();
                imageModel.Well = new HCS.Shared.DTO.Well { Row = item.Row , Col = item.Col };
                imageModel.Invert = string.IsNullOrEmpty(invert) ? false : (invert.Equals("1") ? true : false);
                imageModel.ZIndex = zIndex;
                imageModel.Timepoint = timepoint;
                imageModel.Version = version;
                imageModel.Channels = new List<string>();

                string channel = string.Empty;
                if (string.IsNullOrEmpty(overlay))
                {
                    channel = ResolveImageUrl(imageUrl , "channel" , ref iItemIndex);
                    string color = ResolveImageUrl(imageUrl , "color" , ref iItemIndex);
                    string brightness = ResolveImageUrl(imageUrl , "b" , ref iItemIndex);
                    string auto = ResolveImageUrl(imageUrl , "auto" , ref iItemIndex);
                    //string level = ResolveImageUrl(imageUrl, "level");
                    //if (!string.IsNullOrEmpty(level))
                    //{
                    //    var vals = level.Split(',');
                    //}
                    imageModel.Channels.Add(channel);
                    imageModel.ChannelParameters = new List<ChannelImageParameters>();
                    imageModel.ChannelParameters.Add(new ChannelImageParameters { Channel = channel , Brightness = brightness , Color = color , Auto = auto , /*Level = level, WhitePoint = whitepoint, BlackPoint = blackpoint*/ });
                }
                else // Get the values for the overlay
                {
                    item.Overlay = true;

                    var spec = new OverlayImageSpec();
                    imageModel.ChannelParameters = new List<ChannelImageParameters>();
                    string color = string.Empty;

                    int startIndex = iItemIndex;
                    // Set the channel parameters
                    foreach (ImageSpec colorSpec in spec.imageElements)
                    {
                        color = colorSpec.color;
                        channel = ResolveImageUrl(imageUrl , color , ref iItemIndex);

                        if (!string.IsNullOrEmpty(channel))
                        {
                            imageModel.Channels.Add(channel);
                            string auto = ResolveImageUrl(imageUrl , "auto" , ref iItemIndex);
                            string brightness = ResolveImageUrl(imageUrl , color + "_b" , ref iItemIndex);

                            imageModel.ChannelParameters.Add(new ChannelImageParameters { Channel = channel , Brightness = brightness , Color = color , Auto = auto , /*Level = level, WhitePoint = whitepoint, BlackPoint = blackpoint*/ });
                        }
                        iItemIndex = startIndex;
                    }
                    channel = "Overlay";
                }
                imageModel.Fields = new List<string>();
                imageModel.Fields.Add(field);
                imageModel.Id = Convert.ToInt32(plateId);
                imageModel.DatabaseId = db;

                path = connectHelper.LoadImage(item , baseUrl , siteDir , imageModel);

                ImageInfo model = new ImageInfo
                {
                    Field = field ,
                    Channel = channel ,
                    Row = rowIndex ,
                    Col = colIndex ,
                    CellId = cellId ,
                    Path = path ,
                    Timepoint = timepoint ,
                    Version = version ,
                    ZIndex = zIndex
                };

                return Request.CreateResponse(HttpStatusCode.OK , model);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":GetImage, " + Consts.Messages.GetLayoutError + " " + ex.Message);
                new CException(this.GetType().Name + ":GetImage, " + ex.Message , ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError , new Error { Message = ex.Message });
            }
        }

        /// <summary>
        /// Download
        /// </summary>
        /// <param name="dim"></param>
        /// <returns>A single image for the user to download and open locally</returns>
        [HttpPost]
        [ActionName("DownloadImage")]
        public HttpResponseMessage Download(DownloadImageModel dim)
        {
            HttpResponseMessage res = null;

            try
            {
                var ctx = HttpContext.Current;
                string imgUrl = Uri.UnescapeDataString(dim.ImgUrl);
                string imgPath = Uri.UnescapeDataString(dim.ImgPath);
                string delimiter = ctx.Request.ApplicationPath.EndsWith("/") ? string.Empty : "/";
                string baseUrl = ctx.Request.ApplicationPath + delimiter + WebApp.CachedImagesUrlPath;
                string siteDir = ctx.Server.MapPath("~/" + WebApp.CachedImagesDir);
                imgPath = imgPath.Replace(baseUrl , "");
                string saveImagePath = siteDir + imgPath;     //get the absolute path of the image

                FileLogger.Info(this.GetType().Name + ":Download, baseURL: " + baseUrl + ", siteDir: " + siteDir + ", imgPath: " + imgPath + ", saveImagePath: " + saveImagePath);

                dataService.SaveImage(executionContext, imgUrl, saveImagePath, 0);

                Bitmap myImage = new Bitmap(saveImagePath);

                string channel = ResolveImageUrl(imgUrl , "channel");
                string field = ResolveImageUrl(imgUrl , "field");
                string row = ResolveImageUrl(imgUrl , "row");
                string col = ResolveImageUrl(imgUrl , "col");
                string version = ResolveImageUrl(imgUrl , "version");
                string timepoint = ResolveImageUrl(imgUrl , "timepoint_index");
                string zIndex = ResolveImageUrl(imgUrl , "z_index");

                int rowIndex = int.Parse(row);
                int colIndex = int.Parse(col);
                // Decrease by one because of the array notation
                rowIndex = rowIndex - 1;

                string wellId = ImageUtils.GetCellId(rowIndex , colIndex);

                string path = dim.PlatePath;
                string barcode = dim.Barcode;

                string msg = string.Empty;

                if (dim.PathChBox)
                {
                    msg += "Path: " + path + "\n";
                }
                if (dim.PlateIdChBox)
                {
                    msg += "Barcode: " + barcode + "\n";
                }
                if (dim.ChannelChBox)
                {
                    msg += "Channel: " + channel + "\n";
                }
                if (dim.RowColumnChBox)
                {
                    msg += "Well Id: " + wellId + "\n";
                }
                if (dim.FieldChBox)
                {
                    msg += "Field: " + field + "\n";
                }
                if (dim.FieldChBox)
                {
                    msg += "Field: " + field + "\n";
                }
                if (dim.VersionChBox && !string.IsNullOrEmpty(version))
                {
                    msg += "Version: " + version + "\n";
                }

                if (dim.TimepointChBox && !string.IsNullOrEmpty(timepoint))
                {
                    msg += "Timepoint: " + timepoint + "\n";
                }

                if (dim.ZIndexChBox && !string.IsNullOrEmpty(zIndex))
                {
                    msg += "Z Index: " + zIndex + "\n";
                }

                if (!string.IsNullOrEmpty(dim.CustomText))
                {
                    msg += dim.CustomText;
                }
                float textSize = "default".Equals(dim.TextSize) ? determineBestTextSize(myImage) : float.Parse(dim.TextSize);

                // write text on image 
                Bitmap convBmp = AddTextToImage(myImage , msg , textSize , dim.TextColor);

                myImage.Dispose();

                convBmp.Save(saveImagePath);
                convBmp.Dispose();

                // Return image to user
                FileInfo fullSizeImageFile = new FileInfo(saveImagePath);

                res = Request.CreateResponse(HttpStatusCode.OK);
                res.Content = new PushStreamContent(
                        async (outputStream , httpContent , transportContext) =>
                        {
                            try
                            {
                                int len = (int) fullSizeImageFile.Length, bytes;
                                byte[] buffer = new byte[1024];
                                using (Stream stream = File.OpenRead(saveImagePath))
                                {
                                    while (len > 0 && (bytes = stream.Read(buffer , 0 , buffer.Length)) > 0)
                                    {
                                        outputStream.Write(buffer , 0 , bytes);
                                        len -= bytes;
                                    }
                                }
                            }
                            catch (HttpException ex)
                            {
                                if (ex.ErrorCode == -2147023667) // The remote host closed the connection. 
                                {
                                    return;
                                }
                            }
                            finally
                            {
                                // Close output stream as we are done
                                outputStream.Close();
                            }
                        });

                res.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                res.Content.Headers.ContentDisposition.FileName = fullSizeImageFile.Name;
                res.Content.Headers.ContentLength = fullSizeImageFile.Length;
                res.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ImageUtils.IMAGE_CONTENT_TYPE);
            }
            catch (Exception ex)
            {
                FileLogger.Error(this.GetType().Name + ":Download, " + ex.Message);
                new CException(this.GetType().Name + ":Download, " + ex.Message , ex);
            }

            return res;
        }

        /// <summary>
        /// determineBestTextSize
        ///  - Determine text size according to the path string length
        /// </summary>
        /// <param name="imageToBeAddedText"></param>
        /// <returns></returns>
        protected float determineBestTextSize(Bitmap imageToBeAddedText)
        {
            float maxHeightForTex = imageToBeAddedText.Height / NumberOfLine;

            return maxHeightForTex;
        }

        /// <summary>
        /// determineBestColor
        ///  - Try to determine the best suitable color for the given image by
        /// parsing image's avarage color ratio.
        /// </summary>
        /// <param name="imageToBeAddedText"></param>
        /// <returns></returns>
        protected Color determineBestColor(Bitmap imageToBeAddedText)
        {
            Color myColor = new Color();
            long R = 0;
            long G = 0;
            long B = 0;

            try
            {
                // no need to loop over the whole height
                // text appears in the first one third of the image height
                for (int y = 0 ; y < imageToBeAddedText.Height / 3 ; y++)
                {
                    for (int x = 0 ; x < imageToBeAddedText.Width ; x++)
                    {
                        Color col = imageToBeAddedText.GetPixel(x , y);
                        R += col.R;
                        G += col.G;
                        B += col.B;
                    }
                }

                int r, g, b;

                r = (int) (R / ((imageToBeAddedText.Height / 3) * imageToBeAddedText.Width));
                g = (int) (G / ((imageToBeAddedText.Height / 3) * imageToBeAddedText.Width));
                b = (int) (B / ((imageToBeAddedText.Height / 3) * imageToBeAddedText.Width));

                if ((r > 127 && r < 154) && (g > 127 && g < 154) && (b > 127 && b < 154))
                {
                    myColor = Color.Black;
                }
                else if ((r > 100 && r < 127) && (g > 100 && g < 127) && (b > 100 && b < 127))
                {
                    myColor = Color.White;
                }
                else
                {
                    myColor = Color.FromArgb((255 - r) , (255 - g) , (255 - b));
                }
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":determineBestColor, " + eX.Message , eX);
            }
            return myColor;
        }

        /// <summary>
        /// AddTextToImage
        ///  - Embed text on images
        /// </summary>
        /// <param name="bImg"></param>
        /// <param name="msg"></param>
        /// <param name="textSize"></param>
        /// <param name="textColorStr"></param>
        /// <returns></returns>
        protected Bitmap AddTextToImage(Bitmap bImg , string msg , float textSize , string textColorStr)
        {
            // To void the error due to Indexed Pixel Format
            Image img = new Bitmap(bImg , new Size(bImg.Width , bImg.Height));
            Bitmap tmp = new Bitmap(img);

            try
            {
                Graphics graphic = Graphics.FromImage(tmp);

                SolidBrush brush;
                Color textColor = "default".Equals(textColorStr) ? determineBestColor(tmp) : Color.FromName(textColorStr);
                brush = new SolidBrush(textColor);

                // add text on image
                graphic.DrawString(msg , new Font("Ariel" , textSize , FontStyle.Bold) ,
                    brush , new RectangleF(10 , 10 , bImg.Width - 20 , bImg.Height));

                graphic.Dispose();
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":AddTextToImage, " + eX.Message , eX);
            }
            return tmp;
        }

        /// <summary>
        /// ResolveImageUrl
        ///  - extract the desired field from the whole url using key
        /// </summary>
        /// <param name="urlToBeResolved"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private string ResolveImageUrl(string urlToBeResolved , string key)
        {
            string strResult = string.Empty;

            try
            {
                if (urlToBeResolved.Contains(key))
                {
                    key += "=";
                    int keyLen = key.Length;
                    int indexOfBeg = urlToBeResolved.IndexOf(key);
                    int indexOfEnd = urlToBeResolved.IndexOf("&" , indexOfBeg);
                    if (indexOfEnd == -1)
                    {
                        indexOfEnd = urlToBeResolved.Length;
                    }
                    strResult = urlToBeResolved.Substring((indexOfBeg + keyLen) , (indexOfEnd - (indexOfBeg + keyLen)));
                }
                else if (urlToBeResolved.Contains("overlay"))
                    if (urlToBeResolved.Contains("overlay"))
                    {
                        strResult = "Overlay";
                    }
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":ResolveImageUrl, " + eX.Message , eX);
            }

            return strResult;
        }

        private string ResolveImageUrl(string urlToBeResolved , string key , ref int iItemIndex)
        {
            string strResult = string.Empty;

            try
            {
                string[] stringSeparators = new string[] { "?" , "&" , "=" };
                string[] strItems = urlToBeResolved.Split(stringSeparators , StringSplitOptions.None);

                for (int i = iItemIndex ; i < strItems.Length ; i++)
                {
                    if (strItems[i].Equals(key))
                    {
                        strResult = strItems[++i];
                        iItemIndex += 1;
                        return strResult;
                    }
                }

                if (iItemIndex == 0  && urlToBeResolved.Contains("overlay"))
                {
                    strResult = "Overlay";
                    iItemIndex += 1;
                }
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":ResolveImageUrl, " + eX.Message , eX);
            }

            return strResult;
        }
    }
}
