﻿using System.Web.Mvc;

namespace HCSExplorer.Web.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Image
        /// </summary>
        /// <returns></returns>
        public ActionResult Image()
        {
            return View();
        }
    }
}
