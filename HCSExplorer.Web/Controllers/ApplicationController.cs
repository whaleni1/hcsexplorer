﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HCSExplorer.Services;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.Logger;
using HCSExplorer.Web.Helpers;
using HCSExplorer.Web.Models;
using HCSExplorer.Web.Models.Application;

namespace HCSExplorer.Web.Controllers
{
    /// <summary>
    /// ApplicationController
    /// </summary>
    public class ApplicationController : ApiController
    {
        private IExecutionContext executionContext;
        private IDataService dataService;

        /// <summary>
        /// ApplicationController
        /// </summary>
        /// <param name="executionContext"></param>
        /// <param name="dataService"></param>
        public ApplicationController(IExecutionContext executionContext , IDataService dataService)
        {
            this.executionContext = executionContext;
            this.dataService = dataService;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Layout")]
        public HttpResponseMessage Get()
        {
            try
            {
                var layout = new ApplicationLayout();
                layout.UserName = executionContext.UserName;

                layout.Initialize();

                return Request.CreateResponse(HttpStatusCode.OK , layout);
            }
            catch (Exception eX)
            {
                FileLogger.Error(this.GetType().Name + ":Get, " + Consts.Messages.GetLayoutError , eX);
                new CException(this.GetType().Name + ":Get, " + eX.Message , eX);

                return Request.CreateResponse(HttpStatusCode.InternalServerError ,
                                              new Error
                                              {
                                                  Message = Resources.Application.GetLayoutError
                                              });
            }
        }
    }
}
