﻿using System.Configuration;
using System.Web;
using System.Web.Http.Controllers;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using HCSExplorer.Services;
using HCS.Shared.Context;
using HCSExplorer.Web.Controllers;
using HCSExplorer.Services.MC;

namespace HCSExplorer.Web.Windsor
{
    internal class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            #region Controllers

            container.Register(Classes.FromAssemblyContaining<HomeController>()
                                      .BasedOn<IHttpController>()
                                      .LifestylePerWebRequest());

            #endregion
            container.Register(Component.For(typeof(IExternalServiceLocator))
                                        .ImplementedBy(typeof(ExternalServiceLocator))
                                        .LifeStyle
                                        .Singleton);

            container.Register(Component.For(typeof (IDataService))
                                        .ImplementedBy(typeof (DataService))
                                        .UsingFactoryMethod(instance =>
                                        {
                                            var dataService = new DataService
                                            {
                                                ServiceLocator = (IExternalServiceLocator)container.Resolve(typeof(IExternalServiceLocator))
                                            };
                                            return dataService;
                                        })
                                        .LifeStyle
                                        .Singleton);

            container.Register(Component.For(typeof(IMCService))
                                        .ImplementedBy(typeof(MCService))
                                        .LifeStyle
                                        .Singleton);

            container.Register(Component.For<IExecutionContext>()
                                        .ImplementedBy<ExecutionContext>()
                                        .UsingFactoryMethod(instance =>
                                            {
                                                var request = HttpContext.Current.Request;

                                                string username = HttpContext.Current.User.Identity.Name;
                                                if (string.IsNullOrEmpty(username)) username = "NONAME"; // TODO: remove dummy
                                                var context = new ExecutionContext
                                                    {
                                                        UserName = username,
                                                        BSMediaServiceURL = ConfigurationManager.AppSettings["BSMediaServiceURL"],
                                                        CAMediaServiceURL = ConfigurationManager.AppSettings["CAMediaServiceURL"],
                                                        ZJMediaServiceURL = ConfigurationManager.AppSettings["ZJMediaServiceURL"],
                                                        RIMediaServiceURL = ConfigurationManager.AppSettings["RIMediaServiceURL"],
                                                        MCServiceURL = ConfigurationManager.AppSettings["MCServiceURL"],
                                                        Cookies = request.Headers["Cookie"]
                                                        //Cookies = "ObSSOCookie=iGKGVEHlncTS7puuQ36zFk+EexJsLb5a6cPJhD0Ou9X5/sfTfl9JBDqpxfgD+P6vPTgqI002N7l6mLO/TIkzL6Wg6x/HLaCt7QT+DIl6uD/eyDMNgNEfuoDYkcFhIl+9qrVBKnhwnwe4JEF7eMwqczhVfLHehaZV3FrF58i1tKPdfd0VqPFdOcV1i5ErJ9C7oT4CwF6RSShrtCceXkg6vbNSc5zFiVMvGCGIRBuWVfXdVmMJ4Ygd0lcRk95MWsUpGVCIKTVEZ4y1DCHxjG7mnsNPXQ8uoXH7Y1wm7LJakV+87yS3WK4RfCpnxlriolV5wX6KlJZbE8lgZVPhVvOE/Q==; expires=Tue, 22 Apr 2014 09:05:44 GMT; path=/; domain=.novartis.net"
                                                    };
                                                return context;
                                            })
                                        .LifestylePerWebRequest());
        }
    }
}
