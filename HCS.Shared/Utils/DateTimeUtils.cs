﻿using System;
using System.Globalization;

namespace HCS.Shared.Utils
{
    /// <summary>
    /// DateTimeUtils
    /// </summary>
    public class DateTimeUtils
    {
        const string csSITE_BS = "BS";
        const string csSITE_CA = "CA";

        private static string[] CADateFormat = { "M/d/yyyy h:mm:ss tt" , "MM/dd/yyyy h:mm:ss tt" };
        private static string[] BSDateFormat = { "dd.MM.yyyy HH:mm:ss" };

        /// <summary>
        /// ParseDateTime
        /// </summary>
        /// <param name="strDate"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public static DateTime? ParseDateTime(string strDate , string siteId)
        {
            DateTime? result = null;
            string[] fmt = null;
            if (siteId == csSITE_CA)
                fmt = CADateFormat;
            if (siteId == csSITE_BS)
                fmt = BSDateFormat;
            DateTime dt;
            if (DateTime.TryParseExact(strDate , fmt , CultureInfo.InvariantCulture , DateTimeStyles.None , out dt))
            {
                result = dt;
            }
            else if (DateTime.TryParseExact(strDate , CADateFormat , CultureInfo.InvariantCulture , DateTimeStyles.None , out dt))
            {
                result = dt;
            }
            return result;

        }

        /// <summary>
        /// DateTimeToLocalServerTime
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string DateTimeToLocalServerTime(DateTime date)
        {
            if (null == date)
                return string.Empty;

            return date.ToString("dd-MMM-yyyy HH:mm:ss" , CultureInfo.CreateSpecificCulture("en-US"));
        }
    }

}
