﻿using System;
using System.Runtime.Caching;

namespace HCS.Shared.Caching
{
    public class HCSCache
    {
        private static string DATABASES = "DATABASES";

        /// <summary>
        /// SetDatabases
        /// </summary>
        /// <param name="databases"></param>
        public static void SetDatabases(Object databases)
        {
            ObjectCache cache = MemoryCache.Default;
            cache[DATABASES] = databases;
        }

        /// <summary>
        /// GetDatabases
        /// </summary>
        /// <returns></returns>
        public static Object GetDatabases()
        {
            ObjectCache cache = MemoryCache.Default;
            return cache[DATABASES];
        }
    }
}
