﻿namespace HCS.Shared.DTO
{
    public class ImageInfo
    {
        public string Field { get; set; }
        public string Channel { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
        public string CellId { get; set; }
        public string Path { get; set; }

        public int? ZIndex { get; set; }
        public int? Timepoint { get; set; }
        public string Version { get; set; }
    }
}
