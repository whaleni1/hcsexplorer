﻿namespace HCS.Shared.DTO
{
    public class PlateImagesModel : WellImagesModel
    {
        public string Field { get; set; }
        public ChannelImageParameters PlateChannelParameters { get; set; }
        public int Rows { get; set; }
        public int Cols { get; set; }
        public TestingParameters TestingParams { get; set; }
    }
}
