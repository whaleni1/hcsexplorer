﻿namespace HCS.Shared.DTO
{
    public class Compound
    {
        public string Id { get; set; }
        public string Assay { get; set; }
        public string Barcode { get; set; }
        public string InstrumentId { get; set; }
        public string Instrument { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
        public string DatabaseName { get; set; }
        public bool HasLink { get; set; }
        public string SiteId { get; set; }
    }
}
