﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCS.Shared.DTO
{
    public class PathsModel
    {
        public string SiteId { get; set; }
        public string DatabaseId { get; set; }
        public string[] Paths { get; set; }
    }
}
