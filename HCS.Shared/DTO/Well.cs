﻿namespace HCS.Shared.DTO
{
    public class Well
    {
        public int Col { get; set; }
        public int Row { get; set; }
    }
}
