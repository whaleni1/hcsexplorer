﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace HCS.Shared.DTO
{
    [DataContract]
    public enum PostProcessingType
    {
        [EnumMember(Value = "COUNTS_SUMMED")]
        CountsSummed = 0,

        [EnumMember(Value = "MEAN_MEAN_INTENSITY")]
        MeanMeanIntensity = 1,

        [EnumMember(Value = "MEDIAN_MEDIAN_INTENSITY")]
        MedianMedianIntensity = 2,

        [EnumMember(Value = "STD_DEVIATION_MEAN_INTENSITY")]
        StdDeviationMeanIntensity = 3
    }

    [DataContract]
    public enum ResultFileType
    {
        Undefined = 0,

        [EnumMember(Value = "InOneFile")]
        InOneFile = 1,

        [EnumMember(Value = "Icp4")]
        ICP4 = 2
    }
}
