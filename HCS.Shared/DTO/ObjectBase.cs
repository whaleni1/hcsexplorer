﻿namespace HCS.Shared.DTO
{
    public class ObjectBase
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
