﻿using System.Collections.Generic;

namespace HCS.Shared.DTO
{
    public class UploadImageListModel : UploadedFile
    {
        public List<string> Channels { get; set; }
        public string SiteId { get; set; }
        public string DatabaseId { get; set; }
        public string DataLocation { get; set; }
    }
}
