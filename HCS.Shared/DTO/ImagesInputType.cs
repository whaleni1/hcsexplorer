﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCS.Shared.DTO
{
    public enum ImagesInputType
    {
        SelectImgDirs = 0,
        UploadFromDesktop = 1,
        SelectFromHcsia = 2
    }
}
