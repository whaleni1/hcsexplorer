﻿namespace HCS.Shared.DTO
{
    public class Dir
    {
        public string Path { get; set; }
        public string Name { get; set; }
    }
}
