﻿namespace HCS.Shared.DTO
{
    public class ImageResultModel
    {
        public string Path { get; set; }
        public string GetImgUrl { get; set; }
        public bool IsWebClientDisconnect { get; set; }
    }
}
