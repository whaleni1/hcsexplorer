﻿using HCS.Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCS.Shared.DTO
{
    public class PlateInfo
    {
        public string Barcode { get; set; }
        public int Progress { get; set; }
        public PlateStatus Status { get; set; }
        public string StatusDescription { get; set; }
        public string Detail { get; set; }
        public string Column { get; set; } // will be renamed in the future
        public DateTime LastUpdated { get; set; }

        public string LastUpdatedStr
        {
            get
            {
                return DateTimeUtils.DateTimeToLocalServerTime(LastUpdated);
            }
        }

        public int StatusCode { get { return (int)Status; } }
    }
}
