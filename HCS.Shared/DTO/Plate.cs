﻿using System;

namespace HCS.Shared.DTO
{
    public class Plate
    {
        public int Id { get; set; }
        public string SiteId { get; set; }
        public string DatabaseId { get; set; }
        public string Barcode { get; set; }
        public int Format { get; set; }
        public string DateSrc { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public DateTime? Date { get; set; }
    }
}
