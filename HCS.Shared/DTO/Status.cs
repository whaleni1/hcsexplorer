﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace HCS.Shared.DTO
{
    [DataContract]
    public enum Status
    {
        [EnumMember(Value = "Undefined")]
        Undefined = 0,
        [EnumMember(Value = "Validating")]
        Validating = 1,
        [EnumMember(Value = "On Cluster")]
        OnCluster = 2,
        [EnumMember(Value = "Error")]
        Error = 3,
        [EnumMember(Value = "Complete")]
        Complete = 4,
        [EnumMember(Value = "Warning")]
        Warning = 5,
        [EnumMember(Value = "Timeout")]
        Timeout = 6
    }

    [DataContract]
    public enum PlateStatus
    {
        [EnumMember(Value = "Undefined")]
        Undefined = 0,
        [EnumMember(Value = "Queued")]
        Validating = 1,
        [EnumMember(Value = "Processing")]
        OnCluster = 2,
        [EnumMember(Value = "Error")]
        Error = 3,
        [EnumMember(Value = "Complete")]
        Complete = 4,
        [EnumMember(Value = "Warning")]
        Warning = 5,
        [EnumMember(Value = "Timeout")]
        Timeout = 6
    }
}
