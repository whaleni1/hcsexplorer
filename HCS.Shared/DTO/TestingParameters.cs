﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCS.Shared.DTO
{
    public class TestingParameters
    {
        public int ThreadsCount { get; set; }
        public int DisconnectTimeout { get; set; }
        public int HCSConnectTimeout { get; set; }
        public bool ClearCache { get; set; }
        public bool IsReconnect { get; set; }
    }
}
