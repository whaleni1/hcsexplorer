﻿namespace HCS.Shared.DTO
{
    public class WMapItem
    {
        public string BiologicalConcept { get; set; }
        public string Channel { get; set; }
    }
}
