﻿namespace HCS.Shared.DTO
{
    public class ImageUrlModel
    {
        public int FieldIndex { get; set; }
        public int ChannelIndex { get; set; }
        public bool Overlay { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
        public string Url { get; set; }
        public string PlateKey { get; set; }
        public string FileKey { get; set; }
        public string FileName { get; set; }
        public bool Loaded { get; set; }
        public int? ZIndex { get; set; }
        public int? Timepoint { get; set; }
        public string Version { get; set; }
    }
}
