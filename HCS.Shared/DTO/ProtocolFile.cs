﻿namespace HCSImageAnalysis.Shared.DTO
{
    public class ProtocolFile
    {
        public string Name { get; set; }
        public string Content { get; set; }
    }
}
