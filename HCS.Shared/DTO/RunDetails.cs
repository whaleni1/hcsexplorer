﻿using System;
using System.Collections.Generic;

namespace HCS.Shared.DTO
{
    public class RunDetails : Run
    {
        public string AssayDescription { get; set; }
        public string HeliosId { get; set; }
        public string AresId { get; set; }

        public string Description { get; set; }

        public string DatabaseTitle { get; set; }

        public string SiteId { get; set; }
        public string DatabaseId { get; set; }

        public PlateInfo[] Plates { get; set; }
        public Dir[] Dirs { get; set; }
        public WMapItem[] WavelengthMap { get; set; }
        public string[] Channels { get; set; }

        public string ProtocolLocalFileName { get; set; }
        public string ImgListLocalFileName { get; set; }

        public int Block { get; set; }
        public string DataLocation { get; set; }

        public string Feedback { get; set; }
        public string Note { get; set; }

        public string OutputFolder { get; set; }
        public string OutputFolderUNC { get; set; }

        public string ProtocolRef { get; set; }

        public string ImageListName { get; set; }
        public string ImageListRef { get; set; }

        public bool PostProcessingEnabled { get; set; }
        public bool PpCountsSummed { get; set; }
        public bool PpMeanMeanIntensity { get; set; }
        public bool PpMedianMedianIntensity { get; set; }
        public bool PpStdDeviationMeanIntensity { get; set; }

        public int TotalPlatesCount { get; set; }
        public int CompletePlatesCount { get; set; }

        public string[] Results { get; set; }
        public int DirCount { get; set; }

        public ImagesInputType ImagesInputType { get; set; }

        public RunDetails()
        {
            // default settings
            Block = 12; // TODO: move to constants/config
            InOneFile = true;
            PpCountsSummed = true;
        }

        public string[] BioConcepts
        {
            get
            {
                List<string> res = new List<string>();

                if (WavelengthMap != null && WavelengthMap.Length > 0)
                {
                    foreach (WMapItem i in WavelengthMap)
                    {
                        res.Add(i.BiologicalConcept);
                    }
                }

                return res.ToArray();
            }
        }

        public bool IsRunning { get { return Status != Status.Complete && Status != Status.Error; } }

        //public bool HasResults { get { return Status == Status.Complete && Results.Length > 0; } }
        // Results field that contains array of result file paths is not loaded within /mc/calc/list request. I.e. Results field is not defined for RunList view.
        
        /// <summary>
        /// This flag indicates that run was created when CPCP Wrapper is already updated to create .csv imageList.
        /// </summary>
        public bool HasImageList { get { return ImagesInputType == ImagesInputType.UploadFromDesktop || (Status == Status.Complete && ImageListSupported); } }

        public string GetResultsRelativePath(ResultFileType fileType)
        {
            if (Results.Length == 0)
                return string.Empty;

            string path = GetResultFilePathByType(fileType);

            if (string.IsNullOrEmpty(path)) // Support backward compatibility: in 1st release there was only one result file (BATCH.ZIP), since then naming convention has been changed.
            {
                path = Results[0];
            }

            return path.Substring(path.ToUpper().IndexOf("CLUSTER_RUNS")).Replace('/', '\\');
        }

        private const string RES_FILENAME_ONE_FILE = "OUTPUT_SINGLEFILE";
        private const string RES_FILENAME_ICP4 = "OUTPUT_BYPLATE";

        private string GetResultFilePathByType(ResultFileType fileType)
        {
            string fileMask = null;
            switch (fileType)
            {
                case ResultFileType.InOneFile:
                    fileMask = RES_FILENAME_ONE_FILE;
                    break;
                case ResultFileType.ICP4:
                    fileMask = RES_FILENAME_ICP4;
                    break;
                default:
                    return Results[0];
            }

            foreach (string s in Results)
            {
                if (s.ToUpper().Contains(fileMask))
                {
                    return s;
                }
            }
            return null;
        }

        public string GetOutputFolderRelativePath()
        {
            if (string.IsNullOrEmpty(OutputFolder))
                return string.Empty;

            if (OutputFolder.IndexOf("CLUSTER_RUNS") < 0)
                throw new ApplicationException("Couldn't parse run's output folder.");

            return OutputFolder.Substring(OutputFolder.IndexOf("CLUSTER_RUNS")).Replace('/', '\\');
        }
    }
}
