﻿using System.Collections.Generic;

namespace HCS.Shared.DTO
{
    public class Protocol : UploadedFile
    {
        public List<string> BiologicalConcepts { get; set; }
        public string WarningMessage { get; set; }
    }
}
