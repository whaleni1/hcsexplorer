﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCS.Shared.DTO
{
    public class UploadedFile
    {
        public string FileName { get; set; }
        public string LocalFileName { get; set; }
    }
}
