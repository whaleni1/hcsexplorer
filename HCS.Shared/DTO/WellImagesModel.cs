﻿using System.Collections.Generic;

namespace HCS.Shared.DTO
{
    public class WellImagesModel : ImageParameters
    {
        public string SizeId { get; set; }
        public IList<ChannelImageParameters> ChannelParameters { get; set; }
        public bool EnableOverlay { get; set; }
        public bool Invert { get; set; }
        public int? ZIndex { get; set; }
        public int? Timepoint { get; set; }
        public string  Version { get; set; }
    }
}
