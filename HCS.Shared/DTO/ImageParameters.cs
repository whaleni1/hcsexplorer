﻿using System.Collections.Generic;

namespace HCS.Shared.DTO
{
    public class ImageParameters : Plate
    {
        public Well Well { get; set; }
        public IList<string> Fields { get; set; }
        public IList<string> Channels { get; set; }
        public int ZIndexCount { get; set; }
        public int TimepointCount { get; set; }
        public IList<string> Versions { get; set; }
    }
}
