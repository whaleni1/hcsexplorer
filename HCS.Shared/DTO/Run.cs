﻿using HCS.Shared.Utils;
using System;

namespace HCS.Shared.DTO
{
    public class Run
    {
        public string IdStr {
            get {
                string formatStr = ImageListOnly ? "LIST-{0}" : "HCS-{0}";
                return String.Format(formatStr, Id); 
            }
        }

        public int Id { get; set; }
        //public string RunId { get; set; }
        public string CalcId { get; set; }
        public string Name { get; set; }
        //public string Group { get; set; } // "My runs" or "All runs"
        public Status Status { get; set; }
        public string ProtocolName { get; set; }
        public string CreatedBy { get; set; }
        public bool ImageListOnly { get; set; }

        public bool InOneFile { get; set; }
        public bool Icp4 { get; set; }

        public bool HasResults { get { return Status == Status.Complete; } }
        public bool ImageListSupported { get; set; }

        /*public int Progress
        {
            get
            {
                if (TotalPlatesCount == 0)
                    return 0;
                return (int)(100 * (double)CompletePlatesCount / (double)TotalPlatesCount);
            }
        }*/

        public DateTime CreatedDate { get; set; }
        public string CreatedDateStr
        {
            get
            {
                return DateTimeUtils.DateTimeToLocalServerTime(CreatedDate);
            }
        }
        public DateTime LastUpdated { get; set; }
        public string LastUpdatedStr
        {
            get
            {
                return DateTimeUtils.DateTimeToLocalServerTime(LastUpdated);
            }
        }

        public int StatusCode { get { return (int)Status; } }
    }
}
