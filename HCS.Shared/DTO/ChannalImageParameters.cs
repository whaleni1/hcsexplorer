﻿namespace HCS.Shared.DTO
{
    public class ChannelImageParameters
    {
        public string Channel { get; set; }
        public string Brightness { get; set; }
        public string Color { get; set; }
        public string Auto { get; set; }
        public string Level { get; set; }
        public string WhitePoint { get; set; }
        public string BlackPoint { get; set; }
    }
}
