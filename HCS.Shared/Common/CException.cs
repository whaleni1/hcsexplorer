//*********************************************************************************
// Company:         NOVARTIS / NIBR / NIBRIT / NX
// Copyright:       Copyright � 2015
// Author:          Erik Hesse
// Date:            01/20/2010
//
// Project:         
// Application:     
// File:            CException.cs
//
// Purpose:         Main Exception class
//
//
// History:         Initial design
//
//
//*********************************************************************************
using System;

namespace HCS.Shared.Common
{
    public class CException : System.Exception
    {
        /// <summary>
        /// CException constructor
        /// </summary>
        /// <param name="strMessage">Message string</param>
        public CException(string strMessage) : base(strMessage)
        {
            // Write the exception to the exception log
            WriteToLog(strMessage);
        }

        /// <summary>
        /// CException constructor
        /// </summary>
        /// <param name="strMessage">Message string</param>
        /// <param name="objInner">Exception object</param>
        public CException(string strMessage , Exception objInner)
            : base(strMessage , objInner)
        {
            // Create the message string
            strMessage += "\nServerName: " + User.ServerName + ", Username: " + User.UserName
                        + "\nSource:\n\t" + objInner.Source.ToString()
                        + "\nStack Trace:\n" + objInner.StackTrace.ToString();

            // Write the exception to the exception log
            WriteToLog(strMessage);
        }

        /// <summary>
        /// WriteToLog
        ///     -    Create the message log class and write the message string to the file
        /// </summary>
        /// <param name="strMessage"></param>
        private void WriteToLog(string strMessage)
        {
            string strFile = "Exception.log";

            // Create the message log class with the correct file path
            MessageLog msgLog = new MessageLog(strFile);

            // Write the message to the file
            msgLog.WriteToLog("Exception at:" + strMessage);
        }
    }
}
