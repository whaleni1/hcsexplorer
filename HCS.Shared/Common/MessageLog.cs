//*********************************************************************************
// Company:         NOVARTIS / NIBR / NIBRIT / NX
// Copyright:       Copyright � 2015
// Author:          Erik Hesse
// Date:            01/20/2010
//
// Project:         IntelliScan Global
// Application:     IntelliScanService
// File:            CException.cs
//
// Purpose:         Main message log class
//
//
// History:         Initial design
//
//
//*********************************************************************************
using System;
#if DEBUG || DEV 
using HCS.Shared.AppMsgLogDev;
#elif TEST
using HCS.Shared.AppMsgLogTest;
#else
using HCS.Shared.AppMsgLog;
#endif

namespace HCS.Shared.Common
{
    public class MessageLog
    {
        private string m_strFilePath;

        /// <summary>
        /// CMessageLog
        /// </summary>
        public MessageLog()
        {
            //Get the log file path
            m_strFilePath = "Message.log";
        }

        /// <summary>
        /// CMessageLog
        /// </summary>
        /// <param name="strFilePath">The string of the file path</param>
        public MessageLog(string strFilePath)
        {
            m_strFilePath = strFilePath;
        }

        /// <summary>
        /// WriteToLog
        ///     -    Write the error message to the log file
        /// </summary>
        /// <param name="strMessage">The message string</param>
        public void WriteToLog(string strMessage)
        {
#if DEBUG
            WriteToLog("HCSExplorer_Debug" , strMessage);
#else
            WriteToLog( "HCSExplorer", strMessage );
#endif
        }

        /// <summary>
        /// WriteToLog
        /// </summary>
        /// <param name="strLogName"></param>
        /// <param name="strMessage"></param>
        public void WriteToLog(string strLogName , string strMessage)
        {
            try
            {
                // Open the web service and write the message to the application log file
                using (AppMsgLogServiceClient AppMsgLogClient = new AppMsgLogServiceClient())
                {
                    AppMsgLogClient.WriteMsgAsync(strLogName , strMessage);
                }
            }
            catch (Exception ex)
            {
                // Create the message string
                strMessage += "\nSource:\n\t" + ex.Source.ToString()
                            + "\nStack Trace:\n" + ex.StackTrace.ToString();

                // Write the message to the local log file
                WriteToFile(strMessage);
            }
        }

        /// <summary>
        /// WriteToFileLog
        /// </summary>
        /// <param name="strLogName"></param>
        /// <param name="strMessage"></param>
        public void WriteToFileLog(string strLogName , string strMessage)
        {
            try
            {
                // Open the web service and write the message to the application log file
                using (AppMsgLogServiceClient AppMsgLogClient = new AppMsgLogServiceClient())
                {
                    AppMsgLogClient.WriteMsg_NoTimeStampAsync(strLogName , strMessage);
                }
            }
            catch (Exception ex)
            {
                // Create the message string
                strMessage += "\nSource:\n\t" + ex.Source.ToString()
                            + "\nStack Trace:\n" + ex.StackTrace.ToString();

                // Write the message to the local log file
                WriteToFile(strMessage);
            }
        }

        /// <summary>
        /// WriteToFile
        ///     -   Write the message to a local text file
        /// </summary>
        /// <param name="strMessage"></param>
        private void WriteToFile(string strMessage)
        {
            try
            {
                strMessage += "\n\tat " + System.DateTime.Now.ToString() + "\n\n";

                // Create the file and write the message
                using (System.IO.StreamWriter swFile = new System.IO.StreamWriter(m_strFilePath , true))
                {
                    swFile.Write(strMessage);
                    swFile.Flush();
                    swFile.Close();
                }
            }
            catch
            {
                // Do nothing
            }
        }
    }
}
