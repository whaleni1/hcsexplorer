﻿using System;
using HCS.Shared.Filtering;

namespace HCS.Shared.Filtering
{
    public class PagingModel<T, F, P> where F : Filter where P : Paging
    {

        public Guid Id { get; set; }
        public F Filter { get; set; }
        public P Paging { get; set; }

        public int Count { get; set; }
        public T[] Rows { get; set; }
    }
}
