﻿namespace HCS.Shared.Filtering
{
    public class Paging
    {
        public string SortedBy { get; set; }
        public string SortOrder { get; set; }
        public int From { get; set; }
        public int To { get; set; }
    }
}
