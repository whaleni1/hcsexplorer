﻿namespace HCS.Shared.Context
{
    public class ExecutionContext : IExecutionContext
    {
        public string UserName { get; set; }
        public string BSMediaServiceURL { get; set; }
        public string CAMediaServiceURL { get; set; }
        public string ZJMediaServiceURL { get; set; }
        public string RIMediaServiceURL { get; set; }
        public string MCServiceURL { get; set; }
        public string Cookies { get; set; }
    }
}
