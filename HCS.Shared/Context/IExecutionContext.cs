﻿namespace HCS.Shared.Context
{
    public interface IExecutionContext
    {
        string UserName { get; }
        string BSMediaServiceURL { get; set; }
        string CAMediaServiceURL { get; set; }
        string ZJMediaServiceURL { get; set; }
        string RIMediaServiceURL { get; set; }
        string MCServiceURL { get; set; }
        string Cookies { get; set; }
    }
}
