﻿using System;
using System.Configuration;

namespace HCS.Shared.Helpers
{
    public class AnalysisAccessChecker
    {
        private static string[] AnalysisUsers
        {
            get
            {
                string csvStr = ConfigurationManager.AppSettings["analysisUsers"];
                return csvStr.Split(',');
            }
        }

        private static bool AnalysisUsersFilterEnabled
        {
            get
            {
                string str = ConfigurationManager.AppSettings["enableAnalysisUsersFilter"];
                return Boolean.Parse(str);
            }
        }

        public static bool HasAccess(string username)
        {
            if (!AnalysisUsersFilterEnabled)
                return true; // allow access for all users when filter is disabled.

            if (string.IsNullOrEmpty(username))
                return false;

            foreach (string usr in AnalysisUsers)
            {
                if (username.ToUpper().Equals(usr.ToUpper()))
                    return true;
            }

            return false;
        }
    }
}