﻿using System;
using System.Configuration;
using HCS.Shared.Common;
using log4net;

namespace HCS.Shared.Logger
{
    public static class FileLogger
    {
        private enum LOG_LEVEL { INFO, DEBUG, ERROR };

        private static object locker = new object();
        private static ILog logger;
        private static string FileLoggerName = "FileLogger";
#if DEBUG
        private static string AppLogName = "HCSExplorerFileLogger_Debug";
#else
        private static string AppLogName = "HCSExplorerFileLogger";
#endif
        /// <summary>
        /// GetLoggerName
        /// </summary>
        /// <returns></returns>
        private static string GetLoggerName()
        {
            return ConfigurationManager.AppSettings[FileLoggerName];
        }

        /// <summary>
        /// GetAppMsgLogLevelValue
        /// </summary>
        /// <param name="LogLevel"></param>
        /// <returns></returns>
        private static bool GetAppMsgLogLevelValue(LOG_LEVEL LogLevel)
        {
            bool value = false;

            switch (LogLevel)
            {
                case LOG_LEVEL.INFO:
                    value = Convert.ToBoolean(ConfigurationManager.AppSettings["AppMsgLog_INFO"]);
                    break;
                case LOG_LEVEL.DEBUG:
                    value = Convert.ToBoolean(ConfigurationManager.AppSettings["AppMsgLog_DEBUG"]);
                    break;
                case LOG_LEVEL.ERROR:
                    value = Convert.ToBoolean(ConfigurationManager.AppSettings["AppMsgLog_ERROR"]);
                    break;
            }

            return value;
        }

        /// <summary>
        /// GetLogger
        /// </summary>
        /// <returns></returns>
        public static ILog GetLogger()
        {
            if (logger == null)
            {
                lock (locker)
                {
                    if (logger == null)
                    {
                        logger = LogManager.GetLogger(GetLoggerName());
                    }
                }
            }

            return logger;
        }

        /// <summary>
        /// Info 
        ///  - use the Info function of the FileLogger and Write to the AppMsgLog
        /// </summary>
        /// <param name="strMessage"></param>
        public static void Info(string strMessage)
        {
            strMessage = "ServerName = " + User.ServerName + ", User = " + User.UserName + ", " + strMessage;

            // Write to logger
            GetLogger().Info(strMessage);

            if (GetAppMsgLogLevelValue(LOG_LEVEL.INFO))
                WriteToAppMsgLog("[INFO] " + strMessage);
        }

        /// <summary>
        /// Debug 
        ///  - use the Debug function of the FileLogger and Write to the AppMsgLog
        /// </summary>
        /// <param name="strMessage"></param>
        public static void Debug(string strMessage)
        {
            strMessage = "ServerName = " + User.ServerName + ", User = " + User.UserName + ", " + strMessage;

            // Write to logger
            GetLogger().Debug(strMessage);

            if (GetAppMsgLogLevelValue(LOG_LEVEL.DEBUG))
                WriteToAppMsgLog("[DEBUG] " + strMessage);
        }

        /// <summary>
        /// Error 
        ///  - use the Error function of the FileLogger and Write to the AppMsgLog
        /// </summary>
        /// <param name="message"></param>
        public static void Error(object message)
        {
            message = "ServerName = " + User.ServerName + ", User = " + User.UserName + ", " + message.ToString();

            // Write to logger
            GetLogger().Error(message);

            if (GetAppMsgLogLevelValue(LOG_LEVEL.ERROR))
                WriteToAppMsgLog("[ERROR] " + message);
        }

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="message"></param>
        /// <param name="eX"></param>
        public static void Error(object message , Exception eX)
        {
            message = "ServerName = " + User.ServerName + ", User = " + User.UserName + ", " + message.ToString();


            // Write to logger
            GetLogger().Error(message , eX);

            if (GetAppMsgLogLevelValue(LOG_LEVEL.ERROR))
                WriteToAppMsgLog("[ERROR] " + message);
        }

        /// <summary>
        /// WriteToAppMsgLog
        /// </summary>
        /// <param name="strMessage"></param>
        public static void WriteToAppMsgLog(string strMessage)
        {
            // Write to AppMsgLog - service call
            MessageLog msgLog = new MessageLog();
            msgLog.WriteToFileLog(AppLogName , DateTime.Now.ToString("MM-dd-yyyy hh:mm:ss:fff ") + strMessage);
        }
    }
}
