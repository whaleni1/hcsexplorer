﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web.Configuration;
using System.Xml.Linq;
using EncryptData;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using UserAuthLib;

namespace HCS.Shared
{
    /// <summary>
    /// HCSServiceAuthorizationManager
    /// </summary>
    public class HCSServiceAuthorizationManager : ServiceAuthorizationManager
    {
        /// <summary>
        /// CheckAccessCore
        /// </summary>
        /// <param name="operationContext"></param>
        /// <returns>True if authorized, False otherwise </returns>
        protected override bool CheckAccessCore(OperationContext operationContext)
        {
            bool authorized = false;
            Object value = null;
            string user = string.Empty;
            string strHeader = string.Empty;

            try
            {
                operationContext.RequestContext.RequestMessage.Properties.TryGetValue("httpRequest" , out value);
                HttpRequestMessageProperty httpRequest = (HttpRequestMessageProperty) (value ?? new HttpRequestMessageProperty());
#if DEBUG
                user = Environment.UserName;
#else
                user = httpRequest.Headers["REMOTE_USER"];
                strHeader = "REMOTE_USER";
                if (string.IsNullOrEmpty(user))
                {
                    user = httpRequest.Headers["NIBR521"];
                    strHeader = "NIBR521";
                }
                if (string.IsNullOrEmpty(user))
                {
                    user = httpRequest.Headers["AUTH_USER"];
                    strHeader = "AUTH_USER";
                }
                if (string.IsNullOrEmpty(user))
                {
                    user = httpRequest.Headers["LOGON_USER"];
                    strHeader = "LOGON_USER";
                }

                //ConnectLogger.Info(GetType().Name + ":CheckAccessCore, User=" + user + ", From header = " + strHeader);
#endif
                User.UserName = user;
                User.ServerName = Environment.MachineName;
                string[] saRoles = null;
                string roleString = string.Empty;

                if (!string.IsNullOrEmpty(user))
                {
                    if (user.Contains("SYS_CHBS_HCSDATA") || user.Contains("SYS_USCA_HCSDATA"))
                    {
                        authorized = true;
                    }
                    else
                    {
                        //ConnectLogger.Info(GetType().Name + ":CheckAccessCore, Check user roles in NED");

                        UserAuth objUserCtl = new UserAuth();
                        CEncryption objEncrypt = new CEncryption();
                        string strNEDServer = WebConfigurationManager.AppSettings["NEDServer"];
                        int iPortNumber = Convert.ToInt16(WebConfigurationManager.AppSettings["PortNumber"]);
                        string strSysAccount = WebConfigurationManager.AppSettings["SystemAccount"];
                        byte[] bArray = objEncrypt.HexStringToByteArray(WebConfigurationManager.AppSettings["SystemPassword"]);
                        string strSysPassword = objEncrypt.Decrypt(bArray);

                        saRoles = objUserCtl.GetAllUserRoles(user , strNEDServer , iPortNumber , strSysAccount , strSysPassword);
                        for (int i = 0 ; i < saRoles.Length ; i++)
                        {
                            saRoles[i] = saRoles[i].Replace(":" , ".");
                        }
                        foreach (string str in saRoles)
                            roleString += str + ",";

                        ConnectLogger.Info(GetType().Name + ":CheckAccessCore, User=" + user + ", From NED Roles=" + roleString);

                        if (!(string.IsNullOrEmpty(roleString) || string.IsNullOrEmpty(user)))
                        {
                            string[] roles = roleString.ToUpper().Split(',');
                            authorized = (roles.Contains<string>("HCS.HCS_ADMIN") ||
                                          roles.Contains<string>("HCS.HCS_SUPPORT") ||
                                          roles.Contains<string>("HCS.HCS_USER")) ||
                                          user.Equals("testicr1" , StringComparison.OrdinalIgnoreCase);
                        }
                    }
                }

                if (!authorized)
                {
                    // The error message is padded so that IE shows the response by default
                    string errorHtml = "<html><HEAD><TITLE>Request Error</TITLE></HEAD><BODY>"
                                       + "<H1>Error processing request</H1>"
                                       + "<p><h2>Unauthorized HCS Explorer User</h2>"
                                       + "Please request access using the following link, "
                                       + "<a href = \"http://appauth.app.prod.nibr.novartis.net/AppAuth/f?p=200:2:3355259402969437:POPULATE_SELECTIONS:NO:APP:P2_ORDER_TYPE,P2_APP_NAME,P2_APP_PROFILE,P2_USERS,P2_ORDER_COMMENT:Registration,HCS Explorer,HCS_USER\">HCS Explorer link in AppAuth</a>"
                                       + "</p></BODY></html>";
                    XElement response = XElement.Load(new StringReader(errorHtml));

                    Message reply = Message.CreateMessage(MessageVersion.None , null , response);
                    HttpResponseMessageProperty responseProperty = new HttpResponseMessageProperty()
                    {
                        StatusCode = System.Net.HttpStatusCode.Unauthorized
                    };
                    responseProperty.Headers[HttpResponseHeader.ContentType] = "text/html";
                    reply.Properties[HttpResponseMessageProperty.Name] = responseProperty;
                    operationContext.RequestContext.Reply(reply);
                    operationContext.RequestContext = null;
                }
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":CheckAccessCore, " + eX.Message , eX);
            }

            return authorized;
        }
    }
}