﻿using System;
using System.IO;
using System.Net;
using System.Security.Principal;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;
using EncryptData;
using HCS.Shared.Common;
using HCS.Shared.Helpers;
using HCS.Shared.Logger;
using UserAuthLib;

namespace HCS.Shared
{
    /// <summary>
    /// HCSAuthenticationModule
    /// </summary>
    public class HCSAuthenticationModule : IHttpModule
    {
        static string redirectPage;
        static bool remoteOnly;
        const int AccessDeniedStatusCode = 401;

        static HCSAuthenticationModule()
        {
            CustomErrorsSection section = WebConfigurationManager.GetWebApplicationSection(@"system.web/customErrors") as CustomErrorsSection;
            if (section == null || section.Mode == CustomErrorsMode.Off)
            {
                return;
            }

            remoteOnly = (section.Mode == CustomErrorsMode.RemoteOnly);

            CustomError definedError = section.Errors.Get(AccessDeniedStatusCode.ToString());
            redirectPage = (definedError != null) ? definedError.Redirect : section.DefaultRedirect;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Init
        /// </summary>
        /// <param name="context"></param>
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += new EventHandler(context_AuthenticateRequest);

            context.EndRequest += RedirectWhenAccessDenied;
        }

        void RedirectWhenAccessDenied(object sender , EventArgs e)
        {
            HttpApplication application = (HttpApplication) sender;
            if (remoteOnly && application.Request.IsLocal) return;
            if (application.Response.StatusCode != AccessDeniedStatusCode || !application.Request.IsAuthenticated) return;

            ConnectLogger.Info(GetType().Name + ":RedirectWhenAccessDenied, User=" + User.UserName + ", " +application.Response.StatusCode);

            application.Response.ClearContent();

            // The error message is padded so that IE shows the response by default
            string errorHtml = "<html><HEAD><TITLE>Request Error</TITLE></HEAD><BODY>" 
                + "<H1>Error processing request</H1>"
                + "<p><h2>Unauthorized HCS Explorer User</h2>"
                     + "Please request access using the following link, "
                     + "<a href = \"http://appauth.app.prod.nibr.novartis.net/AppAuth/f?p=200:2:3355259402969437:POPULATE_SELECTIONS:NO:APP:P2_ORDER_TYPE,P2_APP_NAME,P2_APP_PROFILE,P2_USERS,P2_ORDER_COMMENT:Registration,HCS Explorer,HCS_USER\">HCS Explorer link in AppAuth</a>"
                     + "</p></BODY></html>";

            HttpContext context = application.Context;
            context.ClearError();
            context.Response.StatusCode = 200;
            context.Response.Headers.Add("ContentType" , "text/html");
            context.Response.Write(errorHtml);
          }

        /// <summary>
        /// context_AuthenticateRequest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void context_AuthenticateRequest(Object sender , EventArgs e)
        {
            string user = string.Empty;
            string strHeader = string.Empty;

            try
            {
                HttpApplication app = (HttpApplication) sender;
#if DEBUG 
                user = Environment.UserName;
#else
                user = app.Request.Headers["REMOTE_USER"];
                strHeader = "REMOTE_USER";
                if (string.IsNullOrEmpty(user))
                {
                    user = app.Request.Headers["NIBR521"];
                    strHeader = "NIBR521";
                }
                if (string.IsNullOrEmpty(user))
                {
                    user = app.Request.Headers["AUTH_USER"];
                    strHeader = "AUTH_USER";
                }
                if (string.IsNullOrEmpty(user))
                {
                    user = app.Request.Headers["LOGON_USER"];
                    strHeader = "LOGON_USER";
                }
          
                //FileLogger.Info(GetType().Name + ":context_AuthenticateRequest, User=" + user + ", From header = " + strHeader);
#endif

                User.UserName = user;
                User.ServerName = System.Environment.MachineName;
                string[] saRoles = null;
                string strRoles = string.Empty;

                if (!string.IsNullOrEmpty(user))
                {
                    UserAuth objUserCtl = new UserAuth();
                    CEncryption objEncrypt = new CEncryption();
                    string strNEDServer = WebConfigurationManager.AppSettings["NEDServer"];
                    int iPortNumber = Convert.ToInt16(WebConfigurationManager.AppSettings["PortNumber"]);
                    string strSysAccount = WebConfigurationManager.AppSettings["SystemAccount"];
                    byte[] bArray = objEncrypt.HexStringToByteArray(WebConfigurationManager.AppSettings["SystemPassword"]);
                    string strSysPassword = objEncrypt.Decrypt(bArray);

                    saRoles = objUserCtl.GetAllUserRoles(user , strNEDServer , iPortNumber , strSysAccount , strSysPassword);
                    for (int i = 0 ; i < saRoles.Length ; i++)
                    {
                        saRoles[i] = saRoles[i].Replace(":" , ".");
                    }
                    foreach (string str in saRoles)
                        strRoles += str + ",";
                }
                //FileLogger.Info(GetType().Name + ":context_AuthenticateRequest, User=" + user + ", From NED Roles=" + strRoles);

                // restrict access to specified users for the Run Analysis functionality
                string requestedResource = app.Request.AppRelativeCurrentExecutionFilePath;
                if (!string.IsNullOrEmpty(requestedResource) && requestedResource.Contains("api/run/"))
                {
                    if (!AnalysisAccessChecker.HasAccess(user))
                    {
                        return;
                    }
                }
                if (saRoles != null)
                    app.Context.User = new GenericPrincipal(new GenericIdentity(user) , saRoles);
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":context_AuthenticateRequest, " + eX.Message , eX);
            }
        }
    }
}