﻿using System;
using System.Collections;
using System.Collections.Generic;
using HCS.DataAccess.Database;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using Oracle.ManagedDataAccess.Client;

namespace HCS.DataAccess.Databases
{
    public class Incell : HCSDatabaseAccess
    {
        private DatabaseOracle _objDB;

        /// <summary>
        /// Initialize
        /// - (Override) Establish connection to a database
        /// </summary>
        /// <param name="dbName"></param>
        public override void Initialize(string dbName)
        {
            base.Initialize(dbName);

            // Create the database object with the connection string
            _objDB = new DatabaseOracle(CONNECTION_STR);
        }

        /// <summary>
        /// GetPlateInfo
        /// - (Override) Get plate info from barcode
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfo(Plate plate)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB started");

                // Open the database connection
                if (!_objDB.OpenDB())
                    return plates;

                query = "select barcode, plate_id, plate_format, application, plate_name, plate_timestamp, plate_path from v_plate_info where ";

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    query += "upper(barcode) like :pBarcode";

                    // create parameter object for bind variables for barcode
                    _objDB.AddParameter("pBarcode" , "%" + plate.barcode.ToUpper() + "%" , OracleDbType.Varchar2);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        query += "plate_id = :pPlate_id";

                        // create parameter object for bind variables for plate id
                        _objDB.AddParameter("pPlate_id" , plate.plateId , OracleDbType.Varchar2);
                    }
                    else
                    {
                        query += "upper(plate_path) like :pPath";

                        // create parameter object for bind variables for path
                        _objDB.AddParameter("pPath" , ConvertPlatePath(plate.path) , OracleDbType.Varchar2);
                    }
                }

                string str = query;
                _objDB.PrintParameters(ref str , false);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query started. Query = " + str);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query finished");
                int iCount = 0;

                while (reader.Read())
                {
                    // values from query results
                    Plate p = new Plate();
                    p.barcode = Convert.IsDBNull(reader["barcode"]) ? "" : reader["barcode"].ToString();
                    p.plateId = Convert.IsDBNull(reader["plate_id"]) ? "" : reader["plate_id"].ToString();
                    p.format = Convert.IsDBNull(reader["plate_format"]) ? 0 : Convert.ToInt32(reader["plate_format"]);
                    p.name = Convert.IsDBNull(reader["application"]) ? "" : reader["application"].ToString();
                    p.application = Convert.IsDBNull(reader["plate_name"]) ? "" : reader["plate_name"].ToString();
                    p.timestamp = Convert.IsDBNull(reader["plate_timestamp"]) ? "" : reader["plate_timestamp"].ToString();
                    p.path = Convert.IsDBNull(reader["plate_path"]) ? "" : reader["plate_path"].ToString();

                    // add to plate list
                    plates.Add(p);
                    iCount++;
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":GetPlateInfo  DB finished, record count = " + iCount + ", Plate count = " + plates.Count);
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfo, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// GetPlateInfoByParentPath
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfoByParentPath(Plate plate)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB started");

                // Open the database connection
                if (!_objDB.OpenDB())
                    return plates;

                query = "select barcode, plate_id, plate_format, application, plate_name, plate_timestamp, plate_path from v_plate_info where ";

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    query += "upper(barcode) like :pBarcode";

                    // create parameter object for bind variables for barcode
                    _objDB.AddParameter("pBarcode" , "%" + plate.barcode.ToUpper() + "%" , OracleDbType.Varchar2);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        query += "plate_id = :pPlate_id";

                        // create parameter object for bind variables for plate id
                        _objDB.AddParameter("pPlate_id" , plate.plateId , OracleDbType.Varchar2);
                    }
                    else
                    {
                        query += "upper(plate_path) like :pPath";

                        // create parameter object for bind variables for path
                        _objDB.AddParameter("pPath" , FormatPlatePath(plate.path).ToUpper() , OracleDbType.Varchar2);
                    }
                }

                string str = query;
                _objDB.PrintParameters(ref str , false);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query started. Query = " + str);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query finished");
                int iCount = 0;

                while (reader.Read())
                {
                    // values from query results
                    Plate p = new Plate();
                    p.barcode = Convert.IsDBNull(reader["barcode"]) ? "" : reader["barcode"].ToString();
                    p.plateId = Convert.IsDBNull(reader["plate_id"]) ? "" : reader["plate_id"].ToString();
                    p.format = Convert.IsDBNull(reader["plate_format"]) ? 0 : Convert.ToInt32(reader["plate_format"]);
                    p.name = Convert.IsDBNull(reader["application"]) ? "" : reader["application"].ToString();
                    p.application = Convert.IsDBNull(reader["plate_name"]) ? "" : reader["plate_name"].ToString();
                    p.timestamp = Convert.IsDBNull(reader["plate_timestamp"]) ? "" : reader["plate_timestamp"].ToString();
                    p.path = Convert.IsDBNull(reader["plate_path"]) ? "" : reader["plate_path"].ToString();

                    // add to plate list
                    plates.Add(p);
                    iCount++;
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath  DB finished, record count = " + iCount + ", Plate count = " + plates.Count);
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfoByParentPath, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// FindExactPlateInfo
        ///  - (Override) Get plate info from parent path. Used by CPCP Wrapper only.
        /// </summary>
        /// <param name="plate"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public override PlateList FindExactPlateInfo(Plate plate , bool exactMatch = false)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo  DB started");

                if (!_objDB.OpenDB())
                    return plates;

                query = "select barcode, plate_id, plate_format, application, plate_name, plate_timestamp, plate_path from v_plate_info where "
                        + "upper(plate_path) like :pPath";

                // create parameter object for bind variables for path
                _objDB.AddParameter("pPath" , ConvertPlatePathExact(plate.path , exactMatch) , OracleDbType.Varchar2);

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB sql query finished");

                while (reader.Read())
                {
                    // values from query results
                    Plate p = new Plate();
                    p.barcode = Convert.IsDBNull(reader["barcode"]) ? "" : reader["barcode"].ToString();
                    p.plateId = Convert.IsDBNull(reader["plate_id"]) ? "" : reader["plate_id"].ToString();
                    p.format = Convert.IsDBNull(reader["plate_format"]) ? 0 : Convert.ToInt32(reader["plate_format"]);
                    p.name = Convert.IsDBNull(reader["application"]) ? "" : reader["application"].ToString();
                    p.application = Convert.IsDBNull(reader["plate_name"]) ? "" : reader["plate_name"].ToString();
                    p.timestamp = Convert.IsDBNull(reader["plate_timestamp"]) ? "" : reader["plate_timestamp"].ToString();
                    p.path = Convert.IsDBNull(reader["plate_path"]) ? "" : reader["plate_path"].ToString();

                    // add to plate list
                    plates.Add(p);
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":FindExactPlateInfo, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// QueryImageParameters
        ///  - (Override) Get image parameters associated with a plate well
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public override ImageParameters QueryImageParameters(Plate pl , int row , int col)
        {
            // assemble SQL query based on plate identifier
            var barcodeQuery = !(string.IsNullOrEmpty(pl.barcode));
            var plateQuery = (row == 0 || col == 0);
            var query = string.Empty;
            var imageParamList = new ImageParameters();

            try
            {
                ConnectLogger.Info(GetType().Name + ":QueryImageParameters  DB started");

                // Open the database connection
                if (!_objDB.OpenDB())
                    return imageParamList;

                // create SQL command to query parameters (fields, channels, etc.) for all matching plates
                query = plateQuery ? "select distinct " : "select ";
                query += "barcode, plate_id, plate_format, application, plate_name, plate_timestamp, field, channel, plate_path from v_image_parameters where ";
                query += barcodeQuery ? "barcode = :pPlate " : "plate_id = :pPlate ";
                query += plateQuery ? "order by field asc" : "and row_id = :pRow_id and col_id = :pCol_id order by field asc";

                // create parameter object for bind variables for barcode
                _objDB.AddParameter("pPlate" , barcodeQuery ? pl.barcode : pl.plateId , OracleDbType.Varchar2);

                if (!plateQuery)
                {
                    // create parameter object for bind variables for row
                    _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                    // create parameter object for bind variables for col
                    _objDB.AddParameter("pCol_id" , row , OracleDbType.Int16);
                }

                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                // hashtable to hold collections of ImageParameters objects
                Hashtable imageParamCollections = new Hashtable();

                while (reader.Read())
                {
                    decimal plateid = Convert.ToInt32(reader["plate_id"]);

                    // get reference to unique ImageParameter object associated with the plate id
                    ImageParameter imageParam = new ImageParameter();

                    if (imageParamCollections.ContainsKey(plateid))
                    {
                        // object already exists
                        imageParam = (ImageParameter) imageParamCollections[plateid];
                    }
                    else
                    {
                        // create new object
                        imageParamCollections.Add(plateid , imageParam);

                        // set db name
                        imageParam.database = this.GetDatabaseName();

                        // set plate data for ImageParameter
                        // values from query results
                        Plate p = new Plate();
                        p.barcode = Convert.IsDBNull(reader["barcode"]) ? "" : reader["barcode"].ToString();
                        p.plateId = Convert.IsDBNull(reader["plate_id"]) ? "" : reader["plate_id"].ToString();
                        p.format = Convert.IsDBNull(reader["plate_format"]) ? 0 : Convert.ToInt32(reader["plate_format"]);
                        p.name = Convert.IsDBNull(reader["application"]) ? "" : reader["application"].ToString();
                        p.application = Convert.IsDBNull(reader["plate_name"]) ? "" : reader["plate_name"].ToString();
                        p.timestamp = Convert.IsDBNull(reader["plate_timestamp"]) ? "" : reader["plate_timestamp"].ToString();
                        p.path = Convert.IsDBNull(reader["plate_path"]) ? "" : reader["plate_path"].ToString();

                        imageParam.plateData = p;

                        // set row and col of well data for ImageParameter
                        Well w = new Well();
                        w.row = row;
                        w.col = col;
                        imageParam.wellData = w;
                    }

                    // insert unique field in the list
                    imageParam.addField(Convert.ToInt32(reader["field"]));

                    // insert new channel in the list
                    imageParam.addChannel(reader["channel"].ToString());
                }
                reader.Close();

                // loop though all items in hashtable to create list of ImageParameter
                IDictionaryEnumerator en = imageParamCollections.GetEnumerator();
                while (en.MoveNext())
                {
                    ImageParameter im = (ImageParameter) en.Value;
                    imageParamList.Add(im);
                }

                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParameters, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return imageParamList;
        }

        /// <summary>
        /// Get image parameters associated with a whole plate
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public override PlateImgMetadata QueryImageParametersForWholePlate(Plate pl)
        {
            string query = string.Empty;
            PlateImgMetadata metaData = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":QueryImageParametersForWholePlate DB started");

                // Open the database connection
                if (!_objDB.OpenDB())
                    return metaData;

                // create SQL command to query parameters (fields, channels, etc.) for all matching plates
                query = "select barcode, row_id, col_id, field, plate_id, plate_format, application, plate_name, plate_timestamp, channel, plate_path, file_path, computer_name "
                   + "from v_image_parameters "
                   + " where plate_id = :pPlate "
                   + "order by row_id, col_id, field";

                _objDB.AddParameter("pPlate" , pl.plateId , OracleDbType.Varchar2);

                ConnectLogger.Info(GetType().Name + ":QueryImageParametersForWholePlate DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                reader.FetchSize = 2097152; // 2 Mb

                long curRow = 0, curCol = 0, curField = 0;
                ImageMetadata curImgData = new ImageMetadata();
                while (reader.Read())
                {
                    string barcode = (string) reader["barcode"];
                    decimal plateFormat = (decimal) reader["plate_format"];
                    string computerName = (string) reader["computer_name"];

                    if (metaData == null)
                    {
                        metaData = new PlateImgMetadata
                        {
                            Barcode = barcode ,
                            PlateSize = plateFormat.ToString() ,
                            ImagingComputer = computerName ,
                            ImageList = new ImageMetadataList()
                        };
                    }

                    long row = (long) reader["row_id"];
                    long col = (long) reader["col_id"];
                    long field = (long) reader["field"];

                    if (row != curRow || col != curCol || field != curField) // add ImageMetadata to list
                    {
                        curRow = row;
                        curCol = col;
                        curField = field;

                        string platePath = (string) reader["plate_path"];

                        curImgData = new ImageMetadata
                        {

                            Row = row.ToString() ,
                            Column = col.ToString() ,
                            Field = field.ToString() ,
                            PathName = platePath ,
                            Channels = new ChannelMetadataList()
                        };
                        metaData.ImageList.Add(curImgData);
                    }

                    // add channel, filename to current object
                    string channelName = (string) reader["channel"];
                    string fileName = (string) reader["file_path"];
                    ChannelMetadata chItem = new ChannelMetadata
                    {
                        Channel = channelName ,
                        FileName = fileName
                    };
                    curImgData.Channels.Add(chItem);
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":QueryImageParametersForWholePlate DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParametersForWholePlate, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            //return imageParamList;
            return metaData;
        }

        /// <summary>
        /// QueryImagePath
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override MediaDataList QueryImagePath(string pid , int row , int col , ImageSpec spec)
        {
            var list = new MediaDataList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":QueryImagePath  DB started");

                if (!_objDB.OpenDB())
                    return list;

                // create SQL command
                if (row > 0 && col > 0)
                {
                    string channel = spec.channel;
                    int field = spec.field;

                    if (!string.IsNullOrEmpty(channel))
                    {
                        query = "select imagepath "
                                    + " from v_image_path "
                                    + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id and field = :pFld and channel = :pCh";

                        // create bind variable for plateId
                        _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                        // create bind variable for row
                        _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                        // create bind variable for col
                        _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);

                        // create bind variable for field
                        _objDB.AddParameter("pFld" , field , OracleDbType.Int16);

                        // create bind variable for channel
                        _objDB.AddParameter("pCh" , channel , OracleDbType.Varchar2);

                        ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + query);

                        // execute SQL command
                        OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                        string imageFilePath;

                        if (reader.Read())
                        {
                            imageFilePath = reader.GetString(0);

                            list = new MediaDataList
                                {
                                    new MediaData
                                    {
                                        Path = imageFilePath,
                                        Channel = channel,
                                        Field = field,
                                        Column = col,
                                        Row = row
                                    }
                                };
                        }
                        reader.Close();
                    }
                    else
                    {
                        query = "select imagepath,channel "
                            + "from v_image_path "
                            + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id and field = :pFld";

                        // create bind variable for plateId
                        _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                        // create bind variable for row
                        _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                        // create bind variable for col
                        _objDB.AddParameter("pCol_id" , col , OracleDbType.Varchar2);

                        // create bind variable for field
                        _objDB.AddParameter("pFld" , field , OracleDbType.Int16);

                        ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + query);

                        // execute SQL command
                        OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                        string imageFilePath;
                        string ch;

                        while (reader.Read())
                        {
                            imageFilePath = reader.GetString(0);
                            ch = reader.GetString(1);
                            MediaData md = new MediaData
                            {
                                Path = imageFilePath ,
                                Channel = ch ,
                                Field = field ,
                                Column = col ,
                                Row = row
                            };
                            list.Add(md);
                        }
                        reader.Close();
                    }
                }
                else
                {
                    query = "select imagepath,channel,row_id,col_id,field "
                         + " from v_image_path "
                         + " where plate_id = :pPid";

                    // create bind variable for plateId
                    _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                    ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + query);

                    // execute SQL command
                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                    string imageFilePath;
                    string ch;

                    while (reader.Read())
                    {
                        imageFilePath = reader.GetString(0);
                        ch = reader.GetString(1);
                        int srow = (int) reader.GetDouble(2);
                        int scol = (int) reader.GetDouble(3);
                        int sfield = (int) reader.GetDouble(4);

                        MediaData md = new MediaData
                        {
                            Path = imageFilePath ,
                            Channel = ch ,
                            Field = sfield ,
                            Column = scol ,
                            Row = srow
                        };
                        list.Add(md);
                    }
                    reader.Close();
                }
                ConnectLogger.Info(GetType().Name + ":QueryImagePath DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImagePath, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return list;
        }

        /// <summary>
        /// PrepareForImageStream
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public override FrameFile PrepareForImageStream(Plate pl , Well w , ImageSpec spec , string cacheKey)
        {
            // extract spec data
            var pid = pl.plateId;
            var row = w.row;
            var col = w.col;
            var field = spec.field;
            var channel = spec.channel;
            var query = string.Empty;
            var imageFilePath = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream  DB started");

                // create SQL command
                query = "select imagepath from " +
                        "v_image_path where " +
                        "plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id and field = :pFld and channel = :pCh";

                // create bind variable for plateId
                _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                // create bind variable for row
                _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                // create bind variable for col
                _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);

                // create bind variable for field
                _objDB.AddParameter("pFld" , field , OracleDbType.Int16);

                // create bind variable for channel
                _objDB.AddParameter("pCh" , channel , OracleDbType.Varchar2);

                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream DB sql query started. Query = " + query);

                if (!_objDB.OpenDB())
                    return null;

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                if (reader.Read())
                {
                    imageFilePath = reader.GetString(0);
                    reader.Close();
                }
                else
                {
                    // immediately close database connection in case there is no data returned
                    _objDB.CloseDB();

                    // image not found
                    if (!spec.suppressErr)
                    {
                        throw new ImageNotFoundException();
                    }
                    return null;
                }

                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":PrepareForImageStream, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return new FrameFile { ImagePath = imageFilePath };
        }

        /// <summary>
        /// PrepareForOverlayImageStream
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public override Dictionary<string , FrameFile> PrepareForOverlayImageStream(Plate pl , Well w , OverlayImageSpec spec , string cacheKey)
        {
            var result = new Dictionary<string , FrameFile>();
            var pid = pl.plateId;
            var row = w.row;
            var col = w.col;
            var field = spec.field;
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream  DB started");

                if (!_objDB.OpenDB())
                    return result;

                // create SQL command
                query = "select channel, imagepath "
                            + "from v_image_path "
                            + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id and field = :pFld";

                // create bind variable for plateId
                _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                // create bind variable for row
                _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                // create bind variable for col
                _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);

                // create bind variable for field
                _objDB.AddParameter("pFld" , field , OracleDbType.Int16);

                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                // set image file path for each channel
                while (reader.Read())
                {
                    string channel = reader.GetString(0);
                    string imageFilePath = reader.GetString(1);
                    result[channel] = new FrameFile { ImagePath = imageFilePath };
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":PrepareForOverlayImageStream, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }
            return result;
        }

        /// <summary>
        /// GetImageURL
        ///  - (Override) Get image according to spec and return the URL
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override string GetImageURL(Plate pl , Well w , ImageSpec spec)
        {
            return null;
        }
    }
}

