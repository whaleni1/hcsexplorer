﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using HCS.DataAccess.Database;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;

namespace HCS.DataAccess.Databases
{
    /// <summary>
    /// This is a generic class for accessing and searching the Cellomics Store MSSQL Database (shipped with Cellomics ArrayScan)
    /// All HCSDatabaseAccess child classes associated with Cellomics should instantiate this class
    /// </summary>
    public class CellomicsStoreAccess : DataBaseSQL
    {
        private int PIXEL_MULT_FACTOR = 20;

        /// <summary>
        /// CellomicsStoreAccess
        ///  - Constructor: establish connection to the appropriate Cellomics Store DB as indicated
        /// </summary>
        /// <param name="dbName"></param>
        public CellomicsStoreAccess(string dbName)
        {
            string connString = ConfigurationManager.ConnectionStrings[HCSDBConfigSection.GetConnectionStringName(dbName)].ConnectionString;

            PIXEL_MULT_FACTOR = HCSDBConfigSection.GetPixelMultFactor(dbName);

            // open SQL Server connection
            if (!String.IsNullOrEmpty(connString))
            {
                ConnectionString = connString;
            }
        }
        /// <summary>
        /// FindExactPlateInfo
        ///  - (Override) Get plate info from barcode
        /// </summary>
        /// <param name="plate"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public PlateList FindExactPlateInfo(Plate plate , bool exactMatch = false)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB started");

                query = "select p.name, p.cs_plateid, p.formfactordata, p.scanid, p.platestarttime, p.wellcount, p.imagepath"
                    + " from Store.dbo.Plate p"
                    + " where upper(plate_path) like @sqlParam";

                // create parameter object for bind variables for path
                AddParameter("sqlParam" , HCSDatabaseAccess.ConvertPlatePathExact(plate.path , exactMatch) , SqlDbType.VarChar);

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB sql query started. Query = " + query);

                if (!OpenDB())
                    return plates;

                // execute SQL command
                SqlDataReader reader = (SqlDataReader) ExecuteReader(query);
                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB sql query finished");

                while (reader.Read())
                {
                    // determine plate format
                    int iFormat = 0;
                    if (reader.IsDBNull(2))
                    {
                        // use 'wellcount' column if 'formfactordata' is null
                        if (!reader.IsDBNull(5))
                        {
                            iFormat = GetPlateFormat(reader.GetInt32(5));
                        }
                    }
                    else
                    {
                        // infer plate format from 'formfactordata' column
                        iFormat = GetPlateFormat(reader.GetString(2));
                    }

                    //map result set to Plate object and add to plate list
                    plates.Add(new Plate
                    {
                        barcode = reader.GetString(0) ,
                        plateId = reader.GetDecimal(1).ToString() ,
                        format = iFormat ,
                        name = reader.GetString(3) ,
                        timestamp = reader.GetDateTime(4).ToString() ,
                        path = reader.GetString(6)
                    });
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB finished");
            }
            catch (Exception eX)
            {
                PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":FindExactPlateInfo, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                CloseDB();
            }
            return plates;
        }
        /// <summary>
        /// GetPlateInfo
        ///  - Get a list of plate with matching barcode, plate id, or image folder path
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public PlateList GetPlateInfo(Plate plate)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB started");

                // Open the database connection
                if (!OpenDB())
                    return plates;

                // Create the SQL statement
                query = "select p.name, p.cs_plateid, p.formfactordata, p.scanid, p.platestarttime, p.wellcount, p.imagepath from Store.dbo.Plate p where ";

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    // assign barcode to SqlParameter value
                    query += "upper(p.name) like @sqlParam";
                    AddParameter("@sqlParam" , "%" + plate.barcode.ToUpper() + "%" , SqlDbType.VarChar);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        // assign plate id to SqlParameter value
                        query += "p.cs_plateid = @sqlParam";
                        AddParameter("@sqlParam" , plate.plateId , SqlDbType.VarChar);
                    }
                    else
                    {
                        // assign plate folder path to SqlParameter value
                        query += "upper(p.imagepath) like @sqlParam";
                        AddParameter("@sqlParam" , convertPlatePath(plate.path) , SqlDbType.VarChar);
                    }
                }

                string strTemp = query;
                PrintParameters(ref strTemp , false);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query started. Query = " + strTemp);

                SqlDataReader reader = (SqlDataReader) ExecuteReader(query , false);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query finished");

                while (reader.Read())
                {
                    // plate barcode from cellomics db
                    string intBarcode = reader.GetString(0);

                    // plate id from query
                    int plateid = reader.GetInt32(1);

                    // determine plate format
                    int format = 0;
                    if (reader.IsDBNull(2))
                    {
                        // use 'wellcount' column if 'formfactordata' is null
                        if (!reader.IsDBNull(5))
                        {
                            format = GetPlateFormat(reader.GetInt32(5));
                        }
                    }
                    else
                    {
                        // infer plate format from 'formfactordata' column
                        format = GetPlateFormat(reader.GetString(2));
                    }

                    // plate name
                    string name = "";
                    if (!reader.IsDBNull(3))
                    {
                        name = reader.GetString(3);
                    }

                    // plate timestamp
                    DateTime timestamp = reader.GetDateTime(4);

                    // plate path
                    string path = reader.GetString(6);

                    // create Plate object
                    Plate p = new Plate();
                    p.barcode = intBarcode;
                    p.plateId = plateid.ToString();
                    p.format = format;
                    p.name = name;
                    p.timestamp = timestamp.ToString();
                    p.path = path;

                    // add to plate list
                    plates.Add(p);
                }
                reader.Close();
            }
            catch (Exception eX)
            {
                PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfo, Error = " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// GetPlateInfoByParentPath
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public PlateList GetPlateInfoByParentPath(Plate plate)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB started");

                // Open the database connection
                if (!OpenDB())
                    return plates;

                // Create the SQL statement
                query = "select p.name, p.cs_plateid, p.formfactordata, p.scanid, p.platestarttime, p.wellcount, p.imagepath from Store.dbo.Plate p where ";

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    // assign barcode to SqlParameter value
                    query += "upper(p.name) like @sqlParam";
                    AddParameter("@sqlParam" , "%" + plate.barcode.ToUpper() + "%" , SqlDbType.VarChar);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        // assign plate id to SqlParameter value
                        query += "p.cs_plateid = @sqlParam";
                        AddParameter("@sqlParam" , plate.plateId , SqlDbType.VarChar);
                    }
                    else
                    {
                        // assign plate folder path to SqlParameter value
                        query += "upper(p.imagepath) like @sqlParam";
                        AddParameter("@sqlParam" , HCSDatabaseAccess.FormatPlatePath(plate.path).ToUpper() , SqlDbType.VarChar);
                    }
                }

                string strTemp = query;
                PrintParameters(ref strTemp , false);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query started. Query = " + strTemp);

                SqlDataReader reader = (SqlDataReader) ExecuteReader(query , false);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query finished");

                while (reader.Read())
                {
                    // plate barcode from cellomics db
                    string intBarcode = reader.GetString(0);

                    // plate id from query
                    int plateid = reader.GetInt32(1);

                    // determine plate format
                    int format = 0;
                    if (reader.IsDBNull(2))
                    {
                        // use 'wellcount' column if 'formfactordata' is null
                        if (!reader.IsDBNull(5))
                        {
                            format = GetPlateFormat(reader.GetInt32(5));
                        }
                    }
                    else
                    {
                        // infer plate format from 'formfactordata' column
                        format = GetPlateFormat(reader.GetString(2));
                    }

                    // plate name
                    string name = "";
                    if (!reader.IsDBNull(3))
                    {
                        name = reader.GetString(3);
                    }

                    // plate timestamp
                    DateTime timestamp = reader.GetDateTime(4);

                    // plate path
                    string path = reader.GetString(6);

                    // create Plate object
                    Plate p = new Plate();
                    p.barcode = intBarcode;
                    p.plateId = plateid.ToString();
                    p.format = format;
                    p.name = name;
                    p.timestamp = timestamp.ToString();
                    p.path = path;

                    // add to plate list
                    plates.Add(p);
                }
                reader.Close();
            }
            catch (Exception eX)
            {
                PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfoByParentPath, Error = " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// QueryImageParameters
        ///  - Get image parameters associated with a plate well
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public ImageParameters QueryImageParameters(string dbName , Plate pl , int row , int col)
        {
            var imageParamList = new ImageParameters();
            var query = string.Empty;
            var plateQuery = (row == 0 || col == 0);

            try
            {
                // Open the database connection
                if (!OpenDB())
                    return imageParamList;

                if (plateQuery)
                {
                    // add "distinct" keyword to query if no row and col are not specified
                    query =
                        "select distinct p.name, p.cs_plateid, p.formfactordata, p.scanid, p.platestarttime, wf.wcol, pc.name as channelname, pc.channelnumber, p.wellcount, p.imagepath from " +
                        "STORE.dbo.plate p inner join " +
                        "STORE.dbo.protocolchannel pc on (p.protocolid = pc.protocolid) inner join " +
                        "STORE.dbo.well w on (p.cs_plateid = w.cs_plateid) inner join " +
                        "STORE.dbo.wfield wf on (w.id = wf.wellid) ";
                }
                else
                {
                    query =
                        "select p.name, p.cs_plateid, p.formfactordata, p.scanid, p.platestarttime, wf.wcol, pc.name as channelname, pc.channelnumber, p.wellcount, p.imagepath from " +
                        "STORE.dbo.Plate p inner join " +
                        "STORE.dbo.protocolchannel pc on (p.protocolid = pc.protocolid) inner join " +
                        "STORE.dbo.well w on (p.cs_plateid = w.cs_plateid) inner join " +
                        "STORE.dbo.wfield wf on (w.id = wf.wellid) ";
                }

                if (!String.IsNullOrEmpty(pl.barcode))
                {
                    // use "like" query for barcode due to inconsistent naming in cellomics database
                    query += "where p.name like @plateSqlParam ";
                    AddParameter("@plateSqlParam" , "%" + pl.barcode + "%" , SqlDbType.VarChar);
                }
                else
                {
                    query += "where p.cs_plateid = @plateSqlParam ";
                    AddParameter("@plateSqlParam" , pl.plateId , SqlDbType.VarChar);
                }

                if (!plateQuery)
                {
                    query += "and w.prow = @rowSqlParam and w.pcol = @colSqlParam ";
                    AddParameter("@rowSqlParam" , row - 1 , SqlDbType.VarChar);
                    AddParameter("@colSqlParam" , col - 1 , SqlDbType.VarChar);
                }

                query += "order by wf.wcol asc";

                string strTemp = query;
                PrintParameters(ref strTemp , false);
                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB sql query started. Query = " + strTemp);

                SqlDataReader reader = (SqlDataReader) ExecuteReader(query , false);

                // hashtable to hold collections of ImageParameters objects
                Hashtable imageParamCollections = new Hashtable();

                while (reader.Read())
                {
                    string barcode = reader.GetString(0);
                    int plateid = reader.GetInt32(1);

                    // determine plate format
                    int format = 0;
                    if (reader.IsDBNull(2))
                    {
                        // use 'wellcount' column if 'formfactordata' is null
                        if (!reader.IsDBNull(8))
                        {
                            //format = reader.GetInt32(8);
                            format = GetPlateFormat(reader.GetInt32(8));
                        }
                    }
                    else
                    {
                        // infer plate format from 'formfactordata' column
                        format = GetPlateFormat(reader.GetString(2));
                    }

                    // get reference to unique ImageParameter object associated with the plate id
                    ImageParameter imageParam = new ImageParameter();

                    if (imageParamCollections.ContainsKey(plateid))
                    {
                        // object already exists
                        imageParam = (ImageParameter) imageParamCollections[plateid];
                    }
                    else
                    {
                        // create new object
                        imageParamCollections.Add(plateid , imageParam);

                        // set db name
                        imageParam.database = dbName;

                        // set plate data for ImageParameter
                        Plate p = new Plate();
                        p.barcode = barcode;
                        p.plateId = plateid.ToString();

                        // plate format
                        p.format = format;

                        // name of plate
                        string name = "";
                        if (!reader.IsDBNull(3))
                        {
                            name = reader.GetString(3);
                        }
                        p.name = name;

                        // timestamp of plate
                        DateTime timestamp = reader.GetDateTime(4);
                        p.timestamp = timestamp.ToString();

                        // path of plate
                        string path = reader.GetString(9);
                        p.path = path;

                        imageParam.plateData = p;

                        // set row and col of well data for ImageParameter
                        Well w = new Well();
                        w.row = row;
                        w.col = col;
                        imageParam.wellData = w;
                    }

                    // insert unique field in the list
                    int field = reader.GetInt32(5);
                    imageParam.addField(field);

                    // insert channel name
                    string channel = reader.GetString(6);
                    imageParam.addChannel(channel);
                }
                reader.Close();

                // loop though all items in hashtable to create list of ImageParameter
                IDictionaryEnumerator en = imageParamCollections.GetEnumerator();
                while (en.MoveNext())
                {
                    ImageParameter im = (ImageParameter) en.Value;
                    imageParamList.Add(im);
                }

                ConnectLogger.Info(GetType().Name + ":QueryImageParameters  DB finished");
            }
            catch (Exception eX)
            {
                PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParameters, Error = " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                CloseDB();
            }

            return imageParamList;
        }

        /// <summary>
        /// QueryImageParametersForWholePlate
        ///  - Get image parameters associated with a whole plate
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public PlateImgMetadata QueryImageParametersForWholePlate(Plate pl)
        {
            PlateImgMetadata metaData = null;

            // assemble SQL query based on plate identifier
            string query = string.Empty;

            try
            {
                // Open the database connection
                if (!OpenDB())
                    return metaData;

                // add "distinct" keyword to query if no row and col are not specified
                query =
                    "select distinct p.name, w.prow, w.pcol, p.cs_plateid, p.formfactordata, p.scanid, p.platestarttime, wf.wcol, pc.name as channelname, pc.channelnumber, p.wellcount, p.imagepath from " +
                    "STORE.dbo.plate p inner join " +
                    "STORE.dbo.protocolchannel pc on (p.protocolid = pc.protocolid) inner join " +
                    "STORE.dbo.well w on (p.cs_plateid = w.cs_plateid) inner join " +
                    "STORE.dbo.wfield wf on (w.id = wf.wellid) " +
                    "where p.cs_plateid = @plateSqlParam " +
                    "order by wf.wcol asc";

                AddParameter("@plateSqlParam" , pl.plateId , SqlDbType.VarChar);

                string strTemp = query;
                PrintParameters(ref strTemp , false);
                ConnectLogger.Info(GetType().Name + ":QueryImageParametersForWholePlate DB sql query started. Query = " + strTemp);

                SqlDataReader reader = (SqlDataReader) ExecuteReader(query , false);

                ConnectLogger.Info(GetType().Name + ":QueryImageParametersForWholePlate DB sql query finished");

                // hashtable to hold collections of ImageParameters objects
                Hashtable imageParamCollections = new Hashtable();

                int curRow = 0,
                    curCol = 0,
                    curField = 0,
                    curZIndex = 0,
                    curTimelineIndex = 0,
                    curActionListIndex = 0;

                ImageMetadata curImgData = new ImageMetadata();
                while (reader.Read())
                {
                    string barcode = reader.GetString(0);
                    int plateid = reader.GetInt32(1);
                    string computerName = "";

                    // determine plate format
                    int plateFormat = 0;
                    if (reader.IsDBNull(2))
                    {
                        // use 'wellcount' column if 'formfactordata' is null
                        if (!reader.IsDBNull(8))
                        {
                            //format = reader.GetInt32(8);
                            plateFormat = GetPlateFormat(reader.GetInt32(8));
                        }
                    }
                    else
                    {
                        // infer plate format from 'formfactordata' column
                        plateFormat = GetPlateFormat(reader.GetString(2));
                    }

                    if (metaData == null)
                    {
                        metaData = new PlateImgMetadata
                        {
                            Barcode = barcode ,
                            PlateSize = plateFormat.ToString() ,
                            ImagingComputer = computerName ,
                            ImageList = new ImageMetadataList()
                        };
                    }

                    int row = (int) reader["prow"];
                    int col = (int) reader["pcol"];
                    int field = 0;// (int) reader["field_idx"];
                    int zIndex = 0;//(int) reader["z_idx"];
                    int timelineIndex = 0;// (int) reader["timeline_idx"];
                    int actionListIndex = 0;// (int) reader["action_list_idx"];

                    if (row != curRow || col != curCol || field != curField || timelineIndex != curTimelineIndex || actionListIndex != curActionListIndex)
                    {
                        curRow = row;
                        curCol = col;
                        curField = field;
                        curZIndex = zIndex;
                        curTimelineIndex = timelineIndex;
                        curActionListIndex = actionListIndex;

                        string platePath = (string) reader["imagepath"];

                        curImgData = new ImageMetadata
                        {

                            Row = row.ToString() ,
                            Column = col.ToString() ,
                            Field = field.ToString() ,
                            ZIndex = zIndex.ToString() ,
                            TimelineIndex = timelineIndex.ToString() ,
                            ActionListIndex = actionListIndex.ToString() ,
                            PathName = platePath ,
                            Channels = new ChannelMetadataList()
                        };
                        metaData.ImageList.Add(curImgData);
                    }

                    // add channel, filename to current object
                    string channelName = (string) reader["channelnumber"];
                    string fileName = (string) reader["imagepath"];
                    ChannelMetadata chItem = new ChannelMetadata
                    {
                        Channel = channelName ,
                        FileName = fileName
                    };
                    curImgData.Channels.Add(chItem);
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":QueryImageParametersForWholePlate  DB finished");
            }
            catch (Exception eX)
            {
                PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParametersForWholePlate, Error = " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                CloseDB();
            }

            return metaData;
        }
        /// <summary>
        /// GetImagePath
        ///  - Search the Cellomics Store database to get the UNC path for a image file
        ///  - Return empty string is no image file is found
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public string GetImagePath(Plate pl , Well w , ImageSpec spec)
        {
            // extract spec data
            var plateId = Convert.ToInt16(pl.plateId);
            var row = w.row;
            var col = w.col;
            var field = spec.field;
            var channel = spec.channel;
            var format = spec.format;
            var query = string.Empty;
            var imageFilePath = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImagePath DB started");

                // Open the database connection
                if (!OpenDB())
                    return imageFilePath;

                // create SQL command
                query = "select f.filelocation, f.filename " +
                    "from STORE.dbo.Plate p inner join " +
                    "STORE.dbo.ProtocolChannel pc on (p.protocolid = pc.protocolid) inner join " +
                    "STORE.dbo.Well w on (p.cs_plateid = w.cs_plateid) inner join " +
                    "STORE.dbo.wfield wf on(w.id = wf.wellid) inner join " +
                    "STORE.dbo.fimage f on(wf.id = f.wfieldid) " +
                    "where p.cs_plateid = @plateIdSqlParam and w.prow = @rowSqlParam and w.pcol = @colSqlParam and wf.wcol = @fldSqlParam and pc.name = @chIdSqlParam and f.imagenumber = pc.channelnumber";

                // Initialize SqlParameter
                AddParameter("@plateIdSqlParam" , plateId , SqlDbType.Int);
                // subtract 1 since row starts with 0 in Cellomics Store
                AddParameter("@rowSqlParam" , row - 1 , SqlDbType.Int);
                // subtract 1 since col starts with 0 in Cellomics Store
                AddParameter("@colSqlParam" , col - 1 , SqlDbType.Int);
                AddParameter("@fldSqlParam" , field , SqlDbType.Int);
                AddParameter("@chIdSqlParam" , channel , SqlDbType.VarChar);

                string strTemp = query;
                PrintParameters(ref strTemp , false);
                ConnectLogger.Info(GetType().Name + ":GetImagePath DB sql query started. Query = " + strTemp);

                // execute SQL command
                SqlDataReader reader = (SqlDataReader) ExecuteReader(query , false);

                ConnectLogger.Info(GetType().Name + ":GetImagePath DB sql query finished");

                if (reader.Read())
                {
                    // concatenate to form UNC path for image file
                    string fileLocation = reader.GetString(0);
                    string fileName = reader.GetString(1);
                    imageFilePath = fileLocation + fileName;
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":GetImagePath  DB finished");
            }
            catch (Exception eX)
            {
                PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetImagePath, Error = " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                CloseDB();
            }

            return imageFilePath;
        }

        /// <summary>
        /// QueryImagePath
        /// </summary>
        /// <param name="plateId"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public MediaDataList QueryImagePath(string plateId , int row , int col , ImageSpec spec)
        {
            Plate pl = new Plate { plateId = plateId };
            MediaDataList list = null;

            try
            {
                if (row > 0 && col > 0)
                {
                    Well w = new Well { col = col , row = row };
                    string channel = spec.channel;
                    if (!string.IsNullOrEmpty(channel))
                    {
                        string origImageFilePath = GetImagePath(pl , w , spec);
                        if (String.IsNullOrEmpty(origImageFilePath))
                        {
                            list = new MediaDataList();
                        }
                        else
                        {
                            list = new MediaDataList
                            {
                                new MediaData
                                {
                                    Path = origImageFilePath,
                                    Channel = channel,
                                    Row = row,
                                    Column = col,
                                    Field = spec.field
                                }
                            };
                        }
                    }
                    else
                    {
                        list = GetImagePaths(pl , w , spec);
                    }
                }
                else
                {
                    //all plreturn 
                    list = GetImagePaths(pl , null , null);
                }
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":QueryImagePath" , eX);
            }

            return list;
        }

        /// <summary>
        /// GetImagePaths
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public MediaDataList GetImagePaths(Plate pl , Well w , ImageSpec spec)
        {
            // extract spec data
            var plateId = Convert.ToInt16(pl.plateId);
            var list = new MediaDataList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImagePaths  DB started");

                // Open the database connection
                if (!OpenDB())
                    return list;

                if (w != null)
                {
                    int row = w.row;
                    int col = w.col;
                    int field = spec.field;
                    string format = spec.format;

                    // create SQL command
                    query = "select f.filelocation, f.filename, pc.name " +
                            "from STORE.dbo.Plate p inner join " +
                            "STORE.dbo.ProtocolChannel pc on (p.protocolid = pc.protocolid) inner join " +
                            "STORE.dbo.Well w on (p.cs_plateid = w.cs_plateid) inner join " +
                            "STORE.dbo.wfield wf on(w.id = wf.wellid) inner join " +
                            "STORE.dbo.fimage f on(wf.id = f.wfieldid) " +
                            "where p.cs_plateid = @plateIdSqlParam and w.prow = @rowSqlParam and w.pcol = @colSqlParam and wf.wcol = @fldSqlParam and f.imagenumber = pc.channelnumber";

                    // Initialize SqlParameter
                    AddParameter("@plateIdSqlParam" , plateId , SqlDbType.Int);
                    // subtract 1 since row starts with 0 in Cellomics Store
                    AddParameter("@rowSqlParam" , row - 1 , SqlDbType.Int);
                    // subtract 1 since col starts with 0 in Cellomics Store
                    AddParameter("@colSqlParam" , col - 1 , SqlDbType.Int);
                    AddParameter("@fldSqlParam" , field , SqlDbType.Int);

                    string strTemp = query;
                    PrintParameters(ref strTemp , false);
                    ConnectLogger.Info(GetType().Name + ":GetImagePaths DB sql query started. Query = " + query);

                    // execute SQL command
                    SqlDataReader reader = (SqlDataReader) ExecuteReader(query , false);

                    string imageFilePath = "";

                    while (reader.Read())
                    {
                        // concatenate to form UNC path for image file
                        string fileLocation = reader.GetString(0);
                        string fileName = reader.GetString(1);
                        string ch = reader.GetString(2);
                        imageFilePath = fileLocation + fileName;
                        MediaData md = new MediaData { Path = imageFilePath , Channel = ch };
                        list.Add(md);

                    }
                    reader.Close();
                }
                else
                {
                    query = "select f.filelocation, f.filename, pc.name, w.prow, w.pcol,  wf.wcol " +
                            "from STORE.dbo.Plate p inner join " +
                            "STORE.dbo.ProtocolChannel pc on (p.protocolid = pc.protocolid) inner join " +
                            "STORE.dbo.Well w on (p.cs_plateid = w.cs_plateid) inner join " +
                            "STORE.dbo.wfield wf on(w.id = wf.wellid) inner join " +
                            "STORE.dbo.fimage f on(wf.id = f.wfieldid) " +
                            "where p.cs_plateid = @sqlParam and f.imagenumber = pc.channelnumber";

                    // Initialize SqlParameter
                    AddParameter("@sqlParam" , plateId.ToString() , SqlDbType.VarChar);

                    string strTemp = query;
                    PrintParameters(ref strTemp , false);
                    ConnectLogger.Info(GetType().Name + ":GetImagePaths DB sql query started. Query = " + strTemp);

                    // execute SQL command
                    SqlDataReader reader = (SqlDataReader) ExecuteReader(query , false);

                    string imageFilePath = "";

                    while (reader.Read())
                    {
                        // concatenate to form UNC path for image file
                        string fileLocation = reader.GetString(0);
                        string fileName = reader.GetString(1);
                        string ch = reader.GetString(2);
                        int row = (reader.GetInt32(3) + 1);
                        int col = (reader.GetInt32(4) + 1);
                        int field = (reader.GetInt32(5));
                        imageFilePath = fileLocation + fileName;
                        MediaData md = new MediaData
                        {
                            Path = imageFilePath ,
                            Channel = ch ,
                            Field = field ,
                            Row = row ,
                            Column = col
                        };
                        list.Add(md);

                    }
                    reader.Close();
                }

                ConnectLogger.Info(GetType().Name + ":GetImagePaths DB finished");
            }
            catch (Exception eX)
            {
                PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetImagePaths, Error = " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                CloseDB();
            }
            return list;
        }

        /// <summary>
        /// PrepareForImageStream
        ///  - Get image path according to spec
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public string PrepareForImageStream(Plate pl , Well w , ImageSpec spec , string cacheKey)
        {
            string origImageFilePath = GetImagePath(pl , w , spec);

            try
            {
                if (String.IsNullOrEmpty(origImageFilePath))
                {
                    // image not found
                    if (!spec.suppressErr)
                    {
                        throw new ImageNotFoundException();
                    }
                    origImageFilePath = null;
                }
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":PrepareForImageStream" , eX);
            }
            return origImageFilePath;

        }

        /// <summary>
        /// PrepareForOverlayImageStream
        ///  - Return dictionary of img paths per channel
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public Dictionary<string , FrameFile> PrepareForOverlayImageStream(Plate pl , Well w , OverlayImageSpec spec , string cacheKey)
        {
            var result = new Dictionary<string , FrameFile>();

            try
            {
                IDictionaryEnumerator en = GetOverlayImageAssoc(pl , w , spec).GetEnumerator();
                while (en.MoveNext())
                {
                    string chName = (string) en.Key;
                    string imageFilePath = (string) en.Value;
                    result[chName] = new FrameFile { ImagePath = imageFilePath };
                }
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":PrepareForOverlayImageStream" , eX);
            }
            return result;

        }

        /// <summary>
        /// GetOverlayImageAssoc
        ///  - Look up image file path for each constituent channel of a overlay image
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns>hashtable to associate channel name with the image file path</returns>
        public Hashtable GetOverlayImageAssoc(Plate pl , Well w , OverlayImageSpec spec)
        {
            var overlayImageAssoc = new Hashtable();
            var plateId = Convert.ToInt16(pl.plateId);
            var row = w.row;
            var col = w.col;
            var field = spec.field;
            var format = spec.format;
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetOverlayImageAssoc  DB started");

                // Open the database connection
                if (!OpenDB())
                    return overlayImageAssoc;

                // create SQL command
                query = "select pc.name, f.filelocation, f.filename " +
                    "from STORE.dbo.Plate p inner join " +
                    "STORE.dbo.ProtocolChannel pc on (p.protocolid = pc.protocolid) inner join " +
                    "STORE.dbo.Well w on (p.cs_plateid = w.cs_plateid) inner join " +
                    "STORE.dbo.wfield wf on(w.id = wf.wellid) inner join " +
                    "STORE.dbo.fimage f on(wf.id = f.wfieldid) " +
                    "where p.cs_plateid = @plateIdSqlParam and w.prow = @rowSqlParam and w.pcol = @colSqlParam and wf.wcol = @fldSqlParam and f.imagenumber = pc.channelnumber";

                // Initialize SqlParameter
                AddParameter("@plateIdSqlParam" , plateId , SqlDbType.Int);
                // subtract 1 since row starts with 0 in Cellomics Store
                AddParameter("@rowSqlParam" , row - 1 , SqlDbType.Int);
                // subtract 1 since col starts with 0 in Cellomics Store
                AddParameter("@colSqlParam" , col - 1 , SqlDbType.Int);
                AddParameter("@fldSqlParam" , field , SqlDbType.Int);

                string strTemp = query;
                PrintParameters(ref strTemp , false);
                ConnectLogger.Info(GetType().Name + ":GetOverlayImageAssoc DB sql query started. Query = " + strTemp);

                // execute SQL command
                SqlDataReader reader = (SqlDataReader) ExecuteReader(query , false);

                while (reader.Read())
                {
                    string chName = reader.GetString(0);
                    string imageFilePath = reader.GetString(1) + reader.GetString(2);

                    // insert new record into hashtable
                    overlayImageAssoc.Add(chName , imageFilePath);
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":GetOverlayImageAssoc DB finished");
            }
            catch (Exception eX)
            {
                PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetOverlayImageAssoc, Error = " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                CloseDB();
            }
            return overlayImageAssoc;
        }

        /// <summary>
        /// convertPlatePath
        ///  - Translate mapped image folder path to proper UNC path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string convertPlatePath(string path)
        {
            return HCSDatabaseAccess.ConvertPlatePath(path);
        }


        /// <summary>
        /// GetPlateFormat
        /// - Determine plate format (96, 384, 1536) based on 'wellcount' column in cellomics database
        ///  All numbers less than 96 will be rounded up to 96
        /// </summary>
        /// <param name="wellcount"></param>
        /// <returns></returns>
        private int GetPlateFormat(int wellcount)
        {
            int iPlateFormat = 0;

            if (wellcount <= 96)
            {
                iPlateFormat = 96;
            }
            else
            {
                if (wellcount <= 384)
                {
                    iPlateFormat = 384;
                }
                else
                {
                    iPlateFormat = 1536;
                }
            }

            return iPlateFormat;
        }

        /// <summary>
        /// GetPlateFormat
        ///  - Determine plate format based on 'formfactordata' column in cellomics database
        /// </summary>
        /// <param name="formfactordata"></param>
        /// <returns></returns>
        private int GetPlateFormat(string formfactordata)
        {
            var format = 0;

            try
            {
                if (!String.IsNullOrEmpty(formfactordata))
                {
                    // split formfactordata string and extract the first two values (row, col)
                    string[] parts = formfactordata.Split(';');
                    int row = 0;
                    int col = 0;

                    int.TryParse(parts[0] , out row);
                    int.TryParse(parts[1] , out col);

                    // calculate plate format
                    format = row * col;
                }
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":GetPlateFormat" , eX);
            }

            return GetPlateFormat(format);
        }
    }
}