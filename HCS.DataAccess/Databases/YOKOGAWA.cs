﻿using System;
using System.Collections.Generic;
using System.IO;
using HCS.DataAccess.Database;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using Oracle.ManagedDataAccess.Client;

namespace HCS.DataAccess.Databases
{
    public class Yokogawa : HCSDatabaseAccess
    {
        private DatabaseOracle _objDB;

        /// <summary>
        /// YokogawaImagingComputer
        /// </summary>
        private static string YokogawaImagingComputer
        {
            get
            {
                string strValue = System.Configuration.ConfigurationManager.AppSettings["YokogawaImagingComputer"];
                if (strValue == null)
                    strValue = string.Empty;

                return strValue;
            }
        }

        /// <summary>
        /// Initialize
        ///  - (Override) Establish connection to a database
        /// </summary>
        /// <param name="dbName"></param>
        public override void Initialize(string dbName)
        {
            base.Initialize(dbName);

            // Create the database object with the connection string
            _objDB = new DatabaseOracle(CONNECTION_STR);
        }

        /// <summary>
        /// GetPlateInfo
        ///  - (Override) Get plate info from barcode
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfo(Plate plate)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB started");

                if (!_objDB.OpenDB())
                    return plates;

                query = "select barcode, plate_id, plate_format, plate_name, plate_timestamp, plate_path "
                + "from v_yokogawa_plate_info "
                + "where ";

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    query += "upper(barcode) like :pBarcode";

                    // create parameter object for bind variables for barcode
                    _objDB.AddParameter("pBarcode" , "%" + plate.barcode.ToUpper() + "%" , OracleDbType.Varchar2);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        query += "plate_id = :pPlate_id";

                        // create parameter object for bind variables for plate id
                        _objDB.AddParameter("pPlate_id" , plate.plateId , OracleDbType.Varchar2);
                    }
                    else
                    {
                        query += "upper(plate_path) like :pPath";

                        // create parameter object for bind variables for path
                        _objDB.AddParameter("pPath" , ConvertPlatePath(plate.path) , OracleDbType.Varchar2);
                    }
                }

                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query finished");

                while (reader.Read())
                {
                    //map result set to Plate object and add to plate list
                    plates.Add(new Plate
                    {
                        barcode = reader.GetString(0) ,
                        plateId = reader.GetDecimal(1).ToString() ,
                        format = (int) reader.GetDecimal(2) ,
                        name = reader.GetString(3) ,
                        timestamp = reader.GetDateTime(4).ToString() ,
                        path = reader.GetString(5)
                    });
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfo, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// GetPlateInfoByParentPath
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfoByParentPath(Plate plate)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB started");

                if (!_objDB.OpenDB())
                    return plates;

                query = "select barcode, plate_id, plate_format, plate_name, plate_timestamp, plate_path "
                + "from v_yokogawa_plate_info "
                + "where ";

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    query += "upper(barcode) like :pBarcode";

                    // create parameter object for bind variables for barcode
                    _objDB.AddParameter("pBarcode" , "%" + plate.barcode.ToUpper() + "%" , OracleDbType.Varchar2);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        query += "plate_id = :pPlate_id";

                        // create parameter object for bind variables for plate id
                        _objDB.AddParameter("pPlate_id" , plate.plateId , OracleDbType.Varchar2);
                    }
                    else
                    {
                        query += "upper(plate_path) like :pPath";

                        // create parameter object for bind variables for path
                        _objDB.AddParameter("pPath" , FormatPlatePath(plate.path).ToUpper() , OracleDbType.Varchar2);
                    }
                }

                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query finished");

                while (reader.Read())
                {
                    //map result set to Plate object and add to plate list
                    plates.Add(new Plate
                    {
                        barcode = reader.GetString(0) ,
                        plateId = reader.GetDecimal(1).ToString() ,
                        format = (int) reader.GetDecimal(2) ,
                        name = reader.GetString(3) ,
                        timestamp = reader.GetDateTime(4).ToString() ,
                        path = reader.GetString(5)
                    });
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfoByParentPath, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// FindExactPlateInfo
        ///  - (Override) Get plate info from barcode
        /// </summary>
        /// <param name="plate"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public override PlateList FindExactPlateInfo(Plate plate , bool exactMatch = false)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB started");

                if (!_objDB.OpenDB())
                    return plates;

                query = "select barcode, plate_id, plate_format, plate_name, plate_timestamp, plate_path "
                + "from v_yokogawa_plate_info "
                + "where upper(plate_path) like :pPath";

                // create parameter object for bind variables for path
                _objDB.AddParameter("pPath" , ConvertPlatePathExact(plate.path , exactMatch) , OracleDbType.Varchar2);

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB sql query finished");

                while (reader.Read())
                {
                    //map result set to Plate object and add to plate list
                    plates.Add(new Plate
                    {
                        barcode = reader.GetString(0) ,
                        plateId = reader.GetDecimal(1).ToString() ,
                        format = (int) reader.GetDecimal(2) ,
                        name = reader.GetString(3) ,
                        timestamp = reader.GetDateTime(4).ToString() ,
                        path = reader.GetString(5)
                    });
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":FindExactPlateInfo, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }
            return plates;
        }

        /// <summary>
        /// QueryImageParameters
        ///  - (Override) Get image parameters associated with a plate well
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public override ImageParameters QueryImageParameters(Plate pl , int row , int col)
        {
            //query by barcode if present, otherwise by plate_id. query for all wells or a single well if specified.
            var barcodeQuery = !(string.IsNullOrEmpty(pl.barcode));
            var plateQuery = (row == 0 || col == 0);
            var query = string.Empty;
            var imageParamList = new ImageParameters();

            try
            {
                ConnectLogger.Info(GetType().Name + ":QueryImageParameters  DB started");

                if (!_objDB.OpenDB())
                    return imageParamList;

                query = plateQuery ? "select distinct " : "select ";
                query += "barcode, plate_id, plate_format, plate_name, plate_timestamp, field, channel, timepoint_count, z_count, corrected, plate_path ";
                query += "from v_yokogawa_image_parameters where ";
                query += barcodeQuery ? "barcode = :pPlate " : "plate_id = :pPlate ";
                query += plateQuery ? "order by field asc" : "and row_id = :pRow_id and col_id = :pCol_id order by field asc";

                // create parameter object for bind variables for barcode
                _objDB.AddParameter("pPlate" , barcodeQuery ? pl.barcode : pl.plateId , OracleDbType.Varchar2);

                if (!plateQuery)
                {
                    // create parameter object for bind variables for row
                    _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                    // create parameter object for bind variables for col
                    _objDB.AddParameter("pCol_id" , row , OracleDbType.Int16);
                }

                //Query returns one record per unique plate, well, channel, field which is found. Iterate through
                //the records, constructing a single ImageParameter per well, adding each field and channel to that
                //ImageParameter
                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB sql query started. Query = " + query);

                var imageParamDict = new Dictionary<decimal , ImageParameter>();

                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                while (reader.Read())
                {
                    var plateId = reader.GetDecimal(1);
                    ImageParameter imageParam;
                    if (imageParamDict.ContainsKey(plateId))
                    {
                        imageParam = imageParamDict[plateId];
                    }
                    else
                    {
                        imageParam = new ImageParameter();
                        imageParamDict.Add(plateId , imageParam);

                        imageParam.database = this.GetDatabaseName();
                        var p = new Plate
                        {
                            barcode = reader.GetString(0) ,
                            plateId = plateId.ToString() ,
                            format = (int) reader.GetDecimal(2) ,
                            name = reader.GetString(3) ,
                            timestamp = reader.GetDateTime(4).ToString() ,
                            path = reader.GetString(10)
                        };
                        imageParam.plateData = p;
                        imageParam.wellData = new Well { row = row , col = col };
                        imageParam.timepointCount = (int) reader.GetDecimal(7);
                        imageParam.zCount = (int) reader.GetDecimal(8);
                    }

                    imageParam.addField((int) reader.GetDecimal(5));
                    imageParam.addChannel(reader.GetString(6));
                    imageParam.addVersion(reader.GetString(9) == "N" ? "raw" : "corrected");
                }
                reader.Close();

                // loop though all items in hashtable to create list of ImageParameter
                IEnumerator<ImageParameter> en = imageParamDict.Values.GetEnumerator();
                while (en.MoveNext())
                {
                    imageParamList.Add(en.Current);
                }

                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParameters, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }
            return imageParamList;
        }

        /// <summary>
        /// Get image parameters associated with a whole plate
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public override PlateImgMetadata QueryImageParametersForWholePlate(Plate pl)
        {
            var query = string.Empty;
            PlateImgMetadata res = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":QueryImageParametersForWholePlate DB finished");

                if (!_objDB.OpenDB())
                    return res;

                // create SQL command to query parameters (fields, channels, etc.) for all matching plates
                query = "select barcode, row_id, col_id, field, plate_id, plate_format, plate_name, plate_timestamp, channel, plate_path, file_path, computer_name, z_index, timeline_index, action_list_index "
                    + "from v_yokogawa_image_parameters where plate_id = :pPlate ";

                query += "order by row_id, col_id, field, z_index";

                // create parameter object for bind variables for barcode
                _objDB.AddParameter("pPlate" , pl.plateId , OracleDbType.Varchar2);

                DateTime start = DateTime.Now;

                ConnectLogger.Info(GetType().Name + ":QueryImageParametersForWholePlate DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                reader.FetchSize = 2097152; // 2 Mb

                long curRow = 0,
                    curCol = 0,
                    curField = 0,
                    curZIndex = 0,
                    curTimelineIndex = 0,
                    curActionListIndex = 0;
                ImageMetadata curImgData = new ImageMetadata();
                while (reader.Read())
                {
                    string barcode = (string) reader["barcode"];
                    decimal plateFormat = (decimal) reader["plate_format"];
                    string computerName = YokogawaImagingComputer;

                    if (res == null)
                    {
                        res = new PlateImgMetadata
                        {
                            Barcode = barcode ,
                            PlateSize = plateFormat.ToString() ,
                            ImagingComputer = computerName ,
                            ImageList = new ImageMetadataList()
                        };
                    }

                    long row = (long) reader["row_id"];
                    long col = (long) reader["col_id"];
                    long field = (long) reader["field"];
                    long zIndex = (long) reader["z_index"];
                    // DB is not updated yet (05/28/2014); timeline_index and action_list_index are null
                    long timelineIndex = Convert.IsDBNull(reader["timeline_index"])
                        ? 0
                        : (long) reader["timeline_index"];
                    long actionListIndex = Convert.IsDBNull(reader["action_list_index"])
                        ? 0
                        : (long) reader["action_list_index"];

                    if (row != curRow || col != curCol || field != curField || zIndex != curZIndex ||
                        timelineIndex != curTimelineIndex || actionListIndex != curActionListIndex)
                    // add ImageMetadata to list
                    {
                        curRow = row;
                        curCol = col;
                        curField = field;
                        curZIndex = zIndex;
                        curTimelineIndex = timelineIndex;
                        curActionListIndex = actionListIndex;

                        string platePath = (string) reader["plate_path"];

                        curImgData = new ImageMetadata
                        {

                            Row = row.ToString() ,
                            Column = col.ToString() ,
                            Field = field.ToString() ,
                            ZIndex = zIndex.ToString() ,
                            TimelineIndex = timelineIndex.ToString() ,
                            ActionListIndex = actionListIndex.ToString() ,
                            PathName = platePath ,
                            Channels = new ChannelMetadataList()
                        };
                        res.ImageList.Add(curImgData);
                    }
                    // add channel, filename to current object
                    string channelName = (string) reader["channel"];
                    string fileName = (string) reader["file_path"];
                    ChannelMetadata chItem = new ChannelMetadata
                    {
                        Channel = channelName ,
                        FileName = fileName
                    };
                    curImgData.Channels.Add(chItem);
                }
                reader.Close();

                DateTime stop = DateTime.Now;

                TimeSpan elapsed = stop - start;

                ConnectLogger.Info(string.Format("{0}:QueryImageParametersForWholePlate: plateId = {1}, elapsed time = {2} ms" , GetType().Name , pl.plateId , elapsed.TotalMilliseconds));
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParametersForWholePlate, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return res;
        }

        /// <summary>
        /// QueryImagePath
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override MediaDataList QueryImagePath(string pid , int row , int col , ImageSpec spec)
        {
            var list = new MediaDataList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":QueryImagePath started");

                if (!_objDB.OpenDB())
                    return list;

                if (row > 0 && col > 0)
                {
                    int field = spec.field;
                    string channel = spec.channel;
                    int timepoint = spec.timepoint;
                    int z_index = spec.z_index;
                    string corrected = spec.version.Equals("raw") ? "N" : "Y";

                    if (!string.IsNullOrEmpty(channel))
                    {
                        // create SQL command
                        query = "select imagepath from v_yokogawa_image_path "
                                + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id and field = :pFld and channel = :pCh and "
                                + "timepoint_index = :pTimepoint and z_index = :pZ_index and corrected = :pCorrected";

                        _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);
                        _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);
                        _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);
                        _objDB.AddParameter("pFld" , field , OracleDbType.Int16);
                        _objDB.AddParameter("pCh" , channel , OracleDbType.Varchar2);
                        _objDB.AddParameter("pTimepoint" , timepoint , OracleDbType.Int16);
                        _objDB.AddParameter("pZ_index" , z_index , OracleDbType.Int16);
                        _objDB.AddParameter("pCorrected" , corrected , OracleDbType.Varchar2);

                        ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + query);

                        OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                        string imageFilePath;
                        if (reader.Read())
                        {
                            imageFilePath = reader.GetString(0);

                            list = new MediaDataList
                                {
                                    new MediaData {Path = imageFilePath, Channel = channel}
                                };
                        }
                        reader.Close();
                    }
                    else
                    {
                        // create SQL command
                        query = "select imagepath, channel "
                                + "from v_yokogawa_image_path "
                                + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pcol_id and field = :pFld and "
                                + "timepoint_index = :pTimepoint and z_index = :pZ_index and corrected = :pCorrected";

                        _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);
                        _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);
                        _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);
                        _objDB.AddParameter("pFld" , field , OracleDbType.Int16);
                        _objDB.AddParameter("pTimepoint" , timepoint , OracleDbType.Int16);
                        _objDB.AddParameter("pZ_index" , z_index , OracleDbType.Int16);
                        _objDB.AddParameter("pCorrected" , corrected , OracleDbType.Varchar2);

                        ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + query);

                        OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                        while (reader.Read())
                        {
                            string imageFilePath = reader.GetString(0);
                            string ch = reader.GetString(1);

                            list.Add(
                                new MediaData { Path = imageFilePath , Channel = ch });

                        }
                        reader.Close();
                    }
                }
                else
                {
                    // create SQL command
                    query = "select imagepath, channel,row_id, col_id , field, timepoint_index,z_index,corrected "
                            + "from v_yokogawa_image_path "
                            + "where plate_id = :pPid";

                    // create bind variable for plateId
                    _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                    ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + query);

                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                    while (reader.Read())
                    {
                        string imageFilePath = reader.GetString(0);
                        string ch = reader.GetString(1);
                        int srow = (int) reader.GetDouble(2);
                        int scol = (int) reader.GetDouble(3);
                        int sfield = (int) reader.GetDouble(4);
                        int stimepoint = (int) reader.GetDouble(5);
                        int szindex = (int) reader.GetDouble(6);
                        string sversion = reader.GetString(7);

                        list.Add(
                            new MediaData
                            {
                                Path = imageFilePath ,
                                Channel = ch ,
                                Column = scol ,
                                Row = srow ,
                                Field = sfield ,
                                Timepoint = stimepoint ,
                                ZIndex = szindex ,
                                Version = (sversion == "Y") ? "corrected" : "raw"
                            });
                    }
                    reader.Close();
                }

                ConnectLogger.Info(GetType().Name + ":QueryImagePath DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImagePath, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return list;
        }

        /// <summary>
        /// PrepareForImageStream
        ///  - (Override) Get image according to spec and return as stream
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public override FrameFile PrepareForImageStream(Plate pl , Well w , ImageSpec spec , string cacheKey)
        {
            // extract spec data
            var pid = pl.plateId;
            var row = w.row;
            var col = w.col;
            var field = spec.field;
            var channel = spec.channel;
            var timepoint = spec.timepoint;
            var z_index = spec.z_index;
            var corrected = spec.version.Equals("raw") ? "N" : "Y";
            var query = string.Empty;
            var imageFilePath = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream  DB started");

                if (!_objDB.OpenDB())
                    return null;

                // create SQL command
                query = "select imagepath "
                    + "from v_yokogawa_image_path "
                    + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id and field = :pFld and channel = :pCh and "
                    + "timepoint_index = :pTimepoint and z_index = :pZ_index and corrected = :pCorrected";

                _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);
                _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);
                _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);
                _objDB.AddParameter("pFld" , field , OracleDbType.Int16);
                _objDB.AddParameter("pCh" , channel , OracleDbType.Varchar2);
                _objDB.AddParameter("pTimepoint" , timepoint , OracleDbType.Int16);
                _objDB.AddParameter("pZ_index" , z_index , OracleDbType.Int16);
                _objDB.AddParameter("pCorrected" , corrected , OracleDbType.Varchar2);

                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream DB sql query started. Query = " + query);

                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                if (reader.Read())
                {
                    imageFilePath = reader.GetString(0);
                    reader.Close();
                }
                else
                {
                    reader.Close();
                    // immediately close database connection in case there is no data returned
                    _objDB.CloseDB();

                    // image not found
                    if (!spec.suppressErr) throw new ImageNotFoundException();
                }
                if (!File.Exists(imageFilePath))
                {
                    // image path not valid
                    if (!spec.suppressErr) throw new ImageNotFoundException();
                }

                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":PrepareForImageStream, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return new FrameFile { ImagePath = imageFilePath };
        }

        /// <summary>
        /// PrepareForOverlayImageStream
        ///  - (Override) Overlay multiple channels , return cached path if exists
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public override Dictionary<string , FrameFile> PrepareForOverlayImageStream(Plate pl , Well w , OverlayImageSpec spec , string cacheKey)
        {
            var result = new Dictionary<string , FrameFile>();
            var pid = pl.plateId;
            var row = w.row;
            var col = w.col;
            var field = spec.field;
            var timepoint = spec.timepoint;
            var z_index = spec.z_index;
            var corrected = spec.version.Equals("raw") ? "N" : "Y";
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream  DB started");

                if (!_objDB.OpenDB())
                    return result;

                // create SQL command
                query = "select channel, imagepath "
                + "from v_yokogawa_image_path "
                + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id and field = :pFld and "
                + "timepoint_index = :pTimepoint and z_index = :pZ_index and corrected = :pCorrected";

                _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);
                _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);
                _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);
                _objDB.AddParameter("pFld" , field , OracleDbType.Int16);
                _objDB.AddParameter("pTimepoint" , timepoint , OracleDbType.Int16);
                _objDB.AddParameter("pZ_index" , z_index , OracleDbType.Int16);
                _objDB.AddParameter("pCorrected" , corrected , OracleDbType.Varchar2);

                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                // set image file path for each channel
                while (reader.Read())
                {
                    string channel = reader.GetString(0);
                    string imageFilePath = reader.GetString(1);
                    result[channel] = new FrameFile { ImagePath = imageFilePath };
                }
                reader.Close();
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":PrepareForOverlayImageStream, " + eX.Message + " SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return result;
        }

        /// <summary>
        /// GetImageURL
        ///  - (Override) Get image according to spec and return the URL
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override string GetImageURL(Plate pl , Well w , ImageSpec spec)
        {
            return null;
        }
    }
}
