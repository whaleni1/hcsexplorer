﻿using System.Collections.Generic;

namespace HCS.DataAccess.Databases
{
    /// <summary>
    /// CELLOMICS
    /// </summary>
    public class Cellomics : HCSDatabaseAccess
    {
        private CellomicsStoreAccess _cellomicsAccess;

        /// <summary>
        /// Instantiates a CellomicsStoreAccess object to search the Cellomics Store MSSQL Server
        /// </summary>
        public override void Initialize(string dbName)
        {
            base.Initialize(dbName);
            _cellomicsAccess = new CellomicsStoreAccess(dbName);
        }

        /// <summary>
        /// Get plate info from barcode, plate id, or image folder path
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfo(Plate plate)
        {
            return _cellomicsAccess.GetPlateInfo(plate);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfoByParentPath(Plate plate)
        {
            return _cellomicsAccess.GetPlateInfoByParentPath(plate);
        }

        /// <summary>
        /// Get plate info from parent path
        /// </summary>
        /// <param name="plate"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public override PlateList FindExactPlateInfo(Plate plate , bool exactMatch = false)
        {
            return _cellomicsAccess.FindExactPlateInfo(plate , exactMatch);
        }

        /// <summary>
        /// Get image parameters associated with a plate well
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public override ImageParameters QueryImageParameters(Plate pl , int row , int col)
        {
            return _cellomicsAccess.QueryImageParameters(this.GetDatabaseName() , pl , row , col);
        }

        /// <summary>
        /// Get image parameters associated with a whole plate
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public override PlateImgMetadata QueryImageParametersForWholePlate(Plate pl)
        {
            return _cellomicsAccess.QueryImageParametersForWholePlate(pl);
        }

        /// <summary>
        /// Get image path according to spec
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override FrameFile PrepareForImageStream(Plate pl , Well w , ImageSpec spec , string cacheKey)
        {
            return new FrameFile { ImagePath = _cellomicsAccess.PrepareForImageStream(pl , w , spec , cacheKey) };
        }

        /// <summary>
        /// Get image according to spec and return the URL
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override string GetImageURL(Plate pl , Well w , ImageSpec spec)
        {
            return null;
        }

        /// <summary>
        /// Get image paths for media service
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override MediaDataList QueryImagePath(string plateId , int row , int col , ImageSpec spec)
        {
            return _cellomicsAccess.QueryImagePath(plateId , row , col , spec);
        }

        /// <summary>
        /// Return dictionary of img paths per channel
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override Dictionary<string , FrameFile> PrepareForOverlayImageStream(Plate pl , Well w , OverlayImageSpec spec , string cacheKey)
        {
            return _cellomicsAccess.PrepareForOverlayImageStream(pl , w , spec , cacheKey);
        }
    }
}

