﻿using System;
using System.Collections.Generic;
using System.Configuration;
using HCS.DataAccess.Database;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using Oracle.ManagedDataAccess.Client;

namespace HCS.DataAccess.Databases
{
    public class GenericDb : HCSDatabaseAccess
    {

        #region Member Variables & Properties

        private DatabaseOracle _objDB;

        /// <summary>
        /// RunIdSequenceDbName
        /// </summary>
        private static string RunIdSequenceDbName
        {
            get
            {
                return ConfigurationManager.AppSettings["RUN_ID_SEQ_DB"];
            }
        }

        /// <summary>
        /// ConnectionString
        /// </summary>
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[RunIdSequenceDbName].ConnectionString;
            }
        }

        #endregion

        /// <summary>
        /// YokogawaImagingComputer
        /// </summary>
        private static string YokogawaImagingComputer
        {
            get
            {
                string strValue = System.Configuration.ConfigurationManager.AppSettings["YokogawaImagingComputer"];
                if (strValue == null)
                    strValue = string.Empty;

                return strValue;
            }
        }

        /// <summary>
        /// Initialize
        ///  - (Override) Establish connection to a database
        /// </summary>
        /// <param name="dbName"></param>
        public override void Initialize(string dbName)
        {
            try
            {
                base.Initialize(dbName);

                // Create the database object with the connection string
                _objDB = new DatabaseOracle(CONNECTION_STR);
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + "Initialize, " + eX.Message , eX);
            }
        }

        /// <summary>
        /// GetNextId
        ///  - get new RunId from DB sequence
        /// </summary>
        /// <returns></returns>
        public int GetNextId()
        {
            int result = -1;

            try
            {
                _objDB = new DatabaseOracle(ConnectionString);
                if (!_objDB.OpenDB())
                    return result;

                object ores = _objDB.ExecuteScalar("select SEQ_RUN_ID_CTR.nextval from dual");
                if (!Convert.IsDBNull(ores))
                    result = int.Parse(ores.ToString());
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetNextId, " + ex.Message);
                throw new CException(this.GetType().Name + ":GetNextId, " + ex.Message , ex);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return result;
        }

        /// <summary>
        /// FindExactPlateInfo
        ///  - (Override) Get plate info from parent path.
        /// </summary>
        /// <param name="plate"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public override PlateList FindExactPlateInfo(Plate plate , bool exactMatch = false)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo GenericDB started");

                // Create SQL query
                query = "select barcode, plate_id, format, plate_description, acquisition_time, uncpath "
                    + " from v_generic_plate_info"
                    + " where upper(uncpath) like :pPath";

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo Plate path: " + plate.path);

                // JIRA HCSE-762: 
                // Note: there is no '\' at the end of the paths for YOKOGAWA so dont add '\' at the end. 
                // Otherwise, findPlateByParentPaths returns 0 results and disrupts the CPCP wrapper from finding plates for HCSIA. 
                if (!plate.path.EndsWith("\\"))
                {
                    string path = plate.path;
                    path = "%" + path + "%";
                    _objDB.AddParameter("pPath" , path , OracleDbType.Varchar2);
                }
                else
                {
                    _objDB.AddParameter("pPath" , ConvertPlatePathExact(plate.path , exactMatch).ToUpper() , OracleDbType.Varchar2);
                }

                string str = query;
                _objDB.PrintParameters(ref str , false);
                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB sql query started. Query = " + str);

                // Open the database connection
                if (!_objDB.OpenDB())
                    return plates;

                // execute SQL command
                var reader = (OracleDataReader) _objDB.ExecuteReader(query);

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo DB sql query finished");

                while (reader.Read())
                {
                    //map result set to Plate object and add to plate list
                    plates.Add(new Plate
                    {
                        barcode = reader.GetString(0) ,
                        plateId = reader.GetDecimal(1).ToString() ,
                        format = (int) reader.GetDecimal(2) ,
                        name = reader.GetString(3) ,
                        timestamp = reader.GetDateTime(4).ToString() ,
                        path = reader.GetString(5)
                    });
                }

                ConnectLogger.Info(GetType().Name + ":FindExactPlateInfo GenericDB finished, plate count=" + plates.Count);
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":FindExactPlateInfo, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// GetPlateInfo
        ///  - (Override) Get plate info from barcode
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfo(Plate plate)
        {
            var queryString = string.Empty;
            PlateList plates = new PlateList();

            try
            {
                queryString = "select barcode, " +
                              "plate_id as plateId, " +
                              "format, " +
                              "plate_description as name, " +
                              "acquisition_time as timestamp, " +
                              "uncpath as path " +
                              "from v_generic_plate_info " +
                              "where upper(datasource_id) = :pDB and ";


                // create parameter object for bind variables for databasename
                _objDB.AddParameter("pDB" , GetDatabaseName().ToUpper() , OracleDbType.Varchar2);

                ConnectLogger.Info("Begin " + GetType().Name + ":GetPlateInfo:  " + plate.path);

                //TODO escape special chars in LIKE query
                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    queryString += "upper(barcode) like :pBarcode";
                    _objDB.AddParameter("pBarcode" , "%" + plate.barcode.ToUpper() + "%" , OracleDbType.Varchar2);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        queryString += "plate_id = :pPlate_id";
                        _objDB.AddParameter("pPlate_id" , plate.plateId , OracleDbType.Varchar2);
                    }
                    else
                    {
                        queryString += "upper(uncpath) like :pPath";
                        _objDB.AddParameter("pPlate_id" , ConvertPlatePath(plate.path.ToUpper()) , OracleDbType.Varchar2);
                    }
                }

                // Open the database and perform the query
                if (!_objDB.OpenDB())
                    return new PlateList(plates);

                string str = queryString;
                _objDB.PrintParameters(ref str , false);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query started. Query = " + str);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(queryString);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query finished");
                int iCount = 0;

                while (reader.Read())
                {
                    // values from query results
                    string exactBarcode = Convert.IsDBNull(reader[0]) ? "" : reader.GetString(0);
                    decimal plateid = Convert.IsDBNull(reader[1]) ? 0 : reader.GetDecimal(1);
                    decimal format = Convert.IsDBNull(reader[2]) ? 0 : reader.GetDecimal(2);
                    string name = Convert.IsDBNull(reader[3]) ? "" : reader.GetString(3);
                    DateTime timestamp = Convert.IsDBNull(reader[4]) ? DateTime.MinValue : reader.GetDateTime(4);
                    string path = Convert.IsDBNull(reader[5]) ? "" : reader.GetString(5);

                    // create Plate object
                    Plate p = new Plate();
                    p.barcode = exactBarcode;
                    p.plateId = plateid.ToString();
                    p.format = (int) format;
                    p.name = name;
                    p.timestamp = timestamp.ToString();
                    p.path = path;

                    // add to plate list
                    plates.Add(p);
                    iCount++;
                }
                reader.Close();

                ConnectLogger.Info("End " + GetType().Name + ":GetPlateInfo, plate count=" + plates.Count);
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref queryString);
                throw new ConnectException(GetType().Name + ":GetPlateInfo, " + eX.Message + ", SQL = [" + queryString + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return new PlateList(plates);
        }

        /// <summary>
        /// GetPlateInfoByParentPath
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfoByParentPath(Plate plate)
        {
            var query = string.Empty;
            PlateList plates = new PlateList();

            try
            {
                query = "select barcode, " +
                          "plate_id as plateId, " +
                          "format, " +
                          "plate_description as name, " +
                          "acquisition_time as timestamp, " +
                          "uncpath as path " +
                          "from v_generic_plate_info " +
                          "where upper(datasource_id) = :pDB and ";

                ConnectLogger.Info("Begin " + GetType().Name + ":GetPlateInfoByParentPath:  " + plate.path);

                // create parameter object for bind variables for databasename
                _objDB.AddParameter("pDB" , GetDatabaseName().ToUpper() , OracleDbType.Varchar2);

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    query += "upper(barcode) like :pBarcode";

                    // create parameter object for bind variables for barcode
                    _objDB.AddParameter("pBarcode" , "%" + plate.barcode.ToUpper() + "%" , OracleDbType.Varchar2);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        query += "plate_id = :pPlate_id";

                        // create parameter object for bind variables for plate id
                        _objDB.AddParameter("pPlate_id" , plate.plateId , OracleDbType.Varchar2);
                    }
                    else
                    {
                        query += "upper(uncpath) like :pPath";

                        // create parameter object for bind variables for path
                        _objDB.AddParameter("pPath" , FormatPlatePath(plate.path).ToUpper() , OracleDbType.Varchar2); 
                    }
                }

                // Open the database connection
                if (_objDB.OpenDB())
                {
                    string str = query;
                    _objDB.PrintParameters(ref str , false);
                    ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query started. Query = " + str);

                    // execute SQL command
                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                    ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query finished");
                    int iCount = 0;

                    while (reader.Read())
                    {
                        // values from query results
                        string exactBarcode = Convert.IsDBNull(reader[0]) ? "" : reader.GetString(0);
                        decimal plateid = Convert.IsDBNull(reader[1]) ? 0 : reader.GetDecimal(1);
                        decimal format = Convert.IsDBNull(reader[2]) ? 0 : reader.GetDecimal(2);
                        string name = Convert.IsDBNull(reader[3]) ? "" : reader.GetString(3);
                        DateTime timestamp = Convert.IsDBNull(reader[4]) ? DateTime.MinValue : reader.GetDateTime(4);
                        string path = Convert.IsDBNull(reader[5]) ? "" : reader.GetString(5);

                        // create Plate object
                        Plate p = new Plate();
                        p.barcode = exactBarcode;
                        p.plateId = plateid.ToString();
                        p.format = (int) format;
                        p.name = name;
                        p.timestamp = timestamp.ToString();
                        p.path = path;

                        // add to plate list
                        plates.Add(p);
                        iCount++;
                    }
                    reader.Close();
                    
                    if (iCount == 0) //FormatPlatePath() appends a '/' to the end of the plate path. if the query returns 0, try again without the '\'
                    {
                        query = string.Empty;
                        _objDB.ClearParameters();

                        query = "select barcode, " +
                        "plate_id as plateId, " +
                        "format, " +
                        "plate_description as name, " +
                        "acquisition_time as timestamp, " +
                        "uncpath as path " +
                        "from v_generic_plate_info " +
                        "where upper(datasource_id) = :pDB and ";

                        ConnectLogger.Info("Begin " + GetType().Name + ":GetPlateInfoByParentPath:  " + plate.path);

                        // create parameter object for bind variables for databasename
                        _objDB.AddParameter("pDB", GetDatabaseName().ToUpper(), OracleDbType.Varchar2);

                        // create SQL query based on search criteria
                        if (!String.IsNullOrEmpty(plate.barcode))
                        {
                            query += "upper(barcode) like :pBarcode";

                            // create parameter object for bind variables for barcode
                            _objDB.AddParameter("pBarcode", "%" + plate.barcode.ToUpper() + "%", OracleDbType.Varchar2);
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(plate.plateId))
                            {
                                query += "plate_id = :pPlate_id";

                                // create parameter object for bind variables for plate id
                                _objDB.AddParameter("pPlate_id", plate.plateId, OracleDbType.Varchar2);
                            }
                            else
                            {
                                query += "upper(uncpath) like :pPath";

                                // create parameter object for bind variables for path
                                _objDB.AddParameter("pPath", FormatPlatePathAfterNoResults(plate.path).ToUpper(), OracleDbType.Varchar2);
                            }
                        }

                        str = query;
                        _objDB.PrintParameters(ref str, false);
                        ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query started. Query = " + str);

                        // execute SQL command
                        OracleDataReader reader2 = (OracleDataReader)_objDB.ExecuteReader(query);
                        ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query finished");
                        //start the query again
                        while (reader2.Read())
                        {
                            // values from query results
                            string exactBarcode = Convert.IsDBNull(reader2[0]) ? "" : reader2.GetString(0);
                            decimal plateid = Convert.IsDBNull(reader2[1]) ? 0 : reader2.GetDecimal(1);
                            decimal format = Convert.IsDBNull(reader2[2]) ? 0 : reader2.GetDecimal(2);
                            string name = Convert.IsDBNull(reader2[3]) ? "" : reader2.GetString(3);
                            DateTime timestamp = Convert.IsDBNull(reader2[4]) ? DateTime.MinValue : reader2.GetDateTime(4);
                            string path = Convert.IsDBNull(reader2[5]) ? "" : reader2.GetString(5);

                            // create Plate object
                            Plate p = new Plate();
                            p.barcode = exactBarcode;
                            p.plateId = plateid.ToString();
                            p.format = (int)format;
                            p.name = name;
                            p.timestamp = timestamp.ToString();
                            p.path = path;

                            // add to plate list
                            plates.Add(p);
                            iCount++;
                        }
                    }
                    ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath  DB finished, record count = " + iCount + ", Plate count = " + plates.Count);
                }
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfoByParentPath, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return new PlateList(plates);
        }

        /// <summary>
        /// QueryImageParameters
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public override ImageParameters QueryImageParameters(Plate pl , int row , int col)
        {
            // assemble SQL query based on plate identifier
            var barcodeQuery = !(string.IsNullOrEmpty(pl.barcode));
            var plateQuery = (row == 0 || col == 0);
            var query = string.Empty;
            ImageParameters imageParamList = new ImageParameters();

            try
            {
                ConnectLogger.Info("Begin " + GetType().Name + ":QueryImageParameters: " + pl.path);

                query = "select distinct " +
                       "field_idx as field, " +
                       "channel_name as channel, " +
                       "timepoint_count as timepointCount, " +
                       "z_count as zCount, " +
                       "version_name as Version, " +
                       "plate_id as plateId, " +
                       "barcode, " +
                       "plate_format as format, " +
                       "plate_description as name, " +
                       "acquisition_time as timestamp, " +
                       "uncpath as path, " +
                       row + " as \"row\", " +
                       col + " as \"col\" " +
                       "from v_generic_image_details where " +
                       (barcodeQuery ? "barcode = :pPlate " : "plate_id = :pPlate ") +
                       (plateQuery ? "" : "and row_idx = :pRow_id and col_idx = :pCol_id ") +
                       "order by field asc";

                // create parameter object for bind variables for barcode
                _objDB.AddParameter("pPlate" , barcodeQuery ? pl.barcode : pl.plateId , OracleDbType.Varchar2);

                if (!plateQuery)
                {
                    // create parameter object for bind variables for row
                    _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                    // create parameter object for bind variables for col
                    _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);
                }

                string str = query;
                _objDB.PrintParameters(ref str , false);
                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB sql query started. Query = " + str);

                // Open the database connection
                if (_objDB.OpenDB())
                {
                    // execute SQL command
                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                    // dictionary to hold collections of ImageParameters objects
                    var imageParamDict = new Dictionary<decimal , ImageParameter>();

                    while (reader.Read())
                    {
                        var plateId = reader.GetDecimal(5);
                        ImageParameter imageParam;
                        if (imageParamDict.ContainsKey(plateId))
                        {
                            imageParam = imageParamDict[plateId];
                        }
                        else
                        {
                            imageParam = new ImageParameter();
                            imageParamDict.Add(plateId , imageParam);

                            imageParam.database = GetDatabaseName();
                            var p = new Plate
                            {
                                barcode = Convert.IsDBNull(reader[6]) ? "" : reader.GetString(6) ,
                                plateId = plateId.ToString() ,
                                format = Convert.IsDBNull(reader[7]) ? 0 : (int) reader.GetDecimal(7) ,
                                name = Convert.IsDBNull(reader[8]) ? "" : reader.GetString(8) ,
                                timestamp = Convert.IsDBNull(reader[9]) ? "" : reader.GetDateTime(9).ToString() ,
                                path = Convert.IsDBNull(reader[10]) ? "" : reader.GetString(10)
                            };
                            imageParam.plateData = p;
                            imageParam.wellData = new Well { row = row , col = col };
                            imageParam.timepointCount = Convert.IsDBNull(reader[2]) ? 0 : (int) reader.GetDecimal(2);
                            imageParam.zCount = Convert.IsDBNull(reader[3]) ? 0 : (int) reader.GetDecimal(3);
                        }

                        imageParam.addVersion(reader.GetString(4));
                        imageParam.addField((int) reader.GetDecimal(0));
                        imageParam.addChannel(reader.GetString(1));
                    }
                    reader.Close();

                    // loop though all items in hashtable to create list of ImageParameter
                    IEnumerator<ImageParameter> en = imageParamDict.Values.GetEnumerator();
                    while (en.MoveNext())
                    {
                        imageParamList.Add(en.Current);
                    }
                }

                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB finished, imageParamList count=" + imageParamList.Count);
                ConnectLogger.Info("End " + GetType().Name + ":QueryImageParameters: " + pl.path);
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParameters, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return imageParamList;
        }

        /// <summary>
        /// QueryImageParametersForWholePlate
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public override PlateImgMetadata QueryImageParametersForWholePlate(Plate pl)
        {
            PlateImgMetadata res = null;
            string query = string.Empty;

            try
            {
                // create SQL command to query parameters (fields, channels, etc.) for all matching plates
                query = "select barcode, row_idx, col_idx, field_idx, plate_id, plate_format, plate_description, "
                        + "acquisition_time, channel_name, uncpath, imagepath, z_idx, timeline_idx, action_list_idx "
                        + "from v_generic_image_details where plate_id = :pPlate "
                        //+ "order by row_idx, col_idx, field_idx, channel_name, z_idx";
                        + "order by row_idx, col_idx, field_idx, z_idx";

                // create parameter object for bind variables for barcode
                _objDB.AddParameter("pPlate" , pl.plateId , OracleDbType.Int32);

                string str = query;
                _objDB.PrintParameters(ref str , false);
                ConnectLogger.Info("Begin " + GetType().Name + ":queryImageParametersForWholePlate: PlateId= " + pl.plateId + ", query=" + str);

                DateTime start = DateTime.Now;

                // Open the database connection
                if (_objDB.OpenDB())
                {
                    // execute SQL command
                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                    reader.FetchSize = 2097152; // 2 Mb

                    int curRow = 0,
                        curCol = 0,
                        curField = 0,
                        curZIndex = 0;

                    // curActionListIndex = 0;
                    ImageMetadata curImgData = new ImageMetadata();
                    while (reader.Read())
                    {
                        string barcode = (string) reader["barcode"];
                        int plateFormat = reader["plate_format"] != DBNull.Value ? Convert.ToInt32(reader["plate_format"]) : 0;
                        string computerName = YokogawaImagingComputer;

                        if (res == null)
                        {
                            res = new PlateImgMetadata
                            {
                                Barcode = barcode ,
                                PlateSize = plateFormat.ToString() ,
                                ImagingComputer = computerName ,
                                ImageList = new ImageMetadataList()
                            };
                        }

                        int row = reader["row_idx"] != DBNull.Value ? Convert.ToInt32(reader["row_idx"]) : 0;
                        int col = reader["col_idx"] != DBNull.Value ? Convert.ToInt32(reader["col_idx"]) : 0;
                        int field = reader["field_idx"] != DBNull.Value ? Convert.ToInt32(reader["field_idx"]) : 0;
                        int zIndex = reader["z_idx"] != DBNull.Value ? Convert.ToInt32(reader["z_idx"]) : 0;
                        int timelineIndex = reader["timeline_idx"] != DBNull.Value ? Convert.ToInt32(reader["timeline_idx"]) : 0;
                        int actionListIndex = reader["action_list_idx"] != DBNull.Value ? Convert.ToInt32(reader["action_list_idx"]) : 0;

                        if (row != curRow || col != curCol || field != curField || zIndex != curZIndex)// || timelineIndex != curTimelineIndex || zIndex != curZIndex || actionListIndex != curActionListIndex)
                        {
                            curRow = row;
                            curCol = col;
                            curField = field;
                            curZIndex = zIndex;
                            //curTimelineIndex = timelineIndex;
                            //curActionListIndex = actionListIndex;

                            string platePath = (string) reader["uncpath"];

                            curImgData = new ImageMetadata
                            {

                                Row = row.ToString() ,
                                Column = col.ToString() ,
                                Field = field.ToString() ,
                                ZIndex = zIndex.ToString() ,
                                TimelineIndex = timelineIndex.ToString() ,
                                ActionListIndex = actionListIndex.ToString() ,
                                PathName = platePath ,
                                Channels = new ChannelMetadataList()
                            };
                            res.ImageList.Add(curImgData);
                        }
                        // add channel, filename to current object
                        string channelName = (string) reader["channel_name"];
                        string fileName = (string) reader["imagepath"];
                        ChannelMetadata chItem = new ChannelMetadata
                        {
                            Channel = channelName ,
                            FileName = fileName
                        };
                        curImgData.Channels.Add(chItem);
                    }
                }
                DateTime stop = DateTime.Now;
                TimeSpan elapsed = stop - start;

                ConnectLogger.Info("Finished " + GetType().Name + ":queryImageParametersForWholePlate, res.ImageList.Count=" + res.ImageList.Count);
                ConnectLogger.Info(String.Format("{0}:queryImageParametersForWholePlate: plateId = {1}, elapsed time = {2} ms" ,
                    GetType().Name , pl.plateId , elapsed.TotalMilliseconds));
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParametersForWholePlate, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return res;
        }

        /// <summary>
        /// PrepareForImageStream
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public override FrameFile PrepareForImageStream(Plate pl , Well w , ImageSpec spec , string cacheKey)
        {
            // extract spec data
            string pid = pl.plateId;
            int row = w.row;
            int col = w.col;
            int field = spec.field;
            string channel = spec.channel;
            string format = spec.format;
            var query = string.Empty;
            string imageFilePath = string.Empty;

            try
            {
                ConnectLogger.Info("Begin " + GetType().Name + ":PrepareForImageStream");

                query = "select imagepath, frame_idx as frame "
                        + "from v_generic_image_details "
                        + "where plate_id = :pPlate_id and row_idx = :pRow_id and col_idx = :pCol_id and field_idx = :pField_id and channel_name = :pCh";

                // create bind variable for plateId
                _objDB.AddParameter("pPlate_id" , pid , OracleDbType.Varchar2);

                // create bind variable for row
                _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                // create bind variable for col
                _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);

                // create bind variable for field
                _objDB.AddParameter("pField_id" , field , OracleDbType.Int16);

                // create bind variable for channel
                _objDB.AddParameter("pCh" , channel , OracleDbType.Varchar2);

                string str = query;
                _objDB.PrintParameters(ref str , false);
                ConnectLogger.Info(GetType().Name + ":prepareForImageStream DB sql query started. Query = " + str);

                // Open the database connection
                if (_objDB.OpenDB())
                {
                    // execute SQL command
                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                    if (reader.Read())
                    {
                        imageFilePath = reader.GetString(0);
                        reader.Close();
                    }
                    else
                    {
                        // immediately close database connection in case there is no data returned
                        _objDB.CloseDB();

                        // image not found
                        if (!spec.suppressErr)
                        {
                            throw new ImageNotFoundException();
                        }
                        return null;
                    }
                }

                ConnectLogger.Info("Finished " + GetType().Name + ":prepareForImageStream, imageFilePath=" + imageFilePath);
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":PrepareForImageStream, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }
            return new FrameFile { ImagePath = imageFilePath };
        }

        /// <summary>
        /// GetImageURL
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override string GetImageURL(Plate pl , Well w , ImageSpec spec)
        {
            ConnectLogger.Info(GetType().Name + ":GetImageURL not implemented");

            return null;
        }

        /// <summary>
        /// PrepareForOverlayImageStream
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public override Dictionary<string , FrameFile> PrepareForOverlayImageStream(Plate pl , Well w , OverlayImageSpec spec , string cacheKey)
        {
            Dictionary<string , FrameFile> result = new Dictionary<string , FrameFile>();

            // extract spec data
            string plateId = pl.plateId;
            int row = w.row;
            int col = w.col;
            int field = spec.field;
            int timepoint = spec.timepoint;
            int z_index = spec.z_index;
            string version = spec.version;
            var query = string.Empty;

            try
            {
                ConnectLogger.Info("Begin " + GetType().Name + ":PrepareForOverlayImageStream");

                query = "select imagepath, channel_name as channel, frame_idx as "
                        + "frame from v_generic_image_details "
                        + "where plate_id = :pPlate_id and row_idx = :pRow_id and col_idx = :pCol_id and field_idx = :pField_id and "
                        + "timepoint_idx = :pTimepoint and z_idx = :pZ_index and lower(version_name) = :pVerson";

                _objDB.AddParameter("pPlate_id" , plateId , OracleDbType.Varchar2);
                _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);
                _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);
                _objDB.AddParameter("pField_id" , field , OracleDbType.Int16);
                _objDB.AddParameter("pTimepoint" , timepoint , OracleDbType.Int16);
                _objDB.AddParameter("pZ_index" , z_index , OracleDbType.Int16);
                _objDB.AddParameter("pVerson" , version.ToLower() , OracleDbType.Varchar2);

                string str = query;
                _objDB.PrintParameters(ref str , false);
                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream DB sql query started. Query = " + str);

                // Open the database connection
                if (_objDB.OpenDB())
                {
                    // execute SQL command
                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                    // set image file path for each channel
                    while (reader.Read())
                    {
                        string imageFilePath = reader.GetString(0);
                        string channel = reader.GetString(1);
                        result[channel] = new FrameFile { ImagePath = imageFilePath };
                    }
                    reader.Close();
                }

                ConnectLogger.Info("Finish " + GetType().Name + ":PrepareForOverlayImageStream, result count=" + result.Count);
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new CException(GetType().Name + ":PrepareForOverlayImageStream, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                // close connection
                _objDB.CloseDB();
            }

            return result;
        }

        /// <summary>
        /// QueryImagePath
        /// </summary>
        /// <param name="plateId"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override MediaDataList QueryImagePath(string plateId , int row , int col , ImageSpec spec)
        {
            MediaDataList list = null;
            var query = string.Empty;

            try
            {
                // create SQL command
                if (row > 0 && col > 0)
                {
                    string channel = spec.channel;
                    int field = spec.field;
                    int timepoint = spec.timepoint;
                    int z_index = spec.z_index;
                    string version = spec.version;

                    if (!string.IsNullOrEmpty(channel))
                    {
                        query = "select imagepath as path, frame_idx as frame "
                                + "from v_generic_image_details "
                                + "where plate_id = :pPlate_id and row_idx = :pRow_id and col_idx = :pCol_id and field_idx = :pField_id and channel_name = :pCh and "
                                + "timepoint_idx = :pTimepoint and z_idx = :pZ_index and lower(version_name) = :pVersion";

                        _objDB.AddParameter("pPlate_id" , plateId , OracleDbType.Varchar2);
                        _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);
                        _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);
                        _objDB.AddParameter("pField_id" , field , OracleDbType.Int16);
                        _objDB.AddParameter("pCh" , channel , OracleDbType.Varchar2);
                        _objDB.AddParameter("pTimepoint" , timepoint , OracleDbType.Int16);
                        _objDB.AddParameter("pZ_index" , z_index , OracleDbType.Int16);
                        _objDB.AddParameter("pVerson" , version.ToLower() , OracleDbType.Varchar2);

                        string str = query;
                        _objDB.PrintParameters(ref str , false);
                        ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + str);

                        // Open the database connection
                        if (_objDB.OpenDB())
                        {
                            // execute SQL command
                            OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                            string imageFilePath;

                            if (reader.Read())
                            {
                                imageFilePath = reader.GetString(0);

                                list = new MediaDataList
                                {
                                    new MediaData
                                    {
                                        Path = imageFilePath,
                                        Channel = channel,
                                        Field = field,
                                        Column = col,
                                        Row = row,
                                        Timepoint = timepoint,
                                        ZIndex = z_index,
                                        Version = version.ToLower()
                                    }
                                };
                            }
                            reader.Close();
                        }
                    }
                    else
                    {
                        query = "select imagepath as path, channel_name as channel, frame_idx as frame "
                            + "from v_generic_image_details "
                            + "where plate_id = :pPlate_id and row_idx = :pRow_id and col_idx = :pCol_id and field_idx = :pField_id and "
                            + "timepoint_idx = :pTimepoint and z_idx = :pZ_index and lower(version_name) = :pVersion";

                        _objDB.AddParameter("pPlate_id" , plateId , OracleDbType.Varchar2);
                        _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);
                        _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);
                        _objDB.AddParameter("pField_id" , field , OracleDbType.Int16);
                        _objDB.AddParameter("pTimepoint" , timepoint , OracleDbType.Int16);
                        _objDB.AddParameter("pZ_index" , z_index , OracleDbType.Int16);
                        _objDB.AddParameter("pVerson" , version.ToLower() , OracleDbType.Varchar2);

                        string str = query;
                        _objDB.PrintParameters(ref str , false);
                        ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + str);

                        if (_objDB.OpenDB())
                        {
                            // execute SQL command
                            OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                            string imageFilePath;
                            string ch;
                            list = new MediaDataList();

                            while (reader.Read())
                            {
                                imageFilePath = reader.GetString(0);
                                ch = reader.GetString(1);
                                MediaData md = new MediaData
                                {
                                    Path = imageFilePath ,
                                    Channel = channel ,
                                    Field = field ,
                                    Column = col ,
                                    Row = row ,
                                    Timepoint = timepoint ,
                                    ZIndex = z_index ,
                                    Version = version.ToLower()
                                };
                                list.Add(md);
                            }
                            reader.Close();
                        }
                    }
                }
                else // get media data for whole plate
                {
                    query = "select imagepath as path, channel_name as channel, row_idx as \"row\", col_idx as \"column\", field_idx as field, timepoint_idx as timepoint, z_idx as zindex, frame_idx as frame, version_name as version "
                       + "from v_generic_image_details "
                       + "where plate_id = :pPlate_id";

                    _objDB.AddParameter("pPlate_id" , plateId , OracleDbType.Varchar2);

                    string str = query;
                    _objDB.PrintParameters(ref str , false);
                    ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + str);

                    if (_objDB.OpenDB())
                    {
                        // execute SQL command
                        OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                        string imageFilePath;
                        string ch;
                        list = new MediaDataList();

                        while (reader.Read())
                        {
                            imageFilePath = reader["path"].ToString();
                            ch = reader["channel"].ToString();
                            int srow = Convert.ToInt16(reader["row"]);
                            int scol = Convert.ToInt16(reader["column"]);
                            int sfield = Convert.ToInt16(reader["field"]);
                            int stimepoint = Convert.ToInt16(reader["timepoint"]);
                            int szindex = Convert.ToInt16(reader["zindex"]);
                            string sversion = reader["version"].ToString();

                            MediaData md = new MediaData
                            {
                                Path = imageFilePath ,
                                Channel = ch ,
                                Field = sfield ,
                                Column = scol ,
                                Row = srow ,
                                Timepoint = stimepoint ,
                                ZIndex = szindex ,
                                Version = sversion.ToLower()
                            };
                            list.Add(md);
                        }
                        reader.Close();
                    }
                }

                ConnectLogger.Info("End " + GetType().Name + ":QueryImagePath");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new CException(GetType().Name + ":QueryImagePath, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                // close connection
                _objDB.CloseDB();
            }

            return list;
        }
    }
}
