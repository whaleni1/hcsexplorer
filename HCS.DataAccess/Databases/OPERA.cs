﻿using System;
using System.Collections.Generic;
using System.IO;
using HCS.DataAccess.Database;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using Oracle.ManagedDataAccess.Client;

namespace HCS.DataAccess.Databases
{
    public class Opera : HCSDatabaseAccess
    {
        public static string CHANNEL_NAME_PREFIX = "Exp";
        private DatabaseOracle _objDB;

        /// <summary>
        /// Initialize
        ///  - (Override) Establish connection to a database
        /// </summary>
        /// <param name="dbName"></param>
        public override void Initialize(string dbName)
        {
            base.Initialize(dbName);

            // Create the database object with the connection string
            _objDB = new DatabaseOracle(CONNECTION_STR);
        }

        /// <summary>
        /// FindExactPlateInfo
        ///  - (Override) Get plate info from parent path
        /// </summary>
        /// <param name="plate"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public override PlateList FindExactPlateInfo(Plate plate , bool exactMatch = false)
        {
            throw new System.NotImplementedException(GetType().Name + ":FindExactPlateInfo");
        }

        /// <summary>
        /// GetPlateInfo
        ///  - (Override) Get plate info from barcode
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfo(Plate plate)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB started");

                if (!_objDB.OpenDB())
                    return plates;

                query = "select barcode, plate_id, plate_format, plate_name, plate_timestamp, plate_path "
                    + "from v_opera_plate_info where ";

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    query += "upper(barcode) like :pBarcode";

                    // create parameter object for bind variables for barcode
                    _objDB.AddParameter("pBarcode" , "%" + plate.barcode.ToUpper() + "%" , OracleDbType.Varchar2);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        query += "plate_id = :pPlate_id";

                        // create parameter object for bind variables for plate id
                        _objDB.AddParameter("pPlate_id" , plate.plateId , OracleDbType.Varchar2);
                    }
                    else
                    {
                        query += "upper(plate_path) like :pPath";

                        // create parameter object for bind variables for path
                        _objDB.AddParameter("pPath" , ConvertPlatePath(plate.path) , OracleDbType.Varchar2);
                    }
                }

                ConnectLogger.Info(GetType().Name + ":GetPlateInfo DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                while (reader.Read())
                {
                    // values from query results
                    string exactBarcode = reader.GetString(0);
                    decimal plateid = reader.GetDecimal(1);
                    decimal format = reader.GetDecimal(2);
                    string name = reader.GetString(3);
                    DateTime timestamp = reader.GetDateTime(4);
                    string path = reader.GetString(5);

                    // extract plate name 
                    name = name.Replace(@"\\nibr.novartis.net\chbs-dfs\LABDATA\Inbox\PHCHBS-I21465\OperaDB\" , "");
                    name = name.Replace(".mea" , "");

                    // create Plate object
                    Plate p = new Plate();
                    p.barcode = exactBarcode;
                    p.plateId = plateid.ToString();
                    p.format = (int) format;
                    p.name = name;
                    p.timestamp = timestamp.ToString();
                    p.path = path;

                    // add to plate list
                    plates.Add(p);
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":GetPlateInfo  DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfo, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// GetPlateInfoByParentPath
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public override PlateList GetPlateInfoByParentPath(Plate plate)
        {
            var plates = new PlateList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB started");

                if (!_objDB.OpenDB())
                    return plates;

                query = "select barcode, plate_id, plate_format, plate_name, plate_timestamp, plate_path "
                    + "from v_opera_plate_info where ";

                // create SQL query based on search criteria
                if (!String.IsNullOrEmpty(plate.barcode))
                {
                    query += "upper(barcode) like :pBarcode";

                    // create parameter object for bind variables for barcode
                    _objDB.AddParameter("pBarcode" , "%" + plate.barcode.ToUpper() + "%" , OracleDbType.Varchar2);
                }
                else
                {
                    if (!String.IsNullOrEmpty(plate.plateId))
                    {
                        query += "plate_id = :pPlate_id";

                        // create parameter object for bind variables for plate id
                        _objDB.AddParameter("pPlate_id" , plate.plateId , OracleDbType.Varchar2);
                    }
                    else
                    {
                        query += "upper(plate_path) like :pPath";

                        // create parameter object for bind variables for path
                        _objDB.AddParameter("pPath" , FormatPlatePath(plate.path).ToUpper() , OracleDbType.Varchar2);
                    }
                }

                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);
                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath DB sql query finished");

                while (reader.Read())
                {
                    // values from query results
                    string exactBarcode = reader.GetString(0);
                    decimal plateid = reader.GetDecimal(1);
                    decimal format = reader.GetDecimal(2);
                    string name = reader.GetString(3);
                    DateTime timestamp = reader.GetDateTime(4);
                    string path = reader.GetString(5);

                    // extract plate name 
                    name = name.Replace(@"\\nibr.novartis.net\chbs-dfs\LABDATA\Inbox\PHCHBS-I21465\OperaDB\" , "");
                    name = name.Replace(".mea" , "");

                    // create Plate object
                    Plate p = new Plate();
                    p.barcode = exactBarcode;
                    p.plateId = plateid.ToString();
                    p.format = (int) format;
                    p.name = name;
                    p.timestamp = timestamp.ToString();
                    p.path = path;

                    // add to plate list
                    plates.Add(p);
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":GetPlateInfoByParentPath  DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfoByParentPath, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return plates;
        }

        /// <summary>
        /// QueryImageParameters
        ///  - (Override) Get image parameters associated with a plate well
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public override ImageParameters QueryImageParameters(Plate pl , int row , int col)
        {
            var imageParamList = new ImageParameters();
            var query = string.Empty;
            var pid = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":QueryImageParameters  DB started");

                if (!_objDB.OpenDB())
                    return imageParamList;

                // create SQL command to query parameters (fields, channels, etc.) for all matching plates
                // assemble SQL query based on plate identifier
                query = "select barcode, plate_id, plate_format, plate_name, plate_timestamp, plate_path, field_count, channel_count "
                    + "from v_opera_plate_info where ";

                if (!String.IsNullOrEmpty(pl.barcode))
                {
                    pid = pl.barcode;
                    query += "barcode = :pPlate";
                }
                else
                {
                    pid = pl.plateId;
                    query += "plate_id = :pPlate";
                }
                // create parameter object for bind variables for barcode
                _objDB.AddParameter("pPlate" , pid , OracleDbType.Varchar2);

                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                while (reader.Read())
                {
                    string barcode = reader.GetString(0);
                    decimal plateid = reader.GetDecimal(1);
                    decimal format = reader.GetDecimal(2);

                    // get reference to unique ImageParameter object associated with the plate id
                    ImageParameter imageParam = new ImageParameter();
                    // set db name
                    imageParam.database = this.GetDatabaseName();

                    // set plate data for ImageParameter
                    Plate p = new Plate();
                    p.barcode = barcode;
                    p.plateId = plateid.ToString();
                    // plate format
                    p.format = (int) format;
                    // extract plate name
                    string name = reader.GetString(3);
                    name = name.Replace(@"\\nibr.novartis.net\chbs-dfs\LABDATA\Inbox\PHCHBS-I21465\OperaDB\" , "");
                    name = name.Replace(".mea" , "");
                    p.name = name;
                    // timestamp of plate
                    DateTime timestamp = reader.GetDateTime(4);
                    p.timestamp = timestamp.ToString();
                    // path of plate
                    string path = reader.GetString(5);
                    p.path = path;

                    imageParam.plateData = p;

                    // set row and col of well data for ImageParameter
                    Well w = new Well();
                    w.row = row;
                    w.col = col;
                    imageParam.wellData = w;

                    // loop through number of fields and insert each one into image parameter object
                    decimal fieldCount = reader.GetDecimal(6);
                    for (int f = 1 ; f <= fieldCount ; f++)
                    {
                        imageParam.fields.Add(f);
                    }

                    // loop through number of channels and insert each one into image parameter object
                    decimal channelCount = reader.GetDecimal(7);
                    for (int c = 1 ; c <= channelCount ; c++)
                    {
                        imageParam.channels.Add(CHANNEL_NAME_PREFIX + c.ToString());
                    }

                    // add image parameter object to list
                    imageParamList.Add(imageParam);
                }
                reader.Close();

                ConnectLogger.Info(GetType().Name + ":QueryImageParameters DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":QueryImageParameters, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return imageParamList;
        }

        /// <summary>
        /// QueryImageParametersForWholePlate
        ///  - Get image parameters associated with a whole plate
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public override PlateImgMetadata QueryImageParametersForWholePlate(Plate pl)
        {
            throw new System.NotImplementedException(GetType().Name + ":QueryImageParametersForWholePlate");
        }

        /// <summary>
        /// PrepareForImageStream
        ///  - (Override) 
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public override FrameFile PrepareForImageStream(Plate pl , Well w , ImageSpec spec , string cacheKey)
        {
            // extract spec data
            var pid = pl.plateId;
            var row = w.row;
            var col = w.col;
            var query = string.Empty;
            var imageFilePath = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream  DB started");

                if (!_objDB.OpenDB())
                    return null;

                // create SQL command
                query = "select imagepath, field_count, channel_count, compress_type, compress_factor "
                + "from v_opera_image_path "
                + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id";

                // create bind variable for plateId
                _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                // create bind variable for row
                _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                // create bind variable for col
                _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);

                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                int channelCount = 0;
                string compressType;
                int compressFactor;

                if (reader.Read())
                {
                    imageFilePath = reader.GetString(0);
                    channelCount = (int) reader.GetDecimal(2);
                    compressType = reader.GetString(3);
                    compressFactor = (int) reader.GetDecimal(4);

                    spec.compressType = compressType;
                    spec.compressFactor = compressFactor;
                    spec.channelCount = channelCount;

                    reader.Close();
                }
                else
                {
                    reader.Close();

                    // image not found
                    if (!spec.suppressErr) throw new ImageNotFoundException();
                }

                if (!File.Exists(imageFilePath))
                {
                    // image path not valid
                    if (!spec.suppressErr) throw new ImageNotFoundException();
                }

                ConnectLogger.Info(GetType().Name + ":PrepareForImageStream DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":PrepareForImageStream, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return new FrameFile { ImagePath = imageFilePath };
        }

        /// <summary>
        /// PrepareForOverlayImageStream
        ///  - (Override) Overlay 
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public override Dictionary<string , FrameFile> PrepareForOverlayImageStream(Plate pl , Well w , OverlayImageSpec spec , string cacheKey)
        {
            var result = new Dictionary<string , FrameFile>();
            var pid = pl.plateId;
            var row = w.row;
            var col = w.col;
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream  DB started");

                if (!_objDB.OpenDB())
                    return result;

                // create SQL command
                query = "select imagepath, field_count, channel_count, compress_type, compress_factor "
                    + "from v_opera_image_path "
                    + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id";

                // create bind variable for plateId
                _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                // create bind variable for row
                _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                // create bind variable for col
                _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);

                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream DB sql query started. Query = " + query);

                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                string imageFilePath = string.Empty;
                int channelCount = 0;
                string compressType;
                int compressFactor;

                if (reader.Read())
                {
                    imageFilePath = reader.GetString(0);
                    channelCount = (int) reader.GetDecimal(2);
                    compressType = reader.GetString(3);
                    compressFactor = (int) reader.GetDecimal(4);
                    spec.compressType = compressType;
                    spec.compressFactor = compressFactor;
                }
                else
                {
                    // image not found
                    if (!spec.suppressErr)
                    {
                        throw new ImageNotFoundException();
                    }
                }
                reader.Close();

                if (File.Exists(imageFilePath))
                {
                    // associate each channel with the path to the image file
                    for (int c = 1 ; c <= channelCount ; c++)
                    {
                        string chName = CHANNEL_NAME_PREFIX + c.ToString();
                        result[chName] = new FrameFile { ImagePath = imageFilePath };
                    }
                }
                else
                {
                    // image path not valid
                    if (!spec.suppressErr)
                    {
                        throw new ImageNotFoundException();
                    }
                }

                ConnectLogger.Info(GetType().Name + ":PrepareForOverlayImageStream DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":PrepareForOverlayImageStream, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return result;
        }

        /// <summary>
        /// GetImageURL
        ///  - (Override) Get image according to spec and return the URL
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override string GetImageURL(Plate pl , Well w , ImageSpec spec)
        {
            return null;
        }

        /// <summary>
        /// QueryImagePath
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public override MediaDataList QueryImagePath(string pid , int row , int col , ImageSpec spec)
        {
            var list = new MediaDataList();
            var query = string.Empty;

            try
            {
                ConnectLogger.Info(GetType().Name + ":QueryImagePath  DB started");

                if (!_objDB.OpenDB())
                    return list;

                // extract channel id
                //int channel = this.extractChannelId(spec.channel);
                if (row > 0 && col > 0)
                {
                    // create SQL command
                    query = "select imagepath, field_count, channel_count, compress_type, compress_factor "
                       + "from v_opera_image_path "
                       + "where plate_id = :pPid and row_id = :pRow_id and col_id = :pCol_id";

                    // create bind variable for plateId
                    _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                    // create bind variable for row
                    _objDB.AddParameter("pRow_id" , row , OracleDbType.Int16);

                    // create bind variable for col
                    _objDB.AddParameter("pCol_id" , col , OracleDbType.Int16);

                    ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + query);

                    // execute SQL command
                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                    string imageFilePath;
                    int channelCount = 0;
                    string compressType;
                    int compressFactor;
                    int field;

                    if (reader.Read())
                    {
                        imageFilePath = reader.GetString(0);
                        channelCount = (int) reader.GetDecimal(2);
                        compressType = reader.GetString(3);
                        compressFactor = (int) reader.GetDecimal(4);
                        field = (int) reader.GetDecimal(1);
                        MediaData md = new MediaData
                        {
                            Path = imageFilePath ,
                            CompressFactor = compressFactor ,
                            CompressType = compressType ,
                            ChannelCount = channelCount ,
                            Column = col ,
                            Row = row ,
                            Field = field
                        };
                        list.Add(md);
                    }
                    reader.Close();
                }
                else
                {
                    // create SQL command
                    query = "select imagepath, field_count, channel_count, compress_type, compress_factor,row_id,col_id "
                        + "from v_opera_image_path "
                        + "where plate_id = :pPid";

                    // create bind variable for plateId
                    _objDB.AddParameter("pPid" , pid , OracleDbType.Varchar2);

                    ConnectLogger.Info(GetType().Name + ":QueryImagePath DB sql query started. Query = " + query);

                    // execute SQL command
                    OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                    string imageFilePath;
                    int channelCount = 0;
                    string compressType;
                    int compressFactor;

                    while (reader.Read())
                    {
                        imageFilePath = reader.GetString(0);
                        channelCount = (int) reader.GetDecimal(2);
                        compressType = reader.GetString(3);
                        compressFactor = (int) reader.GetDecimal(4);
                        int srow = (int) reader.GetDecimal(5);
                        int scol = (int) reader.GetDecimal(6);
                        int field = (int) reader.GetDecimal(1);

                        MediaData md = new MediaData
                        {
                            Path = imageFilePath ,
                            CompressFactor = compressFactor ,
                            CompressType = compressType ,
                            ChannelCount = channelCount ,
                            Column = scol ,
                            Row = srow ,
                            Field = field
                        };
                        list.Add(md);
                    }
                    reader.Close();
                }

                ConnectLogger.Info(GetType().Name + ":QueryImagePath DB finished");
            }
            catch (Exception eX)
            {
                _objDB.PrintParameters(ref query);
                throw new ConnectException(GetType().Name + ":GetPlateInfo, " + eX.Message + ", SQL = [" + query + "]" , eX);
            }
            finally
            {
                _objDB.CloseDB();
            }

            return list;
        }
    }
}

