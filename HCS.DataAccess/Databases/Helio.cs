﻿using System;
using System.Configuration;
using HCS.DataAccess.Database;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using Oracle.ManagedDataAccess.Client;

namespace HCS.DataAccess.Databases
{
    public class Helio : DatabaseOracle
    {
        // type of data source to search compound
        public enum DATA_SOURCE { HELIOS_PROD1, HELIOS_PROD2 };

        private readonly string PROD1_CONNECTION_STR = ConfigurationManager.ConnectionStrings["heliosProd1"].ConnectionString;
        private readonly string PROD2_CONNECTION_STR = ConfigurationManager.ConnectionStrings["heliosProd2"].ConnectionString;

        private DatabaseOracle _objDB;

        /// <summary>
        /// Constructor
        /// </summary>
        public Helio()
        {
        }

        /// <summary>
        /// Delegated compound search method based on data source
        /// </summary>
        /// <param name="compoundId"></param>
        /// <param name="dataSrc"></param>
        /// <returns></returns>
        public CompoundList DoSearch(string compoundId , DATA_SOURCE dataSrc)
        {
            var cpdList = new CompoundList();

            switch (dataSrc)
            {
                case DATA_SOURCE.HELIOS_PROD1:
                    _objDB = new DatabaseOracle(PROD1_CONNECTION_STR);
                    cpdList = DoHeliosSearch(compoundId);
                    break;
                case DATA_SOURCE.HELIOS_PROD2:
                    _objDB = new DatabaseOracle(PROD2_CONNECTION_STR);
                    cpdList = DoHeliosSearch(compoundId);
                    break;
            }

            return cpdList;
        }

        /// <summary>
        /// Search instrument, assay, and plate logistics related to a compound in the Helios database
        /// </summary>
        /// <param name="compoundId"></param>
        /// <returns></returns>
        private CompoundList DoHeliosSearch(string compoundId)
        {
            var cpdList = new CompoundList();
            string query = string.Empty;

            try
            {
                // Open the database connection
                if (!_objDB.OpenDB())
                    return cpdList;

                // create the SQL query
                query = "select distinct instrument, instrument_name, path, assay_plate, sample_name, col_id, row_id " +
                            "from bioc_r.v_measuree_plate where technology = :technology and sample_name like :sample_name";

                // create parameter object for bind variables
                _objDB.AddParameter("technology" , "HCS" , OracleDbType.Varchar2);
                _objDB.AddParameter("sample_name" , compoundId + "%" , OracleDbType.Varchar2);


                // execute SQL command
                OracleDataReader reader = (OracleDataReader) _objDB.ExecuteReader(query);

                while (reader.Read())
                {
                    decimal instrument = reader.GetDecimal(0);
                    string instrumentName = reader.GetString(1);
                    string assayName = reader.GetString(2);
                    string plateBarcode = reader.GetString(3);
                    string sampleName = reader.GetString(4);
                    decimal col = reader.GetDecimal(5);
                    decimal row = reader.GetDecimal(6);

                    // create new compound object
                    Compound cpd = new Compound();
                    cpd.compoundId = sampleName;
                    cpd.assay = assayName;
                    cpd.plateBarcode = plateBarcode;
                    cpd.wellData.row = (int) row;
                    cpd.wellData.col = (int) col;
                    cpd.dataSource.id = instrument.ToString();
                    cpd.dataSource.description = instrumentName;

                    // add new compound to list
                    cpdList.Add(cpd);
                }
                // close database connection
                reader.Close();
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(this.GetType().Name + ":doHeliosSearch, Error=" + ex.Message , ex);
                _objDB.PrintParameters(ref query);
                new ConnectException(this.GetType().Name + ":doHeliosSearch, Error=" + ex.Message + " SQL=" + query , ex);

                return null;
            }
            finally
            {
                _objDB.CloseDB();
            }

            return cpdList;
        }
    }
}