﻿using System.Collections.Generic;

namespace HCS.DataAccess
{
    public interface IHCSDatabaseAccess
    {
        void Initialize(string dbName);
        string GetDatabaseName();
        int getPixelMultFactor();
        string GetDatabaseDescription();
        string GetDefaultImageFormat();
        PlateList GetPlateInfo(Plate plate);
        PlateList GetPlateInfoByParentPath(Plate plate);
        PlateList FindExactPlateInfo(Plate plate , bool exactMatch = false);
        ImageParameters QueryImageParameters(Plate pl , int row , int col);
        PlateImgMetadata QueryImageParametersForWholePlate(Plate pl);
        FrameFile PrepareForImageStream(Plate pl , Well w , ImageSpec spec , string cacheKey);
        string GetImageURL(Plate pl , Well w , ImageSpec spec);
        Dictionary<string , FrameFile> PrepareForOverlayImageStream(Plate pl , Well w , OverlayImageSpec spec , string cacheKey);
        MediaDataList QueryImagePath(string plateId , int row , int col , ImageSpec spec);
    }
}
