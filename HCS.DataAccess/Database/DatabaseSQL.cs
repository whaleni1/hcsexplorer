//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright � 2010
// Author:          Erik Hesse
// Date:            12/14/2009
//
// Project:                 
// Application:     
// File:            DataBaseSQL.cs
//
// Comment:         
//
// Purpose:         SQL Server database class
//
//
// History:        
//
//
//*********************************************************************************

using System;
using System.Data;
using System.Data.SqlClient;
using HCS.Shared.Common;
using HCS.Shared.Logger;

namespace HCS.DataAccess.Database
{
    /// <summary>
    /// DataBaseSQL class
    /// </summary>
    public class DataBaseSQL : IDatabase
    {
        private SqlConnection m_objDataConn;
        private SqlCommand m_objDataComm;
        private SqlDataAdapter m_objDataAdp;
        private SqlTransaction m_SQLTransaction;
        private string m_strConnenctionString;
        protected bool m_IsOpen;

        public SqlConnection DBConnection
        {
            get { return m_objDataConn; }
        }

        public string ConnectionString
        {
            set
            {
                m_strConnenctionString = value;
                m_objDataConn.ConnectionString = value;
            }
        }

        /// <summary>
        /// CDatabase_SQL constructor
        /// </summary>
        public DataBaseSQL()
        {
            // Open the connection to the database
            m_objDataConn = new SqlConnection();
            m_objDataAdp = new SqlDataAdapter();
            m_objDataComm = new SqlCommand();
            m_objDataComm.Connection = m_objDataConn;
        }

        public DataBaseSQL(string strConnectionString)
        {
            m_objDataConn = new SqlConnection();
            m_objDataAdp = new SqlDataAdapter();
            m_objDataComm = new SqlCommand();

            // create the connection string
            m_strConnenctionString = strConnectionString;

            // set the connection information
            m_objDataConn.ConnectionString = m_strConnenctionString;
            m_objDataComm.Connection = m_objDataConn;
        }

        /// <summary>
        /// OpenDB the database connection
        /// </summary>
        public bool OpenDB()
        {
            try
            {
                if (!m_IsOpen)
                {
                    m_objDataConn.Open();
                    m_IsOpen = true;
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Info(GetType().Name + ":OpenDB, Database failed to open, " + eX.Message);
                m_IsOpen = false;
            }

            return m_IsOpen;
        }

        ///<summary>
        ///check if connection is open
        /// </summary>
        public bool IsOpen()
        {
            if (m_objDataConn.State == ConnectionState.Open)
            {
                m_IsOpen = true;
            }
            else
            {
                m_IsOpen = false;
            }
            return m_IsOpen;
        }

        /// <summary>
        /// CloseDB the database connection
        /// </summary>
        public void CloseDB()
        {
            if (m_IsOpen)
            {
                ClearParameters();
                m_objDataConn.Close();
                m_IsOpen = false;
            }
        }

        /// <summary>
        /// BeginTransaction
        /// </summary>
        public void BeginTransaction()
        {
            // Start a local transaction
            m_SQLTransaction = m_objDataConn.BeginTransaction(IsolationLevel.Serializable);

            // Assign transaction object for a pending local transaction
            m_objDataComm.Connection = m_objDataConn;
            //m_objDataComm.Transaction = m_objTrans;
        }

        /// <summary>
        /// CommitTransaction
        /// </summary>
        public void CommitTransaction()
        {
            m_SQLTransaction.Commit();
        }

        /// <summary>
        /// RollbackTransaction
        /// </summary>
        public void RollbackTransaction()
        {
            m_SQLTransaction.Rollback();
        }

        /// <summary>
        /// GetDataSet method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        /// <returns>DataSet object</returns>
        public DataSet GetDataSet(string strSQL)
        {
            DataSet objDS = new DataSet();

            // create the SQL statement
            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.Text;

            // get the data
            m_objDataAdp.SelectCommand = m_objDataComm;
            m_objDataAdp.Fill(objDS);

            // Remove the parameters - if any where used
            ClearParameters();

            return objDS;
        }

        /// <summary>
        /// ExecuteNonQuery method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        public void ExecuteNonQuery(string strSQL)
        {
            ExecuteNonQuery(strSQL , true);

            return;
        }

        /// <summary>
        /// ExecuteNonQuery method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        public void ExecuteNonQuery(string strSQL , bool bClearParameters)
        {
            // create the SQL statement
            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.Text;
            m_objDataComm.ExecuteNonQuery();

            // Remove the parameters - if any where used
            if (bClearParameters)
                ClearParameters();

            return;
        }

        /// <summary>
        /// ExecuteStoredProcedure method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        public object ExecuteStoredProcedure(string strSQL)
        {
            return ExecuteStoredProcedure(strSQL , true);
        }

        /// <summary>
        /// ExecuteStoredProcedure method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        public object ExecuteStoredProcedure(string strSQL , bool bClearParameters)
        {
            object objReturnScalar;

            // create the SQL statement
            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.StoredProcedure;
            objReturnScalar = m_objDataComm.ExecuteNonQuery();

            // Remove the parameters - if any where used
            if (bClearParameters)
                ClearParameters();

            return objReturnScalar;
        }

        /// <summary>
        /// ExecuteScalar method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        /// <returns>Object containing SQL return value.</returns>
        public object ExecuteScalar(string strSQL)
        {
            object objReturnScalar;

            // create the SQL statement
            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.Text;

            // get the data
            objReturnScalar = m_objDataComm.ExecuteScalar();

            return objReturnScalar;
        }

        /// <summary>
        /// ExecuteReader method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        /// <returns>DataReader object</returns>
        public object ExecuteReader(string strSQL)
        {
            SqlDataReader objDataRdr;

            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.Text;

            objDataRdr = m_objDataComm.ExecuteReader();

            // Remove the parameters - if any where used
            ClearParameters();

            return objDataRdr;
        }

        /// <summary>
        /// ExecuteReader
        /// </summary>
        /// <param name="strSQL"></param>
        /// <param name="bClearParameters"></param>
        /// <returns></returns>
        public object ExecuteReader(string strSQL , bool bClearParameters)
        {
            SqlDataReader objDataRdr;

            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.Text;

            objDataRdr = m_objDataComm.ExecuteReader();

            // Remove the parameters - if any where used
            if (bClearParameters)
                ClearParameters();

            return objDataRdr;
        }

        /// <summary>
        /// PrintParameters
        ///     -   Print the Parameter values to the string
        /// </summary>
        /// <param name="strMessage"></param>
        /// <returns></returns>
        public void PrintParameters(ref string strMessage)
        {
            for (int i = 0 ; i < m_objDataComm.Parameters.Count ; i++)
                strMessage += "\n\tParamter:" + m_objDataComm.Parameters[i].ParameterName + "[" + m_objDataComm.Parameters[i].Value + "]";

            // Remove the parameters - if any where used
            ClearParameters();
        }

        /// <summary>
        /// PrintParameters
        ///     -   Print the Parameter values to the string
        /// </summary>
        /// <param name="strMessage"></param>
        /// <param name="bClearParameters"></param>
        /// <returns></returns>
        public void PrintParameters(ref string strMessage , bool bClearParameters)
        {
            for (int i = 0 ; i < m_objDataComm.Parameters.Count ; i++)
                strMessage += "\n\tParamter:" + m_objDataComm.Parameters[i].ParameterName + "[" + m_objDataComm.Parameters[i].Value + "]";

            // Remove the parameters - if any where used
            if (bClearParameters)
                ClearParameters();
        }

        /// <summary>
        /// ClearParameters
        /// </summary>
        public void ClearParameters()
        {
            m_objDataComm.Parameters.Clear();
        }

        /// <summary>
        /// AddParameter
        /// </summary>
        /// <param name="strName"></param>
        /// <param name="oValue"></param>
        /// <param name="dbParamType"></param>
        public void AddParameter(string strName , object oValue , SqlDbType dbParamType)
        {
            try
            {
                m_objDataComm.Parameters.AddWithValue(strName , dbParamType).Value = oValue;
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":AddParamter " + eX.Message , eX);
            }
        }
    }
}
