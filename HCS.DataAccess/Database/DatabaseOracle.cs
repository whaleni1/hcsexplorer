//*********************************************************************************
// Company:         Novartis NIBRIT
// Copyright:       Copyright � 2012
// Author:          Erik Hesse
// Date:            08/05/2010
//
// Project:         
// Application:     
// File:            DataBaseOracle.cs
//
// Comment:         
//
// Purpose:         This class was written to wrap the ODP.NET Oracle classes to 
//                  simplify all data access.
//
//
// History:        
//
//
//*********************************************************************************
using System;
using System.Data;
using System.Diagnostics;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using Oracle.ManagedDataAccess.Client;

namespace HCS.DataAccess.Database
{
    /// <summary>
    /// DataBaseOracle class
    /// </summary>
    public class DatabaseOracle : IDatabase
    {
        private OracleConnection m_objDataConn;
        private OracleDataAdapter m_objDataAdp;
        protected OracleCommand m_objDataComm;
        private OracleTransaction m_objTrans;
        protected string m_strConnenctionString;
        protected string m_strDBInstance;
        protected string m_strUsername;
        protected string m_strPassword;
        protected bool m_IsOpen;

        #region "Properties"

        public OracleConnection Connection
        {
            get { return m_objDataConn; }
        }

        public OracleParameterCollection Parameters
        {
            get { return m_objDataComm.Parameters; }
        }

        public string ConnectionString
        {
            set
            {
                m_strConnenctionString = value;
                m_objDataConn.ConnectionString = value;
            }
        }
        #endregion

        /// <summary>
        /// CDatabase_Oracle constructor
        /// </summary>
        public DatabaseOracle()
        {
            m_objDataConn = new OracleConnection();
            m_objDataAdp = new OracleDataAdapter();
            m_objDataComm = new OracleCommand();

            // create the connection string
            m_strConnenctionString = "Data Source=" + m_strDBInstance
                + ";Password=" + m_strPassword + ";User ID=" + m_strUsername;

            // set the connection information
            m_objDataConn.ConnectionString = m_strConnenctionString;
            m_objDataComm.Connection = m_objDataConn;
        }

        public DatabaseOracle(string strConnectionString)
        {
            m_objDataConn = new OracleConnection();
            m_objDataAdp = new OracleDataAdapter();
            m_objDataComm = new OracleCommand();

            // create the connection string
            m_strConnenctionString = strConnectionString;

            // set the connection information
            m_objDataConn.ConnectionString = m_strConnenctionString;
            m_objDataComm.Connection = m_objDataConn;
        }

        /// <summary>
        /// CDatabase_Oracle constructor
        /// </summary>
        public DatabaseOracle(string strDBInstance , string strUsername , string strPassword)
        {
            Debug.Assert(!String.IsNullOrEmpty(strDBInstance) , "strDBInstance is null or empty.");
            Debug.Assert(!String.IsNullOrEmpty(strUsername) , "strUsername is null or empty.");
            Debug.Assert(!String.IsNullOrEmpty(strPassword) , "strPassword is null or empty.");

            if (String.IsNullOrEmpty(strDBInstance) || String.IsNullOrEmpty(strUsername) || String.IsNullOrEmpty(strPassword))
                return;

            m_objDataConn = new OracleConnection();
            m_objDataAdp = new OracleDataAdapter();
            m_objDataComm = new OracleCommand();

            m_strDBInstance = strDBInstance;
            m_strUsername = strUsername;
            m_strPassword = strPassword;

            // create the connection string
            m_strConnenctionString = "Data Source=" + m_strDBInstance
                + ";Password=" + m_strPassword + ";User Id=" + m_strUsername;

            // set the connection information
            m_objDataConn.ConnectionString = m_strConnenctionString;
            m_objDataComm.Connection = m_objDataConn;
            //m_objDataComm.BindByName = true;
        }

        /// <summary>
        /// OpenDB the database connection
        /// </summary>
        public bool OpenDB()
        {
            try
            {
                if (!m_IsOpen)
                {
                    m_objDataConn.Open();
                    m_IsOpen = true;
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Info(GetType().Name + ":OpenDB, Database failed to open, " + eX.Message);
                m_IsOpen = false;
            }

            return m_IsOpen;
        }

        ///<summary>
        ///check if connection is open
        /// </summary>
        public bool IsOpen()
        {
            if (m_objDataConn.State == ConnectionState.Open)
            {
                m_IsOpen = true;
            }
            else
            {
                m_IsOpen = false;
            }
            return m_IsOpen;
        }

        /// <summary>
        /// CloseDB the database connection
        /// </summary>
        public void CloseDB()
        {
            if (m_IsOpen)
            {
                ClearParameters();
                m_objDataConn.Close();
                m_IsOpen = false;
            }
        }

        /// <summary>
        /// BeginTransaction
        /// </summary>
        public void BeginTransaction()
        {
            // Start a local transaction
            m_objTrans = m_objDataConn.BeginTransaction();

            // Assign transaction object for a pending local transaction
            m_objDataComm.Connection = m_objDataConn;
            //m_objDataComm.Transaction = m_objTrans;
        }

        /// <summary>
        /// CommitTransaction
        /// </summary>
        public void CommitTransaction()
        {
            m_objTrans.Commit();
        }

        /// <summary>
        /// RollbackTransaction
        /// </summary>
        public void RollbackTransaction()
        {
            m_objTrans.Rollback();
        }

        /// <summary>
        /// GetDataSet method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        /// <returns>DataSet object</returns>
        public DataSet GetDataSet(string strSQL)
        {
            DataSet objDS = new DataSet();

            // create the SQL statement
            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.Text;

            // get the data
            m_objDataAdp.SelectCommand = m_objDataComm;
            m_objDataAdp.Fill(objDS);

            // Remove the parameters - if any where used
            ClearParameters();

            return objDS;
        }

        /// <summary>
        /// ExecuteNonQuery method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        public void ExecuteNonQuery(string strSQL)
        {
            ExecuteNonQuery(strSQL , true);
        }

        /// <summary>
        /// ExecuteNonQuery method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        public void ExecuteNonQuery(string strSQL , bool bClearParameters)
        {
            // create the SQL statement
            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.Text;
            m_objDataComm.ExecuteNonQuery();

            // Remove the parameters - if any where used
            if (bClearParameters)
                ClearParameters();
        }

        /// <summary>
        /// ExecuteStoredProcedure method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        public object ExecuteStoredProcedure(string strSQL)
        {
            return ExecuteStoredProcedure(strSQL , true);
        }

        /// <summary>
        /// ExecuteStoredProcedure method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        public object ExecuteStoredProcedure(string strSQL , bool bClearParameters)
        {
            object objReturnScalar;

            // create the SQL statement
            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.StoredProcedure;
            objReturnScalar = m_objDataComm.ExecuteNonQuery();

            // Remove the parameters - if any where used
            if (bClearParameters)
                ClearParameters();

            return objReturnScalar;
        }

        /// <summary>
        /// ExecuteScalar method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        /// <returns>Object containing SQL return value.</returns>
        public object ExecuteScalar(string strSQL)
        {
            object objReturnScalar;

            // create the SQL statement
            m_objDataComm.CommandText = strSQL;
            m_objDataComm.CommandType = CommandType.Text;

            // get the data
            objReturnScalar = m_objDataComm.ExecuteScalar();

            // Remove the parameters - if any where used
            ClearParameters();

            return objReturnScalar;
        }

        /// <summary>
        /// GetDataReader method
        /// </summary>
        /// <param name="strSQL">SQL string</param>
        /// <returns>DataReader object</returns>
        public object ExecuteReader(string strSQL)
        {
            OracleDataReader objDataRdr;

            m_objDataComm.CommandText = strSQL;

            objDataRdr = m_objDataComm.ExecuteReader();

            // Remove the parameters - if any where used
            ClearParameters();

            return objDataRdr;
        }

        /// <summary>
        /// ExecuteReader
        /// </summary>
        /// <param name="strSQL"></param>
        /// <param name="bClearParameters"></param>
        /// <returns></returns>
        public object ExecuteReader(string strSQL , bool bClearParameters)
        {
            OracleDataReader objDataRdr;

            m_objDataComm.CommandText = strSQL;

            objDataRdr = m_objDataComm.ExecuteReader();

            // Remove the parameters - if any where used
            if (bClearParameters)
                ClearParameters();

            return objDataRdr;
        }

        /// <summary>
        /// ClearParameters
        /// </summary>
        public void ClearParameters()
        {
            m_objDataComm.Parameters.Clear();
        }

        /// <summary>
        /// PrintParameters
        ///     -   Print the Parameter values to the string
        /// </summary>
        /// <param name="strSQL"></param>
        /// <param name="bClearParameters"></param>
        /// <returns></returns>
        public void PrintParameters(ref string strSQL , bool bClearParameters)
        {
            for (int i = 0 ; i < m_objDataComm.Parameters.Count ; i++)
                strSQL += "\n\tParamter:" + m_objDataComm.Parameters[i].ParameterName + "[" + m_objDataComm.Parameters[i].Value + "]";

            // Remove the parameters - if any where used
            if (bClearParameters)
                ClearParameters();
        }

        /// <summary>
        /// PrintParameters
        ///     -   Print the Parameter values to the string
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public void PrintParameters(ref string strSQL)
        {
            for (int i = 0 ; i < m_objDataComm.Parameters.Count ; i++)
                strSQL += "\n\tParamter:" + m_objDataComm.Parameters[i].ParameterName + "[" + m_objDataComm.Parameters[i].Value + "]";

            // Remove the parameters - if any where used
            ClearParameters();
        }

        /// <summary>
        /// AddParameter
        ///  - add a parameter to the command object
        /// </summary>
        /// <param name="strName"></param>
        /// <param name="oValue"></param>
        /// <param name="dbParamType"></param>
        public void AddParameter(string strName , object oValue , OracleDbType dbParamType)
        {
            try
            {
                m_objDataComm.Parameters.Add(strName , dbParamType).Value = oValue;
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":AddParamter " + eX.Message , eX);
            }
        }

        /// <summary>
        /// AddParameter
        /// </summary>
        /// <param name="strName"></param>
        /// <param name="oValue"></param>
        /// <param name="dbParamType"></param>
        /// <param name="iSize"></param>
        public void AddParameter(string strName , object oValue , OracleDbType dbParamType , int iSize)
        {
            try
            {
                m_objDataComm.Parameters.Add(strName , dbParamType , iSize).Value = oValue;
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":AddParameter " + eX.Message , eX);
            }
        }

        /// <summary>
        /// AddParameter
        /// </summary>
        /// <param name="strName"></param>
        /// <param name="oValue"></param>
        /// <param name="dbParamType"></param>
        /// <param name="iSize"></param>
        /// <param name="pDirection"></param>
        public void AddParameter(string strName , object oValue , OracleDbType dbParamType , int iSize ,
            ParameterDirection pDirection)
        {
            try
            {
                m_objDataComm.Parameters.Add(strName , dbParamType , iSize).Direction = pDirection;
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":AddParameter " + eX.Message , eX);
            }
        }
    }
}
