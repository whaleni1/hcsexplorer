//*********************************************************************************
// Company:         Novartis NIBRIT
// Copyright:       Copyright � 2012
// Author:          Erik Hesse
// Date:            08/05/2010
//
// Project:         
// Application:     
// File:            IDatabase.cs
//
// Comment:         
//
// Purpose:         Base database interface
//
//
// History:        
//
//
//*********************************************************************************
using System.Data;

namespace HCS.DataAccess.Database
{
    public interface IDatabase
    {
        bool OpenDB();
        void CloseDB();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
        DataSet GetDataSet(string strSQL);
        void ExecuteNonQuery(string strSQL);
        void ExecuteNonQuery(string strSQL , bool bClearParameters);
        object ExecuteStoredProcedure(string strSQL);
        object ExecuteStoredProcedure(string strSQL , bool bClearParameters);
        object ExecuteScalar(string strSQL);
        object ExecuteReader(string strSQL);
        object ExecuteReader(string strSQL , bool bClearParameters);
        bool IsOpen();
        void ClearParameters();
        void PrintParameters(ref string strSQL);
        void PrintParameters(ref string strSQL , bool bClearParameters);
    }
}
