﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using HCS.DataAccess.Helpers;

namespace HCS.DataAccess
{
    public abstract class HCSDatabaseAccess : IHCSDatabaseAccess
    {
        private string _dbName;
        protected string CONNECTION_STR;
        protected int PIXEL_MULT_FACTOR;

        /// <summary>
        /// GetDatabaseInstance
        ///  - Initialize connection to database
        ///    (To be implemented by derived class based on database type)
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static HCSDatabaseAccess GetDatabaseInstance(string dbName)
        {
            HCSDatabaseAccess dbObj = null;
            // invoke the subclass corresponding to db name
            Type dbType = Type.GetType(HCSDBConfigSection.GetTypeName(dbName) , true);
            dbObj = (HCSDatabaseAccess) Activator.CreateInstance(dbType);
            dbObj.Initialize(dbName);
            return dbObj;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="dbName"></param>
        public virtual void Initialize(string dbName)
        {
            _dbName = dbName;
            CONNECTION_STR = ConfigurationManager.ConnectionStrings[HCSDBConfigSection.GetConnectionStringName(_dbName)].ConnectionString;
            PIXEL_MULT_FACTOR = HCSDBConfigSection.GetPixelMultFactor(_dbName);
        }


        /// <summary>
        /// GetDatabaseName
        ///  - Get database name which is the name of the class
        /// </summary>
        /// <returns></returns>
        public string GetDatabaseName()
        {
            return _dbName;
        }

        /// <summary>
        /// getPixelMultFactor
        ///  - Get database name which is the name of the class
        /// </summary>
        public int getPixelMultFactor()
        {
            return PIXEL_MULT_FACTOR;
        }

        /// <summary>
        /// GetDatabaseDescription
        ///  - Get description of this database
        /// </summary>
        /// <returns></returns>
        public string GetDatabaseDescription()
        {
            return HCSDBConfigSection.GetDescription(_dbName);
        }

        /// <summary>
        /// GetDefaultImageFormat
        ///  - Default image format
        /// </summary>
        /// <returns></returns>
        public string GetDefaultImageFormat()
        {
            return "tiff";
        }

        /// <summary>
        /// GetPlateInfo
        ///  - Get information about a plate from barcode
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public abstract PlateList GetPlateInfo(Plate plate);

        /// <summary>
        /// GetPlateInfoByParentPath
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public abstract PlateList GetPlateInfoByParentPath(Plate plate);

        /// <summary>
        /// FindExactPlateInfo
        ///  - Get information about a plate from parent path. Used by CPCP Wrapper only
        /// </summary>
        /// <param name="plate"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public abstract PlateList FindExactPlateInfo(Plate plate , bool exactMatch = false);

        /// <summary>
        /// QueryImageParameters
        ///  - Get image parameters associated with a plate well
        ///    (To be implemented by derived class)
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public abstract ImageParameters QueryImageParameters(Plate pl , int row , int col);

        /// <summary>
        /// QueryImageParametersForWholePlate
        ///  - Get image parameters associated with a whole plate
        ///    (To be implemented by derived class)
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public abstract PlateImgMetadata QueryImageParametersForWholePlate(Plate pl);

        /// <summary>
        /// PrepareForImageStream
        ///  -  Get image according to spec and return as stream
        ///     (To be implemented by derived class)
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public abstract FrameFile PrepareForImageStream(Plate pl , Well w , ImageSpec spec , string cacheKey);

        /// <summary>
        /// GetImageURL
        ///  - Get image according to spec and return URL
        ///   (To be implemented by derived class)
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public abstract string GetImageURL(Plate pl , Well w , ImageSpec spec);

        /// <summary>
        /// PrepareForOverlayImageStream
        ///  - Overlay multiple channels and return image as stream
        ///    (To be implemented by derived class)
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public abstract Dictionary<string , FrameFile> PrepareForOverlayImageStream(Plate pl , Well w , OverlayImageSpec spec , string cacheKey);

        /// <summary>
        /// QueryImagePath
        /// </summary>
        /// <param name="plateId"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public abstract MediaDataList QueryImagePath(string plateId , int row , int col , ImageSpec spec);

        /// <summary>
        /// ReplaceDriveLetter
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ReplaceDriveLetter(string path)
        {
            if (string.IsNullOrEmpty(path))
                return path;

            Regex r = new Regex("^[A-Z]:\\\\" , RegexOptions.IgnoreCase);

            var match = r.Match(path);
            //if (match.Groups.Count > 0)
            if (match.Success)
            {
                return path.Substring(2);
            }

            return path;
        }

        /// <summary>
        /// ConvertPlatePath
        ///  - Translate mapped image folder path (L drive, etc.) to path search string
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ConvertPlatePath(string path)
        {
            path = path.ToUpper();

            if (path.EndsWith("\\"))
            {
                path = path.Remove(path.Length - 1 , 1);
            }

            path = ReplaceDriveLetter(path);
            path = "%" + path + "%";

            return path;
        }

        /// <summary>
        /// ConvertPlatePathExact
        ///  - Translate mapped image folder path (L drive, etc.) to path search string
        /// </summary>
        /// <param name="path"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public static string ConvertPlatePathExact(string path , bool exactMatch)
        {
            path = path.ToUpper();

            if (exactMatch)
            {
                if (path.EndsWith("\\"))
                {
                    path = path.Remove(path.Length - 1 , 1);
                }
                path = ReplaceDriveLetter(path);
                path = "%" + path;
            }
            else
            {
                if (!path.EndsWith("\\"))
                {
                    path += "\\";
                }
                path = ReplaceDriveLetter(path);
                path = "%" + path + "%";
            }

            return path;
        }

        /// <summary>
        /// FormatPlatePath
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string FormatPlatePath(string path)
        {
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }
            path = ReplaceDriveLetter(path);
            path += "%";

            return path;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string FormatPlatePathAfterNoResults(string path)
        {
            if (path.EndsWith("\\"))
            {
                //path += "\\";
                path = path.Remove(path.Length - 2);
            }

            path += "%";

            return path;
        }
    }
}
