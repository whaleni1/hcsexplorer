﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HCS.DataAccess
{
    // List of databases
    [CollectionDataContract(Name = "databases" , ItemName = "database")]
    public class DBList : List<HCSDatabase> { }

    // List of plates
    [CollectionDataContract(Name = "plates" , ItemName = "plate")]
    public class PlateList : List<Plate>
    {
        public PlateList() : base()
        {
        }

        public PlateList(IEnumerable<Plate> collection) : base(collection)
        {
        }
    }

    // List of compounds
    [CollectionDataContract(Name = "compounds" , ItemName = "compound")]
    public class CompoundList : List<Compound> { }

    // List of fields
    [CollectionDataContract(ItemName = "field")]
    public class FieldList : List<int>
    {
        public FieldList() : base()
        {
        }

        public FieldList(IEnumerable<int> collection) : base(collection)
        {
        }
    }


    // List of channels
    [CollectionDataContract(ItemName = "channel")]
    public class ChannelList : List<string>
    {
        public ChannelList() : base()
        {
        }

        public ChannelList(IEnumerable<string> collection) : base(collection)
        {
        }
    }


    [CollectionDataContract(ItemName = "version")]
    public class VersionList : HashSet<string>
    {
        public VersionList() : base()
        {
        }

        public VersionList(IEnumerable<string> collection) : base(collection)
        {
        }
    }

    // List of ImageParameter
    [CollectionDataContract(Name = "imageParametersArray" , ItemName = "imageParameters")]
    public class ImageParameters : List<ImageParameter>
    {
        public ImageParameters() : base()
        {
        }

        public ImageParameters(IEnumerable<ImageParameter> collection) : base(collection)
        {
        }
    }

    // List of ImageCollection
    [CollectionDataContract(ItemName = "imageCollection" , Name = "imageCollections")]
    public class ImageCollections : List<ImageCollection> { }

    // List of ImageSpec
    [CollectionDataContract(ItemName = "image")]
    public class ImageList : List<ImageSpec> { }

    // HCS data source
    [DataContract]
    public class HCSDatabase
    {
        [DataMember(Order = 0)]
        public string id;

        [DataMember(Order = 1)]
        public string description;
    }

    // Metadata for a plate
    [DataContract]
    public class Plate
    {
        [DataMember(Order = 0)]
        public string barcode;

        [DataMember(Order = 1)]
        public string plateId;

        // plate format (96, 384, 1536)
        [DataMember(Order = 2)]
        public int format;

        [DataMember(Order = 3)]
        public string name;

        [DataMember(Order = 4)]
        public string timestamp;

        [DataMember(Order = 5)]
        public string path;

        [DataMember(Order = 6)]
        public string application;

    }

    // Metadata for a well
    [DataContract]
    public class Well
    {
        [DataMember(Order = 0)]
        public int row;

        [DataMember(Order = 1)]
        public int col;
    }

    // Metadata (from Helios) for a compound
    [DataContract]
    public class Compound
    {
        public Compound()
        {
            dataSource = new HCSDatabase();
            wellData = new Well();
        }

        [DataMember(Order = 0)]
        public string compoundId;

        [DataMember(Order = 1)]
        public string assay;

        [DataMember(Order = 2)]
        public string plateBarcode;

        [DataMember(Order = 3 , Name = "dataSource")]
        public HCSDatabase dataSource;

        [DataMember(Order = 4 , Name = "well")]
        public Well wellData;
    }

    // ImageParameter contains all parameters associated with images from a plate well
    [DataContract]
    public class ImageParameter
    {
        public ImageParameter()
        {
            plateData = new Plate();
            wellData = new Well();
            fields = new FieldList();
            channels = new ChannelList();
            versions = new VersionList();
            // private objects
            uniqueFields = new Hashtable();
            uniqueChannels = new Hashtable();
        }

        [DataMember(Order = 0)]
        public string database;

        [DataMember(Order = 1 , Name = "plate")]
        public Plate plateData;

        [DataMember(Order = 2 , Name = "well")]
        public Well wellData;

        [DataMember(Order = 3)]
        public FieldList fields;

        [DataMember(Order = 4)]
        public ChannelList channels;

        [DataMember(Order = 5 , EmitDefaultValue = false , IsRequired = false)]
        public int zCount;

        [DataMember(Order = 6 , EmitDefaultValue = false , IsRequired = false)]
        public int timepointCount;

        [DataMember(Order = 7 , EmitDefaultValue = false , IsRequired = false)]
        public VersionList versions;

        [NonSerialized]
        public int Field;
        [NonSerialized]
        public string Channel;
        [NonSerialized]
        public string Version;

        /*
         * Hashtable to store unique fields
         */
        [NonSerialized]
        private Hashtable uniqueFields;

        /*
         * Hashtable to store unique channels
         */
        [NonSerialized]
        private Hashtable uniqueChannels;

        /// <summary>
        /// Adds field if it has not already been added; otherwise does nothing.
        /// </summary>
        /// <param name="field">the field to be added</param>
        public void addField(int field)
        {
            // insert field if it does not already exist
            if (!uniqueFields.ContainsKey(field))
            {
                uniqueFields.Add(field , field);
                fields.Add(field);
            }
        }

        /// <summary>
        /// Adds channel if it has not already been added; otherwise does nothing.
        /// </summary>
        /// <param name="channel"></param>
        public void addChannel(string channel)
        {
            // insert channel if it does not already exist
            if (!uniqueChannels.ContainsKey(channel))
            {
                uniqueChannels.Add(channel , channel);
                channels.Add(channel);
            }
        }

        /// <summary>
        /// sort channels 
        /// </summary>
        /// <param name="channel"></param>
        public void SortChannels()
        {

            if (channels != null)
            {
                channels.Sort();
            }
        }


        /// <summary>
        /// Adds version if it has not already been added; otherwise does nothing.
        /// </summary>
        /// <param name="version">the version to be added</param>
        public void addVersion(string version)
        {
            versions.Add(version);
        }
    }

    // ImageCollection contains all ImageSpec objects associated with a well
    [DataContract]
    public class ImageCollection
    {
        public ImageCollection()
        {
            plateData = new Plate();
            wellData = new Well();
            images = new ImageList();
        }

        [DataMember(Order = 0)]
        public string database;

        [DataMember(Order = 0 , Name = "plate")]
        public Plate plateData;

        [DataMember(Order = 1 , Name = "well")]
        public Well wellData;

        [DataMember(Order = 2)]
        public ImageList images;
    }

    // Parameters associated with an image
    [DataContract]
    public class ImageSpec
    {
        [DataMember(Order = 0)]
        public int field;

        [DataMember(Order = 1)]
        public string channel;

        [DataMember(Order = 2)]
        public string imageURI;


        //used for input, not output, so not serialized
        [NonSerialized]
        public int timepoint;

        [NonSerialized]
        public int z_index;

        [NonSerialized]
        public string version;


        // determines if additional image processing is required
        [NonSerialized]
        public bool reqImageProcess = false;

        [NonSerialized]
        public string format;

        [NonSerialized]
        public int thumbPixelSize = 0;

        [NonSerialized]
        public bool invert = false;

        [NonSerialized]
        public bool auto = false;

        [NonSerialized]
        public double brightness = 0;

        [NonSerialized]
        public bool level = false;

        [NonSerialized]
        public long whitePoint = long.MaxValue;

        [NonSerialized]
        public long blackPoint = 0;

        [NonSerialized]
        public double contrast = 0;

        [NonSerialized]
        public double gamma = 1.0;

        [NonSerialized]
        public string color;

        [NonSerialized]
        public bool suppressErr = false;

        [NonSerialized]
        public string compressType;

        [NonSerialized]
        public int compressFactor;

        [NonSerialized]
        public int channelCount;
    }

    // Parameters associated with overlay image
    [DataContract]
    public class OverlayImageSpec
    {
        // constructor: Initialize individual channel image object 
        public OverlayImageSpec()
        {
            imageElements = new ImageList();

            // colors for the three channels
            string[] chColors = new string[] { "Red" , "Green" , "Blue" , "Cyan" , "Magenta" , "Yellow" };

            // create 3 image objects, one for each channel 
            foreach (string chColor in chColors)
            {
                ImageSpec spec = new ImageSpec();
                spec.color = chColor;
                imageElements.Add(spec);
            }
        }

        [DataMember(Order = 0)]
        public int field;

        [DataMember(Order = 1)]
        public string format;

        [DataMember(Order = 2)]
        public int thumbPixelSize = 0;

        [NonSerialized]
        public bool invert = false;

        [NonSerialized]
        public bool suppressErr = false;

        [DataMember(Order = 3)]
        public ImageList imageElements;

        //used for input, not output, so not serialized
        [NonSerialized]
        public int timepoint;

        [NonSerialized]
        public int z_index;

        [NonSerialized]
        public string version;

        [NonSerialized]
        public string compressType;

        [NonSerialized]
        public int compressFactor;

        // find the correct color matching a channel name and set image file path
        public void setImageFilePath(string channelName , string path)
        {
            foreach (ImageSpec img in imageElements)
            {
                if (!String.IsNullOrEmpty(img.channel) && img.channel.Equals(channelName))
                {
                    img.imageURI = path;
                    break;
                }
            }
        }
    }

    [DataContract]
    public class PathsModel
    {
        [DataMember]
        public string SiteId { get; set; }

        [DataMember]
        public string DatabaseId { get; set; }

        [DataMember]
        public string[] Paths { get; set; }
    }

    // List of ImageParameter
    [CollectionDataContract(Name = "ImageMetadataArray" , ItemName = "ImageMetadata")]
    public class ImageMetadataList : List<ImageMetadata> { }

    // List of ImageParameter
    [CollectionDataContract(Name = "channelMetadataArray" , ItemName = "Img")]
    public class ChannelMetadataList : List<ChannelMetadata> { }


    // for img list
    [DataContract]
    public class PlateImgMetadata
    {
        [DataMember(Order = 0 , Name = "Barcode")]
        public string Barcode { get; set; }

        [DataMember(Order = 1 , Name = "PlateSize")]
        public string PlateSize { get; set; }

        [DataMember(Order = 2 , Name = "ImagingComputer")]
        public string ImagingComputer { get; set; }

        [DataMember(Order = 3 , Name = "ImageList")]
        public ImageMetadataList ImageList { get; set; }
    }

    [DataContract]
    public class ImageMetadata
    {
        [DataMember(Order = 0 , Name = "RowNumber")]
        public string Row { get; set; }

        [DataMember(Order = 1 , Name = "Column")]
        public string Column { get; set; }

        [DataMember(Order = 2 , Name = "FieldIndex")]
        public string Field { get; set; }

        [DataMember(Order = 3 , Name = "ZIndex")]
        public string ZIndex { get; set; }

        [DataMember(Order = 4 , Name = "TimelineIndex")]
        public string TimelineIndex { get; set; }

        [DataMember(Order = 5 , Name = "ActionListIndex")]
        public string ActionListIndex { get; set; }

        [DataMember(Order = 6 , Name = "Image_PathName")]
        public string PathName { get; set; }

        [DataMember(Order = 7 , Name = "Images")]
        public ChannelMetadataList Channels { get; set; }
    }

    [DataContract]
    public class ChannelMetadata
    {
        [DataMember(Order = 0 , Name = "Channel")]
        public string Channel { get; set; }

        [DataMember(Order = 1 , Name = "Image_Filename")]
        public string FileName { get; set; }
    }

    [DataContract]
    public class MediaData
    {
        [DataMember(Order = 0 , Name = "path")]
        public string Path { get; set; }

        [DataMember(Order = 1 , Name = "compressType")]
        public string CompressType { get; set; }

        [DataMember(Order = 2 , Name = "compressFactor")]
        public int CompressFactor { get; set; }

        [DataMember(Order = 4 , Name = "channel")]
        public string Channel { get; set; }

        [DataMember(Order = 5 , Name = "channelCount")]
        public int ChannelCount { get; set; }

        [DataMember(Order = 6 , Name = "row")]
        public int Row { get; set; }

        [DataMember(Order = 7 , Name = "col")]
        public int Column { get; set; }

        [DataMember(Order = 8 , Name = "field")]
        public int Field { get; set; }

        [DataMember(Order = 9 , Name = "timepoint")]
        public int Timepoint { get; set; }

        [DataMember(Order = 10 , Name = "zindex")]
        public int ZIndex { get; set; }

        [DataMember(Order = 11 , Name = "version")]
        public string Version { get; set; }

        [DataMember(Order = 12 , Name = "frame")]
        public int Frame { get; set; } // FrameIndex is used for OPERA .flex files where each file contains several images - frames.
    }

    // List of ImageParameter
    [CollectionDataContract(Name = "mediaDataArray" , ItemName = "imageMediaData")]
    public class MediaDataList : List<MediaData>
    {
        public MediaDataList() : base()
        {
        }

        public MediaDataList(IEnumerable<MediaData> collection) : base(collection)
        {
        }
    }

    public class FrameFile
    {
        public string ImagePath { get; set; }
        public int Frame { get; set; }
        public string Channel { get; set; }
    }
}
