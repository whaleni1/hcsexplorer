﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Configuration;

namespace HCS.DataAccess.Helpers
{
    public class HCSDBConfigSection : ConfigurationSection
    {
        private static HCSDBConfigSection _dbConfig = null;

        public static HCSDBConfigSection DBConfig
        {
            get
            {
                if (_dbConfig == null)
                {
                    _dbConfig = (HCSDBConfigSection) WebConfigurationManager.GetSection("hcsdbConfigSection");
                }
                return _dbConfig;
            }
        }
        public static bool IsOpera(string dbName)
        {
            string dbClassName = GetTypeName(dbName);

            if (!String.IsNullOrEmpty(dbClassName))
            {
                return dbClassName.EndsWith("OPERA" , StringComparison.InvariantCultureIgnoreCase);
            }
            return false;
        }
        public static bool IsYokogawa(string dbName)
        {
            string dbClassName = GetTypeName(dbName);

            if (!String.IsNullOrEmpty(dbClassName))
            {
                return dbClassName.EndsWith("YOKOGAWA" , StringComparison.InvariantCultureIgnoreCase);
            }
            return false;
        }

        public static bool IsPhenix(string dbName)
        {
            string dbClassName = GetTypeName(dbName);

            if (!String.IsNullOrEmpty(dbClassName))
            {
                return dbClassName.EndsWith("PHENIX", StringComparison.InvariantCultureIgnoreCase);
            }
            return false;
        }
        public static string GetTypeName(string dbName)
        {
            foreach (ConfigSiteElement site in DBConfig.Sites)
            {
                if (site.DataSources[dbName] != null)
                    return site.DataSources[dbName].TypeName;
            }
            return null;

        }
        public static string GetConnectionStringName(string dbName)
        {
            foreach (ConfigSiteElement site in DBConfig.Sites)
            {
                if (site.DataSources[dbName] != null)
                    return site.DataSources[dbName].ConnectionStringName;
            }
            return null;
        }
        public static int GetPixelMultFactor(string dbName)
        {
            foreach (ConfigSiteElement site in DBConfig.Sites)
            {
                if (site.DataSources[dbName] != null)
                    return site.DataSources[dbName].PixelMultFactor;
            }
            return 20;
        }

        public static string GetDescription(string dbName)
        {
            foreach (ConfigSiteElement site in DBConfig.Sites)
            {
                if (site.DataSources[dbName] != null)
                    return site.DataSources[dbName].Description;
            }

            return null;
        }

        /*[ConfigurationProperty("site", IsRequired = true)]
        public ConfigSiteElement Site
        {
            get { return (ConfigSiteElement)this["site"]; }
        }*/
        public ConfigSiteElement Site
        {
            get
            {
                // "Site" property is used by HCSConnect only where HCSDBConfigSection contains only one "site" element
                return Sites.Cast<ConfigSiteElement>().FirstOrDefault();
            }
        }

        [ConfigurationProperty("" , IsRequired = true , IsDefaultCollection = true)]
        public ConfigSitesCollection Sites
        {
            get { return (ConfigSitesCollection) this[""]; }
        }
    }

    [ConfigurationCollection(typeof(ConfigSiteElement) , AddItemName = "site" , CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class ConfigSitesCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ConfigSiteElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ConfigSiteElement) element).Name;
        }

        new public ConfigSiteElement this[string key]
        {
            get
            {
                return (ConfigSiteElement) BaseGet(key);
            }
        }
    }

    public class ConfigSiteElement : ConfigurationElement
    {
        [ConfigurationProperty("" , IsRequired = true , IsDefaultCollection = true)]
        public ConfigDataSourceCollection DataSources
        {
            get { return (ConfigDataSourceCollection) this[""]; }
        }
        [ConfigurationProperty("name" , IsKey = true , IsRequired = true)]
        public string Name
        {
            get { return (string) base["name"]; }
        }
    }

    public class ConfigDataSourceCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ConfigDataSourceElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ConfigDataSourceElement) element).DataSource;
        }

        new public ConfigDataSourceElement this[string key]
        {
            get
            {
                return (ConfigDataSourceElement) BaseGet(key);
            }
        }
    }

    public class ConfigDataSourceElement : ConfigurationElement
    {
        [ConfigurationProperty("datasource" , IsKey = true , IsRequired = true)]
        public string DataSource
        {
            get { return (string) base["datasource"]; }
        }
        [ConfigurationProperty("connectionStringName" , IsKey = false , IsRequired = true)]
        public string ConnectionStringName
        {
            get { return (string) base["connectionStringName"]; }
        }
        [ConfigurationProperty("description" , IsKey = false , IsRequired = true)]
        public string Description
        {
            get { return (string) base["description"]; }
        }
        [ConfigurationProperty("pixelMultFactor" , IsKey = false , IsRequired = true)]
        public int PixelMultFactor
        {
            get { return (int) base["pixelMultFactor"]; }
        }

        [ConfigurationProperty("type" , IsKey = false , IsRequired = true)]
        public string TypeName
        {
            get { return (string) base["type"]; }
        }
    }
}