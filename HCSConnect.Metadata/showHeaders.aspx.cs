﻿using System;
using HCS.Shared.Common;

namespace HCSConnect.Metadata
{
    public partial class ShowHeaders : System.Web.UI.Page
    {
        protected void Page_Load(object sender , EventArgs e)
        {
            try
            {
                System.Collections.Specialized.NameValueCollection coll = Request.Headers;

                // Put the names of all keys into a string array.
                String[] keys = coll.AllKeys;
                for (int keyIndex = 0 ; keyIndex < keys.Length ; keyIndex++)
                {
                    Response.Write("<p>Key: " + keys[keyIndex] + "<br>");
                    // Get all values under this key.
                    String[] values = coll.GetValues(keys[keyIndex]);
                    for (int valueIndex = 0 ; valueIndex < values.Length ; valueIndex++)
                    {
                        Response.Write("Value " + valueIndex + ": " + Server.HtmlEncode(values[valueIndex]) + "<br>");
                    }
                }
            }
            catch (Exception eX)
            {
                throw new ConnectException(GetType().Name + ":Page_Load, " + eX.Message , eX);
            }
        }
    }
}