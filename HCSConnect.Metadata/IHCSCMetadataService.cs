﻿using System.ServiceModel;
using System.ServiceModel.Web;
using HCS.DataAccess;

namespace HCSConnect.Metadata
{
    // NOTE: If you change the interface name "IService1" here, you must also update the reference to "IService1" in Web.config.
    [ServiceContract]
    public interface IHCSCMetadataService
    {
        /// <summary>
        /// GetDatabases
        ///  - Get available databases provided by this service
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/GetDatabases")]
        DBList GetDatabases();

        /// <summary>
        /// getPlates
        ///  - Get plate information
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/getPlate")]
        PlateList getPlates();

        /// <summary>
        /// GetImageParameters
        ///  - Get all image parameters and their values 
        ///    Row and Col ID are optional.If not provided , image parameters for the whole plate will be returned.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/GetImageParameters")]
        ImageParameters GetImageParameters();

        /// <summary>
        /// GetImages
        ///  - Get metadata for each image associated with a well
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/images")]
        ImageCollections GetImages();

        /// <summary>
        /// getCompoundInfo
        ///  - Get compound information (from Helios)
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/getCompound")]
        CompoundList getCompoundInfo();

        /// <summary>
        /// getChannelsByParentPaths
        /// </summary>
        /// <param name="dirs"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/getChannelsByParentPaths" ,
            RequestFormat = WebMessageFormat.Json ,
            ResponseFormat = WebMessageFormat.Json , Method = "POST")]
        ChannelList getChannelsByParentPaths(PathsModel dirs);

        /// <summary>
        /// findPlatesByParentPaths
        ///  - Search for plates by parent paths. Return list of plate IDs.
        /// </summary>
        /// <param name="dirs">Input param - object that contains databaseId and list of paths to be searched</param>
        /// <returns>List of plate IDs.</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/findPlatesByParentPaths" ,
            RequestFormat = WebMessageFormat.Json ,
            ResponseFormat = WebMessageFormat.Json , Method = "POST")]
        string[] findPlatesByParentPaths(PathsModel dirs);

        /// <summary>
        /// GetImageParametersForWholePlate
        ///  - Get image parameters and their values for the whole plate.
        ///    PlateId is required.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/GetImagesByPlate" , ResponseFormat = WebMessageFormat.Json)]
        PlateImgMetadata GetImageParametersForWholePlate();

        /// <summary>
        /// getMediaData
        ///  - Get all image path for Media service 
        ///    Row and Col ID are optional.  If not provided, image parameters for the whole plate will be returned.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/getMediadata")]
        MediaDataList getMediaData();
    }
}
