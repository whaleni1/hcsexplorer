﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;
using log4net.Config;

namespace HCSConnect.Metadata
{

    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in Web.config and in the associated .svc file.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class HCSCMetadataService : IHCSCMetadataService
    {
        /// <summary>
        /// HCSCMetadataService
        ///  - Static constructor. Initialize log4net.
        /// </summary>
        static HCSCMetadataService()
        {
            try
            {
                XmlConfigurator.Configure();
                ConnectLogger.Debug("HCS Connect Service has been started.");
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("HCSCMetadataService" , eX);
                new ConnectException("HCSCMetadataService, " + eX.Message , eX);
            }
        }

        /// <summary>
        /// SetErrorResponse
        /// </summary>
        /// <param name="code"></param>
        /// <param name="description"></param>
        /// <param name="message"></param>
        private void SetErrorResponse(HttpStatusCode code , string description , string message)
        {
            try
            {
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = code;
                response.StatusDescription = description;
                HttpContext.Current.Response.Write(message);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":SetErrorResponse" , eX);
                new ConnectException(this.GetType().Name + ":SetErrorResponse, " + eX.Message , eX);
            }
        }

        /// <summary>
        /// GetDatabases
        ///  - Return information on databases provided by this service
        /// </summary>
        /// <returns></returns>
        public DBList GetDatabases()
        {
            // get all database types
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            DBList dbs = new DBList();

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetDatabase uriMatch = " + uriMatch.RequestUri);

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                dbs = connect.GetDatabases();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetDatabases" , eX);
                new ConnectException(this.GetType().Name + ":GetDatabases, " + eX.Message , eX);
            }
            return dbs;
        }

        /// <summary>
        /// getPlates
        ///  - Return information on plates
        /// </summary>
        /// <returns></returns>
        public PlateList getPlates()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            PlateList plates = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":getPlates, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                plates = connect.GetPlates();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":getPlates" , eX);
                new ConnectException(GetType().Name + ":getPlates, " + eX.Message , eX);
            }

            return plates;
        }

        /// <summary>
        /// GetImageParameters
        ///  - This service returns image metadata associated with a plate well.
        ///    If no well is specified, all available image parameters for the plate will be returned
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageParameters GetImageParameters()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            ImageParameters imageParams = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetImageParameters, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                imageParams = connect.GetImageParameters();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageParameters" , eX);
                new ConnectException(GetType().Name + ":GetImageParameters, " + eX.Message , eX);
            }

            return imageParams;
        }

        /// <summary>
        /// GetImageParametersForWholePlate
        ///  - This service returns all available image parameters for the plate.
        /// </summary>
        /// <returns></returns>
        public PlateImgMetadata GetImageParametersForWholePlate()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            PlateImgMetadata pImgMetadata = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetImageParametersForWholePlate, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                pImgMetadata = connect.GetImageParametersForWholePlate();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageParametersForWholePlate" , eX);
                new ConnectException(GetType().Name + ":GetImageParametersForWholePlate, " + eX.Message , eX);
            }

            return pImgMetadata;
        }

        /// <summary>
        /// GetImages
        ///  - This service returns all matching images and their metadata (including url) associated with a well
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageCollections GetImages()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            ImageCollections imageColls = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetImages, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                imageColls = connect.GetImages();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetImages" , eX);
                new ConnectException(GetType().Name + ":GetImages, " + eX.Message , eX);
            }

            return imageColls;
        }

        /// <summary>
        /// getCompoundInfo
        ///  - Look up experimental metadata (including assay and plate logistics) related to a compound from Helios database
        /// </summary>
        /// <returns></returns>
        public CompoundList getCompoundInfo()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            CompoundList cpdList = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":getCompoundInfo, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                cpdList = connect.GetCompoundInfo();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":getCompoundInfo" , eX);
                new ConnectException(GetType().Name + ":getCompoundInfo, " + eX.Message , eX);
            }

            return cpdList;
        }

        /// <summary>
        /// getChannelsByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public ChannelList getChannelsByParentPaths(PathsModel pathsModel)
        {
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            ChannelList res = new ChannelList();

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":getChannelsByParentPaths, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                res = connect.GetChannelsByParentPaths(pathsModel);
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":getChannelsByParentPaths" , ex);
                new ConnectException(GetType().Name + ":getChannelsByParentPaths, " + ex.Message , ex);
            }
            return res;
        }

        /// <summary>
        /// findPlatesByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public string[] findPlatesByParentPaths(PathsModel pathsModel)
        {
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            string[] uniqueIDs = null;

            ConnectLogger.Info(this.GetType().Name + ":findPlatesByParentPaths started.");

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":findPlatesByParentPaths, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                uniqueIDs = connect.FindExactPlatesByParentPaths(pathsModel);
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":findPlatesByParentPaths" , ex);
                new ConnectException(GetType().Name + ":findPlatesByParentPaths, " + ex.Message , ex);
            }

            return uniqueIDs;
        }

        /// getMediaData
        ///  - This service returns image path for Media Service
        /// </summary>
        /// <returns></returns>
        public MediaDataList getMediaData()
        {
            ConnectLogger.Info(GetType().Name + ":getMediaData started");

            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            MediaDataList res = new MediaDataList();

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":getMediaData, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                res = connect.GetMediaData();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":getMediaData" , eX);
                new ConnectException(GetType().Name + ":getMediaData," + eX.Message , eX);
            }

            return res;
        }
    }
}
