﻿using System;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;
using log4net.Config;

namespace HCSConnect.Service
{
    //// <summary>
    /// HCSWebServices
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class HCSWebServices : IHCSWebServices
    {
        /// <summary>
        /// Static constructor. Initialize log4net.
        /// </summary>
        static HCSWebServices()
        {
            XmlConfigurator.Configure();
            ConnectLogger.Debug("HCS Connect Service has been started.");
        }

        /// <summary>
        /// SetErrorResponse
        /// </summary>
        /// <param name="code"></param>
        /// <param name="description"></param>
        /// <param name="message"></param>
        private void SetErrorResponse(HttpStatusCode code, string description, string message)
        {
            ConnectLogger.Error(GetType().Name + ":SetErrorResponse " + message);

            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = code;
            response.StatusDescription = description;
            HttpContext.Current.Response.Write(message);
        }

        /// <summary>
        /// GetDatabases
        ///  - Return information on databases provided by this service
        /// </summary>
        /// <returns></returns>
        public DBList GetDatabases()
        {
            // get all database types
            //AuthCORS.authHeader(HttpContext.Current.Response);
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            DBList dbs = new DBList();

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetDatabase uriMatch = " + uriMatch.RequestUri);

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                dbs = connect.GetDatabases();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetDatabases", eX);
                new ConnectException(GetType().Name + ":GetDatabases, " + eX.Message, eX);
            }

            return dbs;
        }

        /// <summary>
        /// GetPlates
        /// </summary>
        /// <returns></returns>
        public PlateList GetPlates()
        {
            PlateList plates = null;
            ConnectLogger.Info("HCSC API: getPlates started");

            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetPlates, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                plates = connect.GetPlates();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetPlates" , eX);
                new ConnectException(GetType().Name + ":GetPlates, " + eX.Message, eX);
            }
            return plates;
        }

        /// <summary>
        /// GetImageParameters
        ///  - This service returns image metadata associated with a plate well.
        ///    If no well is specified, all available image parameters for the plate will be returned
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageParameters GetImageParameters()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            ImageParameters imgParams = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetImageParameters, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                imgParams = connect.GetImageParameters();
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":GetImageParameters", ex);
                new ConnectException(GetType().Name + ":GetImageParameters, " + ex.Message, ex);
            }
            return imgParams;
        }

        /// <summary>
        /// GetImages
        ///  - This service returns all matching images and their metadata (including url) associated with a well
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageCollections GetImages()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            ImageCollections imageColls = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetImages, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                imageColls = connect.GetImages();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetImages", eX);
                new ConnectException(GetType().Name + ":GetImages, " + eX.Message, eX);
            }

            return imageColls;
        }

        /// <summary>
        /// GetImageCacheMetadata
        ///  - Return an image object as stream
        /// </summary>
        /// <returns></returns>
        public Stream GetImageCacheMetadata()
        {
            ConnectLogger.Info("Media API: GetImageCacheMetadata with plate metadata caching started");
            Stream imagestream = null;

            try
            {
                // match URL to a template
                UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;

                ConnectLogger.Info(this.GetType().Name + ":GetImageCacheMetadata, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                imagestream = connect.GetImageCacheMetadata();

                // close returned stream when operation is complete
                OperationContext clientContext = OperationContext.Current;
                clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
                {
                    if (imagestream != null)
                    {
                        imagestream.Dispose();
                        imagestream = null;
                    }
                });
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageCacheMetadata", eX);
                new ConnectException(this.GetType().Name + ":GetImageCacheMetadata, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetImage
        ///  - Return an image object as stream
        /// </summary>
        /// <returns></returns> 
        public Stream GetImage()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            Stream imagestream = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetImage, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                imagestream = connect.GetImage();

                // close returned stream when operation is complete
                OperationContext clientContext = OperationContext.Current;
                clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
                {
                    if (imagestream != null)
                    {
                        imagestream.Dispose();
                        imagestream = null;
                    }
                });
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImage , " + eX.Message);
                new ConnectException(this.GetType().Name + ":GetImage, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetImageMedia
        ///  - MEDIA method. No oracle connections. TODO: duplicated logic with GetImage(), refactor.
        ///    Return an image object as stream
        /// </summary>
        /// <returns></returns>
        public Stream GetImageMedia()
        {
            ConnectLogger.Info("Media API: GetImage started");
            Stream imagestream = null;

            try
            {
                // match URL to a template
                UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;

                ConnectLogger.Info(this.GetType().Name + ":GetImageMedia, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                imagestream = connect.GetImageMedia();

                // close returned stream when operation is complete
                OperationContext clientContext = OperationContext.Current;
                clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
                {
                    if (imagestream != null)
                    {
                        imagestream.Dispose();
                        imagestream = null;
                    }
                });
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImage, " + eX.Message);
                new ConnectException(this.GetType().Name + ":GetImage, " + eX.Message, eX);
                return null;
            }

            return imagestream;
        }

        /// <summary>
        /// OverlayImage
        ///  - Merge images from individual channels into one composite image
        /// </summary>
        /// <returns></returns>
        public Stream OverlayImage()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            Stream imagestream = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":OverlayImage, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                imagestream = connect.OverlayImage();

                // close returned stream when operation is complete
                OperationContext clientContext = OperationContext.Current;
                clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
                {
                    if (imagestream != null)
                    {
                        imagestream.Dispose();
                        imagestream = null;
                    }
                });
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":OverlayImage , " + eX.Message);
                new ConnectException(this.GetType().Name + ":OverlayImage, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// OverlayImageMedia
        ///  - MEDIA method. No oracle connections. TODO: duplicated logic with overlayImage(), refactor.
        ///    Merge images from individual channels into one composite image
        /// </summary>
        /// <returns></returns>
        public Stream OverlayImageMedia()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            Stream imagestream = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":OverlayImageMedia, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                imagestream = connect.OverlayImageMedia();

                // close returned stream when operation is complete
                OperationContext clientContext = OperationContext.Current;
                clientContext.OperationCompleted += new EventHandler(delegate (object sender, EventArgs args)
                {
                    if (imagestream != null)
                    {
                        imagestream.Dispose();
                        imagestream = null;
                    }
                });
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":OverlayImageMedia, " + eX.Message);
                new ConnectException(this.GetType().Name + ":OverlayImageMedia, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetCompoundInfo
        ///  - Look up experimental metadata (including assay and plate logistics) related to a compound from Helios database
        /// </summary>
        /// <returns></returns>
        public CompoundList GetCompoundInfo()
        {
            CompoundList cpdList = null;

            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetCompoundInfo, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                cpdList = connect.GetCompoundInfo();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetCompoundInfo, " + eX.Message);
                new ConnectException(this.GetType().Name + ":GetCompoundInfo, " + eX.Message, eX);
            }

            return cpdList;
        }

        /// <summary>
        /// GetChannelsByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public ChannelList GetChannelsByParentPaths(PathsModel pathsModel)
        {
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            ChannelList res = new ChannelList();

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetChannelsByParentPaths, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                res = connect.GetChannelsByParentPaths(pathsModel);
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":GetChannelsByParentPaths" , ex);
                new ConnectException(GetType().Name + ":GetChannelsByParentPaths, " + ex.Message , ex);
            }
            return res;
        }

        /// <summary>
        /// FindPlatesByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public PlateList FindPlatesByParentPaths(PathsModel pathsModel)
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            PlateList plates = null;

            try
            {

                ConnectLogger.Info(this.GetType().Name + ":FindPlatesByParentPaths, URL: " + uriMatch.RequestUri.ToString());

                string strTemp = string.Empty;
                foreach (string str in pathsModel.Paths)
                {
                    strTemp += str + ",";
                }
                ConnectLogger.Info(this.GetType().Name + ":FindPlatesByParentPaths, pathsModel.DatabaseId = " + pathsModel.DatabaseId + ", pathsModel.SiteId = " + pathsModel.SiteId + " " + strTemp);

                ConnectWrapper connect = new ConnectWrapper(uriMatch, HttpRequestHeader.Cookie.ToString());

                plates = connect.FindPlatesByParentPaths(pathsModel);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":FindPlatesByParentPaths, " + eX.Message);
                new ConnectException(this.GetType().Name + ":FindPlatesByParentPaths, " + eX.Message, eX);
            }

            return plates;
        }

        /// <summary>
        /// GetImageParametersForWholePlate
        ///  - This service returns all available image parameters for the plate.
        /// </summary>
        /// <returns></returns>
        public PlateImgMetadata GetImageParametersForWholePlate()
        {
            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            PlateImgMetadata pImgMetadata = null;

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetImageParametersForWholePlate, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                pImgMetadata = connect.GetImageParametersForWholePlate();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageParametersForWholePlate" , eX);
                new ConnectException(GetType().Name + ":GetImageParametersForWholePlate, " + eX.Message , eX);
            }

            return pImgMetadata;
        }

        /// GetMediaData
        ///  - This service returns image path for Media Service
        /// </summary>
        /// <returns></returns>
        public MediaDataList GetMediaData()
        {
            ConnectLogger.Info(GetType().Name + ":GetMediaData started");

            // match URL to a template
            UriTemplateMatch uriMatch = WebOperationContext.Current.IncomingRequest.UriTemplateMatch;
            MediaDataList res = new MediaDataList();

            try
            {
                ConnectLogger.Info(this.GetType().Name + ":GetMediaData, URL: " + uriMatch.RequestUri.ToString());

                ConnectWrapper connect = new ConnectWrapper(uriMatch , HttpRequestHeader.Cookie.ToString());

                res = connect.GetMediaData();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetMediaData" , eX);
                new ConnectException(GetType().Name + ":GetMediaData," + eX.Message , eX);
            }

            return res;
        }
    }
}
