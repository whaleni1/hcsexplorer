﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using HCS.DataAccess;

namespace HCSConnect.Service
{
    // NOTE: If you change the interface name "IService1" here, you must also update the reference to "IService1" in Web.config.
    [ServiceContract]
    public interface IHCSWebServices
    {
        /// <summary>
        /// GetDatabases
        ///  - Get available databases provided by this service
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/GetDatabases")]
        DBList GetDatabases();

        /// <summary>
        /// getPlates
        ///  - Get plate information
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/getPlate")]
        PlateList GetPlates();


        /// <summary>
        /// GetImageParameters
        ///  - Get all image parameters and their values 
        ///   Row and Col ID are optional.If not provided , image parameters for the whole plate will be returned.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/GetImageParameters")]
        ImageParameters GetImageParameters();

        /// <summary>
        /// GetImages
        ///  - Get metadata for each image associated with a well
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/images")]
        ImageCollections GetImages();

        /// <summary>
        /// GetImageCacheMetadata
        ///  - MEDIA method. Retrieve image, cache metadata for whole plate
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetImageCacheMetadata")]
        //[WebCache(CacheProfileName = "CacheImage")]
        Stream GetImageCacheMetadata();

        /// <summary>
        /// GetImageMedia
        ///  - MEDIA method, doesn't use direct oracle connection but requests HCSMetadata service to get required metadata instead.
        ///    Retrieve image
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetImageMedia")]
        //[WebCache(CacheProfileName = "CacheImage")]
        Stream GetImageMedia();

        /// <summary>
        /// GetImage
        ///  - Retrieve image 
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetImage")]
        //[WebCache(CacheProfileName = "CacheImage")]
        Stream GetImage();

        /// <summary>
        /// overlayImageMedia
        ///  - MEDIA method, doesn't use direct oracle connection but requests HCSMetadata service to get required metadata instead.
        ///    Retrieve image with multiple channels
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/overlayImageMedia")]
        //[WebCache(CacheProfileName = "CacheImage")]
        Stream OverlayImageMedia();

        /// <summary>
        /// overlayImage
        ///  - Retrieve image with multiple channels
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/overlayImage")]
        //[WebCache(CacheProfileName = "CacheImage")]
        Stream OverlayImage();

        /// <summary>
        /// getCompoundInfo
        ///  - Get compound information (from Helios)
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/getCompound")]
        CompoundList GetCompoundInfo();

        /// <summary>
        /// getChannelsByParentPaths
        /// </summary>
        /// <param name="dirs"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/getChannelsByParentPaths" ,
            RequestFormat = WebMessageFormat.Json ,
            ResponseFormat = WebMessageFormat.Json , Method = "POST")]
        ChannelList GetChannelsByParentPaths(PathsModel dirs);

        /// <summary>
        /// FindPlatesByParentPaths
        /// </summary>
        /// <param name="dirs"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/findPlatesByParentPaths" ,
            RequestFormat = WebMessageFormat.Json ,
            ResponseFormat = WebMessageFormat.Json , Method = "POST")]
        PlateList FindPlatesByParentPaths(PathsModel dirs);

        /// <summary>
        /// GetImageParametersForWholePlate
        ///  - Get image parameters and their values for the whole plate.
        ///    PlateId is required.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/GetImagesByPlate" , ResponseFormat = WebMessageFormat.Json)]
        PlateImgMetadata GetImageParametersForWholePlate();

        /// <summary>
        /// getMediaData
        ///  - Get all image path for Media service 
        ///    Row and Col ID are optional.  If not provided, image parameters for the whole plate will be returned.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet
            (UriTemplate = "/getMediadata")]
        MediaDataList GetMediaData();
    }
}
