﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HCSConnect.WebAPI.AppMsgLog {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="AppMsgLog.IAppMsgLogService")]
    public interface IAppMsgLogService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAppMsgLogService/WriteMsg", ReplyAction="http://tempuri.org/IAppMsgLogService/WriteMsgResponse")]
        void WriteMsg(string strApplicationName, string strMessage);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IAppMsgLogService/WriteMsg", ReplyAction="http://tempuri.org/IAppMsgLogService/WriteMsgResponse")]
        System.IAsyncResult BeginWriteMsg(string strApplicationName, string strMessage, System.AsyncCallback callback, object asyncState);
        
        void EndWriteMsg(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAppMsgLogService/WriteMsg_NoTimeStamp", ReplyAction="http://tempuri.org/IAppMsgLogService/WriteMsg_NoTimeStampResponse")]
        void WriteMsg_NoTimeStamp(string strApplicationName, string strMessage);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IAppMsgLogService/WriteMsg_NoTimeStamp", ReplyAction="http://tempuri.org/IAppMsgLogService/WriteMsg_NoTimeStampResponse")]
        System.IAsyncResult BeginWriteMsg_NoTimeStamp(string strApplicationName, string strMessage, System.AsyncCallback callback, object asyncState);
        
        void EndWriteMsg_NoTimeStamp(System.IAsyncResult result);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IAppMsgLogServiceChannel : HCSConnect.WebAPI.AppMsgLog.IAppMsgLogService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AppMsgLogServiceClient : System.ServiceModel.ClientBase<HCSConnect.WebAPI.AppMsgLog.IAppMsgLogService>, HCSConnect.WebAPI.AppMsgLog.IAppMsgLogService {
        
        private BeginOperationDelegate onBeginWriteMsgDelegate;
        
        private EndOperationDelegate onEndWriteMsgDelegate;
        
        private System.Threading.SendOrPostCallback onWriteMsgCompletedDelegate;
        
        private BeginOperationDelegate onBeginWriteMsg_NoTimeStampDelegate;
        
        private EndOperationDelegate onEndWriteMsg_NoTimeStampDelegate;
        
        private System.Threading.SendOrPostCallback onWriteMsg_NoTimeStampCompletedDelegate;
        
        public AppMsgLogServiceClient() {
        }
        
        public AppMsgLogServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AppMsgLogServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AppMsgLogServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AppMsgLogServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> WriteMsgCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> WriteMsg_NoTimeStampCompleted;
        
        public void WriteMsg(string strApplicationName, string strMessage) {
            base.Channel.WriteMsg(strApplicationName, strMessage);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginWriteMsg(string strApplicationName, string strMessage, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginWriteMsg(strApplicationName, strMessage, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public void EndWriteMsg(System.IAsyncResult result) {
            base.Channel.EndWriteMsg(result);
        }
        
        private System.IAsyncResult OnBeginWriteMsg(object[] inValues, System.AsyncCallback callback, object asyncState) {
            string strApplicationName = ((string)(inValues[0]));
            string strMessage = ((string)(inValues[1]));
            return this.BeginWriteMsg(strApplicationName, strMessage, callback, asyncState);
        }
        
        private object[] OnEndWriteMsg(System.IAsyncResult result) {
            this.EndWriteMsg(result);
            return null;
        }
        
        private void OnWriteMsgCompleted(object state) {
            if ((this.WriteMsgCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.WriteMsgCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void WriteMsgAsync(string strApplicationName, string strMessage) {
            this.WriteMsgAsync(strApplicationName, strMessage, null);
        }
        
        public void WriteMsgAsync(string strApplicationName, string strMessage, object userState) {
            if ((this.onBeginWriteMsgDelegate == null)) {
                this.onBeginWriteMsgDelegate = new BeginOperationDelegate(this.OnBeginWriteMsg);
            }
            if ((this.onEndWriteMsgDelegate == null)) {
                this.onEndWriteMsgDelegate = new EndOperationDelegate(this.OnEndWriteMsg);
            }
            if ((this.onWriteMsgCompletedDelegate == null)) {
                this.onWriteMsgCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnWriteMsgCompleted);
            }
            base.InvokeAsync(this.onBeginWriteMsgDelegate, new object[] {
                        strApplicationName,
                        strMessage}, this.onEndWriteMsgDelegate, this.onWriteMsgCompletedDelegate, userState);
        }
        
        public void WriteMsg_NoTimeStamp(string strApplicationName, string strMessage) {
            base.Channel.WriteMsg_NoTimeStamp(strApplicationName, strMessage);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginWriteMsg_NoTimeStamp(string strApplicationName, string strMessage, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginWriteMsg_NoTimeStamp(strApplicationName, strMessage, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public void EndWriteMsg_NoTimeStamp(System.IAsyncResult result) {
            base.Channel.EndWriteMsg_NoTimeStamp(result);
        }
        
        private System.IAsyncResult OnBeginWriteMsg_NoTimeStamp(object[] inValues, System.AsyncCallback callback, object asyncState) {
            string strApplicationName = ((string)(inValues[0]));
            string strMessage = ((string)(inValues[1]));
            return this.BeginWriteMsg_NoTimeStamp(strApplicationName, strMessage, callback, asyncState);
        }
        
        private object[] OnEndWriteMsg_NoTimeStamp(System.IAsyncResult result) {
            this.EndWriteMsg_NoTimeStamp(result);
            return null;
        }
        
        private void OnWriteMsg_NoTimeStampCompleted(object state) {
            if ((this.WriteMsg_NoTimeStampCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.WriteMsg_NoTimeStampCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void WriteMsg_NoTimeStampAsync(string strApplicationName, string strMessage) {
            this.WriteMsg_NoTimeStampAsync(strApplicationName, strMessage, null);
        }
        
        public void WriteMsg_NoTimeStampAsync(string strApplicationName, string strMessage, object userState) {
            if ((this.onBeginWriteMsg_NoTimeStampDelegate == null)) {
                this.onBeginWriteMsg_NoTimeStampDelegate = new BeginOperationDelegate(this.OnBeginWriteMsg_NoTimeStamp);
            }
            if ((this.onEndWriteMsg_NoTimeStampDelegate == null)) {
                this.onEndWriteMsg_NoTimeStampDelegate = new EndOperationDelegate(this.OnEndWriteMsg_NoTimeStamp);
            }
            if ((this.onWriteMsg_NoTimeStampCompletedDelegate == null)) {
                this.onWriteMsg_NoTimeStampCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnWriteMsg_NoTimeStampCompleted);
            }
            base.InvokeAsync(this.onBeginWriteMsg_NoTimeStampDelegate, new object[] {
                        strApplicationName,
                        strMessage}, this.onEndWriteMsg_NoTimeStampDelegate, this.onWriteMsg_NoTimeStampCompletedDelegate, userState);
        }
    }
}
