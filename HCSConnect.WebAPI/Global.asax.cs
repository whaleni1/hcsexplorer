﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using HCS.Shared.Logger;
using log4net.Config;

namespace HCSWebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            XmlConfigurator.Configure();
            WebAPILogger.Info("HCS WebAPI Service has been started.");

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_BeginRequest()
        {
  
             //   Response.Headers.Remove("Access-Control-Allow-Origin");
             //   Response.Headers.Add("Access-Control-Allow-Origin",HttpContext.Current.Request.Url.ToString()); 
            
        }
    }
}
