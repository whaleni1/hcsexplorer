﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace HCSWebAPI.Helpers
{
    public static class CookieHelper
    {
        public static string GetVordelCookieNew(HttpRequest request)
        {
            string test = request.Cookies["ObSSOCookie"].Value.ToString();

            HttpCookie myCookie = new HttpCookie("ObSSOCookie");
            myCookie = request.Cookies["ObSSOCookie"];

            return myCookie.ToString();
        }

        public static string GetVordelCookie(HttpRequestMessage request)
        {
            IEnumerable<CookieHeaderValue> cookies = request.Headers.GetCookies("ObSSOCookie");

            if (cookies.Any())
            {
                IEnumerable<CookieState> cookie = cookies.First().Cookies;
                if (cookie.Any())
                {

                    return cookie.First().ToString();
                }
            }

            return null;
        }
    }
}