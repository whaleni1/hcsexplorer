﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using HCS.Shared.Logger;

namespace HCSWebAPI.Helpers
{
    public static class RequestHelper
    {
        public static UriTemplateMatch GetRequest(HttpRequestMessage request)
        {
            Dictionary<string , string> requestParams = request.GetQueryNameValuePairs().ToDictionary(kv => kv.Key , kv => kv.Value , StringComparer.OrdinalIgnoreCase);

            UriTemplate template = new UriTemplate("");
            Uri prefix = request.RequestUri;

            Uri fullUri = request.RequestUri;

            UriTemplateMatch results = template.Match(prefix , fullUri);
            if (requestParams.Count > 0)
                results.Data = requestParams;
            else
                results.Data = null;

            WebAPILogger.Info("RequestHelper:GetRequest, results.Data=" + results.Data);

            return results;
        }

    }
}