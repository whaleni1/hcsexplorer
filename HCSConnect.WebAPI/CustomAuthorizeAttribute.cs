﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using EncryptData;
using HCS.Shared;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using UserAuthLib;

namespace HCSWebAPI
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (AuthorizeRequest(actionContext))
            {
                return;
            }

            HandleUnauthorizedRequest(actionContext);
        }

        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //Code to handle unauthorized request 
        }

        private bool AuthorizeRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            bool authorized = false;
            string user = string.Empty;

            try
            {
#if DEBUG
                user = "hesseer2";
#else
                IEnumerable<string> keys = null;
                if (!actionContext.Request.Headers.TryGetValues("NIBR521" , out keys))
                    return authorized;

                user = keys.First();

                WebAPILogger.Info(GetType().Name + ":AuthorizeRequest, User=" + user);
#endif
                User.UserName = user;
                User.ServerName = System.Environment.MachineName;
                string[] saRoles = null;
                string strRoles = string.Empty;

                if (!string.IsNullOrEmpty(user))
                {
                    if (user.Contains("SYS_CHBS_HCSDATA") || user.Contains("SYS_USCA_HCSDATA"))
                    {
                        authorized = true;
                    }
                    else
                    {
                        UserAuth objUserCtl = new UserAuth();
                        CEncryption objEncrypt = new CEncryption();
                        string strNEDServer = WebConfigurationManager.AppSettings["NEDServer"];
                        int iPortNumber = Convert.ToInt16(WebConfigurationManager.AppSettings["PortNumber"]);
                        string strSysAccount = WebConfigurationManager.AppSettings["SystemAccount"];
                        byte[] bArray = objEncrypt.HexStringToByteArray(WebConfigurationManager.AppSettings["SystemPassword"]);
                        string strSysPassword = objEncrypt.Decrypt(bArray);

                        saRoles = objUserCtl.GetAllUserRoles(user , strNEDServer , iPortNumber , strSysAccount , strSysPassword);

                        foreach (string str in saRoles)
                            strRoles += str + ",";

                        if (!(string.IsNullOrEmpty(strRoles) || string.IsNullOrEmpty(user)))
                        {
                            string[] roles = strRoles.ToUpper().Split(',');
                            authorized = (roles.Contains<string>("HCS:HCS_ADMIN") ||
                                          roles.Contains<string>("HCS:HCS_SUPPORT") ||
                                          roles.Contains<string>("HCS:HCS_USER"));
                        }
                    }
                }
            }
            catch(Exception eX)
            {
                throw new WebAPIException(GetType().Name + ":AuthorizeRequest, " + eX.Message , eX);
            }

            //Write your code here to perform authorization 
            return authorized;
        }
    }
}