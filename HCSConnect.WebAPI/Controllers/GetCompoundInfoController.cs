﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;

namespace HCSWebAPI.Controllers
{
    public class GetCompoundInfoController : ApiController
    {
        [Route("api/getCompound")]
        [EnableCors(origins: "*" , headers: "*" , methods: "*" , SupportsCredentials = true)]
        public CompoundList GetAllCompoundInfo(HttpRequestMessage request)
        {
            CompoundList compounds = null;

            try
            {
                ConnectWrapper serviceMethods = new ConnectWrapper(Helpers.RequestHelper.GetRequest(request) , HttpRequestHeader.Cookie.ToString());

                WebAPILogger.Info(this.GetType().Name + ":GetCompoundInfo");

                compounds = serviceMethods.GetCompoundInfo();
            }
            catch (Exception eX)
            {
                WebAPILogger.Error(this.GetType().Name + ":GetAllCompoundInfo" , eX);
                new WebAPIException(GetType().Name + ":GetAllCompoundInfo, " + eX.Message , eX);
            }

            return compounds;
        }
    }
}
