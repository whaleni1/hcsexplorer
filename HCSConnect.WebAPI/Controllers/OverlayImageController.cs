﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;

namespace HCSWebAPI.Controllers
{
    public class OverlayImageController : ApiController
    {
        [Route("api/overlayImage")]
        [EnableCors(origins: "*" , headers: "*" , methods: "*" , SupportsCredentials = true)]
        public HttpResponseMessage GetAllOverlayImage(HttpRequestMessage request)
        {

            HttpResponseMessage response = null;
            try
            {
                ConnectWrapper serviceMethods = new ConnectWrapper(Helpers.RequestHelper.GetRequest(request) , Helpers.CookieHelper.GetVordelCookie(request));

                WebAPILogger.Info(this.GetType().Name + ":OverlayImage");

                Stream stream = serviceMethods.OverlayImage();
                response = new HttpResponseMessage { Content = new StreamContent(stream) };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
                response.Content.Headers.ContentLength = stream.Length;
            }
            catch (Exception eX)
            {
                WebAPILogger.Error(this.GetType().Name + ":GetAllOverlayImage" , eX);
                new WebAPIException(GetType().Name + ":GetAllOverlayImage, " + eX.Message , eX);
            }

            return response;
        }
    }
}
