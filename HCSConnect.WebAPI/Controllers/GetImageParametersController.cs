﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;

namespace HCSWebAPI.Controllers
{
    public class GetImageParametersController : ApiController
    {
        [Route("api/GetImageParameters")]
        [EnableCors(origins: "*" , headers: "*" , methods: "*" , SupportsCredentials = true)]
        public ImageParameters GetAllImageParameters(HttpRequestMessage request)
        {
            ImageParameters imgParams = null;

            try
            {
                ConnectWrapper serviceMethods = new ConnectWrapper(Helpers.RequestHelper.GetRequest(request) , HttpRequestHeader.Cookie.ToString());

                WebAPILogger.Info(this.GetType().Name + ":GetImageParameters");

                imgParams = serviceMethods.GetImageParameters();
            }
            catch (Exception eX)
            {
                WebAPILogger.Error(this.GetType().Name + ":GetAllImageParameters" , eX);
                new WebAPIException(GetType().Name + ":GetAllImageParameters, " + eX.Message , eX);
            }

            return imgParams;
        }
    }
}
