﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;

namespace HCSWebAPI.Controllers
{
    public class GetImagesController : ApiController
    {
        [Route("api/images")]
        [Route("api/GetImages")]
        [EnableCors(origins: "*" , headers: "*" , methods: "*" , SupportsCredentials = true)]
        public ImageCollections GetAllImage(HttpRequestMessage request)
        {
            ImageCollections imageCollection = null;

            try
            {
                ConnectWrapper serviceMethods = new ConnectWrapper(Helpers.RequestHelper.GetRequest(request) , HttpRequestHeader.Cookie.ToString());

                WebAPILogger.Info(this.GetType().Name + ":GetImages");

                imageCollection = serviceMethods.GetImages();
            }
            catch (Exception eX)
            {
                WebAPILogger.Error(this.GetType().Name + ":GetAllImage" , eX);
                new WebAPIException(GetType().Name + ":GetAllImage, " + eX.Message , eX);
            }

            return imageCollection;
        }
    }
}
