﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;

namespace HCSWebAPI.Controllers
{
    public class TestController : ApiController
    {
        [Route("api/GetImageCacheMetadataTest")]
        [EnableCors(origins: "*" , headers: "*" , methods: "*" , SupportsCredentials = true)]
        public HttpResponseMessage GetAllImageCacheMetadata(HttpRequestMessage request)
        {
            HttpResponseMessage response = null;

            try
            {
                ConnectWrapper serviceMethods = new ConnectWrapper(Helpers.RequestHelper.GetRequest(request) , Helpers.CookieHelper.GetVordelCookie(request));

                WebAPILogger.Info(this.GetType().Name + ":GetImageCacheMetadata");

                Stream stream = serviceMethods.GetImageCacheMetadata();
                response = new HttpResponseMessage { Content = new StreamContent(stream) };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
                response.Content.Headers.ContentLength = stream.Length;
            }
            catch (System.Exception eX)
            {
                WebAPILogger.Error(this.GetType().Name + ":GetAllImageCacheMetadata" , eX);
                new WebAPIException(GetType().Name + ":GetAllImageCacheMetadata, " + eX.Message , eX);
            }

            return response;
        }
    }
}
