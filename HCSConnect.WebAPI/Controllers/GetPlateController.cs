﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;

namespace HCSWebAPI.Controllers
{
    public class GetPlateController : ApiController
    {
        [Route("api/getPlate")]
        [EnableCors(origins: "*" , headers: "*" , methods: "*" , SupportsCredentials = true)]
        public PlateList GetAllPlate(HttpRequestMessage request)
        {
            PlateList plates = null;

            try
            {
                ConnectWrapper serviceMethods = new ConnectWrapper(Helpers.RequestHelper.GetRequest(request) , HttpRequestHeader.Cookie.ToString());

                WebAPILogger.Info(this.GetType().Name + ":GetPlates");

                plates = serviceMethods.GetPlates();
            }
            catch (Exception eX)
            {
                WebAPILogger.Error(this.GetType().Name + ":GetAllPlate" , eX);
                new WebAPIException(GetType().Name + ":GetAllPlate, " + eX.Message , eX);
            }

            return plates;
        }
    }
}
