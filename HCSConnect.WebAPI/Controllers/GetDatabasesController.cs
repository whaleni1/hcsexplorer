﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;

namespace HCSWebAPI.Controllers
{
    public class GetDatabasesController : ApiController
    {
        [Route("api/GetDatabases")]
        [EnableCors(origins: "*" , headers: "*" , methods: "*" , SupportsCredentials = true)]
        public DBList GetAllDatabases(HttpRequestMessage request)
        {
            DBList dbs = null;

            try
            {
                ConnectWrapper serviceMethods = new ConnectWrapper(Helpers.RequestHelper.GetRequest(request) , HttpRequestHeader.Cookie.ToString());

                WebAPILogger.Info(this.GetType().Name + ":GetDatabases");

                dbs = serviceMethods.GetDatabases();
            }
            catch (Exception eX)
            {
                WebAPILogger.Error(this.GetType().Name + ":GetAllDatabases" , eX);
                new WebAPIException(GetType().Name + ":GetAllDatabases, " + eX.Message , eX);
            }

            return dbs;
        }
    }
}
