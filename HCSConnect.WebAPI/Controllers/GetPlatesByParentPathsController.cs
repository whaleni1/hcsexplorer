﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HCS.DataAccess;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library;

namespace HCSWebAPI.Controllers
{
    public class GetPlatesByParentPathsController : ApiController
    {
        [Route("api/findPlatesByParentPaths")]
        [EnableCors(origins: "*" , headers: "*" , methods: "*" , SupportsCredentials = true)]
        public PlateList GetAllPlatesByParentPaths(HttpRequestMessage request , PathsModel dirs)
        {
            PlateList plates = null;

            try
            {
                ConnectWrapper serviceMethods = new ConnectWrapper(Helpers.RequestHelper.GetRequest(request) , HttpRequestHeader.Cookie.ToString());

                WebAPILogger.Info(this.GetType().Name + ":FindPlatesByParentPaths");

                plates = serviceMethods.FindPlatesByParentPaths(dirs);
            }
            catch (Exception eX)
            {
                WebAPILogger.Error(this.GetType().Name + ":GetAllPlatesByParentPaths" , eX);
                new WebAPIException(GetType().Name + ":GetAllPlatesByParentPaths, " + eX.Message , eX);
            }

            return plates;
        }
    }
}
