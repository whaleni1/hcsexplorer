﻿using System;
using System.Collections.Generic;
using System.IO;
using HCS.DataAccess;
using HCS.DataAccess.Databases;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library.Helpers;

namespace HCSConnect.Library
{
    public class Connect
    {
        private HCSDatabaseAccess _objDb = null;
        private string _dbName = string.Empty;

        /// <summary>
        /// GetDatabaseInstance
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public HCSDatabaseAccess GetDatabaseInstance(string dbName)
        {
            if (string.IsNullOrEmpty(_dbName))
            {
                _dbName = dbName;
                _objDb = HCSDatabaseAccess.GetDatabaseInstance(dbName);
            }

            return _objDb;
        }

        /// <summary>
        /// GetDatabases
        ///  - Return information on databases provided by this service
        /// </summary>
        /// <returns></returns>
        public DBList GetDatabases()
        {
            var dbs = new DBList();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetDatabases");

                foreach (ConfigSiteElement csite in HCSDBConfigSection.DBConfig.Sites)
                {
                    foreach (ConfigDataSourceElement cdb in csite.DataSources)
                    {
                        // instantiate database object
                        HCSDatabase db = new HCSDatabase();
                        db.id = cdb.DataSource;
                        db.description = cdb.Description;
                        dbs.Add(db);
                    }
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetDatabases, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetDatabases, " + eX.Message , eX);
            }

            return dbs;
        }

        /// <summary>
        /// GetDatabases
        ///  - Return information on databases provided by this service
        /// </summary>
        /// <param name="strSite"></param>
        /// <returns></returns>
        public DBList GetDatabases(string strSite)
        {
            var dbs = new DBList();

            try
            {
                foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Sites[strSite].DataSources)
                {
                    // instantiate database object
                    HCSDatabase db = new HCSDatabase();
                    db.id = cdb.DataSource;
                    db.description = cdb.Description;
                    dbs.Add(db);
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetDatabases, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetDatabases, " + eX.Message , eX);
            }

            return dbs;
        }

        /// <summary>
        /// FindDatabase
        /// - Return the database id where the plate is found.
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public string FindDatabase(Plate plate)
        {
            var dbName = String.Empty;

            try
            {
                foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Site.DataSources)
                {
                    PlateList plates = GetPlates(cdb.DataSource , plate);
                    
                    // exit loop if the plate info is found from this database
                    if (plates != null && plates.Count > 0)
                    {
                        dbName = cdb.DataSource;
                        break;
                    }
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":FindDatabase, " + eX.Message);
                throw new ConnectException(GetType().Name + ":FindDatabase, " + eX.Message , eX);
            }

            return dbName;
        }

        /// <summary>
        /// GetPlates
        /// </summary>
        /// <returns></returns>
        public PlateList GetPlates(Plate plate)
        {
            var plates = new PlateList();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlates, No db specified");

                foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Site.DataSources)
                {
                    plates = GetPlates(cdb.DataSource , plate);

                    // exit loop if plate info is found from this database
                    if (plates != null && plates.Count > 0)
                    {
                        break;
                    }
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetPlates, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetPlates, " + eX.Message , eX);
            }
            return plates;
        }

        /// <summary>
        /// GetPlates
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="plate"></param>
        /// <returns></returns>
        public PlateList GetPlates(string dbName , Plate plate)
        {
            PlateList plates = new PlateList();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlates, dbName=" + dbName + ", plate.plateId" + plate.plateId + ", plate" + plate.barcode);

                var dbObj = GetDatabaseInstance(dbName);
                plates = dbObj.GetPlateInfo(plate);

                ConnectLogger.Info(GetType().Name + ":GetPlates, Num plates found = " + plates.Count);
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":GetPlates, " + ex.Message);
                throw new ConnectException(GetType().Name + ":GetPlates, " + ex.Message , ex);
            }

            return plates;
        }

        /// <summary>
        /// GetPlatesByParentPath
        /// </summary>
        /// <param name="plate"></param>
        /// <returns></returns>
        public PlateList GetPlatesByParentPath(Plate plate)
        {
            var plates = new PlateList();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlatesByParentPath, No db specified");

                foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Site.DataSources)
                {
                    plates = GetPlatesByParentPath(cdb.DataSource , plate);

                    // exit loop if image parameter is found from this database
                    if (plates != null && plates.Count > 0)
                    {
                        break;
                    }
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetPlatesByParentPath, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetPlatesByParentPath, " + eX.Message , eX);
            }

            return plates;
        }

        /// <summary>
        /// GetPlatesByParentPath
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="plate"></param>
        /// <returns></returns>
        public PlateList GetPlatesByParentPath(string dbName , Plate plate)
        {
            var plates = new PlateList();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetPlatesByParentPath");

                var dbObj = GetDatabaseInstance(dbName);
                plates = dbObj.GetPlateInfoByParentPath(plate);

                ConnectLogger.Info(GetType().Name + ":GetPlatesByParentPath, Num plates found = " + plates.Count);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetPlatesByParentPath, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetPlatesByParentPath, " + eX.Message , eX);
            }

            return plates;
        }

        /// <summary>
        /// FindExactPlate
        ///  - get plate info by parent path. Used only by CPCP Wrapper
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="plate"></param>
        /// <returns></returns>
        public PlateList FindExactPlate(string dbName , Plate plate)
        {
            var plates = new PlateList();

            try
            {
                ConnectLogger.Info(GetType().Name + ":FindExactPlate");

                var dbObj = GetDatabaseInstance(dbName);
                plates = dbObj.FindExactPlateInfo(plate);
                if (plates == null || plates.Count == 0)
                {
                    plates = dbObj.FindExactPlateInfo(plate , true);
                }

                ConnectLogger.Info(GetType().Name + ":FindExactPlate, Num plates found = " + plates.Count);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":FindExactPlate, " + eX.Message);
                throw new ConnectException(GetType().Name + ":FindExactPlate, " + eX.Message , eX);
            }

            return plates;
        }

        /// <summary>
        /// GetImageParametersForWholePlate
        ///  - This service returns all available image parameters for the plate.
        /// </summary>
        /// <param name="plate">Plate object containing plate details to query for in all databases</param>
        /// <returns></returns>
        public PlateImgMetadata GetImageParametersForWholePlate(Plate plate)
        {
            var pImgMetadata = new PlateImgMetadata();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImageParametersForWholePlate");

                foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Site.DataSources)
                {
                    pImgMetadata = GetImageParametersForWholePlate(cdb.DataSource , plate);

                    // exit loop if plate image meta data is found from this database
                    if (pImgMetadata != null)
                    {
                        break;
                    }
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageParametersForWholePlate, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetImageParametersForWholePlate, " + eX.Message , eX);
            }

            return pImgMetadata;
        }

        /// <summary>
        /// GetImageParametersForWholePlate
        ///  - This service returns all available image parameters for the plate.
        /// </summary>
        /// <param name="dbName">Database identifier of the database to query</param>
        /// <param name="plate">Plate object containing plate details to query for</param>
        /// <returns></returns>
        public PlateImgMetadata GetImageParametersForWholePlate(string dbName , Plate plate)
        {
            var pImgMetadata = new PlateImgMetadata();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImageParametersForWholePlate");

                // invoke the subclass corresponding to db name
                var dbObj = GetDatabaseInstance(dbName);
                pImgMetadata = dbObj.QueryImageParametersForWholePlate(plate);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageParametersForWholePlate, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetImageParametersForWholePlate, " + eX.Message , eX);
            }

            return pImgMetadata;
        }

        /// <summary>
        /// GetImageParameters
        ///  - This service returns image metadata associated with a plate well.
        ///    If no well is specified, all available image parameters for the plate will be returned
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageParameters GetImageParameters(Plate plate , int row , int col)
        {
            var imgParams = new ImageParameters();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImageParameters");

                foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Site.DataSources)
                {
                    imgParams = GetImageParameters(cdb.DataSource , plate , row , col);

                    // exit loop if image parameter is found from this database
                    if (imgParams != null && imgParams.Count > 0)
                    {
                        break;
                    }
                }

                ConnectLogger.Info(GetType().Name + ":GetImageParameters, Num ImageParameters found = " + imgParams.Count);
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":GetImageParameters, " + ex.Message);
                throw new ConnectException(GetType().Name + ":GetImageParameters, " + ex.Message , ex);
            }
            return imgParams;
        }

        /// <summary>
        /// GetImageParameters
        ///  - get ImageParameters with given database name
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="pl"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public ImageParameters GetImageParameters(string dbName , Plate pl , int row , int col)
        {
            var imgParams = new ImageParameters();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImageParameters");

                // invoke the subclass corresponding to db name
                var dbObj = GetDatabaseInstance(dbName);
                imgParams = dbObj.QueryImageParameters(pl , row , col);

                foreach (ImageParameter p in imgParams)
                    p.SortChannels();

                ConnectLogger.Info(GetType().Name + ":GetImageParameters, Num ImageParameters found = " + imgParams.Count);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageParameters, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetImageParameters, " + eX.Message , eX);
            }

            return imgParams;
        }

        /// <summary>
        /// GetImages
        ///  - This service returns all matching images and their metadata (including url) associated with a well
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageCollections GetImages(Plate pl , int row , int col , int fld , string ch , ImageSpec inImageSpec , string baseUri)
        {
            var imageColls = new ImageCollections();

            try
            {
                foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Site.DataSources)
                {
                    imageColls = GetImages(cdb.DataSource, pl , row , col , fld , ch , inImageSpec , baseUri);

                    // exit loop if images are found from this database
                    if (imageColls != null && imageColls.Count > 0)
                    {
                        break;
                    }
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetImages, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetImages, " + eX.Message , eX);
            }

            return imageColls;
        }

        /// <summary>
        /// GetImages
        ///  - This service returns all matching images and their metadata (including url) associated with a well
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageCollections GetImages(string dbName , Plate pl , int row , int col , int fld , string ch , ImageSpec inImageSpec , string baseUri)
        {
            var imageColls = new ImageCollections();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImages");
                ImageParameters imageParams = new ImageParameters();

                // check if db name is specified as part of the URL
                if (String.IsNullOrEmpty(dbName))
                {
                    foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Site.DataSources)
                    {
                        imageParams = GetImageParameters(cdb.DataSource , pl , row , col);

                        // exit loop if image parameter is found from this database
                        if (imageParams != null && imageParams.Count > 0)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    // search specified database directly
                    imageParams = GetImageParameters(dbName , pl , row , col);
                }

                // format ImageParameters into ImageCollections
                foreach (ImageParameter imageParam in imageParams)
                {
                    // create new ImageCollection
                    ImageCollection imageColl = new ImageCollection();

                    // set db name
                    imageColl.database = imageParam.database;

                    // set plate data
                    imageColl.plateData = imageParam.plateData;

                    // set well data
                    imageColl.wellData = imageParam.wellData;

                    // check if field is part of the URL parameters
                    if (fld >= 0)
                    {
                        // replace field list with the given value if it exists
                        if (imageParam.fields.IndexOf(fld) != -1)
                        {
                            FieldList fields = new FieldList();
                            fields.Add(fld);
                            imageParam.fields = fields;
                        }
                    }

                    // check if channel is part of the URL parameters
                    if (!String.IsNullOrEmpty(ch))
                    {
                        // replace channel list with the given value if it exists
                        if (imageParam.channels.IndexOf(ch) != -1)
                        {
                            ChannelList channels = new ChannelList();
                            channels.Add(ch);
                            imageParam.channels = channels;
                        }
                    }

                    // create ImageSpec including URL for each matching image for this plate
                    foreach (int f in imageParam.fields)
                    {
                        foreach (string c in imageParam.channels)
                        {
                            var spec = new ImageSpec();
                            spec.field = f;
                            spec.channel = c;
                            var timepoint = inImageSpec.timepoint;
                            var zIndex = inImageSpec.z_index;
                            var version = inImageSpec.version;
                            spec.imageURI = baseUri + "/GetImage?db=" + imageParam.database + "&plateId=" +
                                            imageParam.plateData.plateId + "&row=" + imageParam.wellData.row +
                                            "&col=" + imageParam.wellData.col + "&field=" + f + "&channel=" + c +
                                            "&timepoint=" + timepoint + "&z_index=" + zIndex + "&version=" + version;
                            imageColl.images.Add(spec);
                        }
                    }

                    // add ImageCollection object to ImageCollections list
                    imageColls.Add(imageColl);
                }

                ConnectLogger.Info(GetType().Name + ":GetImages, Num Images found = " + imageColls.Count);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetImages, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetImages, " + eX.Message , eX);
            }

            return imageColls;
        }

        /// <summary>
        /// GetImageCacheMetadata
        ///  - Return an image object as stream
        /// </summary>
        /// <returns></returns>
        public Stream GetImageCacheMetadata(string dbName , Plate plate , int row , int col , ImageSpec spec)
        {
            Stream imagestream = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImageCacheMetadata");

                // create well data
                Well w = new Well();
                w.row = row;
                w.col = col;

                MediaData md = null;
                List<MediaData> list = new List<MediaData>();

                list = CacheHelper.GetMediaData(this , dbName , plate.plateId , row , col , spec);
                if (list != null && list.Count > 0)
                {
                    ConnectLogger.Info(GetType().Name + ":GetImageCacheMetadata list.count=" + list.Count);
                    foreach (MediaData mdata in list)
                    {
                        if(mdata.Row == row && mdata.Column == col && spec.field == mdata.Field && spec.z_index == mdata.ZIndex && spec.timepoint == mdata.Timepoint && spec.channel == mdata.Channel)
                        {
                            md = mdata;
                            break;
                        }
                    }
                    if(md == null)
                    {
                        foreach (MediaData mdata in list)
                        {
                            if (mdata.Row == row && mdata.Column == col && spec.field == mdata.Field && spec.channel == mdata.Channel)
                            {
                                md = mdata;
                                break;
                            }
                        }
                    }
                    if (md == null)
                    {
                        foreach (MediaData mdata in list)
                        {
                            if (mdata.Row == row && mdata.Column == col)
                            {
                                md = mdata;
                                break;
                            }
                        }
                    }
                }
                else
                    throw new ImageNotFoundException();

                // Get the plate data
                PlateList plates = GetPlates(dbName , plate);
                plate = plates[0];
         
                spec.compressFactor = md.CompressFactor;
                spec.compressType = md.CompressType;
                spec.channelCount = md.ChannelCount;
                imagestream = ImageHelper.GetImageStream(dbName , plate , spec , md.Path , HCSDBConfigSection.GetPixelMultFactor(dbName) , md.Frame);

                ConnectLogger.Info(GetType().Name + ":GetImageCacheMetadata, Stream length = " + imagestream.Length);
            }
            catch (ImageNotFoundException eX)
            {
                throw eX;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageCacheMetadata, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":GetImageCacheMetadata, " + eX.Message , eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetImage
        ///  - Return an image object as stream
        /// </summary>
        /// <returns></returns> 
        public Stream GetImage(string dbName , Plate plate , int row , int col , ImageSpec spec , string cacheKey)
        {
            Stream imagestream = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImage");

                if (String.IsNullOrEmpty(plate.plateId))
                {
                    // barcode needs to be resolved to a unique plate id
                    ImageParameters imageParams = GetImageParameters(dbName , plate , 0 , 0);

                    // get the plate id for the first match 
                    if (imageParams.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        plate.plateId = imageParams[0].plateData.plateId;
                    }
                }

                // create well data
                Well w = new Well();
                w.row = row;
                w.col = col;

                HCSDatabaseAccess obj = null;

                obj = GetDatabaseInstance(dbName);
                FrameFile fn = null;

                fn = obj.PrepareForImageStream(plate , w , spec , cacheKey);

                // Get the plate data
                PlateList plates = GetPlates(dbName , plate);
                plate = plates[0];

                imagestream = ImageHelper.GetImageStream(dbName , plate , spec , fn.ImagePath ,
                    obj.getPixelMultFactor() , fn.Frame);

                ConnectLogger.Info(GetType().Name + ":GetImage, Stream length = " + imagestream.Length);
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {

                ConnectLogger.Error(this.GetType().Name + ":GetImage, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":GetImage, " + eX.Message , eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetImageMedia
        ///  - MEDIA method. No oracle connections. TODO: duplicated logic with GetImage(), refactor.
        ///    Return an image object as stream
        /// </summary>
        /// <returns></returns>
        public Stream GetImageMedia(string dbName , Plate plate , int row , int col , ImageSpec spec)
        {
            Stream imagestream = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetImageMedia");

                if (String.IsNullOrEmpty(plate.plateId))
                {
                    PlateList plates = GetPlates(dbName , plate);
                    int id = Convert.ToInt32(plates[0].plateId);

                    // get the plate id for the first match 
                    if (id == 0)
                    {
                        ConnectLogger.Debug("id = 0, error");
                        return null;
                    }
                    else
                    {
                        plate = plates[0];
                    }
                }

                // create well data
                Well w = new Well();
                w.row = row;
                w.col = col;

                MediaData md = null;

                List<MediaData> list = CacheHelper.GetWellMediaData(this , dbName , plate.plateId , row , col , spec);
                if (list != null && list.Count > 0)
                    md = list[0];
                else
                    throw new ImageNotFoundException();

                spec.compressFactor = md.CompressFactor;
                spec.compressType = md.CompressType;
                spec.channelCount = md.ChannelCount;
                imagestream = ImageHelper.GetImageStream(dbName , plate , spec , md.Path , HCSDBConfigSection.GetPixelMultFactor(dbName) , md.Frame);

                ConnectLogger.Info(GetType().Name + ":GetImageMedia, Stream length = " + imagestream.Length);
            }
            catch (ImageNotFoundException eX)
            {
                throw eX;
            }
            catch (ConnectException eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageMedia, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":GetImageMedia, " + eX.Message , eX);
            }

            return imagestream;
        }

        /// <summary>
        /// OverlayImage
        ///  - Merge images from individual channels into one composite image
        /// </summary>
        /// <returns></returns>
        public Stream OverlayImage(string dbName , Plate plate , Well w , OverlayImageSpec spec , string cacheKey)
        {
            Stream imagestream = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":OverlayImage, ");

                // assign plate id
                if (String.IsNullOrEmpty(plate.plateId))
                {
                    ImageParameters imageParams = GetImageParameters(dbName , plate , 0 , 0);

                    // get the plate id for the first match 
                    if (imageParams.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        plate = imageParams[0].plateData;
                    }
                }

                var dbObj = GetDatabaseInstance(dbName);

                if (string.IsNullOrEmpty(spec.format))
                {
                    spec.format = dbObj.GetDefaultImageFormat();
                }

                // Get the plate data
                PlateList plates = dbObj.GetPlateInfo(plate);
                plate = plates[0];

                imagestream = CacheHelper.GetStreamFromCache(cacheKey , spec.format);
                if (imagestream == null)
                {
                    Dictionary<string , FrameFile> fns = dbObj.PrepareForOverlayImageStream(plate , w , spec , cacheKey);
                    imagestream = ImageHelper.GetOverlayImageStream(dbName , plate , w , spec , fns , dbObj.getPixelMultFactor());
                }

                ConnectLogger.Info(GetType().Name + ":OverlayImage, Stream length = " + imagestream.Length);
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":OverlayImage, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":OverlayImage, " + eX.Message , eX);
            }

            return imagestream;
        }

        /// <summary>
        /// OverlayImageMedia
        ///  - MEDIA method. No oracle connections. TODO: duplicated logic with OverlayImage(), refactor.
        ///    Merge images from individual channels into one composite image
        /// </summary>
        /// <returns></returns>
        public Stream OverlayImageMedia(string dbName , Plate plate , int row , int col , OverlayImageSpec spec , ImageSpec specimg , string cacheKey)
        {
            Stream imagestream = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":OverlayImageMedia");

                // assign plate id
                if (String.IsNullOrEmpty(plate.plateId))
                {
                    ImageParameters imageParams = GetImageParameters(dbName , plate , 0 , 0);

                    // get the plate id for the first match 
                    if (imageParams.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        plate = imageParams[0].plateData;
                    }
                }

                // create well data
                Well w = new Well();
                w.row = row;
                w.col = col;

                imagestream = CacheHelper.GetStreamFromCache(cacheKey , spec.format);

                if (imagestream == null)
                {
                    List<MediaData> list = CacheHelper.GetMediaData(this , dbName , plate.plateId , row , col , specimg);
                    Dictionary<string , FrameFile> fns = new Dictionary<string , FrameFile>();

                    if (!HCSDBConfigSection.IsOpera(dbName) || HCSDBConfigSection.IsYokogawa(dbName) || HCSDBConfigSection.IsPhenix(dbName))
                    {
                        if ("OPERA_2".Equals(dbName))
                        {
                            string imgPath = list[0].Path;
                            if (!File.Exists(imgPath))
                            {
                                ConnectLogger.Error("File not found " + imgPath);
                                throw new FileNotFoundException("File not found " , imgPath);
                            }
                        }
                        if ("YOKOGAWA_2".Equals(dbName) || "YOKOGAWA_1".Equals(dbName) || "YOKOGAWA_3".Equals(dbName) || "PHENIX_1".Equals(dbName))
                        {
                            string imgPath = list[0].Path;
                            if (!File.Exists(imgPath))
                            {
                                ConnectLogger.Error("File not found " + imgPath);
                                throw new FileNotFoundException("File not found " , imgPath);
                            }
                        }
                        foreach (MediaData md in list)
                        {
                            if (!fns.ContainsKey(md.Channel))
                                fns[md.Channel] = new FrameFile { ImagePath = md.Path , Frame = md.Frame }; // currently "Frame" field is required only for the OPERA_2 (generic tables)
                        }
                    }
                    else
                    {
                        MediaData md = list[0];
                        string imageFilePath = md.Path;
                        if (!File.Exists(imageFilePath))
                        {
                            ConnectLogger.Error("File not found " + imageFilePath);
                            throw new FileNotFoundException("File not found " , imageFilePath);
                        }

                        // associate each channel with the path to the image file
                        for (int c = 1 ; c <= md.ChannelCount ; c++)
                        {
                            string chName = ImageHelper.CHANNEL_NAME_PREFIX + c.ToString();
                            fns[chName] = new FrameFile { ImagePath = imageFilePath };
                        }

                        spec.compressType = md.CompressType;
                        spec.compressFactor = md.CompressFactor;
                    }

                    // Get the plate data
                    PlateList plates = GetPlates(dbName , plate);
                    plate = plates[0];

                    imagestream = ImageHelper.GetOverlayImageStream(dbName , plate , w , spec , fns , HCSDBConfigSection.GetPixelMultFactor(dbName));
                }

                ConnectLogger.Info(GetType().Name + ":OverlayImageMedia, Stream length = " + imagestream.Length);
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":OverlayImageMedia, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":OverlayImageMedia, " + eX.Message , eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetCompoundInfo
        ///  - Look up experimental metadata (including assay and plate logistics) related to a compound from Helios database
        /// </summary>
        /// <returns></returns>
        public CompoundList GetCompoundInfo(string compoundId , string srcStr)
        {
            var cpdList = new CompoundList();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetCompoundInfo");

                Helio.DATA_SOURCE srcValue = (Helio.DATA_SOURCE) Enum.Parse(typeof(Helio.DATA_SOURCE) , srcStr);
                Helio cpdSearch = new Helio();
                cpdList = cpdSearch.DoSearch(compoundId , srcValue);

                ConnectLogger.Info(GetType().Name + ":GetCompoundInfo, Num Compounds found = " + cpdList.Count);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetCompoundInfo, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetCompoundInfo , " + eX.Message , eX);
            }

            return cpdList;
        }

        /// <summary>
        /// GetChannelsByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public ChannelList GetChannelsByParentPaths(PathsModel pathsModel)
        {
            var res = new ChannelList();

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetChannelsByParentPaths, No db specified");

                string dbId = pathsModel.DatabaseId;

                bool plateFound = false;
                foreach (string path in pathsModel.Paths)
                {
                    PlateList plates = GetPlatesByParentPath(dbId , new Plate { path = path });

                    if (plates != null)
                    {
                        foreach (Plate p in plates)
                        {
                            Plate pl = new Plate { plateId = p.plateId };
                            ImageParameters ip = GetImageParameters(dbId , pl , 0 , 0);
                            if (ip.Count > 0)
                            {
                                res = ip[0].channels;
                                plateFound = true;
                                break;
                            }
                        }
                    }
                    if (plateFound)
                        break;
                }

                ConnectLogger.Info(GetType().Name + ":GetChannelsByParentPaths, Num Channels found = " + res.Count);
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":GetChannelsByParentPaths, " + ex.Message);
                throw new ConnectException(GetType().Name + ":GetChannelsByParentPaths, " + ex.Message , ex);
            }
            return res;
        }

        /// <summary>
        /// FindPlatesByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public PlateList FindPlatesByParentPaths(PathsModel pathsModel)
        {
            PlateList plateList = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":FindPlatesByParentPaths");

                List<string> uniqueIDs = new List<string>();

                string dbId = pathsModel.DatabaseId;
                plateList = new PlateList();

                foreach (string path in pathsModel.Paths)
                {
                    PlateList plates = GetPlates(dbId , new Plate { path = path });
                    if (plates != null && plates.Count > 0)
                    {
                        foreach (Plate pl in plates)
                        {
                            string plateId = pl.plateId;
                            if (!uniqueIDs.Contains(plateId))
                            {
                                uniqueIDs.Add(plateId);
                                plateList.Add(pl);
                            }
                        }
                    }
                }

                ConnectLogger.Info(GetType().Name + ":FindPlatesByParentPaths, Num Plates found = " + plateList.Count);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":FindPlatesByParentPaths, " + eX.Message);
                throw new ConnectException(GetType().Name + ":FindPlatesByParentPaths, " + eX.Message , eX);
            }

            return plateList;
        }

        /// <summary>
        /// findPlatesByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public string[] FindExactPlatesByParentPaths(PathsModel pathsModel)
        {
            string[] uniqueIDs = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":FindExactPlatesByParentPaths");

                List<string> uniqueIDsList = new List<string>();
                string dbId = pathsModel.DatabaseId;

                foreach (string path in pathsModel.Paths)
                {
                    PlateList plates = FindExactPlate(dbId , new Plate { path = path.ToUpper() });
                    if (plates != null && plates.Count > 0)
                    {
                        foreach (Plate pl in plates)
                        {
                            string plateId = pl.plateId;
                            if (!uniqueIDsList.Contains(plateId))
                            {
                                uniqueIDsList.Add(plateId);
                            }
                        }
                    }
                }

                uniqueIDs = uniqueIDsList.ToArray();

                ConnectLogger.Info(GetType().Name + ":findPlatesByParentPaths, Num Plates found = " + uniqueIDs.Length);
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":findPlatesByParentPaths, " + ex.Message);
                throw new ConnectException(GetType().Name + ":findPlatesByParentPaths, " + ex.Message , ex);
            }

            return uniqueIDs;
        }

        /// GetMediaData
        ///  - This service returns image path for Media Service
        /// </summary>
        /// <returns></returns>
        public MediaDataList GetMediaData(Plate plate , int row , int col , ImageSpec spec)
        {
            MediaDataList mediaDataList = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetMediaData");
                foreach (ConfigDataSourceElement cdb in HCSDBConfigSection.DBConfig.Site.DataSources)
                {
                    mediaDataList = GetMediaData(cdb.DataSource , plate , row , col , spec);

                    // exit loop if image parameter is found from this database
                    if (mediaDataList != null && mediaDataList.Count > 0)
                    {
                        break;
                    }
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetMediaData, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetMediaData," + eX.Message , eX);
            }

            return mediaDataList;
        }

        /// GetMediaData
        ///  - This service returns image path for Media Service
        /// </summary>
        /// <returns></returns>
        public MediaDataList GetMediaData(string dbName , Plate plate , int row , int col , ImageSpec spec)
        {
            MediaDataList mediaDataList = null;

            try
            {
                ConnectLogger.Info(GetType().Name + ":GetMediaData");

                // invoke the subclass corresponding to db name
                var dbObj = GetDatabaseInstance(dbName);

                if (String.IsNullOrEmpty(plate.plateId))
                {
                    // barcode needs to be resolved to a unique plate id
                    PlateList plates = plates = dbObj.GetPlateInfo(plate);

                    if (plates == null || plates.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        plate.plateId = plates[0].plateId;
                    }
                }

                mediaDataList = dbObj.QueryImagePath(plate.plateId , row , col , spec);

                ConnectLogger.Info(GetType().Name + ":GetMediaData, Num items found = " + mediaDataList.Count);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetMediaData, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetMediaData," + eX.Message , eX);
            }

            return mediaDataList;
        }
    }
}
