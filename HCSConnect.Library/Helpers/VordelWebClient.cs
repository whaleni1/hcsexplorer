﻿using System;
using System.Net;

namespace HCSConnect.Library.Helpers
{
    public class VordelWebClient : WebClient
    {
        public int Timeout { get; set; }

        public string Cookie
        {
            get
            {
                return this.Headers[HttpRequestHeader.Cookie];
            }
            set
            {
                this.Headers.Remove(HttpRequestHeader.Cookie);
                if (!string.IsNullOrEmpty(value))
                    this.Headers.Add(HttpRequestHeader.Cookie , value);
            }
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var objWebRequest = base.GetWebRequest(address);
            if (Timeout > 0)
                objWebRequest.Timeout = Timeout;

            return objWebRequest;
        }
    }
}
