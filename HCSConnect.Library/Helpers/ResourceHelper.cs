﻿using System;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace HCSConnect.Library.Helpers
{
    public static class ResourceHelper
    {
        public static string IMAGE_MAGICK_CMD = WebConfigurationManager.AppSettings["IMAGE_MAGICK_CMD"];

        // Java is used to execute BioFormats program to convert Cellomics C01 files
        public static string JAVA_PATH = WebConfigurationManager.AppSettings["JAVA_PATH"];//C:/Program Files/Java/jre1.8.0_111/bin/java.exe
        public static string BIOFORMATS_PATH = WebConfigurationManager.AppSettings["BIOFORMATS_PATH"];//"C:/Program Files (x86)/bftools/loci_tools.jar";

        //Installed from Opera DVD to convert/decompress some Opera files
        public static string LURAWAVE_CONVERTER = WebConfigurationManager.AppSettings["LURAWAVE_CONVERTER"];//"C:/Program Files/PerkinElmer/ImageView 1.1/Bin/ImageConv.exe";

        /// <summary>
        /// CACHE_DIRECTORY
        /// </summary>
        private static string CACHE_DIRECTORY
        {
            get
            {
                string appPath = string.Empty;
                try
                {
                    appPath = HttpContext.Current.Request.PhysicalApplicationPath;
                }

                catch
                {
                    appPath = System.AppDomain.CurrentDomain.BaseDirectory;
                }
                if (!appPath.EndsWith("\\"))
                {
                    appPath += "\\";
                }

                return appPath + WebConfigurationManager.AppSettings["CACHE_DIRECTORY"] + "\\";
            }
        }

        /// <summary>
        /// GetLocalFileDirectory
        /// </summary>
        /// <returns></returns>
        public static string GetLocalFileDirectory()
        {
            return CACHE_DIRECTORY;
        }

        /// <summary>
        /// GetLocalFilePath
        /// </summary>
        /// <param name="key"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static string GetLocalFilePath(string key , string extension)
        {
            return GetLocalFilePath(extension);
        }

        /// <summary>
        /// GetLocalFilePath
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static string GetLocalFilePath(string extension)
        {
            string path = null;
            do
            {
                path = CACHE_DIRECTORY + Guid.NewGuid().ToString() + "." + extension;
            } while (File.Exists(path));

            return path;
        }
    }
}