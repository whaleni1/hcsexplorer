﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using HCS.DataAccess;
using HCS.DataAccess.Databases;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using ImageMagick;

namespace HCSConnect.Library.Helpers
{
    public class ImageHelper
    {
        public static string CHANNEL_NAME_PREFIX = "Exp";

        //public static string INCELL_6000_INDICATOR = "USCA-INCELL6000-I10292";
        public static string INCELL_6000_INDICATOR = "IN Cell Analyzer 6000";
        public static int INCELL_6000_PIXEL_MULT_FACTOR = 1;

        /// <summary>
        /// GetDefaultImageFormat
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static string GetDefaultImageFormat(string dbName)
        {
            return "tiff";
        }

        /// <summary>
        /// GetImageStream
        ///  - frame is used only for Generic data sources
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="pl"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="tiffFilePath"></param>
        /// <param name="PIXEL_MULT_FACTOR"></param>
        /// <param name="frame"></param>
        /// <returns></returns>
        public static Stream GetImageStream(string dbName , Plate plate , ImageSpec spec , string tiffFilePath , int PIXEL_MULT_FACTOR , int frame)
        {
            Stream stream;

            try
            {
                tiffFilePath = tiffFilePath.Replace(System.Environment.NewLine , string.Empty);
                using (var tfc = new TempFileCollection())
                {
                    // reduce brightness for the INCELL 6000 images
                    if (!string.IsNullOrEmpty(plate.application))
                    {
                        if (!string.IsNullOrEmpty(tiffFilePath) && plate.application.Contains(INCELL_6000_INDICATOR))
                        {
                            PIXEL_MULT_FACTOR = INCELL_6000_PIXEL_MULT_FACTOR;
                            ConnectLogger.Info("ImageHelper:GetImageStream, Application = " + INCELL_6000_INDICATOR + ", Set pixel factor = " + PIXEL_MULT_FACTOR);
                        }
                    }

                    tiffFilePath = ConvertDBSpecific(dbName , tfc , tiffFilePath , spec);
                    tiffFilePath = ChannelSpecificFilename(dbName , tiffFilePath , spec , frame);

                    if (spec.reqImageProcess)
                    {
                        HCS_ImageMagick imageM = new HCS_ImageMagick(tiffFilePath);
                        MagickImage magicImage = imageM.GetMagicImage();

                        // create thumbnail
                        if (spec.thumbPixelSize > 0)
                        {
                            magicImage = imageM.CreateThumbnail(magicImage , spec.thumbPixelSize);
                        }

                        if (spec.auto)
                        {
                            magicImage = imageM.AddAutoContrast();
                            //magicImage = imageM.ConvertBpp(PIXEL_MULT_FACTOR , (int) spec.brightness);
                        }
                        else if (spec.level)
                        {
                            magicImage = imageM.Level(spec.blackPoint , spec.whitePoint);
                        }
                        if (spec.brightness > 0)
                        {
                            magicImage = imageM.ConvertBpp(PIXEL_MULT_FACTOR , (int) spec.brightness);
                        }

                        // invert image
                        if (spec.invert)
                        {
                            magicImage = imageM.Invert();
                        }

                        // fill with color
                        if (!String.IsNullOrEmpty(spec.color))
                        {
                            magicImage = imageM.Colorize(spec.color);
                        }

                        if (!String.IsNullOrEmpty(plate.application) && plate.application.Contains(INCELL_6000_INDICATOR))
                        {
                            magicImage = imageM.ConvertBpp(PIXEL_MULT_FACTOR , 0);
                        }

                        string cachedFilePath = ResourceHelper.GetLocalFilePath(spec.format);

                        tfc.AddFile(cachedFilePath , false);
                        stream = imageM.SaveImageStream(cachedFilePath , true , 90);

                        imageM._image.Dispose();
                    }
                    else //return original
                    {
                        // read raw file into memory stream
                        byte[] imageFileStream = File.ReadAllBytes(tiffFilePath);
                        stream = new MemoryStream(imageFileStream);
                    }
                }
            }
            catch (ImageNotFoundException ex)
            {
                ConnectLogger.Error(":mageHelper:GetImageStream, " + ex.Message);
                throw ex;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("ImageHelper:GetImageStream, " + eX.Message);
                throw new ConnectException("ImageHelper:GetImageStream, " + eX.Message , eX);
            }

            return stream;
        }

        /// <summary>
        /// GetOverlayImageStream
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="plate"></param>
        /// <param name="w"></param>
        /// <param name="spec"></param>
        /// <param name="fileNames"></param>
        /// <param name="PIXEL_MULT_FACTOR"></param>
        /// <returns></returns>
        public static Stream GetOverlayImageStream(string dbName , Plate plate , Well w , OverlayImageSpec spec , Dictionary<string , FrameFile> fileNames , int PIXEL_MULT_FACTOR)
        {
            Stream stream;

            try
            {
                using (var tfc = new TempFileCollection())
                {
                    ConvertDBSpecificOverlay(dbName , tfc , fileNames , spec);

                    // reduce brightness for the INCELL 6000 images
                    if (spec.imageElements.Count > 0)
                    {
                        string imgPath = spec.imageElements.ElementAt(0).imageURI;
                        if (!string.IsNullOrEmpty(plate.application))
                        {
                            if (plate.application.Contains(INCELL_6000_INDICATOR))
                            {
                                PIXEL_MULT_FACTOR = INCELL_6000_PIXEL_MULT_FACTOR;
                                ConnectLogger.Info("ImageHelper:GetOverlayImageStream, Application = " + INCELL_6000_INDICATOR + ", Set pixel factor = " + PIXEL_MULT_FACTOR);
                            }
                        }
                    }

                    // scale each image by PIXEL_MULT_FACTOR (16 in most cases) to increase brightness
                    HCS_ImageMagick imageM = new HCS_ImageMagick(spec , PIXEL_MULT_FACTOR);

                    string cachedFilePath = ResourceHelper.GetLocalFilePath(spec.format);
                    tfc.AddFile(cachedFilePath , false);
                    ConnectLogger.Info("Overlay, " + imageM._image.FileSize.ToString());

                    stream = imageM.SaveImageStream(cachedFilePath , true , 90);
                }
            }
            catch(ImageNotFoundException ex)
            {
                ConnectLogger.Error("ImageHelper:GetOverlayImageStream, " + ex.Message);
                throw ex;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("ImageHelper:GetOverlayImageStream, " + eX.Message);
                throw new ConnectException("ImageHelper:GetOverlayImageStream, " + eX.Message , eX);
            }

            return stream;
        }

        /// <summary>
        /// Use Bio-Formats to convert Cellomics C01 files to standard TIFF
        /// </summary>
        /// <param name="inputFilePath"></param>
        /// <param name="outputFilePath"></param>
        private static void ConvertCellomicsImage(string inputFilePath , string outputFilePath)
        {
            try
            {
                // command and args for Bio-Formats Converter tool (bfconvert)
                // see http://www.loci.wisc.edu/software/bio-formats for more info
                string bfconvertArgs = "-mx512m -cp \"" + ResourceHelper.BIOFORMATS_PATH + "\" loci.formats.tools.ImageConverter " +
                                        "\"" + inputFilePath + "\" \"" + outputFilePath + "\"";

                ConnectLogger.Info("ImageHelper:ConvertCellomicsImage, convertArgs=" + bfconvertArgs);
                ConnectLogger.Info("ImageHelper:ConvertCellomicsImage, JAVA_PATH=" + ResourceHelper.JAVA_PATH);

                // execute command
                ProcessStartInfo procStartInfo = new ProcessStartInfo(ResourceHelper.JAVA_PATH , bfconvertArgs);
                procStartInfo.UseShellExecute = false;
                // execute the command silently
                procStartInfo.CreateNoWindow = true;

                // create a process and assign its ProcessStartInfo
                Process proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                proc.WaitForExit();

                // close process
                proc.Close();

                ConnectLogger.Info("ImageHelper:ConvertCellomicsImage, Process finished");
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("ImageHelper:ConvertCellomicsImage, " + eX.Message);
                throw new ConnectException("ImageHelper:ConvertCellomicsImage, " + eX.Message , eX);
            }
        }

        /// <summary>
        /// ConvertLurawaveCompression
        /// </summary>
        /// <param name="srcFilePath"></param>
        /// <param name="destFilePath"></param>
        private static void ConvertLurawaveCompression(string srcFilePath , string destFilePath)
        {
            try
            {
                string args = "-c -n \"" + srcFilePath + "\" -d \"" + destFilePath;

                // execute command
                ProcessStartInfo procStartInfo = new ProcessStartInfo(ResourceHelper.LURAWAVE_CONVERTER , args);
                procStartInfo.UseShellExecute = false;
                // execute the command silently
                procStartInfo.CreateNoWindow = true;

                // create a process and assign its ProcessStartInfo
                Process proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                proc.WaitForExit();

                // close process
                proc.Close();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("ImageHelper:ConvertLurawaveCompression, " + eX.Message);
                throw new ConnectException("ImageHelper:ConvertLurawaveCompression, " + eX.Message , eX);
            }
        }

        /// <summary>
        /// ConvertDBSpecific
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="tfc"></param>
        /// <param name="filename"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        private static string ConvertDBSpecific(string dbName , TempFileCollection tfc , string filename , ImageSpec spec)
        {
            string typeName = String.Empty;

            try
            {
                typeName = HCSDBConfigSection.GetTypeName(dbName).ToUpper();

                if (typeName.EndsWith("CELLOMICS"))
                {
                    string tiffFilePath = ResourceHelper.GetLocalFilePath("tiff");
                    tfc.AddFile(tiffFilePath , false);
                    ConvertCellomicsImage(filename , tiffFilePath);
                    filename = tiffFilePath;

                }
                else if (typeName.EndsWith("OPERA"))
                {
                    string tiffFilePath = tfc.AddExtension("tiff" , true);
                    // convert image to standard tiff if compressed using lurawave level 2+
                    if (spec.compressType.Equals("lurawave") && spec.compressFactor >= 2)
                    {
                        ConvertLurawaveCompression(filename , tiffFilePath);
                    }
                    else
                    {
                        tiffFilePath = filename;
                    }

                    typeName = tiffFilePath;
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("ImageHelper:ConvertDBSpecific, " + eX.Message);
                throw new ConnectException("ImageHelper:ConvertDBSpecific, " + eX.Message , eX);
            }
            //INCELL, YOKOGAWA
            return filename;
        }

        /// <summary>
        /// ChannelSpecificFilename
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="filename"></param>
        /// <param name="spec"></param>
        /// <param name="frame"></param>
        /// <returns></returns>
        public static string ChannelSpecificFilename(string dbName , string filename , ImageSpec spec , int frame = 0)
        {
            string strFileName = string.Empty;

            try
            {
                if ("OPERA_2".Equals(dbName)) // new generic db that has frame_idx field
                {
                    //ConnectLogger.Info("File.Exists? " + filename);
                    //if (!File.Exists(filename))
                    //{
                    //    ConnectLogger.Error("File not found " + filename);
                    //    throw new FileNotFoundException("File not found" , filename);
                    //}

                    strFileName = filename;// + "[" + frame.ToString() + "]";
                }
                //else if (HCSDBConfigSection.GetTypeName(dbName).ToUpper().EndsWith("OPERA")) 
                //{
                //    int imagePos = ((spec.field - 1) * spec.channelCount) + OperaExtractChannelId(spec.channel) - 1;

                //    strFileName = filename + "[" + imagePos.ToString() + "]";
                //}
                else
                {
                    strFileName = filename;
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("ImageHelper:ChannelSpecificFilename, " + eX.Message);
                throw new ConnectException("ImageHelper:ChannelSpecificFilename, " + eX.Message , eX);
            }

            return strFileName;

        }

        /// <summary>
        /// OperaExtractChannelId
        /// </summary>
        /// <param name="ch"></param>
        /// <returns></returns>
        private static int OperaExtractChannelId(string ch)
        {
            int chId;

            try
            {
                chId = Convert.ToInt16(ch.Replace(Opera.CHANNEL_NAME_PREFIX , ""));
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("ImageHelper:OperaExtractChannelId, " + eX.Message);
                throw new ConnectException("ImageHelper:OperaExtractChannelId, " + eX.Message , eX);
            }

            return chId;
        }

        /// <summary>
        /// ConvertDBSpecificOverlay
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="tfc"></param>
        /// <param name="fileNames"></param>
        /// <param name="spec"></param>
        private static void ConvertDBSpecificOverlay(string dbName , TempFileCollection tfc , Dictionary<string , FrameFile> fileNames , OverlayImageSpec spec)
        {
            try
            {
                string typeName = HCSDBConfigSection.GetTypeName(dbName).ToUpper();

                if (typeName.EndsWith("CELLOMICS"))
                {

                    foreach (string key in fileNames.Keys)
                    {
                        string tiffFilePath = ConvertDBSpecific(dbName , tfc , fileNames[key].ImagePath , null);
                        spec.setImageFilePath(key , tiffFilePath);
                    }
                    return;
                }
                if ("OPERA_2".Equals(dbName)) // new generic db that has frame_idx field
                {
                    string filename = fileNames.Values.First().ImagePath; //the same
                    string tiffFilePath = tfc.AddExtension("tiff" , true);
                    
                    // convert image to standard tiff if compressed using lurawave level 2+
                    if ("lurawave".Equals(spec.compressType) && spec.compressFactor >= 2)
                    {
                        ConvertLurawaveCompression(filename , tiffFilePath);
                    }
                    else
                    {
                        tiffFilePath = filename;
                    }

                    foreach (string key in fileNames.Keys)
                    {
                        int imagePos = fileNames[key].Frame;
                        string imagePosFilePath = tiffFilePath;// + "[" + imagePos.ToString() + "]";
                        spec.setImageFilePath(key , imagePosFilePath);
                    }

                    return;
                }
                if (typeName.EndsWith("OPERA"))
                {
                    string filename = fileNames.Values.First().ImagePath; //the same
                    string tiffFilePath = tfc.AddExtension("tiff" , true);

                    // convert image to standard tiff if compressed using lurawave level 2+
                    if (spec.compressType.Equals("lurawave") && spec.compressFactor >= 2)
                    {
                        ConvertLurawaveCompression(filename , tiffFilePath);
                    }
                    else
                    {
                        tiffFilePath = filename;
                    }
                    int channelCount = fileNames.Keys.Count;

                    for (int c = 1 ; c <= channelCount ; c++)
                    {
                        // determine the index of the image from the multi-page tiff file
                        int imagePos = ((spec.field - 1) * channelCount) + c - 1;
                        string imagePosFilePath = tiffFilePath + "[" + imagePos.ToString() + "]";

                        string chName = Opera.CHANNEL_NAME_PREFIX + c.ToString();
                        spec.setImageFilePath(chName , imagePosFilePath);
                    }
                    return;
                }

                //INCELL , YOKOGAWA
                foreach (string key in fileNames.Keys)
                {
                    spec.setImageFilePath(key , fileNames[key].ImagePath);
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("ImageHelper:ConvertDBSpecificOverlay, " + eX.Message);
                throw new ConnectException("ImageHelper:ConvertDBSpecificOverlay, " + eX.Message , eX);
            }
        }
    }
}