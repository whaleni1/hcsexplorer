﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using HCS.DataAccess;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;

namespace HCSConnect.Library.Helpers
{
    /// <summary>
    /// CacheHelper
    /// </summary>
    public class CacheHelper
    {
        private static ConcurrentDictionary<string , Task> runningTasks = new ConcurrentDictionary<string , Task>();
        private static object locObject = new Object();

        /// <summary>
        /// GetStreamFromCache
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static Stream GetStreamFromCache(string cacheKey , string format)
        {
            MemoryStream ms = null;

            try
            {
                string cachedFilePath = ResourceHelper.GetLocalFilePath(cacheKey , format);
                if (File.Exists(cachedFilePath))
                {
                    // read image file into memory stream
                    byte[] fileBuffer = File.ReadAllBytes(cachedFilePath);
                    ms = new MemoryStream(fileBuffer);
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("CacheHelper:GetStreamFromCache, " + eX.Message);
                new ConnectException("CacheHelper:GetStreamFromCache, " + eX.Message , eX);
            }

            return ms;
        }

        /// <summary>
        /// GetStreamFromCache
        /// </summary>
        /// <param name="strFilename"></param>
        /// <returns></returns>
        public static Stream GetStreamFromCache(string strFilename)
        {
            MemoryStream ms = null;

            try
            {
                if (File.Exists(strFilename))
                {
                    // read image file into memory stream
                    byte[] fileBuffer = File.ReadAllBytes(strFilename);
                    ms = new MemoryStream(fileBuffer);
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("CacheHelper:GetStreamFromCache, " + eX.Message);
                new ConnectException("CacheHelper:GetStreamFromCache, " + eX.Message , eX);
            }

            return ms;
        }

        /// <summary>
        /// GetMediaData
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="plateId"></param>
        /// <returns></returns>
        public static List<MediaData> GetMediaData(string dbName , string plateId)
        {
            string key = string.Empty;
            List<MediaData> lMetaData = null;

            try
            {
                key = dbName + "!" + plateId;
                ObjectCache cache = MemoryCache.Default;
                lMetaData = (List<MediaData>) cache[key];
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("CacheHelper:GetMediaData error with key=" + key + ", " + eX.Message);
                new ConnectException("CacheHelper:GetMediaData, " + eX.Message , eX);
            }

            return lMetaData;
        }

        /// <summary>
        /// GetMediaData
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static List<MediaData> GetMediaData(string key)
        {
            List<MediaData> lMetaData = null;

            try
            {
                ObjectCache cache = MemoryCache.Default;
                lMetaData = (List<MediaData>) cache[key];
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("CacheHelper:GetMediaData, " + eX.Message);
                new ConnectException("CacheHelper:GetMediaData, " + eX.Message , eX);
            }

            return lMetaData;
        }

        /// <summary>
        /// GetMediaData
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="plateId"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public static List<MediaData> GetMediaData(Connect connect , string dbName , string plateId , int row , int col , ImageSpec spec)
        {
            string key = dbName + "!" + plateId;
            List<MediaData> plateData = new List<MediaData>();
            Plate plate = new Plate();
            plate.plateId = plateId;

            try
            {
                if (runningTasks.ContainsKey(key))
                {
                    ConnectLogger.Debug("CacheHelper:GetMediaData - Wait metadata service task finish:" + key);
                    if (runningTasks[key] != null)
                        runningTasks[key].Wait();
                }
                plateData = GetMediaData(dbName , plateId);

                if (plateData == null)
                {
                    Task task = null;
                    lock (locObject)
                    {
                        ConnectLogger.Debug("6 GetMediaData Run  metadata service task :" + key);
                        task = Task.Factory.StartNew(() =>
                        {
                            MediaDataList pd = connect.GetMediaData(dbName , plate , 0 , 0 , null);
                            if (pd != null && pd.Count() > 0)
                                SetMediaData(dbName , plateId , pd);

                        });
                        runningTasks[key] = task;
                        ConnectLogger.Debug("6b");
                    }
                    task.Wait();
                    ConnectLogger.Debug("CacheHelper:GetMediaData - 7 Done Run metadata service task :" + key);
                    Task outt;
                    if (!runningTasks.TryRemove(key , out outt))
                    {
                        ConnectLogger.Debug("CacheHelper:GetMediaData Cannot remove metadata service task from dictionary:" + key);
                    }
                }
                plateData = GetMediaData(dbName , plateId);
                if (plateData == null)
                {
                    plateData = new List<MediaData>();
                    throw new Exception("CacheHelper:GetMediaData Caching error!!!!!");
                }
                ConnectLogger.Debug("CacheHelper:GetMediaData finished");

                plateData = plateData.Where(x => (x.Column == col && x.Row == row)).ToList();

                ConnectLogger.Debug("CacheHelper:GetMediaData - 12 PLATE DATA RETURNED! ");
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("CacheHelper:GetMediaData, Error *** dbName=" + dbName + "!plateId=" + plateId + "!row=" + row.ToString() + "!col=" + col.ToString()
                     + ", Error Message = " + eX.Message);
                new ConnectException("CacheHelper:GetMediaData, " + eX.Message , eX);
            }

            if (plateData.Count == 0)
                ConnectLogger.Debug("CacheHelper:GetMediaData - 13 NO PLATE DATA RETURNED! ");

            return plateData;
        }

        /// <summary>
        /// GetWellMediaData
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="plateId"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        public static List<MediaData> GetWellMediaData(Connect connect , string dbName , string plateId , int row , int col , ImageSpec spec)
        {
            ConnectLogger.Info("GetWellMediaData started");
            string key = dbName + "!" + plateId + "!" + row + "!" + col;
            List<MediaData> wellData = null;
            Plate plate = new Plate();
            plate.plateId = plateId;

            try
            {
                if (spec != null)
                {
                    key += "!" + spec.field + "!" + spec.channel + "!" + spec.z_index.ToString() + "!" + spec.timepoint.ToString() + "!" + spec.version;
                }

                wellData = GetMediaData(key);

                if (wellData == null)
                {
                    List<MediaData> pd = connect.GetMediaData(dbName , plate , row , col , spec);
                    if (pd != null && pd.Count() > 0)
                        SetMediaData(key , pd);
                }
                wellData = GetMediaData(key);
                ConnectLogger.Info("GetWellMediaData finished");
                if (wellData == null)
                {
                    return null;
                }
                if (HCSDBConfigSection.IsOpera(dbName) || HCSDBConfigSection.IsYokogawa(dbName) || HCSDBConfigSection.IsPhenix(dbName))
                {
                    wellData = wellData.Where(x => (x.Column == col && x.Row == row)).ToList();
                }
                else
                {
                    wellData = wellData.Where(x => (x.Column == col && x.Row == row && x.Field == spec.field) &&
                         (string.IsNullOrEmpty(spec.channel) || x.Channel == spec.channel) &&
                         (!HCSDBConfigSection.IsYokogawa(dbName) &&
                          !HCSDBConfigSection.IsPhenix(dbName) ||
                         (x.Timepoint == spec.timepoint && x.Version == spec.version && x.ZIndex == spec.z_index))).ToList();
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("CacheHelper:GetWellMediaData, " + eX.Message);
                new ConnectException("CacheHelper:GetWellMediaData, " + eX.Message , eX);
            }

            return wellData;
        }

        /// <summary>
        /// SetMediaData
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="plateId"></param>
        /// <param name="mdlist"></param>
        public static void SetMediaData(string dbName , string plateId , MediaDataList mdlist)
        {
            string key = dbName + "!" + plateId;
            ObjectCache cache = MemoryCache.Default;
            cache[key] = mdlist;
        }

        /// <summary>
        /// SetMediaData
        /// </summary>
        /// <param name="key"></param>
        /// <param name="mdlist"></param>
        public static void SetMediaData(string key , List<MediaData> mdlist)
        {
            ObjectCache cache = MemoryCache.Default;
            cache[key] = mdlist;
        }
    }
}