﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using HCS.DataAccess;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using ImageMagick;

namespace HCSConnect.Library
{
    /// <summary>
    /// HCS_ImageMagick
    /// </summary>
    public class HCS_ImageMagick
    {
        // dynamic array list to hold ImageMagick command line options 
        //private List<string> _imageMagickOptions;
        public MagickImage _image = null;
        private string _imagePath = string.Empty;

        /// <summary>
        /// ImageMagick
        ///  - Constructor
        /// </summary>
        /// <param name="srcFilePath"></param>
        public HCS_ImageMagick(string srcFilePath)
        {
            try
            {
                if(!File.Exists(srcFilePath))
                    throw new ImageNotFoundException();

                _image = new MagickImage(srcFilePath);
                _imagePath = srcFilePath;

                ConnectLogger.Debug(GetType().Name + "***:HCS_ImageMagick, Image found at File=" + srcFilePath);

            }
            catch(ImageNotFoundException ex)
            {
                ConnectLogger.Debug(GetType().Name + "***:HCS_ImageMagick, Image not found at File=" + srcFilePath );
                throw ex;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + "***:HCS_ImageMagick, File=" + srcFilePath + ", Error=" + eX.Message);
                throw new ConnectException(GetType().Name + ":HCS_ImageMagick, " + eX.Message , eX);
            }
        }

        /// <summary>
        /// ImageMagick
        ///  - Constructor for image overlay
        /// </summary>
        /// <param name="spec"></param>
        /// <param name="multFactor"></param>
        public HCS_ImageMagick(OverlayImageSpec spec , int multFactor)
        {
            // Create placeholder for command line options
            List<MagickImage> images = new List<MagickImage>();
            try
            {
                var chImages = spec.imageElements.Where(x => !String.IsNullOrEmpty(x.channel) && !String.IsNullOrEmpty(x.imageURI)).ToList();

                if (chImages.Count == 0)
                {
                    ConnectLogger.Info(this.GetType().Name + ":HCS_ImageMagick,  No image found in spec");
                    throw new ImageNotFoundException();
                }

                foreach (ImageSpec img in chImages)
                {
                    images.Add(new MagickImage(img.imageURI));
                }

                int num = 0;
                _image = images[num];
                foreach (ImageSpec img in chImages)
                {

                    if (spec.thumbPixelSize > 0)
                    {
                        images[num] = CreateThumbnail(images[num] , spec.thumbPixelSize);
                    }

                    // create thumbnail
                    if (img.auto)
                    {
                        images[num] = AddAutoContrast(images[num]);
                    }
                    else if (img.level)
                    {
                        images[num] = Level(images[num] , img.blackPoint , img.whitePoint);
                    }
                    else if (img.brightness > 0)
                    {
                        images[num].Evaluate(Channels.All , EvaluateOperator.Multiply , multFactor * img.brightness);
                    }

                    images[num] = Colorize(images[num] , img.color);

                    if (num > 0)
                    {
                        _image.Composite(images[num] , CompositeOperator.Blend);
                    }

                    num++;
                }

                // invert image
                if (spec.invert)
                {
                    _image = Invert();
                }

                //_image = setGamma(images[0] , 0.75); //Save the composited image.    
                // Set background color to black
                _image.BackgroundColor = new MagickColor(System.Drawing.Color.Black);
                _image.Depth = 8;
            }
            catch(ImageNotFoundException ex)
            {
                ConnectLogger.Error(GetType().Name + ":HCS_ImageMagick, " + ex.Message);
                throw ex;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":HCS_ImageMagick, " + eX.Message);
                throw new ConnectException(GetType().Name + ":HCS_ImageMagick, " + eX.Message , eX);
            }
        }

        /// <summary>
        /// SetImageQuality
        /// </summary>
        /// <param name="inputBytes"></param>
        /// <param name="jpegQuality"></param>
        /// <returns></returns>
        public Byte[] SetImageQuality(Byte[] inputBytes , int jpegQuality)
        {
            Byte[] outputBytes = null;

            try
            {
                Image image;

                if (inputBytes.Length == 0)
                    return inputBytes;

                using (var inputStream = new MemoryStream(inputBytes))
                {
                    image = Image.FromStream(inputStream);
                    ImageCodecInfo jpegEncoder = GetEncoderInfo("image/jpeg");
                    var encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality , jpegQuality);

                    using (var outputStream = new MemoryStream())
                    {
                        image.Save(outputStream , jpegEncoder , encoderParameters);
                        outputBytes = outputStream.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":SetImageQuality, " + ex.Message);
                new ConnectException(GetType().Name + ":SetImageQuality, " + ex.Message , ex);
            }

            return outputBytes;
        }

        /// <summary>
        /// GetEncoderInfo
        /// </summary>
        /// <param name="mimeType"></param>
        /// <returns></returns>
        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            try
            {
                int j;
                ImageCodecInfo[] encoders;
                encoders = ImageCodecInfo.GetImageEncoders();
                for (j = 0 ; j < encoders.Length ; ++j)
                {
                    if (encoders[j].MimeType == mimeType)
                        return encoders[j];
                }
                return null;
            }

            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":GetEncoderInfo, " + ex.Message);
                new ConnectException(GetType().Name + ":GetEncoderInfo, " + ex.Message , ex);
                return null;
            }
        }

        /// <summary>
        /// GetMagicImage
        /// </summary>
        /// <param name="srcFilePath"></param>
        /// <returns></returns>
        public MagickImage GetMagicImage()
        {
            try
            {
                return _image;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + "***:GetMagicImage path=" + _imagePath + ", Error=" + eX.Message);
                new ConnectException(GetType().Name + ":GetMagicImage, " + eX.Message , eX);
                return null;
            }
        }

        /// <summary>
        /// ConvertBpp
        ///  - Apply pixel arithemetic and linearly scale the image
        /// </summary>
        /// <param name="multFactor"></param>
        /// <param name="brightness"></param>
        public MagickImage ConvertBpp(int multFactor , int brightness)
        {
            try
            {
                // multiply pixel by brightness factor if applicable
                if (brightness > 0)
                {
                    multFactor = multFactor * brightness;
                }

                if (multFactor > 1)
                {
                    _image.Evaluate(Channels.All , EvaluateOperator.Multiply , multFactor);
                }
                _image.BackgroundColor = new MagickColor(System.Drawing.Color.Black);
                _image.Depth = 8;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":ConvertBpp, " + eX.Message);
                new ConnectException(GetType().Name + ":ConvertBpp, " + eX.Message , eX);
            }
            return _image;
        }

        /// <summary>
        /// Scale
        /// </summary>
        /// <param name="brightness"></param>
        public MagickImage Scale(double brightness)
        {
            try
            {
                _image.BrightnessContrast(new Percentage(brightness) , new Percentage(0));

                if (brightness > 0)
                {
                    _image.Evaluate(Channels.All , EvaluateOperator.Multiply , brightness);

                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":Scale, " + eX.Message);
                new ConnectException(GetType().Name + ":Scale, " + eX.Message , eX);
            }

            return _image;
        }

        /// <summary>
        /// Level
        /// </summary>
        /// <param name="blackPoint"></param>
        /// <param name="whitePoint"></param>
        public MagickImage Level(long blackPoint , long whitePoint)
        {
            return Level(_image , blackPoint , whitePoint);
        }

        /// <summary>
        /// Level
        /// </summary>
        /// <param name="blackPoint"></param>
        /// <param name="whitePoint"></param>
        public MagickImage Level(MagickImage inputImage , long blackPoint , long whitePoint)
        {
            try
            {
                inputImage.Level((ushort) blackPoint , (ushort) whitePoint);
                inputImage.BackgroundColor = new MagickColor(System.Drawing.Color.Black);
                inputImage.Depth = 8;
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":Level, " + eX.Message);
                throw new ConnectException(GetType().Name + ":Level, " + eX.Message , eX);
            }

            return inputImage;
        }

        /// <summary>
        /// AddAutoContrast
        /// </summary>
        public MagickImage AddAutoContrast()
        {
            _image = AddAutoContrast(_image);
            return _image;
        }

        /// <summary>
        /// AddAutoContrast
        /// </summary>
        /// <param name="img"></param>
        private MagickImage AddAutoContrast(MagickImage img)
        {
            try
            {
                img.Normalize();
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":AddAutoContrast, " + eX.Message);
                throw new CException(GetType().Name + ":AddAutoContrast, " + eX.Message , eX);
            }
            return img;
        }

        /// <summary>
        /// AddAutoContrast
        /// </summary>
        public MagickImage AddAutoContrast(double gamma)
        {
            return SetGamma(_image , gamma);
        }

        /// <summary>
        /// SetGamma
        /// </summary>
        public MagickImage SetGamma(MagickImage inputImage , double gamma)
        {
            try
            {
                inputImage.GammaCorrect(gamma);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":SetGamma, " + eX.Message);
                throw new ConnectException(GetType().Name + ":SetGamma, " + eX.Message , eX);
            }

            return inputImage;
        }

        /// <summary>
        /// Invert
        /// </summary>
        public MagickImage Invert()
        {
            return Invert(_image);
        }

        /// <summary>
        /// Invert
        /// </summary>
        public MagickImage Invert(MagickImage inputImage)
        {
            try
            {
                ConnectLogger.Info(GetType().Name + ":Invert Start");
                inputImage.Negate();
                ConnectLogger.Info(GetType().Name + ":Invert Stop");
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":Invert, " + eX.Message);
                throw new ConnectException(GetType().Name + ":Invert, " + eX.Message , eX);
            }

            return inputImage;
        }

        /// <summary>
        /// Colorize
        /// </summary>
        /// <param name="color"></param>
        public MagickImage Colorize(string color)
        {
            return Colorize(_image , color);
        }

        /// <summary>
        /// Colorize
        /// </summary>
        /// <param name="color"></param>
        public MagickImage Colorize(MagickImage inputImage , string color)
        {
            try
            {
                ConnectLogger.Info(GetType().Name + ":Colorize Start " + color);

                // create a linear palette based on the provided color
                switch (color.ToUpper())
                {

                    case "RED":
                        inputImage.Colorize(new MagickColor(0 , 100 , 100) , new Percentage(0) , new Percentage(100) , new Percentage(100));
                        break;
                    case "GREEN":
                        inputImage.Colorize(new MagickColor(100 , 0 , 100) , new Percentage(100) , new Percentage(0) , new Percentage(100));
                        break;
                    case "BLUE":
                        inputImage.Colorize(new MagickColor(100 , 100 , 0) , new Percentage(100) , new Percentage(100) , new Percentage(0));
                        break;
                    case "CYAN":
                        inputImage.Colorize(new MagickColor(100 , 0 , 0) , new Percentage(100) , new Percentage(0) , new Percentage(0));
                        break;
                    case "MAGENTA":
                        inputImage.Colorize(new MagickColor(0 , 100 , 0) , new Percentage(0) , new Percentage(100) , new Percentage(0));
                        break;
                    case "YELLOW":
                        inputImage.Colorize(new MagickColor(0 , 0 , 100) , new Percentage(0) , new Percentage(0) , new Percentage(100));
                        break;
                }
                ConnectLogger.Info("Colorize Stop " + color);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":Colorize , " + color + "," + eX.Message);
                new ConnectException(GetType().Name + ":Colorize, " + eX.Message , eX);
            }

            return inputImage;
        }

        /// <summary>
        /// CreateThumbnail
        /// </summary>
        /// <param name="pixelSize"></param>
        public MagickImage CreateThumbnail(int pixelSize)
        {
            return CreateThumbnail(_image , pixelSize);
        }

        /// <summary>
        /// CreateThumbnail
        /// </summary>
        /// <param name="pixelSize"></param>
        public MagickImage CreateThumbnail(MagickImage inputImage , int pixelSize)
        {
            try
            {
                inputImage.Thumbnail(pixelSize , pixelSize);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":CreateThumbnail, " + eX.Message);
                throw new ConnectException(GetType().Name + ":CreateThumbnail, " + eX.Message , eX);
            }

            return inputImage;
        }

        /// <summary>
        /// SaveImageStream
        ///   This function returns the converted image as byte stream
        /// </summary>
        /// <param name="imageMagickCmd"></param>
        /// <param name="destinationPath"></param>
        /// <returns></returns>
        public Stream SaveImageStream(string destinationPath , Boolean compress , int quality)
        {
            Stream stream;

            try
            {
                if (_image.Width == 0 || _image.Height == 0)
                {
                    throw new ConnectException(this.GetType().Name + ":SaveImageStream, destinationPath=" + destinationPath + ", Error=" + "No image found");
                }

                _image.Write(destinationPath);

                ConnectLogger.Info(GetType().Name + ":SaveImageStream, " + destinationPath);
                System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
                System.Drawing.Bitmap bmp = _image.ToBitmap();

                Byte[] imageFileStream = (byte[]) converter.ConvertTo(bmp , typeof(byte[]));

                ConnectLogger.Info(GetType().Name + ":SaveImageStream, imageFileStream=" + imageFileStream.Count().ToString());
                if (compress)
                    stream = new MemoryStream(SetImageQuality(imageFileStream , quality));
                else
                    stream = new MemoryStream(imageFileStream);

            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":SaveImageStream, destinationPath=" + destinationPath + ", Error=" + eX.Message);
                throw new ConnectException(this.GetType().Name + ":SaveImageStream, destinationPath=" + destinationPath + ", Error=" + eX.Message , eX);
            }

            return stream;
        }
    }
}