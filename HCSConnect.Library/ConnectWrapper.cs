﻿using System;
using System.IO;
using System.Net;
using System.ServiceModel.Web;
using System.Web;
using HCS.DataAccess;
using HCS.DataAccess.Databases;
using HCS.DataAccess.Helpers;
using HCS.Shared.Common;
using HCS.Shared.Logger;
using HCSConnect.Library.Helpers;

namespace HCSConnect.Library
{
    public class ConnectWrapper
    {
        UriTemplateMatch _uriMatch;
        string _cookie;
        Connect _connect;

        /// <summary>
        /// HCS_Connect
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="cookie"></param>
        public ConnectWrapper(UriTemplateMatch uri, String cookie)
        {
            _uriMatch = uri;
            _cookie = cookie;
            _connect = new Connect();
        }

        /// <summary>
        /// SetErrorResponse
        /// </summary>
        /// <param name="code"></param>
        /// <param name="description"></param>
        /// <param name="message"></param>
        private void SetErrorResponse(HttpStatusCode code, string description, string message)
        {
            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = code;
            response.StatusDescription = description;
        }

        /// <summary>
        /// GetDatabases
        ///  - Return information on databases provided by this service
        /// </summary>
        /// <returns></returns>
        public DBList GetDatabases()
        {
            var dbs = new DBList();

            try
            {
                if (!string.IsNullOrEmpty(_uriMatch.QueryParameters["site"]))
                {
                    string site = _uriMatch.QueryParameters["site"];

                    ConnectLogger.Info(this.GetType().Name + ":GetDatabases for site " + site + " specified in the QueryParameters");

                    dbs = _connect.GetDatabases(site);
                }
                else
                {
                    ConnectLogger.Info(this.GetType().Name + ":GetDatabases for all sites. No site specified in the QueryParameters");

                    dbs = _connect.GetDatabases();
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetDatabases, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetDatabases, " + eX.Message, eX);
            }

            return dbs;
        }

        /// <summary>
        /// GetPlates
        /// </summary>
        /// <returns></returns>
        public PlateList GetPlates()
        {
            PlateList plates = null;

            try
            {
                // check if barcode is provided
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"]) &&
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]) &&
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["path"]))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                        "Missing Parameters in URL",
                        "Missing Parameters in URL: barcode, plateid, or path is required.");
                    return null;
                }

                // create new Plate object and set the request parameter from URL (barcode, plateid, or path)
                var plate = new Plate();
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"]))
                {
                    plate.barcode = _uriMatch.QueryParameters["barcode"];
                }
                else
                {
                    if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                    {
                        plate.plateId = _uriMatch.QueryParameters["plateid"];
                    }
                    else
                    {
                        plate.path = HttpUtility.UrlDecode(_uriMatch.QueryParameters["path"]);
                    }
                }

                // check if database name is provided as part of the URL
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    plates = _connect.GetPlates(plate);

                    ConnectLogger.Info(this.GetType().Name + ":GetPlates finished. " + plates.Count + " plates found.");
                }
                else
                {
                    // directly search the specified database
                    string db = _uriMatch.QueryParameters["db"];
                    plates = _connect.GetPlates(db, plate);
                    ConnectLogger.Info(this.GetType().Name + ":GetPlates finished. " + plates.Count + " plates found.");
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetPlates, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetPlates, " + eX.Message, eX);
            }

            return plates;
        }

        /// <summary>
        /// GetImageParameters
        ///  - This service returns image metadata associated with a plate well.
        ///    If no well is specified, all available image parameters for the plate will be returned
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageParameters GetImageParameters()
        {
            ImageParameters imgParams = null;

            try
            {
                // check if barcode, row, and col are provided
                if ((String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"]) &&
                     String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"])))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                        "Missing Parameters in URL",
                        "Missing Parameters in URL: barcode or plateid is required.");
                    return null;
                }
                // extract barcode or plateid from url as plate identifier
                var pl = new Plate();
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"]))
                {
                    pl.barcode = _uriMatch.QueryParameters["barcode"];
                }
                else
                {
                    if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                    {
                        pl.plateId = _uriMatch.QueryParameters["plateid"];
                    }
                }

                // extract row and col id from the url if available
                var row = 0;
                var col = 0;
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["row"]) &&
                    !String.IsNullOrEmpty(_uriMatch.QueryParameters["col"]))
                {
                    row = Convert.ToInt32(_uriMatch.QueryParameters["row"]);
                    col = Convert.ToInt32(_uriMatch.QueryParameters["col"]);
                }

                // check if database name is provided as part of the URL
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    imgParams = _connect.GetImageParameters(pl, row, col);
                }
                else
                {
                    // directly search the specified database
                    string db = _uriMatch.QueryParameters["db"];
                    imgParams = _connect.GetImageParameters(db, pl, row, col);
                }
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":GetImageParameters, " + ex.Message);
                throw new ConnectException(GetType().Name + ":GetImageParameters, " + ex.Message, ex);
            }

            return imgParams;
        }

        /// <summary>
        /// GetImageParametersForWholePlate
        ///  - This service returns all available image parameters for the plate.
        /// </summary>
        /// <returns></returns>
        public PlateImgMetadata GetImageParametersForWholePlate()
        {
            PlateImgMetadata pImgMetadata = null;

            try
            {
                // check if plateid is provided
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Missing Parameters in URL",
                                     "Missing Parameters in URL: plateid is required.");
                    return null;
                }
                var plateId = _uriMatch.QueryParameters["plateid"];

                // extract barcode or plateid from url as plate identifier
                var pl = new Plate();
                pl.plateId = plateId;

                // check if database name is provided as part of the URL
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    // directly search the specified database
                    pImgMetadata = _connect.GetImageParametersForWholePlate(pl);
                }
                else
                {
                    string db = _uriMatch.QueryParameters["db"];
                    // directly search the specified database
                    pImgMetadata = _connect.GetImageParametersForWholePlate(db , pl);
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageParametersForWholePlate, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetImageParametersForWholePlate, " + eX.Message, eX);
            }

            return pImgMetadata;
        }

        /// <summary>
        /// GetImages
        ///  - This service returns all matching images and their metadata (including url) associated with a well
        ///    If there are duplicate plates associated with the barcode, separate entries will be returned
        /// </summary>
        /// <returns></returns>
        public ImageCollections GetImages()
        {
            ImageCollections imageColls = null;

            try
            {
                // check if barcode, row, and col are provided
                if ((String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"]) &&
                     String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"])) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["row"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["col"]))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                        "Missing Parameters in URL",
                        "Missing Parameters in URL: barcode (or plateid), row, and col are required.");
                    return null;
                }
                // Get barcode or plateid from url as plate identifier
                var pl = new Plate();
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"]))
                {
                    pl.barcode = _uriMatch.QueryParameters["barcode"];
                }
                else
                {
                    if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                    {
                        pl.plateId = _uriMatch.QueryParameters["plateid"];
                    }
                }

                var row = Convert.ToInt32(_uriMatch.QueryParameters["row"]);
                var col = Convert.ToInt32(_uriMatch.QueryParameters["col"]);
                var fld = -1;
                var ch = string.Empty;
                var baseUri = _uriMatch.BaseUri.ToString();

                // check if field is part of the URL parameters
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["field"]))
                {
                    // extract field from URL
                    fld = Convert.ToInt32(_uriMatch.QueryParameters["field"]);
                }

                // check if channel is part of the URL parameters
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["channel"]))
                {
                    // extract channel from URL
                    ch = _uriMatch.QueryParameters["channel"];
                }

                var spec = new ImageSpec();
                spec.timepoint = GetTimepointOrDefault(_uriMatch);
                spec.z_index = GetZIndexOrDefault(_uriMatch);
                spec.version = GetVersionOrDefault(_uriMatch);

                // check if database name is provided as part of the URL
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    imageColls = _connect.GetImages(pl , row , col , fld , ch , spec , baseUri);
                }
                else
                {
                    var db = _uriMatch.QueryParameters["db"];
                    imageColls = _connect.GetImages(db , pl , row , col , fld , ch , spec , baseUri);
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetImages, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetImages, " + eX.Message, eX);
            }

            return imageColls;
        }

        /// <summary>
        /// GetTimepointOrDefault
        /// </summary>
        /// <param name="uriMatch"></param>
        /// <returns></returns>
        private static int GetTimepointOrDefault(UriTemplateMatch uriMatch)
        {
            return GetParamOrDefault(uriMatch, "timepoint_index", 1);
        }

        /// <summary>
        /// GetTimepointOrDefault
        /// </summary>
        /// <param name="uriMatch"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        private static int GetTimepointOrDefault(UriTemplateMatch uriMatch, string dbName)
        {
            int iValue = 1;

            // Need to change the default zindex value to 0 for OPERA_2 image data
            if ("OPERA_2".Equals(dbName))
                iValue = 0;

            return GetParamOrDefault(uriMatch, "timepoint_index", iValue);
        }

        /// <summary>
        /// GetZIndexOrDefault
        /// </summary>
        /// <param name="uriMatch"></param>
        /// <returns></returns>
        private static int GetZIndexOrDefault(UriTemplateMatch uriMatch)
        {
            return GetParamOrDefault(uriMatch, "z_index", 1);
        }

        /// <summary>
        /// GetZIndexOrDefault
        /// </summary>
        /// <param name="uriMatch"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        private static int GetZIndexOrDefault(UriTemplateMatch uriMatch, string dbName)
        {
            int iValue = 1;

            // Need to change the default zindex value to 0 for OPERA_2 image data
            if ("OPERA_2".Equals(dbName))
                iValue = 0;

            return GetParamOrDefault(uriMatch, "z_index", iValue);
        }

        /// <summary>
        /// GetVersionOrDefault
        /// </summary>
        /// <param name="uriMatch"></param>
        /// <returns></returns>
        private static string GetVersionOrDefault(UriTemplateMatch uriMatch)
        {
            return GetParamOrDefault(uriMatch, "version", "raw");
        }

        /// <summary>
        /// GetParamOrDefault
        /// </summary>
        /// <param name="uriMatch"></param>
        /// <param name="paramName"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        private static int GetParamOrDefault(UriTemplateMatch uriMatch, string paramName, int defaultVal)
        {
            var value = defaultVal;

            try
            {
                if (!String.IsNullOrEmpty(uriMatch.QueryParameters[paramName]))
                {
                    value = Convert.ToInt32(uriMatch.QueryParameters[paramName]);
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error("HCS_Connect:GetParamOrDefault, " + eX.Message);
                throw new ConnectException("HCS_Connect:GetParamOrDefault, " + eX.Message, eX);
            }
            return value;
        }

        /// <summary>
        /// GetParamOrDefault
        /// </summary>
        /// <param name="uriMatch"></param>
        /// <param name="paramName"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        private static string GetParamOrDefault(UriTemplateMatch uriMatch, string paramName, string defaultVal)
        {
            var value = defaultVal;
            if (!String.IsNullOrEmpty(uriMatch.QueryParameters[paramName]))
            {
                value = uriMatch.QueryParameters[paramName];
            }
            return value;
        }

        /// <summary>
        /// GetImageCacheMetadata
        ///  - Return an image object as stream
        /// </summary>
        /// <returns></returns>
        public Stream GetImageCacheMetadata()
        {
            Stream imagestream = null;
            var spec = new ImageSpec();
            var dbName = string.Empty;

            try
            {
                // check if plate id, row, col, field, and channel are provided
                if (( String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]) && 
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"])) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["row"]) || 
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["col"]) || 
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["field"]) || 
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["channel"])  )
                {
                    ConnectLogger.Debug(GetType().Name + ":Missing Parameters in URL");
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Missing Parameters in URL",
                                     "Missing Parameters in URL: plateid (or barcode), row, col, field, and channel are required.");
                    return null;
                }
                ConnectLogger.Info(GetType().Name + ":GetImageCacheMetadata with plate metadata caching started");

                // create plate data
                var pl = new Plate();

                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                {
                    pl.plateId = _uriMatch.QueryParameters["plateid"];
                }

                // check if db name is specified as part of the URL
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    // search specified database directly
                    dbName = _uriMatch.QueryParameters["db"];
                }
                else // find the database id to continue
                {
                    dbName = _connect.FindDatabase(pl);
                }

                // extract parameters
                var row = Convert.ToInt32(_uriMatch.QueryParameters["row"]);
                var col = Convert.ToInt32(_uriMatch.QueryParameters["col"]);

                spec.field = Convert.ToInt32(_uriMatch.QueryParameters["field"]);
                spec.channel = _uriMatch.QueryParameters["channel"];
                spec.timepoint = GetTimepointOrDefault(_uriMatch);
                spec.version = GetVersionOrDefault(_uriMatch);
                spec.z_index = GetZIndexOrDefault(_uriMatch);

                // check if suppress error is requested
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["suppressErr"]))
                {
                    spec.suppressErr = true;
                }

                // set image format 
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["format"]))
                {
                    spec.format = ImageHelper.GetDefaultImageFormat(dbName);
                }
                else
                {
                    spec.reqImageProcess = true;
                    spec.format = _uriMatch.QueryParameters["format"];
                }

                // set invert option 
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["invert"]))
                {
                    spec.reqImageProcess = true;
                    spec.invert = true;
                }

                // set brightness/contrast
                var auto = _uriMatch.QueryParameters["auto"];
                var level = _uriMatch.QueryParameters["level"];
                var b = _uriMatch.QueryParameters["b"];
                if (!String.IsNullOrEmpty(auto) &&
                    (auto.Equals("1") || auto.ToLower().Equals("true")))
                {
                    // TODO: Check value
                    spec.reqImageProcess = true;
                    spec.auto = true;
                }
                else if (!String.IsNullOrEmpty(level))
                {
                    if (!ParseLevels(level, spec))
                    {
                        SetErrorResponse(HttpStatusCode.BadRequest,
                                         "Wrong Parameters in URL",
                                         "Parameter \"level\" must be provided as black_point[,white_point]");
                        return null;
                    }
                }
                else if (!String.IsNullOrEmpty(b))
                {
                    spec.reqImageProcess = true;
                    spec.brightness = Convert.ToDouble(b);
                }

                // TODO: Is this ever used?
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["c"]))
                {
                    spec.reqImageProcess = true;
                    spec.contrast = Convert.ToDouble(_uriMatch.QueryParameters["c"]);
                }

                // TODO: Is this ever used?
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["g"]))
                {
                    spec.reqImageProcess = true;
                    spec.gamma = Convert.ToDouble(_uriMatch.QueryParameters["g"]);
                }

                // create a thumbnail
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["thumb"]))
                {
                    spec.reqImageProcess = true;
                    spec.thumbPixelSize = Convert.ToInt32(_uriMatch.QueryParameters["thumb"]);
                }

                // set color
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["color"]))
                {
                    spec.reqImageProcess = true;
                    spec.color = _uriMatch.QueryParameters["color"].ToUpper();
                }

                imagestream = _connect.GetImageCacheMetadata(dbName , pl , row , col , spec);
            }
            catch (ImageNotFoundException ex)
            {
                ConnectLogger.Error(this.GetType().Name + ":GetImageCacheMetadata, **ImageNotFoundException " + ex.Message);

                if (!spec.reqImageProcess)
                {
                    string err = "Image path not found in the database.";
                    ConnectLogger.Debug(this.GetType().Name + ":GetImageCacheMetadata, *** " + err);
                    SetErrorResponse(System.Net.HttpStatusCode.BadRequest,
                        err, err);
                }
            }
            catch (Exception eX)
            {
                // return error message if suppressErr is false
                if (!spec.suppressErr)
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Bad image request.",
                                     "Failed to download " + spec.format + " image from " + dbName + ":" + eX.ToString());
                }
                ConnectLogger.Error(this.GetType().Name + ":GetImageCacheMetadata, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":GetImageCacheMetadata, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetImage
        ///  - Return an image object as stream
        /// </summary>
        /// <returns></returns> 
        public Stream GetImage()
        {
            Stream imagestream = null;
            var spec = new ImageSpec();
            var dbName = string.Empty;

            try
            {
                // check if plate id, row, col, field, and channel are provided
                if ((String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]) &&
                     String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"])) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["row"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["col"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["field"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["channel"]))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                        "Missing Parameters in URL",
                        "Missing Parameters in URL: plateid (or barcode), row, col, field, and channel are required.");
                    return null;
                }
                // extract parameters
                var row = Convert.ToInt32(_uriMatch.QueryParameters["row"]);
                var col = Convert.ToInt32(_uriMatch.QueryParameters["col"]);
                var pl = new Plate();

                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                {
                    pl.barcode = _uriMatch.QueryParameters["barcode"];
                }
                else
                {
                    pl.plateId = _uriMatch.QueryParameters["plateid"];
                }

                // check if db name is specified as part of the URL
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    // search specified database directly
                    dbName = _uriMatch.QueryParameters["db"];
                }
                else // find the database id to continue
                {
                    dbName = _connect.FindDatabase(pl);
                }

                // create well data
                var w = new Well();
                w.row = row;
                w.col = col;

                spec.field = Convert.ToInt32(_uriMatch.QueryParameters["field"]);
                spec.channel = _uriMatch.QueryParameters["channel"];
                spec.timepoint = GetTimepointOrDefault(_uriMatch);
                spec.version = GetVersionOrDefault(_uriMatch);
                spec.z_index = GetZIndexOrDefault(_uriMatch);

                // check if suppress error is requested
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["suppressErr"]))
                {
                    spec.suppressErr = true;
                }

                // set image format 
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["format"]))
                {
                    HCSDatabaseAccess obj = _connect.GetDatabaseInstance(dbName);
                    spec.format = obj.GetDefaultImageFormat();
                }
                else
                {
                    spec.reqImageProcess = true;
                    spec.format = _uriMatch.QueryParameters["format"];
                }

                // set invert option 
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["invert"]))
                {
                    spec.reqImageProcess = true;
                    spec.invert = true;
                }

                // set brightness/contrast
                var auto = _uriMatch.QueryParameters["auto"];
                var level = _uriMatch.QueryParameters["level"];
                var b = _uriMatch.QueryParameters["b"];
                if (!String.IsNullOrEmpty(auto) &&
                    (auto.Equals("1") || auto.ToLower().Equals("true")))
                {
                    // TODO: Check value
                    spec.reqImageProcess = true;
                    spec.auto = true;
                }
                else if (!String.IsNullOrEmpty(level))
                {
                    if (!ParseLevels(level, spec))
                    {
                        SetErrorResponse(HttpStatusCode.BadRequest,
                                         "Wrong Parameters in URL",
                                         "Parameter \"level\" must be provided as black_point[,white_point]");
                        return null;
                    }
                }
                else if (!String.IsNullOrEmpty(b))
                {
                    spec.reqImageProcess = true;
                    spec.brightness = Convert.ToDouble(b);
                }

                // TODO: used? => Set contrast
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["c"]))
                {
                    spec.reqImageProcess = true;
                    spec.contrast = Convert.ToDouble(_uriMatch.QueryParameters["c"]);
                }

                // TODO: used? => Set gamma
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["g"]))
                {
                    spec.reqImageProcess = true;
                    spec.gamma = Convert.ToDouble(_uriMatch.QueryParameters["g"]);
                }

                // create a thumbnail
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["thumb"]))
                {
                    spec.reqImageProcess = true;
                    spec.thumbPixelSize = Convert.ToInt32(_uriMatch.QueryParameters["thumb"]);
                }

                // set color
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["color"]))
                {
                    spec.reqImageProcess = true;
                    spec.color = _uriMatch.QueryParameters["color"].ToUpper();
                }

                string cacheKey = _uriMatch.RequestUri.AbsoluteUri;

                imagestream = _connect.GetImage(dbName, pl, row, col, spec, cacheKey);
            }
            catch (ImageNotFoundException eX)
            {
                if (!spec.reqImageProcess)
                {
                    string err = "Image path not found in the database.";
                    SetErrorResponse(System.Net.HttpStatusCode.BadRequest,
                        err, err);
                }
                ConnectLogger.Error(this.GetType().Name + ":GetImage, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":GetImage, " + eX.Message, eX);
            }
            catch (Exception eX)
            {
                // return error message if suppressErr is false
                if (!spec.suppressErr)
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                        "Bad image request.",
                        "Failed to download " + spec.format + " image from " + dbName + ":" + eX.ToString());
                }
                ConnectLogger.Error(this.GetType().Name + ":GetImage, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":GetImage, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetImageMedia
        ///  - MEDIA method. No oracle connections. 
        ///    Return an image object as stream
        /// </summary>
        /// <returns></returns>
        public Stream GetImageMedia()
        {
            //TODO: duplicated logic with GetImage(), refactor.
            Stream imagestream = null;
            var spec = new ImageSpec();
            var dbName = string.Empty;

            try
            {
                // check if plate id, row, col, field, and channel are provided
                if ((String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]) && String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"])) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["row"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["col"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["field"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["channel"]))
                {
                    ConnectLogger.Debug("Missing Parameters in URL");
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Missing Parameters in URL",
                                     "Missing Parameters in URL: plateid (or barcode), row, col, field, and channel are required.");
                    return null;
                }
               
                // extract parameters
                var row = Convert.ToInt32(_uriMatch.QueryParameters["row"]);
                var col = Convert.ToInt32(_uriMatch.QueryParameters["col"]);
                var plate = new Plate();

                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                {
                    plate.barcode = _uriMatch.QueryParameters["barcode"];
                }
                else
                {
                    plate.plateId = _uriMatch.QueryParameters["plateid"];
                }

                // check if db name is specified as part of the URL
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    // search specified database directly
                    dbName = _uriMatch.QueryParameters["db"];
                }
                else // find the database id to continue
                {
                    dbName = _connect.FindDatabase(plate);
                }

                spec.field = Convert.ToInt32(_uriMatch.QueryParameters["field"]);
                spec.channel = _uriMatch.QueryParameters["channel"];
                spec.timepoint = GetTimepointOrDefault(_uriMatch);
                spec.version = GetVersionOrDefault(_uriMatch);
                spec.z_index = GetZIndexOrDefault(_uriMatch);

                // check if suppress error is requested
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["suppressErr"]))
                {
                    spec.suppressErr = true;
                }

                // set image format 
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["format"]))
                {
                    spec.format = ImageHelper.GetDefaultImageFormat(dbName);
                }
                else
                {
                    spec.reqImageProcess = true;
                    spec.format = _uriMatch.QueryParameters["format"];
                }

                // set invert option 
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["invert"]))
                {
                    spec.reqImageProcess = true;
                    spec.invert = true;
                }

                // set brightness/contrast
                var auto = _uriMatch.QueryParameters["auto"];
                var level = _uriMatch.QueryParameters["level"];
                var b = _uriMatch.QueryParameters["b"];
                if (!String.IsNullOrEmpty(auto) &&
                    (auto.Equals("1") || auto.ToLower().Equals("true")))
                {
                    // TODO: Check value
                    spec.reqImageProcess = true;
                    spec.auto = true;
                }
                else if (!String.IsNullOrEmpty(level))
                {
                    if (!ParseLevels(level, spec))
                    {
                        SetErrorResponse(HttpStatusCode.BadRequest,
                                         "Wrong Parameters in URL",
                                         "Parameter \"level\" must be provided as black_point[,white_point]");
                        return null;
                    }
                }
                else if (!String.IsNullOrEmpty(b))
                {
                    spec.reqImageProcess = true;
                    spec.brightness = Convert.ToDouble(b);
                }

                // set contrast 
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["c"]))
                {
                    spec.reqImageProcess = true;
                    spec.contrast = Convert.ToDouble(_uriMatch.QueryParameters["c"]);
                }

                // set gamma 
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["g"]))
                {
                    spec.reqImageProcess = true;
                    spec.gamma = Convert.ToDouble(_uriMatch.QueryParameters["g"]);
                }

                // create a thumbnail
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["thumb"]))
                {
                    spec.reqImageProcess = true;
                    spec.thumbPixelSize = Convert.ToInt32(_uriMatch.QueryParameters["thumb"]);
                }

                // set color
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["color"]))
                {
                    spec.reqImageProcess = true;
                    spec.color = _uriMatch.QueryParameters["color"].ToUpper();
                }

                imagestream = _connect.GetImageMedia(dbName , plate , row , col , spec);

                ConnectLogger.Info(this.GetType().Name + ":GetImageMedia finished");
            }
            catch (ImageNotFoundException eX)
            {
                if (!spec.reqImageProcess)
                {
                    string err = "Image path not found in the database.";
                    SetErrorResponse(System.Net.HttpStatusCode.BadRequest,
                        err, err);
                }
                ConnectLogger.Error(this.GetType().Name + ":GetImageMedia, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":GetImageMedia, " + eX.Message, eX);
            }
            catch (Exception eX)
            {
                // return error message if suppressErr is false
                if (!spec.suppressErr)
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Bad image request.",
                                     "Failed to download " + spec.format + " image from " + dbName + ":" + eX.ToString());
                }
                ConnectLogger.Error(this.GetType().Name + ":GetImageMedia, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":GetImageMedia, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// ParseLevels
        ///  - return false if level params cannot be parsed
        /// </summary>
        /// <param name="levels"></param>
        /// <param name="spec"></param>
        /// <returns></returns>
        private bool ParseLevels(string levels, ImageSpec spec)
        {
            try
            {
                if (!String.IsNullOrEmpty(levels))
                {
                    spec.reqImageProcess = true;
                    spec.level = true;
                    string[] levelParams = levels.Split(',');
                    if (levelParams.Length > 2) return false;
                    spec.blackPoint = Convert.ToInt64(levelParams[0]);
                    spec.whitePoint = (levelParams.Length == 2) ? Convert.ToInt64(levelParams[1]) : long.MaxValue;
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(this.GetType().Name + ":ParseLevels, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":ParseLevels, " + eX.Message, eX);
            }
            return true;
        }

        /// <summary>
        /// OverlayImage
        ///  - Merge images from individual channels into one composite image
        /// </summary>
        /// <returns></returns>
        public Stream OverlayImage()
        {
            Stream imagestream = null;
            var spec = new OverlayImageSpec();
            var dbName = string.Empty;

            try
            {

                // check if plate id, row, col, and field are provided
                if ((String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]) && String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"])) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["row"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["col"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["field"]))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Missing Parameters in URL",
                                     "Missing Parameters in URL: plateid (or barcode), row, col, and field are required.");
                    return null;
                }
                // extract parameters
                var row = Convert.ToInt32(_uriMatch.QueryParameters["row"]);
                var col = Convert.ToInt32(_uriMatch.QueryParameters["col"]);
                var pl = new Plate();

                // assign plate id
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                {
                    // barcode needs to be resolved to a unique plate id
                    Plate plate = new Plate();
                    plate.barcode = _uriMatch.QueryParameters["barcode"];
                    ImageParameters imageParams = _connect.GetImageParameters(dbName, plate, 0, 0);

                    // get the plate id for the first match 
                    if (imageParams.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        pl.plateId = imageParams[0].plateData.plateId;
                    }
                }
                else
                {
                    pl.plateId = _uriMatch.QueryParameters["plateid"];
                }

                // check if db name is specified as part of the URL
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    // search specified database directly
                    dbName = _uriMatch.QueryParameters["db"];
                }
                else // find the database id to continue
                {
                    dbName = _connect.FindDatabase(pl);
                }

                // create well data
                var w = new Well();
                w.row = row;
                w.col = col;

                // determine if error message should be suppressed
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["suppressErr"]))
                {
                    spec.suppressErr = true;
                }

                // extract field parameter from URL and set it in the overlay image object
                spec.field = Convert.ToInt32(_uriMatch.QueryParameters["field"]);
                spec.timepoint = GetTimepointOrDefault(_uriMatch, dbName);
                spec.version = GetVersionOrDefault(_uriMatch);
                spec.z_index = GetZIndexOrDefault(_uriMatch, dbName);

                // get the invert flag
                bool invert = false;
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["invert"]))
                {
                    invert = Convert.ToBoolean(_uriMatch.QueryParameters["invert"]);
                }
                spec.invert = invert;

                // Index used for the parsing of the query string - since the auto value can be used more than once we have to parse 
                // the query string manually
                int iItemIndex = 0;
                string color = string.Empty;

                // Set the start index, it will be rest after each color parsing
                int startIndex = iItemIndex;
                string urlString = _uriMatch.RequestUri.AbsoluteUri;
                // Set the channel parameters
                foreach (ImageSpec colorSpec in spec.imageElements)
                {
                    color = colorSpec.color;
                    string channel = ResolveImageUrl(urlString, color, ref iItemIndex);
                    colorSpec.channel = channel;

                    if (!string.IsNullOrEmpty(channel))
                    {
                        string auto = ResolveImageUrl(urlString, "auto", ref iItemIndex);
                        if (!String.IsNullOrEmpty(auto) && (auto.Equals("1") || auto.ToLower().Equals("true")))
                        {
                            colorSpec.auto = true;
                        }
                        string brightness = ResolveImageUrl(urlString, color + "_b", ref iItemIndex);
                        if (!String.IsNullOrEmpty(brightness))
                        {
                            colorSpec.brightness = Convert.ToDouble(brightness);
                        }

                        colorSpec.invert = invert;
                    }
                    iItemIndex = startIndex;
                }

                // check if image sizing is requested
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["thumb"]))
                {
                    spec.thumbPixelSize = Convert.ToInt32(_uriMatch.QueryParameters["thumb"]);
                }

                // determine image format 
                string format = string.Empty;
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["format"]))
                {
                    format = _uriMatch.QueryParameters["format"];
                }
                spec.format = format;

                string cacheKey = _uriMatch.RequestUri.AbsoluteUri;

                imagestream = _connect.OverlayImage(dbName, pl, w, spec, cacheKey);
            }
            catch (ImageNotFoundException eX)
            {
                // return error message if suppressErr is false
                if (!spec.suppressErr)
                {
                    OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
                ConnectLogger.Error(this.GetType().Name + "OverlayImage, " + eX.Message);
                throw eX;
            }
            catch (Exception eX)
            {
                // return error message if suppressErr is false
                if (!spec.suppressErr)
                {
                    OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
                ConnectLogger.Error(this.GetType().Name + "OverlayImage, " + eX.Message);
                throw new ConnectException(this.GetType().Name + "OverlayImage, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// overlayImageMedia
        ///  - MEDIA method. No oracle connections. TODO: duplicated logic with overlayImage(), refactor.
        ///    Merge images from individual channels into one composite image
        /// </summary>
        /// <returns></returns>
        public Stream OverlayImageMedia()
        {
            Stream imagestream = null;
            var spec = new OverlayImageSpec();
            var specimg = new ImageSpec();
            var dbName = string.Empty;

            try
            {
                // check if plate id, row, col, and field are provided
                if ((String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]) && String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"])) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["row"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["col"]) ||
                    String.IsNullOrEmpty(_uriMatch.QueryParameters["field"]))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Missing Parameters in URL",
                                     "Missing Parameters in URL: db, plateid (or barcode), row, col, and field are required.");
                    return null;
                }

                // extract parameters
                int row = Convert.ToInt32(_uriMatch.QueryParameters["row"]);
                int col = Convert.ToInt32(_uriMatch.QueryParameters["col"]);
                Plate plate = new Plate();

                // assign plate id
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                {
                    plate.barcode = _uriMatch.QueryParameters["barcode"];
                }
                else
                {
                    plate.plateId = _uriMatch.QueryParameters["plateid"];
                }

                // check if db name is specified as part of the URL
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    // search specified database directly
                    dbName = _uriMatch.QueryParameters["db"];
                }
                else // find the database id to continue
                {
                    dbName = _connect.FindDatabase(plate);
                }

                // determine if error message should be suppressed
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["suppressErr"]))
                {
                    spec.suppressErr = true;
                }

                // extract field parameter from URL and set it in the overlay image object
                spec.field = Convert.ToInt32(_uriMatch.QueryParameters["field"]);
                spec.timepoint = GetTimepointOrDefault(_uriMatch);
                spec.version = GetVersionOrDefault(_uriMatch);
                spec.z_index = GetZIndexOrDefault(_uriMatch);

                specimg.field = spec.field;
                specimg.timepoint = spec.timepoint;
                specimg.version = spec.version;
                specimg.z_index = spec.z_index;

                // get the invert flag
                bool invert = false;
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["invert"]))
                {
                    invert = Convert.ToBoolean(_uriMatch.QueryParameters["invert"]);
                }
                spec.invert = invert;

                // Index used for the parsing of the query string - since the auto value can be used more than once we have to parse 
                // the query string manually
                int iItemIndex = 0;
                string color = string.Empty;

                // Set the start index, it will be rest after each color parsing
                int startIndex = iItemIndex;
                string urlString = _uriMatch.RequestUri.AbsoluteUri;
                // Set the channel parameters
                foreach (ImageSpec colorSpec in spec.imageElements)
                {
                    color = colorSpec.color;
                    string channel = ResolveImageUrl(urlString, color, ref iItemIndex);
                    colorSpec.channel = channel;

                    if (!string.IsNullOrEmpty(channel))
                    {
                        string auto = ResolveImageUrl(urlString, "auto", ref iItemIndex);
                        if (!String.IsNullOrEmpty(auto) && (auto.Equals("1") || auto.ToLower().Equals("true")))
                        {
                            colorSpec.auto = true;
                        }
                        string brightness = ResolveImageUrl(urlString, color + "_b", ref iItemIndex);
                        if (!String.IsNullOrEmpty(brightness))
                        {
                            colorSpec.brightness = Convert.ToDouble(brightness);
                        }

                        colorSpec.invert = invert;
                    }
                    iItemIndex = startIndex;
                }

                ////check for channels to be specified in overlay, as e.g. red=[channelname]
                //foreach (ImageSpec colorSpec in spec.imageElements)
                //{
                //    var colorString = colorSpec.color.ToLower(); //e.g. red
                //    var channelString = _uriMatch.QueryParameters[colorString];
                //    if (!String.IsNullOrEmpty(channelString))
                //    {
                //        colorSpec.channel = channelString;

                //        //check if there are channel specific intensity settings
                //        //first, check auto setting
                //        String auto = _uriMatch.QueryParameters[colorString + "auto"];
                //        if (!String.IsNullOrEmpty(auto) && (auto.Equals("1") || auto.ToLower().Equals("true")))
                //        {
                //            colorSpec.auto = true;
                //            continue;
                //        }

                //        //fall back to level setting
                //        String level = _uriMatch.QueryParameters[colorString + "level"];
                //        if (!String.IsNullOrEmpty(level))
                //        {
                //            if (!ParseLevels(level , colorSpec))
                //            {
                //                SetErrorResponse(HttpStatusCode.BadRequest ,
                //                                 "Wrong Parameters in URL" ,
                //                                 "Parameter \"" + colorString + "level\" must be provided as black_point[,white_point]");
                //                return null;
                //            }
                //            continue;
                //        }

                //        //fall back to brightness settings
                //        //first, take any overall brightness setting; otherwise take channel specific setting, e.g. red_b
                //        foreach (String brightnessOption in new String[] { "b" , colorString + "_b" })
                //        {
                //            String brightness = _uriMatch.QueryParameters[brightnessOption];
                //            if (!String.IsNullOrEmpty(brightness))
                //            {
                //                colorSpec.brightness = Convert.ToDouble(brightness);
                //                break;
                //            }
                //        }
                //    }
                //}

                // check if image sizing is requested
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["thumb"]))
                {
                    spec.thumbPixelSize = Convert.ToInt32(_uriMatch.QueryParameters["thumb"]);
                }
                // determine image format 
                string format = ImageHelper.GetDefaultImageFormat(dbName);
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["format"]))
                {
                    format = _uriMatch.QueryParameters["format"];
                }
                spec.format = format;

                string cacheKey = _uriMatch.RequestUri.AbsoluteUri;

                            // check if db name is specified as part of the URL
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    // search specified database directly
                    dbName = _uriMatch.QueryParameters["db"];
                }
                imagestream = _connect.OverlayImageMedia(dbName, plate, row, col, spec, specimg, cacheKey);
            }
            catch (Exception eX)
            {
                // return error message if suppressErr is false
                if (!spec.suppressErr)
                {
                    OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
                ConnectLogger.Error(this.GetType().Name + ":overlayImageMedia, " + eX.Message);
                throw new ConnectException(this.GetType().Name + ":overlayImageMedia, " + eX.Message, eX);
            }

            return imagestream;
        }

        /// <summary>
        /// GetCompoundInfo
        ///  - Look up experimental metadata (including assay and plate logistics) related to a compound from Helios database
        /// </summary>
        /// <returns></returns>
        public CompoundList GetCompoundInfo()
        {
            CompoundList cpdList = null;

            try
            {
                // check if required parameters are provided
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["id"]))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Missing cpd in URL",
                                     "Missing Parameters in URL: id is required.");
                    return null;
                }

                string compoundId = _uriMatch.QueryParameters["id"];
                cpdList = new CompoundList();

                // check which data source (Helios_Prod1, Helios_Prod2..)
                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["source"]))
                {
                    string srcStr = _uriMatch.QueryParameters["source"].ToUpper();
                    // convert string representation of data source to enum type
                    if (Enum.IsDefined(typeof(Helio.DATA_SOURCE), srcStr))
                    {
                        cpdList = _connect.GetCompoundInfo(compoundId, srcStr);
                    }
                    else
                    {
                        SetErrorResponse(HttpStatusCode.BadRequest,
                                         "Incorrect Data Source",
                                         "Unknown Data Source:" + srcStr);
                        OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                    }
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetCompoundInfo, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetCompoundInfo , " + eX.Message, eX);
            }

            return cpdList;
        }

        /// <summary>
        /// GetBaseAddressURL
        ///  - Extract base address of the URL
        /// </summary>
        /// <returns></returns>
        private string GetBaseAddressURL()
        {
            string baseUri = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.BaseUri.ToString();
            baseUri = baseUri.Replace("http://", "");

            // Split the string and Get base address
            string[] UriParts = baseUri.Split('/');
            string baseAddress = UriParts[0];

            return baseAddress;
        }

        /// <summary>
        /// GetChannelsByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public ChannelList GetChannelsByParentPaths(PathsModel pathsModel)
        {
            ChannelList res = new ChannelList();

            try
            {
                res = _connect.GetChannelsByParentPaths(pathsModel);
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":GetChannelsByParentPaths, " + ex.Message);
                throw new ConnectException(GetType().Name + ":GetChannelsByParentPaths, " + ex.Message, ex);
            }

            return res;
        }

        /// <summary>
        /// FindPlatesByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public PlateList FindPlatesByParentPaths(PathsModel pathsModel)
        {
            PlateList plateList = null;

            try
            {
                plateList = _connect.FindPlatesByParentPaths(pathsModel);
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":FindPlatesByParentPaths, " + eX.Message);
                throw new ConnectException(GetType().Name + ":FindPlatesByParentPaths, " + eX.Message, eX);
            }

            return plateList;
        }

        /// <summary>
        /// FindPlatesByParentPaths
        /// </summary>
        /// <param name="pathsModel"></param>
        /// <returns></returns>
        public string[] FindExactPlatesByParentPaths(PathsModel pathsModel)
        {
            string[] uniqueIDs = null;

            ConnectLogger.Info(this.GetType().Name + ":findPlatesByParentPaths started.");

            try
            {
                uniqueIDs = _connect.FindExactPlatesByParentPaths(pathsModel);

                ConnectLogger.Info(this.GetType().Name + ":findPlatesByParentPaths complete");
            }
            catch (Exception ex)
            {
                ConnectLogger.Error(GetType().Name + ":findPlatesByParentPaths, " + ex.Message);
                throw new ConnectException(GetType().Name + ":findPlatesByParentPaths, " + ex.Message, ex);
            }

            return uniqueIDs;
        }

        /// GetMediaData
        ///  - This service returns image path for Media Service
        /// </summary>
        /// <returns></returns>
        public MediaDataList GetMediaData()
        {
            MediaDataList mediaDataList = null;

            try
            {
                if ( (String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]) && String.IsNullOrEmpty(_uriMatch.QueryParameters["barcode"])))
                {
                    SetErrorResponse(HttpStatusCode.BadRequest,
                                     "Missing Parameters in URL",
                                     "Missing Parameters in URL: plateid (barcode) are required.");
                    return null;
                }

                var row = 0;
                var col = 0;
                var spec = new ImageSpec();
                var plate = new Plate();
                var dbName = string.Empty;

                // assign plate id
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["plateid"]))
                {
                    plate.barcode = _uriMatch.QueryParameters["barcode"];
                }
                else
                {
                    plate.plateId = _uriMatch.QueryParameters["plateid"];
                }

                if (!String.IsNullOrEmpty(_uriMatch.QueryParameters["row"]) && !String.IsNullOrEmpty(_uriMatch.QueryParameters["col"]))
                {
                    if (String.IsNullOrEmpty(_uriMatch.QueryParameters["field"]))
                    {
                        SetErrorResponse(HttpStatusCode.BadRequest,
                                         "Missing Parameters in URL",
                                         "Missing Parameters in URL:  field is required.");
                        return null;
                    }
                    row = Convert.ToInt32(_uriMatch.QueryParameters["row"]);
                    col = Convert.ToInt32(_uriMatch.QueryParameters["col"]);

                    spec.field = Convert.ToInt32(_uriMatch.QueryParameters["field"]);
                    spec.channel = _uriMatch.QueryParameters["channel"];
                    spec.timepoint = GetTimepointOrDefault(_uriMatch);
                    spec.version = GetVersionOrDefault(_uriMatch);
                    spec.z_index = GetZIndexOrDefault(_uriMatch);
                }

                // check if database name is provided as part of the URL
                if (String.IsNullOrEmpty(_uriMatch.QueryParameters["db"]))
                {
                    mediaDataList = _connect.GetMediaData(plate , row , col , spec);
                }
                else
                {
                    dbName = _uriMatch.QueryParameters["db"];
                    mediaDataList = _connect.GetMediaData(dbName , plate , row , col , spec);
                }
            }
            catch (Exception eX)
            {
                ConnectLogger.Error(GetType().Name + ":GetMediaData, " + eX.Message);
                throw new ConnectException(GetType().Name + ":GetMediaData," + eX.Message, eX);
            }

            return mediaDataList;
        }

        /// <summary>
        /// ResolveImageUrl
        ///  - QueryString parser helper
        /// </summary>
        /// <param name="urlToBeResolved"></param>
        /// <param name="key"></param>
        /// <param name="iItemIndex"></param>
        /// <returns></returns>
        private string ResolveImageUrl(string urlToBeResolved, string key, ref int iItemIndex)
        {
            string strResult = string.Empty;

            try
            {
                string[] stringSeparators = new string[] { "?", "&", "=" };
                string[] strItems = urlToBeResolved.Split(stringSeparators, StringSplitOptions.None);

                for (int i = iItemIndex; i < strItems.Length; i++)
                {
                    if (strItems[i].Equals(key))
                    {
                        strResult = strItems[++i];
                        iItemIndex += 1;
                        return strResult;
                    }
                    else
                    {
                        //ignore the case when comparing for applications like Spotfire where calls are not uniform)
                        if (String.Equals(strItems[i], key, StringComparison.OrdinalIgnoreCase))
                        {
                            strResult = strItems[++i];
                            iItemIndex += 1;
                            return strResult;
                        }
                    }
                }
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":ResolveImageUrl, " + eX.Message, eX);
            }

            return strResult;
        }
    }
}
