﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Runtime.Serialization;

namespace HCSExplorer.Services.Utils
{
    public static class JsonHelper
    {
        /// <summary>
        /// Serialize
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Serialize<T>(T value)
        {
            Stream stream = new MemoryStream();
            var content = new StreamContent(stream);
            JsonMediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            formatter.WriteToStreamAsync(typeof(T) , value , stream , content , null).Wait();
            stream.Position = 0;
            return content.ReadAsStringAsync().Result;
        }

        /// <summary>
        /// Deserialize
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string str) where T : class
        {
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            JsonMediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            return formatter.ReadFromStreamAsync(typeof(T) , stream , null , null).Result as T;
        }

        /// <summary>
        /// Deserialize
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T Deserialize<T>(Stream stream) where T : class
        {
            try
            {
                JsonMediaTypeFormatter formatter = new JsonMediaTypeFormatter();
                return formatter.ReadFromStreamAsync(typeof(T) , stream , null , null).Result as T;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// ToEnumString
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string ToEnumString<T>(T type)
        {
            var enumType = typeof(T);
            var name = Enum.GetName(enumType , type);
            var enumMemberAttribute = ((EnumMemberAttribute[]) enumType.GetField(name).GetCustomAttributes(typeof(EnumMemberAttribute) , true)).Single();
            return enumMemberAttribute.Value;
        }

        /// <summary>
        /// ToEnum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T ToEnum<T>(string str)
        {
            var enumType = typeof(T);
            foreach (var name in Enum.GetNames(enumType))
            {
                var enumMemberAttribute = ((EnumMemberAttribute[]) enumType.GetField(name).GetCustomAttributes(typeof(EnumMemberAttribute) , true)).Single();
                if (enumMemberAttribute.Value == str) return (T) Enum.Parse(enumType , name);
            }
            return default(T);
        }
    }
}
