﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HCS.Shared.Context;
using HCS.Shared.DTO;

namespace HCSExplorer.Services
{
    public interface IExternalServiceLocator
    {
        string GetDatabasesUrl(IExecutionContext context, string siteId);
        string GetPlatesUrl(IExecutionContext context, string siteId,
            string databaseId, string barcodeUrlEncoded);
        string GetPlatesByPathUrl(IExecutionContext context, string siteId,
            string databaseId, string pathUrlEncoded);
        string GetImageParametersUrl(IExecutionContext context, string siteId,
            string databaseId, string plateId, int row, int col);
        string GetImageParametersByBarcodeUrl(IExecutionContext context, string siteId,
            string databaseId, string barcode, int row, int col);

        string GetWellImageUrl(IExecutionContext context, WellImagesModel model, int fieldNum, int chNum);
        string GetWellOverlayImageUrl(IExecutionContext context, WellImagesModel model, int fieldNum, int chNum, string overlParams);

        string GetPlateImageUrl(IExecutionContext context, PlateImagesModel model, int row, int col);
        string GetPlateOverlayImageUrl(IExecutionContext context, PlateImagesModel model, int row, int col, string overlParams);

        // metadata service
        string GetChannelsByPathsUrl(IExecutionContext context, string siteId);
        string FindPlatesByPathsUrl(IExecutionContext context, string siteId);
    }
}
