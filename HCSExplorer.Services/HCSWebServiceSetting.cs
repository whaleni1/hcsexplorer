﻿using HCS.Shared.Context;

namespace HCSExplorer.Services
{
    public static class HCSWebServiceSetting
    {
        public const string csSITE_BS = "BS";
        public const string csSITE_CA = "CA";
        public const string csSITE_RI = "RI";
        public const string csSITE_ZJ = "ZJ";

        public static string GET_DATABASES = "GetDatabases";
        public static string GET_PLATES = "getPlate";
        public static string GET_IMAGE_PARAMETERS = "GetImageParameters";
        //public static string GET_IMAGE = "GetImage";
        public static string GET_IMAGE = "GetImageCacheMetadata";

        public static string GET_OVERLAY_IMAGE = "overlayImage";
        public static string GET_COMPOUND = "getCompound";

        // metadata service
        public static string GET_CHANNELS_BY_PATHS = "getChannelsByParentPaths";
        public static string FIND_PLATES_BY_PATHS = "findPlatesByParentPaths";

        /// <summary>
        /// GetURLFromSite
        ///  - Get HCS Web Service URL based on site
        /// </summary>
        /// <param name="context"></param>
        /// <param name="site"></param>
        /// <returns></returns>
        public static string GetURLFromSite(IExecutionContext context , string site)
        {
            switch (site)
            {
                // return basel web service
                case csSITE_BS:
                    return context.BSMediaServiceURL;
                // return cambridge web service 
                case csSITE_CA:
                    return context.CAMediaServiceURL;
                // return singapore web service for Singapore datasource
                case csSITE_RI:
                    return context.RIMediaServiceURL;
                // return shanghai web service for Shanghai datasource
                case csSITE_ZJ:
                    return context.ZJMediaServiceURL;
                // set url to null in case of neither BS nor CA selected
                default:
                    return null;
            }
        }

        /// <summary>
        /// GetURLMeta
        /// </summary>
        /// <param name="context"></param>
        /// <param name="site"></param>
        /// <returns></returns>
        public static string GetURLMeta(IExecutionContext context , string site)
        {
            //return context.MetadataServiceURL;

            switch (site)
            {
                // return basel web service
                case csSITE_BS:
                    return context.BSMediaServiceURL;
                // return cambridge web service 
                case csSITE_CA:
                    return context.CAMediaServiceURL;
                // return singapore web service for Singapore datasource
                case csSITE_RI:
                    return context.RIMediaServiceURL;
                // return shanghai web service for Shanghai datasource
                case csSITE_ZJ:
                    return context.ZJMediaServiceURL;
                // set url to null in case of neither BS nor CA selected
                default:
                    return null;
            }
        }

        /// <summary>
        /// GetURLFromHeliosInstance
        ///  - Get HCS Web Service URL based on Helios instance
        ///    Prod1 routes to CHBS, Prod2 routes to USCA
        /// </summary>
        /// <param name="context"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static string GetURLFromHeliosInstance(IExecutionContext context , string instance)
        {
            return GetURLMeta(context ,
                                  instance.Equals("HELIOS_PROD1") ? csSITE_BS : (instance.Equals("HELIOS_PROD2") ? csSITE_CA : ""));
        }

        /// <summary>
        /// MakeUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="strurl"></param>
        /// <param name="method"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string MakeUrl(IExecutionContext context , string strurl , string method , string parameters = null)
        {
            if (string.IsNullOrEmpty(strurl) || string.IsNullOrEmpty(method))
                return null;

            if (!strurl.EndsWith("/")) strurl += "/";
            return strurl + method + ((parameters != null) ? ("?" + parameters) : string.Empty);
        }
    }
}
