﻿using System.Collections.Generic;
using HCS.Shared.DTO;
using HCS.Shared.Context;

namespace HCSExplorer.Services
{
    public interface IDataService
    {
        IList<ObjectBase> GetSites(bool forImageAnalysis = false);
        IList<ObjectBase> GetDatabases(IExecutionContext context, string siteId);
        string GetDatabaseTitle(IExecutionContext context, string db_id);
        IList<Plate> GetPlates(IExecutionContext context, string siteId, string databaseId, string barcodeUrlEncoded);
        IList<Plate> GetPlatesByPath(IExecutionContext context, string siteId, string databaseId, string pathUrlEncoded);
        ImageParameters GetImageLayoutByPlateId(IExecutionContext context, string siteId, string databaseId, string plateId, Well well = null);
        ImageParameters GetImageLayoutByBarcode(IExecutionContext context, string siteId, string databaseId, string barcode, Well well = null);
        ImageResultModel LoadWellImage(IExecutionContext context, string baseUrl, string siteDir, WellImagesModel model, int fieldNum, int chNum, bool overlay);
        ImageResultModel LoadPlateImage(IExecutionContext context, string baseUrl, string siteDir, PlateImagesModel model, int row, int col);
        string LoadImgByUrl(IExecutionContext context, string GetImageUrl, string baseUrl, string siteDir, string db, string plateId, int webClientTimeout = 0);
        bool SaveImage(IExecutionContext context, string serviceUrl, string filePath, int webClientTimeout = 0);
        IList<Compound> GetCompounds(IExecutionContext context, string helios, string compoundId);



        IList<ImageUrlModel> WellImageUrls(IExecutionContext context, string baseUrl, string siteDir, WellImagesModel model);
        IList<ImageUrlModel> PlateImageUrls(IExecutionContext context, string baseUrl, string siteDir, PlateImagesModel model);


        string LoadImgByUrl(IExecutionContext context, ImageUrlModel imageUrl, string baseUrl, string siteDir, int webClientTimeout = 0);

        // metadata methods
        List<string> GetChannelsByPaths(IExecutionContext context, PathsModel pathModel);
    }
}
