﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using HCS.DataAccess.Helpers;
using HCS.Shared.Caching;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.DTO;
using HCS.Shared.Logger;
using HCS.Shared.Utils;
using HCSExplorer.Services.Utils;

namespace HCSExplorer.Services
{
    public class DataService : IDataService
    {
        const string csSITE_BS = "BS";
        const string csSITE_CA = "CA";
        const string csSITE_RI = "RI";
        const string csSITE_ZJ = "ZJ";

        public IExternalServiceLocator ServiceLocator { get; set; }

        /// <summary>
        /// GetSites
        /// </summary>
        /// <param name="forImageAnalysis"></param>
        /// <returns></returns>
        public IList<ObjectBase> GetSites(bool forImageAnalysis = false)
        {
            IList<ObjectBase> res = new List<ObjectBase>
            {
                new ObjectBase {Id = csSITE_BS, Name = "Basel"},
                new ObjectBase {Id = csSITE_CA, Name = "Cambridge"}
                //new ObjectBase {Id = csSITE_ZJ, Name = "Shanghai"}
            };

            if (!forImageAnalysis)
            {
                res.Add(new ObjectBase { Id = csSITE_RI , Name = "Singapore" });
                res.Add(new ObjectBase { Id = csSITE_ZJ , Name = "Shanghai" }); //leave out for now, until future service release to address cross-site Isilon mounts
            }

            return res;
        }

        /// <summary>
        /// GetDatabases
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public IList<ObjectBase> GetDatabases(IExecutionContext context , string siteId)
        {
            List<ObjectBase> databases = new List<ObjectBase>();

            try
            {
                string GetDatabasesUrl = ServiceLocator.GetDatabasesUrl(context , siteId);
                FileLogger.Info(GetType().Name + ":GetDatabases " + GetDatabasesUrl);

                VordelWebClient client = new VordelWebClient
                {
                    Cookie = context.Cookies
                };

                using (StreamReader reader = new StreamReader(client.OpenRead(GetDatabasesUrl)))
                {
                    XDocument xdoc = XDocument.Parse(reader.ReadToEnd());

                    databases = xdoc.Root.Descendants().Where(x => x.Name.LocalName == "database").Select(
                        x => new ObjectBase
                        {
                            Id = x.Elements().First(y => y.Name.LocalName == "id").Value ,
                            Name = x.Elements().First(y => y.Name.LocalName == "description").Value ,
                        }
                        ).ToList();
                }
                client.Dispose();
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":GetDatabases, " + eX.Message , eX);
            }
            return databases;
        }

        /// <summary>
        /// GetDatabaseTitle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="db_id"></param>
        /// <returns></returns>
        public string GetDatabaseTitle(IExecutionContext context , string db_id)
        {
            string strDBName = string.Empty;

            try
            {
                List<ObjectBase> databases = (List<ObjectBase>) HCSCache.GetDatabases();
                if (databases == null)
                {
                    try
                    {
                        string GetDatabasesUrl = ServiceLocator.GetDatabasesUrl(context , null);
                        VordelWebClient client = new VordelWebClient()
                        {
                            Cookie = context.Cookies
                        };

                        using (StreamReader reader = new StreamReader(client.OpenRead(GetDatabasesUrl)))
                        {
                            XDocument xdoc = XDocument.Parse(reader.ReadToEnd());

                            databases = xdoc.Root.Descendants().Where(x => x.Name.LocalName == "database").Select(
                                x => new ObjectBase
                                {
                                    Id = x.Elements().First(y => y.Name.LocalName == "id").Value ,
                                    Name = x.Elements().First(y => y.Name.LocalName == "description").Value ,
                                }
                                ).ToList();
                        }
                        client.Dispose();
                        HCSCache.SetDatabases(databases);
                    }
                    catch (Exception ex)
                    {
                        FileLogger.Error(GetType().Name + ":GetDatabaseTitle, " + ex.Message);
                        throw;
                    }
                }
                if (databases == null)
                    return db_id;
                ObjectBase db = databases.Where(x => x.Id.Equals(db_id , StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (db == null)
                    strDBName = db_id;
                else
                    strDBName = db.Name;
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":GetDatabaseTitle, " + eX.Message , eX);
            }

            return strDBName;
        }

        /// <summary>
        /// GetPlates
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="barcodeUrlEncoded"></param>
        /// <returns></returns>
        public IList<Plate> GetPlates(IExecutionContext context , string siteId , string databaseId ,
                                                                  string barcodeUrlEncoded)
        {
            string getPlatesUrl = ServiceLocator.GetPlatesUrl(context , siteId , databaseId , barcodeUrlEncoded);

            FileLogger.Info(this.GetType().Name + ":GetPlates, getPlatesUrl = " + getPlatesUrl + " siteId = " + siteId);

            return SearchPlates(context , getPlatesUrl , siteId);
        }

        /// <summary>
        /// GetPlatesByPath
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="pathUrlEncoded"></param>
        /// <returns></returns>
        public IList<Plate> GetPlatesByPath(IExecutionContext context , string siteId , string databaseId ,
                                                                   string pathUrlEncoded)
        {
            string getPlatesUrl = ServiceLocator.GetPlatesByPathUrl(context , siteId , databaseId , pathUrlEncoded);

            FileLogger.Info(this.GetType().Name + ":GetPlates, GetPlatesByPath = " + getPlatesUrl + " siteId = " + siteId);

            return SearchPlates(context , getPlatesUrl , siteId);
        }

        /// <summary>
        /// GetImageLayoutByPlateId
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="plateId"></param>
        /// <param name="well"></param>
        /// <returns></returns>
        public ImageParameters GetImageLayoutByPlateId(IExecutionContext context , string siteId , string databaseId ,
                                                                                   string plateId , Well well = null)
        {
            ImageParameters il = null;
            int col = 0;
            int row = 0;

            try
            {
                if (well != null)
                {
                    col = well.Col;
                    row = well.Row;
                }
                string GetImageParametersUrl = ServiceLocator.GetImageParametersUrl(context , siteId , databaseId , plateId , row , col);

                VordelWebClient client = new VordelWebClient()
                {
                    Cookie = context.Cookies
                };
                using (StreamReader reader = new StreamReader(client.OpenRead(GetImageParametersUrl)))
                {
                    XDocument xdoc = XDocument.Parse(reader.ReadToEnd());

                    il = LayoutFromXml(xdoc , siteId);
                }
                client.Dispose();
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":GetImageLayoutByPlateId, " + eX.Message , eX);
            }

            return il;
        }

        /// <summary>
        /// GetImageLayoutByBarcode
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="barcode"></param>
        /// <param name="well"></param>
        /// <returns></returns>
        public ImageParameters GetImageLayoutByBarcode(IExecutionContext context , string siteId , string databaseId ,
                                                                                 string barcode , Well well = null)
        {
            ImageParameters il = null;
            int col = 0;
            int row = 0;

            try
            {
                if (well != null)
                {
                    col = well.Col;
                    row = well.Row;
                }
                string GetImageParametersUrl = ServiceLocator.GetImageParametersByBarcodeUrl(context , siteId , databaseId , barcode , row , col);

                VordelWebClient client = new VordelWebClient()
                {
                    Cookie = context.Cookies
                };
                using (StreamReader reader = new StreamReader(client.OpenRead(GetImageParametersUrl)))
                {
                    XDocument xdoc = XDocument.Parse(reader.ReadToEnd());

                    il = LayoutFromXml(xdoc , siteId);
                }
                client.Dispose();
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":GetImageLayoutByBarcode, " + eX.Message , eX);
            }
            return il;
        }

        /// <summary>
        /// LayoutFromXml
        /// </summary>
        /// <param name="xdoc"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private static ImageParameters LayoutFromXml(XDocument xdoc , string siteId)
        {
            ImageParameters il = null;

            try
            {
                var plate = xdoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName == "plate");
                var wellout = xdoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName == "well");
                var fields = xdoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName == "fields");
                var channels = xdoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName == "channels");
                var versions = xdoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName == "versions");
                var zcount = xdoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName == "zCount");
                var tcount = xdoc.Root.Descendants().FirstOrDefault(x => x.Name.LocalName == "timepointCount");
                if (plate != null)
                {
                    il = new ImageParameters
                    {
                        Id = int.Parse(plate.Elements().First(y => y.Name.LocalName == "plateId").Value) ,
                        Barcode = plate.Elements().First(y => y.Name.LocalName == "barcode").Value ,
                        DateSrc = plate.Elements().First(y => y.Name.LocalName == "timestamp").Value
                    };

                    il.Date = DateTimeUtils.ParseDateTime(il.DateSrc , siteId);
                    //Test Service does not have Format,Path,Name fields !!!!!
                    int format;
                    bool parsed = int.TryParse(plate.Elements().First(y => y.Name.LocalName == "format").Value , out format);
                    il.Format = parsed ? format : 96;

                    il.Path = plate.Elements().FirstOrDefault(y => y.Name.LocalName == "path").Value;
                    il.Name = plate.Elements().FirstOrDefault(y => y.Name.LocalName == "name").Value;
                    if (string.IsNullOrEmpty(il.Name))
                        il.Name = "NoName";

                    if (zcount != null)
                    {
                        int zIndexCount;
                        parsed = int.TryParse(zcount.Value , out zIndexCount);
                        il.ZIndexCount = parsed ? zIndexCount : 0;
                    }
                    if (tcount != null)
                    {
                        int timepointCount;
                        parsed = int.TryParse(tcount.Value , out timepointCount);
                        il.TimepointCount = parsed ? timepointCount : 0;
                    }

                    if (wellout != null)
                    {
                        il.Well = new Well
                        {
                            Col = int.Parse(wellout.Elements().First(y => y.Name.LocalName == "col").Value) ,
                            Row = int.Parse(wellout.Elements().First(y => y.Name.LocalName == "row").Value)
                        };
                    }
                    if (fields != null)
                    {
                        il.Fields = fields.Descendants().Where(x => x.Name.LocalName == "field").Select(
                            x => x.Value).ToList();
                    }
                    if (channels != null)
                    {
                        il.Channels = channels.Descendants().Where(x => x.Name.LocalName == "channel").Select(
                            x => x.Value).ToList();
                    }

                    if (versions != null)
                    {
                        il.Versions = versions.Descendants().Where(x => x.Name.LocalName == "version").Select(
                            x => x.Value).ToList();
                    }
                }
                else
                {
                    il = new ImageParameters();
                }
            }
            catch (Exception eX)
            {
                throw new CException("DataService:LayoutFromXml, " + eX.Message , eX);
            }

            return il;
        }

        /// <summary>
        /// LoadImgByUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="imageUrl"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="webClientTimeout"></param>
        /// <returns></returns>
        public string LoadImgByUrl(IExecutionContext context , ImageUrlModel imageUrl , string baseUrl , string siteDir , int webClientTimeout = 0)
        {
            string path = null;

            try
            {
                string plateKey = imageUrl.PlateKey;
                string platePath = Path.Combine(siteDir , plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }

                string fileKey = imageUrl.FileKey;
                string fileName = imageUrl.FileName;
                FileLogger.Debug(this.GetType().Name + ":LoadImgByUrl-->fileKey: " + fileKey + ", fileName: " + ", imageUrl.Url " + imageUrl.Url + ", siteDir: " + siteDir);
                bool found = SynchSaveImage(context , imageUrl.Url , fileName , webClientTimeout);
                if (found)
                {
                    path = baseUrl + "/" + plateKey + "/" + fileKey;
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":LoadImgByUrl, " + eX.Message , eX);
            }

            return path;
        }

        /// <summary>
        /// LoadImgByUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="GetImageUrl"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="db"></param>
        /// <param name="plateId"></param>
        /// <param name="webClientTimeout"></param>
        /// <returns></returns>
        public string LoadImgByUrl(IExecutionContext context , string GetImageUrl , string baseUrl , string siteDir , string db , string plateId , int webClientTimeout = 0)
        {
            string path = null;

            try
            {
                string plateKey = ImageUtils.GetPlateDir(db , plateId);
                string platePath = Path.Combine(siteDir , plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }

                string fileKey = ImageUtils.GetFileKey(GetImageUrl , ImageUtils.IMAGE_FORMAT);
                string fileName = Path.Combine(platePath ,
                                               fileKey);
                bool found = SynchSaveImage(context , GetImageUrl , fileName , webClientTimeout);
                if (found)
                {
                    path = baseUrl + "/" + plateKey + "/" + fileKey;
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":LoadImgByUrl, " + eX.Message , eX);
            }

            return path;
        }

        /// <summary>
        /// LoadWellImage
        /// </summary>
        /// <param name="context"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="model"></param>
        /// <param name="fieldNum"></param>
        /// <param name="chNum"></param>
        /// <param name="overlay"></param>
        /// <returns></returns>
        public ImageResultModel LoadWellImage(IExecutionContext context , string baseUrl , string siteDir , WellImagesModel model , int fieldNum , int chNum ,
                                    bool overlay = false)
        {
            string path = null;
            string overlParams = null;
            ImageResultModel imgResultModel = null;
            string GetImageUrl = null;

            try
            {
                if (!overlay)
                {
                    GetImageUrl = ServiceLocator.GetWellImageUrl(context , model , fieldNum , chNum);
                }
                else
                {
                    overlParams = ImageUtils.OverlayParams(model.ChannelParameters);
                    GetImageUrl = ServiceLocator.GetWellOverlayImageUrl(context , model , fieldNum , chNum , overlParams);
                }

                string plateKey = ImageUtils.GetPlateDir(model.DatabaseId , model.Id.ToString());
                string platePath = Path.Combine(siteDir , plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }
                string fileKey = ImageUtils.GetFileKey(model.Well.Row.ToString() , model.Well.Col.ToString() ,
                                                       model.Fields[fieldNum] , overlay ? "overlay" : model.Channels[chNum] ,
                                                       overlParams , model.Invert.ToString() ,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].Brightness ,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].Level,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].Auto,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].WhitePoint,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].BlackPoint,
                                                       overlay ? null : model.ChannelParameters[chNum].Color ,
                                                       model.SizeId , model.ZIndex == null ? null : model.ZIndex.Value.ToString() ,
                                                       model.Timepoint == null ? null : model.Timepoint.Value.ToString() ,
                                                       model.Version ,
                                                       ImageUtils.IMAGE_FORMAT);
                string fileName = Path.Combine(platePath ,
                                               fileKey);
                bool found = SynchSaveImage(context , GetImageUrl , fileName);

                if (found)
                {
                    path = baseUrl + "/" + plateKey + "/" + fileKey;
                    imgResultModel = new ImageResultModel
                    {
                        Path = path ,
                        GetImgUrl = GetImageUrl
                    };
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":LoadWellImage, " + eX.Message , eX);
            }
            return imgResultModel;
        }

        /// <summary>
        /// PlateImageUrls
        /// </summary>
        /// <param name="context"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public IList<ImageUrlModel> PlateImageUrls(IExecutionContext context , string baseUrl , string siteDir , PlateImagesModel model)
        {
            List<ImageUrlModel> result = new List<ImageUrlModel>();
            string overlParams = null;

            try
            {
                bool overlay = "Overlay".Equals(model.PlateChannelParameters.Channel);
                string plateKey = ImageUtils.GetPlateDir(model.DatabaseId , model.Id.ToString());
                string platePath = Path.Combine(siteDir , plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }

                for (int i = 0 ; i < model.Rows ; i++)
                {
                    for (int j = 0 ; j < model.Cols ; j++)
                    {
                        ImageUrlModel murl = new ImageUrlModel
                        {
                            Row = i ,
                            Col = j ,
                            Overlay = overlay ,
                            PlateKey = plateKey ,
                            ZIndex = model.ZIndex ,
                            Timepoint = model.Timepoint ,
                            Version = model.Version
                        };

                        if (!overlay)
                        {
                            murl.Url = ServiceLocator.GetPlateImageUrl(context , model , i , j);

                        }
                        else
                        {
                            overlParams = ImageUtils.OverlayParams(model.ChannelParameters);
                            murl.Url = ServiceLocator.GetPlateOverlayImageUrl(context , model , i , j , overlParams);
                        }

                        murl.FileKey = ImageUtils.GetFileKey((i + 1).ToString() , (j + 1).ToString() ,
                                                               model.Field ,
                                                               overlay ? "overlay" : model.PlateChannelParameters.Channel ,
                                                               overlParams , model.Invert.ToString() ,
                                                               overlay ? string.Empty : model.PlateChannelParameters.Brightness ,
                                                               overlay ? string.Empty : model.PlateChannelParameters.Level,
                                                               overlay ? string.Empty : model.PlateChannelParameters.Auto,
                                                               overlay ? string.Empty : model.PlateChannelParameters.BlackPoint,
                                                               overlay ? string.Empty : model.PlateChannelParameters.WhitePoint,
                                                               model.PlateChannelParameters.Color ,
                                                               model.SizeId , model.ZIndex == null ? null : model.ZIndex.Value.ToString() ,
                                                               model.Timepoint == null ? null : model.Timepoint.Value.ToString() ,
                                                               model.Version , ImageUtils.IMAGE_FORMAT);
                        murl.FileName = Path.Combine(platePath , murl.FileKey);

                        if (File.Exists(murl.FileName))
                        {
                            murl.Loaded = true;
                            File.SetLastAccessTimeUtc(murl.FileName , DateTime.UtcNow);
                        }
                        result.Add(murl);
                    }
                }
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":PlateImageUrls, " + eX.Message , eX);
            }
            return result;
        }

        /// <summary>
        /// WellImageUrls
        /// </summary>
        /// <param name="context"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public IList<ImageUrlModel> WellImageUrls(IExecutionContext context , string baseUrl , string siteDir , WellImagesModel model)
        {
            List<ImageUrlModel> result = new List<ImageUrlModel>();
            string overlParams = null;
            string auto = string.Empty;

            try
            {
                string plateKey = ImageUtils.GetPlateDir(model.DatabaseId , model.Id.ToString());
                string platePath = Path.Combine(siteDir , plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }

                for (int i = 0 ; i < model.Fields.Count ; i++)
                {
                    for (int j = 0 ; j < model.Channels.Count ; j++)
                    {
                        ImageUrlModel murl = new ImageUrlModel
                        {
                            Row = model.Well.Row ,
                            Col = model.Well.Col ,
                            FieldIndex = i ,
                            ChannelIndex = j ,
                            PlateKey = plateKey ,
                            ZIndex = model.ZIndex ,
                            Timepoint = model.Timepoint ,
                            Version = model.Version

                        };

                        murl.Url = ServiceLocator.GetWellImageUrl(context , model , i , j);

                        murl.FileKey = ImageUtils.GetFileKey(model.Well.Row.ToString() , model.Well.Col.ToString() ,
                                                       model.Fields[i] , model.Channels[j] ,
                                                       overlParams , model.Invert.ToString() ,
                                                       model.ChannelParameters[j].Brightness ,
                                                       model.ChannelParameters[j].Level ,
                                                       model.ChannelParameters[j].Auto ,
                                                       model.ChannelParameters[j].Color ,
                                                       model.ChannelParameters[j].WhitePoint ,
                                                       model.ChannelParameters[j].BlackPoint ,
                                                       model.SizeId , model.ZIndex == null ? null : model.ZIndex.Value.ToString() ,
                                                       model.Timepoint == null ? null : model.Timepoint.Value.ToString() ,
                                                       model.Version ,
                                                       ImageUtils.IMAGE_FORMAT);
                        murl.FileName = Path.Combine(platePath , murl.FileKey);

                        if (string.IsNullOrEmpty(auto) || model.ChannelParameters[j].Auto.Contains("true"))
                            auto = "true";


                            if (File.Exists(murl.FileName))
                        {
                            murl.Loaded = true;
                            File.SetLastAccessTimeUtc(murl.FileName , DateTime.UtcNow);
                        }
                        result.Add(murl);
                    }
                    //overlay
                    if (model.Channels.Count > 1)
                    {
                        if (model.EnableOverlay)
                        {
                            ImageUrlModel murl = new ImageUrlModel
                            {
                                Row = model.Well.Row ,
                                Col = model.Well.Col ,
                                FieldIndex = i ,
                                ChannelIndex = -1 ,
                                Overlay = true ,
                                PlateKey = plateKey,
                                ZIndex = model.ZIndex ,
                                Timepoint = model.Timepoint ,
                                Version = model.Version
                            };
                            overlParams = ImageUtils.OverlayParams(model.ChannelParameters);
                            murl.Url = ServiceLocator.GetWellOverlayImageUrl(context , model , i , 0 , overlParams);

                            murl.FileKey = ImageUtils.GetFileKey(   model.Well.Row.ToString() , 
                                                                    model.Well.Col.ToString() ,
                                                                    model.Fields[i] , "overlay" ,
                                                                    overlParams , model.Invert.ToString() ,
                                                                    string.Empty ,
                                                                    string.Empty ,
                                                                    auto ,
                                                                    null ,
                                                                    string.Empty,
                                                                    string.Empty,
                                                                    model.SizeId ,
                                                                    model.ZIndex == null ? null : model.ZIndex.Value.ToString() ,
                                                                    model.Timepoint == null ? null : model.Timepoint.Value.ToString() ,
                                                                    model.Version ,
                                                                    ImageUtils.IMAGE_FORMAT);
                            murl.FileName = Path.Combine(platePath ,
                                                       murl.FileKey);

                            if (File.Exists(murl.FileName))
                            {
                                murl.Loaded = true;
                            }
                            result.Add(murl);
                        }
                    }
                }
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":WellImageUrls, " + eX.Message , eX);
            }

            return result;
        }

        /// <summary>
        /// LoadWellImageUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="model"></param>
        /// <param name="fieldNum"></param>
        /// <param name="chNum"></param>
        /// <param name="overlay"></param>
        /// <returns></returns>
        public ImageResultModel LoadWellImageUrl(IExecutionContext context , string baseUrl , string siteDir , WellImagesModel model , int fieldNum , int chNum ,
                               bool overlay = false)
        {
            ImageResultModel imgResultModel = null;
            string path = null;
            string overlParams = null;
            string GetImageUrl = null;

            try
            {
                if (!overlay)
                {
                    GetImageUrl = ServiceLocator.GetWellImageUrl(context , model , fieldNum , chNum);
                }
                else
                {
                    overlParams = ImageUtils.OverlayParams(model.ChannelParameters);
                    GetImageUrl = ServiceLocator.GetWellOverlayImageUrl(context , model , fieldNum , chNum , overlParams);
                }

                string plateKey = ImageUtils.GetPlateDir(model.DatabaseId , model.Id.ToString());
                string platePath = Path.Combine(siteDir , plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }
                string fileKey = ImageUtils.GetFileKey(model.Well.Row.ToString() , model.Well.Col.ToString() ,
                                                       model.Fields[fieldNum] , overlay ? "overlay" : model.Channels[chNum] ,
                                                       overlParams , model.Invert.ToString() ,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].Brightness ,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].Level ,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].Auto ,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].WhitePoint,
                                                       overlay ? string.Empty : model.ChannelParameters[chNum].BlackPoint,
                                                       overlay ? null : model.ChannelParameters[chNum].Color ,
                                                       model.SizeId , model.ZIndex == null ? null : model.ZIndex.Value.ToString() ,
                                                       model.Timepoint == null ? null : model.Timepoint.Value.ToString() ,
                                                       model.Version , ImageUtils.IMAGE_FORMAT);
                string fileName = Path.Combine(platePath ,
                                               fileKey);
                bool found = SynchSaveImage(context , GetImageUrl , fileName);
                if (found)
                {
                    path = baseUrl + "/" + plateKey + "/" + fileKey;
                    imgResultModel = new ImageResultModel
                    {
                        Path = path ,
                        GetImgUrl = GetImageUrl
                    };
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":LoadWellImageUrl, " + eX.Message , eX);
            }

            return imgResultModel;
        }

        /// <summary>
        /// LoadPlateImage
        /// </summary>
        /// <param name="context"></param>
        /// <param name="baseUrl"></param>
        /// <param name="siteDir"></param>
        /// <param name="model"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public ImageResultModel LoadPlateImage(IExecutionContext context , string baseUrl , string siteDir , PlateImagesModel model , int row , int col)
        {
            ImageResultModel imgResultModel = null;
            string path = null;
            bool overlay = "Overlay".Equals(model.PlateChannelParameters.Channel);
            string overlParams = null;
            string GetImageUrl = null;

            try
            {
                if (!overlay)
                {
                    GetImageUrl = ServiceLocator.GetPlateImageUrl(context , model , row , col);
                }
                else
                {
                    overlParams = ImageUtils.OverlayParams(model.ChannelParameters);
                    GetImageUrl = ServiceLocator.GetPlateOverlayImageUrl(context , model , row , col , overlParams);
                }

                string plateKey = ImageUtils.GetPlateDir(model.DatabaseId , model.Id.ToString());
                string platePath = Path.Combine(siteDir , plateKey);
                if (!Directory.Exists(platePath))
                {
                    Directory.CreateDirectory(platePath);
                }
                string fileKey = ImageUtils.GetFileKey(row.ToString() , col.ToString() ,
                                                       model.Field ,
                                                       overlay ? "overlay" : model.PlateChannelParameters.Channel ,
                                                       overlParams , model.Invert.ToString() ,
                                                       overlay ? string.Empty : model.PlateChannelParameters.Brightness ,
                                                       overlay ? string.Empty : model.PlateChannelParameters.Level ,
                                                       overlay ? string.Empty : model.PlateChannelParameters.Auto ,
                                                       overlay ? string.Empty : model.PlateChannelParameters.WhitePoint,
                                                       overlay ? string.Empty : model.PlateChannelParameters.BlackPoint,
                                                       model.PlateChannelParameters.Color ,
                                                       model.SizeId , model.ZIndex == null ? null : model.ZIndex.Value.ToString() ,
                                                       model.Timepoint == null ? null : model.Timepoint.Value.ToString() ,
                                                       model.Version , ImageUtils.IMAGE_FORMAT);
                string fileName = Path.Combine(platePath ,
                                               fileKey);

                bool found = SynchSaveImage(context , GetImageUrl , fileName);
                if (found)
                {
                    path = baseUrl + "/" + plateKey + "/" + fileKey;
                    imgResultModel = new ImageResultModel
                    {
                        Path = path ,
                        GetImgUrl = GetImageUrl
                    };
                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":LoadPlateImage, " + eX.Message , eX);
            }
            return imgResultModel;
        }

        /// <summary>
        /// GetCompounds
        /// </summary>
        /// <param name="context"></param>
        /// <param name="helios"></param>
        /// <param name="compoundId"></param>
        /// <returns></returns>
        public IList<Compound> GetCompounds(IExecutionContext context , string helios , string compoundId)
        {
            List<Compound> compounds = null;
            string getCpdUrl = string.Empty;

            try
            {
                helios = helios.ToUpper();

                getCpdUrl = HCSWebServiceSetting.MakeUrl(context ,
                                                                HCSWebServiceSetting.GetURLFromHeliosInstance(context ,
                                                                                                              helios) ,
                                                                HCSWebServiceSetting.GET_COMPOUND ,
                                                                "source=" + helios + "&id=" + compoundId.ToUpper());

                VordelWebClient client = new VordelWebClient
                {
                    Cookie = context.Cookies
                };
                using (StreamReader reader = new StreamReader(client.OpenRead(getCpdUrl)))
                {
                    XDocument xdoc = XDocument.Parse(reader.ReadToEnd());

                    compounds = xdoc.Root.Descendants().Where(x => x.Name.LocalName == "compound").Select(
                        x => new Compound
                        {
                            Barcode = x.Elements().First(y => y.Name.LocalName == "plateBarcode").Value ,
                            Assay = x.Elements().First(y => y.Name.LocalName == "assay").Value ,
                            Id = x.Elements().First(y => y.Name.LocalName == "compoundId").Value ,
                            InstrumentId = x.Descendants().Where(y => y.Name.LocalName == "id").First().Value ,
                            Instrument = x.Descendants().Where(y => y.Name.LocalName == "description").First().Value ,
                            Row = int.Parse(x.Descendants().Where(y => y.Name.LocalName == "row").First().Value) ,
                            Col = int.Parse(x.Descendants().Where(y => y.Name.LocalName == "col").First().Value)
                        }
                        ).ToList();

                    foreach (Compound c in compounds)
                    {
                        string siteId = null;
                        string db = null;
                        ResolveSiteAndDB(c.InstrumentId , out siteId , out db);
                        if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(db))
                        {
                            c.DatabaseName = db;
                            c.HasLink = true;
                            c.SiteId = siteId;
                        }
                    }
                }
                client.Dispose();
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":GetCompounds URL=, " + getCpdUrl + ", Error=" + eX.Message , eX);
            }

            return compounds;
        }

        /// <summary>
        /// SaveImage
        /// </summary>
        /// <param name="context"></param>
        /// <param name="serviceUrl"></param>
        /// <param name="filePath"></param>
        /// <param name="webClientTimeout"></param>
        /// <returns></returns>
        public bool SaveImage(IExecutionContext context , string serviceUrl , string filePath , int webClientTimeout = 0)
        {
            return SynchSaveImage(context , serviceUrl , filePath , webClientTimeout);
        }

        /// <summary>
        /// SynchSaveImage
        /// </summary>
        /// <param name="context"></param>
        /// <param name="url"></param>
        /// <param name="fileName"></param>
        /// <param name="webClientTimeout"></param>
        /// <returns></returns>
        private bool SynchSaveImage(IExecutionContext context , string url , string fileName , int webClientTimeout = 0)
        {
            bool found = false;

            try
            {
                lock (string.Intern(fileName))
                {
                    if (!File.Exists(fileName))
                    {
                        found = SaveImageInternal(context , url , fileName , webClientTimeout);
                    }
                    else
                    {
                        found = true;
                    }

                }
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":SynchSaveImage, " + eX.Message , eX);
            }
            return found;
        }

        /// <summary>
        /// SaveImageInternal
        /// </summary>
        /// <param name="context"></param>
        /// <param name="serviceUrl"></param>
        /// <param name="filePath"></param>
        /// <param name="webClientTimeout"></param>
        /// <returns></returns>
        private bool SaveImageInternal(IExecutionContext context , string serviceUrl , string filePath , int webClientTimeout = 0)
        {
            bool result = false;

            try
            {
                FileLogger.Debug(this.GetType().Name + ":SaveImageInternal, serviceUrl=" + serviceUrl);

                VordelWebClient client = new VordelWebClient
                {
                    Cookie = context.Cookies
                };
                if (webClientTimeout > 0)
                {
                    client.Timeout = webClientTimeout;
                }
                using (Stream strm = client.OpenRead(serviceUrl))
                {
                    string contentType = client.ResponseHeaders["Content-Type"];
                    if (ImageUtils.IMAGE_CONTENT_TYPE.Equals(contentType , StringComparison.OrdinalIgnoreCase)
                        || "application/octet-stream".Equals(contentType , StringComparison.OrdinalIgnoreCase) //for test
                        )
                    {
                        using (FileStream writeStream = new FileStream(filePath , FileMode.Create , FileAccess.Write))
                        {
                            int Length = 256;
                            byte[] buffer = new byte[Length];
                            int bytesRead = strm.Read(buffer , 0 , Length);
                            // write the required bytes
                            while (bytesRead > 0)
                            {
                                writeStream.Write(buffer , 0 , bytesRead);
                                bytesRead = strm.Read(buffer , 0 , Length);
                            }
                            writeStream.Close();
                            result = true;
                        }
                    }
                    else
                    {
                        //  "text/html; charset=utf-8"
                        // not found in plate
                        using (StreamReader reader = new StreamReader(strm))
                        {
                            string ss = reader.ReadToEnd();
                            FileLogger.Debug(this.GetType().Name + ":SaveImageInternal, Image not found: " + ss);
                        }
                    }
                }
                client.Dispose();
            }
            catch (ImageNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception eX)
            {
                FileLogger.Error(this.GetType().Name + ":SaveImageInternal, " + eX.Message + ", serviceUrl=" + serviceUrl + ", filePath=" + filePath);
                throw new CException(this.GetType().Name + ":SaveImageInternal, " + eX.Message , eX);
            }
            return result;
        }

        /// <summary>
        /// ResolveSiteAndDB
        /// </summary>
        /// <param name="instrumentId"></param>
        /// <param name="site"></param>
        /// <param name="db"></param>
        public static void ResolveSiteAndDB(string instrumentId , out string site , out string db)
        {
            site = null;
            db = null;

            switch (instrumentId)
            {
                // HCS:OPERA (CHBS)
                case "1000559662": //OPERA_HCS_1
                    site = csSITE_BS;
                    db = "OPERA_1";
                    break;

                // HCS:INCELL_ANALYZER (CHBS)
                case "17": //INCELLANALYSER_1                // CPC (Lab Gabriel InCell 3000) // No HCS integration
                    site = csSITE_BS;
                    db = "INCELL_1";
                    break;

                case "1000933673": //INCELLANALYSER_6                // CPC (Lab Gabriel)
                case "1001309711":
                    //INCELLANALYSER_12               // DMP (Lab Parker)  //no HCS integration in Helios 
                    site = csSITE_BS;
                    db = "INCELL_2";
                    break;

                // HCS:INCELL_ANALYZER (USCA)
                case "80": //INCELLANALYSER_2
                case "1000425634": //INCELLANALYSER_3
                case "1000425635": //INCELLANALYSER_4
                case "1000425636": //INCELLANALYSER_5
                case "1000933674": //INCELLANALYSER_7
                case "1000985375": //INCELLANALYSER_8
                case "1001060417": //INCELLANALYSER_9                // Fast Lab
                case "1001278484": //INCELLANALYSER_10
                case "1001278565": //INCELLANALYSER_11               // NOT FAST Lab
                    //case "1001785361":    //INCELLANALYSER_13             // ONC  //no HCS integration in Helios
                    site = csSITE_CA;
                    db = "INCELL_1";
                    break;

                // HCS:CELLOMICS_1 (CHBS)
                case "1001171260": //CELLOMICS_5                     //Onc Markus Wartmann
                case "1001171349": //CELLOMICS_6                     //Onc Markus Wartmann
                    site = csSITE_BS;
                    db = "CELLOMICS_1";
                    break;

                // HCS:CELLOMICS_2 (CHBS) ???

                // HCS:CELLOMICS_3 (CHBS)
                case "1001800435": //CELLOMICS_8                     //EPK Patrick Schweigler
                    site = csSITE_BS;
                    db = "CELLOMICS_3";
                    break;
            }
        }

        /// <summary>
        /// SearchPlates
        /// </summary>
        /// <param name="context"></param>
        /// <param name="getPlatesUrl"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private static IList<Plate> SearchPlates(IExecutionContext context , string getPlatesUrl , string siteId)
        {
            IList<Plate> plates = new List<Plate>();
            XmlDocument xmldoc = new XmlDocument();
            XmlElement root = xmldoc.DocumentElement;

            try
            {
                FileLogger.Info("DataService:SearchPlates URL: " + getPlatesUrl);

                VordelWebClient client = new VordelWebClient()
                {
                    Cookie = context.Cookies
                };

                using (Stream strm = client.OpenRead(getPlatesUrl))
                {
                    xmldoc.Load(strm);
                    root = xmldoc.DocumentElement;

                    // extract barcodes from returned xml
                    XmlNodeList barcodeList = root.GetElementsByTagName("barcode");

                    // extract barcodes from returned xml
                    XmlNodeList formatList = root.GetElementsByTagName("format");

                    // extract plateid's from returned xml
                    XmlNodeList pidList = root.GetElementsByTagName("plateId");
                    int pidCount = pidList.Count;

                    // extract names from returned xml
                    XmlNodeList nameList = root.GetElementsByTagName("name");

                    // extract timestamps from returned xml
                    XmlNodeList tsList = root.GetElementsByTagName("timestamp");

                    // extract paths from returned xml
                    XmlNodeList pathList = root.GetElementsByTagName("path");

                    FileLogger.Info("DataService:SearchPlates total number of plates found [" + pidCount + "]");

                    for (int i = 0 ; i < pidCount ; i++)
                    {
                        int plateid = pidList[i].InnerText == string.Empty ? 0 : Convert.ToInt32(pidList[i].InnerText);
                        string barcode = barcodeList[i].InnerText;
                        string pname = nameList[i].InnerText;
                        string ts = tsList[i].InnerText;
                        DateTime? dt = DateTimeUtils.ParseDateTime(ts , siteId);
                        string path = pathList[i].InnerText;
                        int format = formatList[i].InnerText == string.Empty ? 0 : Convert.ToInt32(formatList[i].InnerText);
                        plates.Add(new Plate
                        {
                            Id = plateid ,
                            Barcode = barcode ,
                            Name = pname ,
                            DateSrc = ts ,
                            Path = path ,
                            Format = format ,
                            Date = dt
                        });
                    }

                    FileLogger.Info("DataService:SearchPlates total number of Plate List in [" + plates.Count + "]");
                }
                client.Dispose();

                FileLogger.Info("DataService:SearchPlates finished");
            }
            catch (Exception eX)
            {
                throw new CException("DataService:SearchPlates, URL=" + getPlatesUrl + ", Error=" + eX.Message , eX);
            }

            return plates;
        }

        // HCSC Metadata methods

        /// <summary>
        /// GetChannelsByPaths
        ///  - Looks up for plates by given plate paths and get channels for first found plate.
        /// </summary>
        /// <param name="context">Current user's context</param>
        /// <param name="pathModel">Data model, contains list of paths and databaseId</param>
        /// <returns>List of channels for first found plate</returns>
        public List<string> GetChannelsByPaths(IExecutionContext context , PathsModel pathModel)
        {
            List<string> channels = null;

            try
            {
                string getChannelsUrl = ServiceLocator.GetChannelsByPathsUrl(context, pathModel.SiteId);

                using (VordelWebClient client = new VordelWebClient
                {
                    Cookie = context.Cookies
                })
                {
                    client.Headers.Add(HttpRequestHeader.ContentType , "application/json");
                    client.Encoding = System.Text.Encoding.UTF8;

                    string json = JsonHelper.Serialize(pathModel);
                    string response = client.UploadString(getChannelsUrl , json);

                    channels = JsonHelper.Deserialize<List<string>>(response);
                }
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":GetChannelsByPaths, " + eX.Message , eX);
            }
            return channels;
        }

    }
}
