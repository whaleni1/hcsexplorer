﻿using System;
using System.Net;
using HCS.Shared;
using HCS.Shared.Logger;

namespace HCSExplorer.Services
{
    public class VordelWebClient : WebClient
    {
        public VordelWebClient()
        {
            UseDefaultCredentials = true;
        }

        /// <summary>
        /// Timeout
        /// </summary>
        public int Timeout { get; set; }

        /// <summary>
        /// Cookie
        /// </summary>
        public string Cookie
        {
            get
            {
                return this.Headers[HttpRequestHeader.Cookie];
            }
            set
            {
                this.Headers.Remove(HttpRequestHeader.Cookie);
                if (!string.IsNullOrEmpty(value))
                    this.Headers.Add(HttpRequestHeader.Cookie , value);


                this.Headers.Add("REMOTE_USER" , User.UserName);
                //FileLogger.Info(GetType().Name + ":VordelWebClient, Add header => Headers.Add(REMOTE_USER ," + User.UserName + ")");
            }
        }

        /// <summary>
        /// GetWebRequest
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        protected override WebRequest GetWebRequest(Uri address)
        {
            var objWebRequest = base.GetWebRequest(address);
            if (Timeout > 0)
                objWebRequest.Timeout = Timeout;

            return objWebRequest;
        }
    }
}
