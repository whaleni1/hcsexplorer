﻿using HCS.Shared.Context;
using HCS.Shared.DTO;

namespace HCSExplorer.Services
{
    public class ExternalServiceLocator : IExternalServiceLocator
    {
        /// <summary>
        /// GetDatabasesUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public string GetDatabasesUrl(IExecutionContext context , string siteId)
        {
            return HCSWebServiceSetting.MakeUrl(context ,
                    HCSWebServiceSetting.GetURLMeta(context , siteId) ,
                    HCSWebServiceSetting.GET_DATABASES ,
                     "site=" + siteId
                    );
        }

        /// <summary>
        /// GetPlatesUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="barcodeUrlEncoded"></param>
        /// <returns></returns>
        public string GetPlatesUrl(IExecutionContext context , string siteId ,
             string databaseId ,
             string barcodeUrlEncoded)
        {

            return HCSWebServiceSetting.MakeUrl(context ,
                     HCSWebServiceSetting.GetURLMeta(context , siteId) ,
                     HCSWebServiceSetting.GET_PLATES ,
                     "db=" + databaseId + "&barcode=" + barcodeUrlEncoded);
        }

        /// <summary>
        /// GetPlatesByPathUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="pathUrlEncoded"></param>
        /// <returns></returns>
        public string GetPlatesByPathUrl(IExecutionContext context , string siteId ,
          string databaseId ,
          string pathUrlEncoded)
        {

            return HCSWebServiceSetting.MakeUrl(context ,
                HCSWebServiceSetting.GetURLMeta(context , siteId) ,
                HCSWebServiceSetting.GET_PLATES ,
                "db=" + databaseId + "&path=" + pathUrlEncoded);
        }

        /// <summary>
        /// GetImageParametersUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="plateId"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public string GetImageParametersUrl(IExecutionContext context , string siteId ,
          string databaseId , string plateId , int row , int col)
        {

            return HCSWebServiceSetting.MakeUrl(context ,
                    HCSWebServiceSetting.GetURLMeta(context , siteId) ,
                    HCSWebServiceSetting.GET_IMAGE_PARAMETERS ,
                    "db=" + databaseId + "&plateId=" + plateId +
                    "&col=" + col + "&row=" + row);
        }

        /// <summary>
        /// GetImageParametersByBarcodeUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="siteId"></param>
        /// <param name="databaseId"></param>
        /// <param name="barcode"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public string GetImageParametersByBarcodeUrl(IExecutionContext context , string siteId ,
          string databaseId , string barcode , int row , int col)
        {

            return HCSWebServiceSetting.MakeUrl(context ,
                    HCSWebServiceSetting.GetURLMeta(context , siteId) ,
                    HCSWebServiceSetting.GET_IMAGE_PARAMETERS ,
                    "db=" + databaseId + "&barcode=" + barcode +
                    "&col=" + col + "&row=" + row);
        }

        /// <summary>
        /// GetWellImageUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="model"></param>
        /// <param name="fieldNum"></param>
        /// <param name="chNum"></param>
        /// <returns></returns>
        public string GetWellImageUrl(IExecutionContext context , WellImagesModel model , int fieldNum , int chNum)
        {

            return HCSWebServiceSetting.MakeUrl(context ,
                        HCSWebServiceSetting.GetURLFromSite(context , model.SiteId) ,
                        HCSWebServiceSetting.GET_IMAGE ,
                        ImageUtils.ImageUrlParams(model.DatabaseId ,
                                                    model.Id.ToString() , model.Well.Row ,
                                                    model.Well.Col ,
                                                    model.Fields[fieldNum] ,
                                                    model.ChannelParameters[chNum] ,
                                                    model.SizeId , model.Invert , model.ZIndex , model.Timepoint , model.Version));
        }

        /// <summary>
        /// GetWellOverlayImageUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="model"></param>
        /// <param name="fieldNum"></param>
        /// <param name="chNum"></param>
        /// <param name="overlParams"></param>
        /// <returns></returns>
        public string GetWellOverlayImageUrl(IExecutionContext context , WellImagesModel model , int fieldNum , int chNum , string overlParams)
        {
            return HCSWebServiceSetting.MakeUrl(context ,
                     HCSWebServiceSetting.GetURLFromSite(context , model.SiteId) ,
                     HCSWebServiceSetting.GET_OVERLAY_IMAGE ,
                     ImageUtils.OverlayImageUrlParams(model.DatabaseId ,
                                                     model.Id.ToString() ,
                                                     model.Well.Row ,
                                                     model.Well.Col ,
                                                     model.Fields[fieldNum] ,
                                                     model.SizeId , overlParams ,
                                                     model.ZIndex , model.Timepoint , model.Version));
        }

        /// <summary>
        /// GetPlateImageUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="model"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public string GetPlateImageUrl(IExecutionContext context , PlateImagesModel model , int row , int col)
        {
            return HCSWebServiceSetting.MakeUrl(context ,
                        HCSWebServiceSetting.GetURLFromSite(context , model.SiteId) ,
                        HCSWebServiceSetting.GET_IMAGE ,
                        ImageUtils.ImageUrlParams(model.DatabaseId ,
                                                    model.Id.ToString() , row + 1 , col + 1 ,
                                                    model.Field ,
                                                    model.PlateChannelParameters ,
                                                    model.SizeId , model.Invert , model.ZIndex , model.Timepoint , model.Version));
        }

        /// <summary>
        /// GetPlateOverlayImageUrl
        /// </summary>
        /// <param name="context"></param>
        /// <param name="model"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="overlParams"></param>
        /// <returns></returns>
        public string GetPlateOverlayImageUrl(IExecutionContext context , PlateImagesModel model , int row , int col , string overlParams)
        {
            return HCSWebServiceSetting.MakeUrl(context ,
                                                    HCSWebServiceSetting.GetURLFromSite(context , model.SiteId) ,
                                                    HCSWebServiceSetting.GET_OVERLAY_IMAGE ,
                                                    ImageUtils.OverlayImageUrlParams(model.DatabaseId ,
                                                                model.Id.ToString() , row + 1 ,
                                                                col + 1 ,
                                                                model.Field ,
                                                                model.SizeId , overlParams ,
                                                                model.ZIndex , model.Timepoint , model.Version));
        }

        /// <summary>
        /// GetChannelsByPathsUrl
        ///  - Metadata
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public string GetChannelsByPathsUrl(IExecutionContext context, string siteId)
        {
            return HCSWebServiceSetting.MakeUrl(context , HCSWebServiceSetting.GetURLMeta(context , siteId) , HCSWebServiceSetting.GET_CHANNELS_BY_PATHS);
        }

        /// <summary>
        /// FindPlatesByPathsUrl
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public string FindPlatesByPathsUrl(IExecutionContext context, string siteId)
        {
            return HCSWebServiceSetting.MakeUrl(context , HCSWebServiceSetting.GetURLMeta(context , siteId) , HCSWebServiceSetting.FIND_PLATES_BY_PATHS);
        }
    }
}
