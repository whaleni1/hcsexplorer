﻿using HCSExplorer.Services.MC.Model;
using HCSExplorer.Services.Utils;
using HCS.Shared.Context;
using HCS.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace HCSExplorer.Services.MC
{
    public static class MCHelper
    {
        /// <summary>
        /// CalcDetailsToRuns
        /// </summary>
        /// <param name="context"></param>
        /// <param name="calcs"></param>
        /// <returns></returns>
        public static List<Run> CalcDetailsToRuns(IExecutionContext context, List<CalcDetail> calcs)
        {
            List<Run> res = new List<Run>();

            foreach (CalcDetail c in calcs)
            {
                res.Add(CalcDetailToRun(context, c));
            }

            return res;
        }

        /// <summary>
        /// CalcDetailToRun
        /// </summary>
        /// <param name="context"></param>
        /// <param name="calc"></param>
        /// <returns></returns>
        private static Run CalcDetailToRun(IExecutionContext context, CalcDetail calc)
        {
            Run r = new Run();

            r.CalcId = calc.CalcId;
            //r.Group = "1";

            r.Status = GetStatus(calc.Status);

            r.LastUpdated = calc.ModifiedDate;
            r.CreatedDate = calc.CreateDate;

            foreach (Param p in calc.Params)
            {
                switch (p.Name)
                {
                    case MCConstants.PARAM_RUN_ID:
                        //r.RunId = p.Value;
                        int id;
                        int.TryParse(p.Value, out id);
                        r.Id = id;
                        break;
                    case MCConstants.PARAM_RUN_NAME:
                        r.Name = p.Value;
                        break;
                    case MCConstants.PARAM_USER_ID:
                        r.CreatedBy = p.Value;
                        /*if (!string.IsNullOrEmpty(r.CreatedBy))
                        {
                            r.Group = context.UserName.ToUpper().Equals(r.CreatedBy.ToUpper()) ? "0" : "1";
                        }*/
                        break;
                    case MCConstants.PARAM_PROTOCOL_NAME:
                        r.ProtocolName = p.Value;
                        break;
                    case MCConstants.PARAM_OUTPUT_FILES:
                        SetOutputFiles(p.Value, r);
                        break;
                    case MCConstants.PARAM_IMAGE_LIST_ONLY:
                        r.ImageListSupported = true;
                        bool v;
                        bool.TryParse(p.Value, out v);
                        r.ImageListOnly = v;
                        break;
                }
            }

            return r;
        }

        /// <summary>
        /// CalcDetailToRunDetail
        /// </summary>
        /// <param name="context"></param>
        /// <param name="calc"></param>
        /// <returns></returns>
        public static RunDetails CalcDetailToRunDetail(IExecutionContext context, CalcDetail calc)
        {
           
            RunDetails r = RunToRunDetails(CalcDetailToRun(context, calc));

            Task calcTask = calc.GetCalcTask();
            if (calcTask != null)
                r.Note = calcTask.Note;
            if (calcTask != null && !string.IsNullOrEmpty(calcTask.Feedback))
                r.Feedback = calcTask.Feedback;
            else
                r.Feedback = calc.Feedback;

            foreach (Param p in calc.Params)
            {
                switch (p.Name)
                {
                    case MCConstants.PARAM_RUN_DESCRIPTION:
                        r.Description = p.Value;
                        break;
                    case MCConstants.PARAM_DATASOURCE_ID:
                        r.DatabaseId = p.Value;
                        break;
                    case MCConstants.PARAM_DATA_LOCATION:
                        r.DataLocation = p.Value;
                        break;
                    case MCConstants.PARAM_WAVELENGTH_MAP:
                        r.WavelengthMap = StrToWMap(p.Value).ToArray();
                        break;
                    case MCConstants.PARAM_BLOCK_SIZE:
                        int b;
                        int.TryParse(p.Value, out b);
                        r.Block = b;
                        break;
                    case MCConstants.PARAM_POST_PROCESSING_TYPE:
                        SetPostProcessingType(p.Value, r);
                        break;
                    case MCConstants.PARAM_PROTOCOL_REFERENCE:
                        r.ProtocolRef = p.Value;
                        break;
                    case MCConstants.PARAM_IMAGE_LIST_NAME:
                        r.ImageListName = p.Value;
                        break;
                    case MCConstants.PARAM_IMAGE_LIST_REFERENCE:
                        r.ImageListRef = p.Value;
                        break;
                    case MCConstants.PARAM_IMAGES_INPUT_TYPE:
                        ImagesInputType imgInputType;
                        Enum.TryParse(p.Value, out imgInputType);
                        r.ImagesInputType = imgInputType;
                        break;
                }
            }

            if (string.IsNullOrEmpty(r.ProtocolRef))
            {
                r.ProtocolRef = string.Empty;
                r.ProtocolName = string.Empty;
            }

            List<Task> plateTasks = calc.GetPlateTasks();
            List<PlateInfo> plates = new List<PlateInfo>();
            foreach (Task t in plateTasks)
            {
                PlateInfo plate = TaskToPlate(t);
                plates.Add(plate);
                if (plate.Status == PlateStatus.Complete)
                {
                    r.CompletePlatesCount++;
                }
            }
            r.Plates = plates.OrderBy(p => p.Barcode).ToArray();
            r.TotalPlatesCount = plates.Count;

            List<Dir> dirs = new List<Dir>();
            List<string> resultFiles = new List<string>();

            foreach (TaskObjIO t in calc.TaskObjIOs)
            {
                if (MCConstants.CALC_REF_ID.Equals(t.TaskRefId))
                {
                    if (t.Groups.Count == 0)
                        continue;
                    foreach (Model.Group gr in t.Groups)
                    {
                        foreach (InputObj obj in gr.dataObjects)
                        {
                            if (MCConstants.IO_RULE_OUT.Equals(obj.Rule))
                            {
                                if (MCConstants.IO_TYPE_FILE.Equals(obj.Type))
                                {
                                    if (!string.IsNullOrEmpty(obj.Name))
                                    {
                                        resultFiles.Add(obj.Name);
                                    }
                                }
                                else if (MCConstants.IO_TYPE_SHAREDIR.Equals(obj.Type))
                                {
                                    r.OutputFolder = obj.Name;
                                }
                            }
                            else if (MCConstants.IO_RULE_INP.Equals(obj.Rule) && MCConstants.IO_TYPE_DIR.Equals(obj.Type))
                            {
                                dirs.Add(PathToDir(obj.Name));
                            }
                        }
                    }
                }
            }

            r.Dirs = dirs.ToArray();
            r.Results = resultFiles.ToArray();

            return r;
        }

        public static string GetSiteIdFromDBId(string dbId)
        {
            string site = "";
            switch (dbId)
            {
                case MCConstants.INCELL_1:
                    site = "CA";
                    break;
                case MCConstants.YOKOGAWA_3:
                    site = "CA";
                    break;
                case MCConstants.PHENIX_1:
                    site = "CA";
                    break;
                case MCConstants.INCELL_2:
                    site = "BS";
                    break;
                case MCConstants.YOKOGAWA_1:
                    site = "BS";
                    break;
                case MCConstants.YOKOGAWA_2:
                    site = "BS";
                    break;
                case MCConstants.CELLOMICS_CNZJ_1:
                    site = "ZJ";
                    break;
            }
            return site;
        }
        /// <summary>
        /// RunToRunDetails
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        private static RunDetails RunToRunDetails(Run r)
        {
            RunDetails rd = new RunDetails();
            rd.Id = r.Id;
            //rd.RunId = r.RunId;
            rd.CalcId = r.CalcId;
            rd.Name = r.Name;
            rd.Status = r.Status;
            rd.ProtocolName = r.ProtocolName;
            rd.CreatedBy = r.CreatedBy;
            rd.ImageListOnly = r.ImageListOnly;
            rd.InOneFile = r.InOneFile;
            rd.Icp4 = r.Icp4;
            rd.CreatedDate = r.CreatedDate;
            rd.LastUpdated = r.LastUpdated;
            rd.ImageListSupported = r.ImageListSupported;
            return rd;
        }

        /// <summary>
        /// TaskToPlate
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static PlateInfo TaskToPlate(Task t)
        {
            PlateInfo plate = new PlateInfo
            {
                Barcode = t.RefId,
                Status = GetPlateStatus(t.Status),
                Detail = t.Note,
                Progress = GetPlateProgress(t.Feedback),
                LastUpdated = t.Timestamp
            };
            if (plate.Status == PlateStatus.Complete)
            {
                plate.Progress = 100;
            }
            return plate;
        }

        /// <summary>
        /// GetPlateProgress
        /// </summary>
        /// <param name="strFeedback"></param>
        /// <returns></returns>
        private static int GetPlateProgress(string strFeedback)
        {
            int res = 0;
            int.TryParse(strFeedback, out res);
            return res;
        }

        /// <summary>
        /// GetStatus
        /// </summary>
        /// <param name="mcStatus"></param>
        /// <returns></returns>
        private static Status GetStatus(string mcStatus)
        {
            Status status;
            switch (mcStatus)
            {
                case "WAIT":
                    status = Status.Validating;
                    break;
                case "PROCESSING":
                    status = Status.OnCluster;
                    break;
                case "FAILED":
                    status = Status.Error;
                    break;
                case "DONE":
                    status = Status.Complete;
                    break;
                case "WARNING":
                    status = Status.Warning;
                    break;
                case "TIMEOUT":
                    status = Status.Timeout;
                    break;
                default:
                    status = Status.Undefined;
                    break;
            }
            return status;
        }

        /// <summary>
        /// GetPlateStatus
        /// </summary>
        /// <param name="mcStatus"></param>
        /// <returns></returns>
        private static PlateStatus GetPlateStatus(string mcStatus)
        {
            PlateStatus status;
            switch (mcStatus)
            {
                case "WAIT":
                    status = PlateStatus.Validating;
                    break;
                case "PROCESSING":
                    status = PlateStatus.OnCluster;
                    break;
                case "FAILED":
                    status = PlateStatus.Error;
                    break;
                case "DONE":
                    status = PlateStatus.Complete;
                    break;
                case "WARNING":
                    status = PlateStatus.Warning;
                    break;
                case "TIMEOUT":
                    status = PlateStatus.Timeout;
                    break;
                default:
                    status = PlateStatus.Undefined;
                    break;
            }
            return status;
        }

        /// <summary>
        /// WMapToStr
        /// </summary>
        /// <param name="wMap"></param>
        /// <returns></returns>
        public static string WMapToStr(WMapItem[] wMap)
        {
            if (wMap == null || wMap.Length == 0)
                return string.Empty;

            StringBuilder sb = new StringBuilder();

            bool first = true;
            foreach (WMapItem wm in wMap)
            {
                if (!first)
                {
                    sb.Append("/");
                }
                sb.Append(String.Format("{0}:{1}", wm.BiologicalConcept, wm.Channel));
                first = false;
            }

            return sb.ToString();
        }

        /// <summary>
        /// StrToWMap
        /// </summary>
        /// <param name="mapStr"></param>
        /// <returns></returns>
        public static List<WMapItem> StrToWMap(string mapStr)
        {
            List<WMapItem> res = new List<WMapItem>();

            if (string.IsNullOrEmpty(mapStr))
                return res;

            string[] pairs = mapStr.Split('/');
            foreach (string p in pairs)
            {
                if (string.IsNullOrEmpty(p))
                    continue;
                string[] tmp = p.Split(':');
                if (tmp.Length != 2)
                    continue;
                WMapItem item = new WMapItem
                {
                    BiologicalConcept = tmp[0],
                    Channel = tmp[1]
                };
                res.Add(item);
            }
            return res;
        }

        /// <summary>
        /// PathToDir
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Dir PathToDir(string path)
        {
            string name = path.Substring(path.LastIndexOf("\\") + 1);
            Dir dir = new Dir
            {
                Name = name,
                Path = path
            };
            return dir;
        }

        /// <summary>
        /// OutputFilesToStr
        /// </summary>
        /// <param name="rd"></param>
        /// <returns></returns>
        public static string OutputFilesToStr(RunDetails rd)
        {
            string res = string.Empty;

            if (rd.InOneFile)
            {
                res += MCConstants.OUTPUT_ONE_FILE + MCConstants.DELIMITER;
            }

            if (rd.Icp4)
            {
                res += MCConstants.OUTPUT_ICP4 + MCConstants.DELIMITER;
            }

            return res;
        }

        /// <summary>
        /// SetOutputFiles
        /// </summary>
        /// <param name="str"></param>
        /// <param name="r"></param>
        public static void SetOutputFiles(string str, Run r)
        {
            if (string.IsNullOrEmpty(str))
                return;

            r.InOneFile = false;
            r.Icp4 = false;

            string[] types = str.Split(MCConstants.DELIMITER);

            foreach (string s in types)
            {
                switch (s)
                {
                    case MCConstants.OUTPUT_ONE_FILE:
                        r.InOneFile = true;
                        break;
                    case MCConstants.OUTPUT_ICP4:
                        r.Icp4 = true;
                        break;
                }
            }
        }

        /// <summary>
        /// PostProcessingTypeToStr
        /// </summary>
        /// <param name="rd"></param>
        /// <returns></returns>
        public static string PostProcessingTypeToStr(RunDetails rd)
        {
            string res = string.Empty;

            if (rd.PpCountsSummed)
            {
                res += JsonHelper.ToEnumString(PostProcessingType.CountsSummed) + MCConstants.DELIMITER;
            }

            if (rd.PpMeanMeanIntensity)
            {
                res += JsonHelper.ToEnumString(PostProcessingType.MeanMeanIntensity) + MCConstants.DELIMITER;
            }

            if (rd.PpMedianMedianIntensity)
            {
                res += JsonHelper.ToEnumString(PostProcessingType.MedianMedianIntensity) + MCConstants.DELIMITER;
            }

            if (rd.PpStdDeviationMeanIntensity)
            {
                res += JsonHelper.ToEnumString(PostProcessingType.StdDeviationMeanIntensity) + MCConstants.DELIMITER;
            }

            return res;
        }

        /// <summary>
        /// SetPostProcessingType
        /// </summary>
        /// <param name="str"></param>
        /// <param name="rd"></param>
        private static void SetPostProcessingType(string str, RunDetails rd)
        {
            if (string.IsNullOrEmpty(str))
                return;

            rd.PostProcessingEnabled = true;

            string[] types = str.Split(MCConstants.DELIMITER);

            foreach (string s in types)
            {
                if (string.IsNullOrEmpty(s))
                    continue;

                PostProcessingType type = JsonHelper.ToEnum<PostProcessingType>(s);
                switch (type)
                {
                    case PostProcessingType.CountsSummed:
                        rd.PpCountsSummed = true;
                        break;
                    case PostProcessingType.MeanMeanIntensity:
                        rd.PpMeanMeanIntensity = true;
                        break;
                    case PostProcessingType.MedianMedianIntensity:
                        rd.PpMedianMedianIntensity = true;
                        break;
                    case PostProcessingType.StdDeviationMeanIntensity:
                        rd.PpStdDeviationMeanIntensity = true;
                        break;
                }
            }
        }
    }
}
