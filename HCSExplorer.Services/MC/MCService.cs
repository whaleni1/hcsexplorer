﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using HCS.Shared.Common;
using HCS.Shared.Context;
using HCS.Shared.DTO;
using HCS.Shared.Filtering;
using HCS.Shared.Logger;
using HCSExplorer.Services.MC.Model;
using HCSExplorer.Services.Utils;

namespace HCSExplorer.Services.MC
{
    public class MCService : IMCService
    {
        /// <summary>
        /// GetRuns
        /// </summary>
        /// <param name="context"></param>
        /// <param name="pagingModel"></param>
        /// <returns></returns>
        public RunListPagingModel GetRuns(IExecutionContext context , RunListPagingModel pagingModel)
        {
            try
            {
                List<Run> res;

                bool isMy = pagingModel.Filter.My;
                int from = pagingModel.Paging.From;
                int pageSize = pagingModel.Paging.To - pagingModel.Paging.From;

                ListFilter filter = new ListFilter
                {
                    Constraints = new Constraints
                    {
                        MethodIds = new List<string> { MCConstants.CA_METHOD_GUID , MCConstants.BS_METHOD_GUID , MCConstants.ZJ_METHOD_GUID } ,
                        Param = new FilterParam
                        {
                            Name = MCConstants.PARAM_USER_ID ,
                            Value = context.UserName ,
                            Operator = pagingModel.Filter.My ? MCConstants.OPERATOR_EQUAL : MCConstants.OPERATOR_NOT_EQUAL
                        }
                    } ,
                    StartIndex = from + 1 ,
                    PageSize = pageSize ,
                    ReturnListCount = true
                };

                ObjectLoadingRule rule = new ObjectLoadingRule
                {
                    LoadParamDetail = true ,
                    LoadCalcDetail = true ,
                    LoadTaskDetail = false
                };

                string jsonFilter = JsonHelper.Serialize(filter);
                string jsonRule = JsonHelper.Serialize(rule);

                NameValueCollection values = new NameValueCollection();
                VordelWebClient client = new VordelWebClient
                {
                    Cookie = context.Cookies
                };

                values.Add(MCConstants.FORMPARAM_QUERY , jsonFilter);
                values.Add(MCConstants.FORMPARAM_OBJECT_LOAD_RULE , jsonRule);

                string listUrl = HCSWebServiceSetting.MakeUrl(null , context.MCServiceURL , MCConstants.METHOD_LIST , null);

                byte[] response = client.UploadValues(listUrl , values);

                string resp = Encoding.UTF8.GetString(response);

                CalcDetails calcs = JsonHelper.Deserialize<CalcDetails>(resp);

                res = MCHelper.CalcDetailsToRuns(context , calcs.Details);

                pagingModel.Paging.To = from + res.Count;
                pagingModel.Rows = res.ToArray();
                pagingModel.Count = calcs.TotalCount;


            }
            catch (System.Net.WebException ex)
            {
                FileLogger.Error(this.GetType().Name + ":GetRuns, " + ex.Message);
                Error er = null;
                if (ex.Response != null)
                    er = JsonHelper.Deserialize<Error>(ex.Response.GetResponseStream());
                String errorMsg = er == null ? MCConstants.MC_UNAVAILABLE_MESSAGE : er.ErrorMessage;
                throw new CException(this.GetType().Name + ":GetRuns, " + errorMsg , ex);
            }

            return pagingModel;
        }

        /// <summary>
        /// GetRun
        /// </summary>
        /// <param name="context"></param>
        /// <param name="calcId"></param>
        /// <returns></returns>
        public RunDetails GetRun(IExecutionContext context , string calcId)
        {
            RunDetails r = new RunDetails();

            try
            {
                CalcIDs ids = new CalcIDs
                {
                    ids = new List<string> { calcId }
                };

                ObjectLoadingRule rule = new ObjectLoadingRule
                {
                    LoadTaskDetail = true ,
                    LoadParamDetail = true ,
                    LoadCalcDetail = true ,
                    LoadTaskObjIODetail = true
                };

                string jsonIds = JsonHelper.Serialize(ids);
                string jsonRule = JsonHelper.Serialize(rule);

                NameValueCollection values = new NameValueCollection();
                VordelWebClient client = new VordelWebClient
                {
                    Cookie = context.Cookies
                };

                values.Add(MCConstants.FORMPARAM_CALC_IDS , jsonIds);
                values.Add(MCConstants.FORMPARAM_OBJECT_LOAD_RULE , jsonRule);

                string detailUrl = HCSWebServiceSetting.MakeUrl(null , context.MCServiceURL , MCConstants.METHOD_DETAIL , null);

                byte[] response = client.UploadValues(detailUrl , values);

                string resp = Encoding.UTF8.GetString(response);

                CalcDetails calcs = JsonHelper.Deserialize<CalcDetails>(resp);

                if (calcs == null || calcs.Details == null || calcs.Details.Count == 0 || string.IsNullOrEmpty(calcs.Details[0].MethodId))
                    return null;

                CalcDetail calcDetail = calcs.Details[0];
                
                r = MCHelper.CalcDetailToRunDetail(context , calcDetail);
            }
            catch (System.Net.WebException ex)
            {
                FileLogger.Error(this.GetType().Name + ":GetRun, " + ex.Message);
                Error er = null;
                if (ex.Response != null)
                    er = JsonHelper.Deserialize<Error>(ex.Response.GetResponseStream());
                String errorMsg = er == null ? MCConstants.MC_UNAVAILABLE_MESSAGE : er.ErrorMessage;
                throw new CException(this.GetType().Name + ":GetRun, " + errorMsg , ex);
            }

            return r;
        }

        /// <summary>
        /// SubmitRun
        /// </summary>
        /// <param name="context"></param>
        /// <param name="run"></param>
        public void SubmitRun(IExecutionContext context , RunDetails run)
        {
            try
            {
                List<MCCalc> calcs = new List<MCCalc>();
                calcs.Add(CreateSubmitCalc(run));

                Calcs res = new Calcs
                {
                    MCCalcs = calcs.ToArray()
                };
                string json = JsonHelper.Serialize(res);

                NameValueCollection values = new NameValueCollection();

                values.Add(MCConstants.FORMPARAM_CALCS , json);
                string submitUrl = HCSWebServiceSetting.MakeUrl(null , context.MCServiceURL , MCConstants.METHOD_SUBMIT , null);

                VordelWebClient client = new VordelWebClient
                {
                    Cookie = context.Cookies
                };
                byte[] response = client.UploadValues(submitUrl , values);
                string resp = Encoding.UTF8.GetString(response);
                FileLogger.Info("MC Submit response: " + resp);
            }
            catch (System.Net.WebException ex)
            {
                FileLogger.Error(this.GetType().Name + ":SubmitRun, " + ex.Message);
                Error er = null;
                if (ex.Response != null)
                    er = JsonHelper.Deserialize<Error>(ex.Response.GetResponseStream());
                String errorMsg = er == null ? MCConstants.MC_UNAVAILABLE_MESSAGE : er.ErrorMessage;
                throw new CException(this.GetType().Name + ":SubmitRun, " + errorMsg , ex);
            }
        }

        /// <summary>
        /// CreateSubmitCalc
        /// </summary>
        /// <param name="run"></param>
        /// <returns></returns>
        private MCCalc CreateSubmitCalc(RunDetails run)
        {
            string siteGuid = "";
            if (String.IsNullOrEmpty(run.SiteId))
            {
                //set the run id if empty by deriving from the db id
                run.SiteId = MCHelper.GetSiteIdFromDBId(run.DatabaseId);
            }
            if ("CA".Equals(run.SiteId))
            {
                siteGuid = MCConstants.CA_METHOD_GUID;
            }
            if ("BS".Equals(run.SiteId))
            {
                siteGuid = MCConstants.BS_METHOD_GUID;
            }
            if ("ZJ".Equals(run.SiteId))
            {
                siteGuid = MCConstants.ZJ_METHOD_GUID;
            }
            MCCalc calc = new MCCalc
            {
                GUID = siteGuid
                //GUID = "CA".Equals(run.SiteId) ? MCConstants.CA_METHOD_GUID : MCConstants.BS_METHOD_GUID 
            };

            try
            {
                ParamList prms = new ParamList();

                prms.Add(MCConstants.PARAM_RUN_ID , run.Id.ToString());
                prms.Add(MCConstants.PARAM_RUN_NAME , run.Name);
                prms.Add(MCConstants.PARAM_RUN_DESCRIPTION , run.Description);
                prms.Add(MCConstants.PARAM_USER_ID , run.CreatedBy);
                prms.Add(MCConstants.PARAM_DATASOURCE_ID , run.DatabaseId);
                prms.Add(MCConstants.PARAM_DATA_LOCATION , run.DataLocation);
                prms.Add(MCConstants.PARAM_BLOCK_SIZE , run.Block.ToString());
                prms.Add(MCConstants.PARAM_OUTPUT_FILES , MCHelper.OutputFilesToStr(run));
                prms.Add(MCConstants.PARAM_IMAGE_LIST_ONLY , run.ImageListOnly.ToString());

                prms.Add(MCConstants.PARAM_IMAGES_INPUT_TYPE , run.ImagesInputType.ToString());
                prms.Add(MCConstants.PARAM_IMAGE_LIST_REFERENCE , run.ImageListRef);
                prms.Add(MCConstants.PARAM_IMAGE_LIST_NAME , run.ImageListName);

                prms.Add(MCConstants.PARAM_PROTOCOL_REFERENCE , run.ProtocolRef);
                prms.Add(MCConstants.PARAM_PROTOCOL_NAME , run.ProtocolName);
                prms.Add(MCConstants.PARAM_WAVELENGTH_MAP , MCHelper.WMapToStr(run.WavelengthMap));

                if (run.PostProcessingEnabled)
                {
                    prms.Add(MCConstants.PARAM_POST_PROCESSING_TYPE , MCHelper.PostProcessingTypeToStr(run));
                }

                calc.Params = prms.ToArray();
                calc.Objs = DirsToInputObjects(run.Dirs);
            }
            catch (Exception eX)
            {
                throw new CException(this.GetType().Name + ":CreateSubmitCalc, " + eX.Message , eX);
            }

            return calc;
        }

        /// <summary>
        /// DirsToInputObjects
        /// </summary>
        /// <param name="dirs"></param>
        /// <returns></returns>
        private InputObj[] DirsToInputObjects(Dir[] dirs)
        {
            List<InputObj> res = new List<InputObj>();

            foreach (Dir d in dirs)
            {
                InputObj io = new InputObj
                {
                    Type = MCConstants.IO_TYPE_DIR ,
                    Rule = MCConstants.IO_RULE_INP ,
                    Name = d.Path
                };
                res.Add(io);
            }

            return res.ToArray();
        }
    }
}
