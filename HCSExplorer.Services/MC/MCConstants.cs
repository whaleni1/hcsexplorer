﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCSExplorer.Services.MC
{
    public static class MCConstants
    {
        public const char DELIMITER = ';';

        public const string METHOD_LIST = "calc/list.json";
        public const string METHOD_DETAIL = "calc/detail.json";
        public const string METHOD_SUBMIT = "execute.json";

        public const string CA_METHOD_GUID = "F4DE86288668A0C5E040A7931D1C5BBD";
        public const string BS_METHOD_GUID = "F4DE86288669A0C5E040A7931D1C5BBD";
        public const string ZJ_METHOD_GUID = "2C96440951E0465DE050A793EC5122AE";

        public const string CALC_REF_ID = "ATLAS";

        public const string IO_RULE_OUT = "OUT";
        public const string IO_RULE_INP = "INP";

        public const string IO_TYPE_SHAREDIR = "SHAREDIR";
        public const string IO_TYPE_FILE = "FILE";
        public const string IO_TYPE_DIR = "DIR";

        public const string PARAM_RUN_ID = "RunId";
        public const string PARAM_RUN_NAME = "RunName";
        public const string PARAM_RUN_DESCRIPTION = "RunDescription";
        public const string PARAM_USER_ID = "UserId";
        public const string PARAM_DATASOURCE_ID = "DatasourceId";
        public const string PARAM_DATA_LOCATION = "DataLocation";
        public const string PARAM_BLOCK_SIZE = "BlockSize";
        public const string PARAM_PROTOCOL_REFERENCE = "ProtocolReference";
        public const string PARAM_PROTOCOL_NAME = "ProtocolName";
        public const string PARAM_WAVELENGTH_MAP = "WavelengthMap";
        public const string PARAM_OUTPUT_FILES = "OutputFiles";
        public const string PARAM_IMAGE_LIST_ONLY = "ImageListOnly";

        public const string PARAM_IMAGE_LIST_NAME = "ImageListName";
        public const string PARAM_IMAGE_LIST_REFERENCE = "ImageListReference";
        public const string PARAM_IMAGES_INPUT_TYPE = "ImagesInputType";

        public const string PARAM_POST_PROCESSING_TYPE = "PostProcessingType";

        public const string OPERATOR_EQUAL = "Equal";
        public const string OPERATOR_NOT_EQUAL = "NotEqual";

        public const string FORMPARAM_CALCS = "calcs";
        public const string FORMPARAM_QUERY = "query";
        public const string FORMPARAM_OBJECT_LOAD_RULE = "objectLoadRule";
        public const string FORMPARAM_CALC_IDS = "calcIds";

        public const string MC_UNAVAILABLE_MESSAGE = "MCR service is not accessible";

        public const string OUTPUT_ONE_FILE = "IN_ONE_FILE";
        public const string OUTPUT_ICP4 = "ICP4";

        //USCA DB ID's
        public const string YOKOGAWA_3 = "YOKOGAWA_3";
        public const string PHENIX_1 = "PHENIX_1";
        public const string INCELL_1 = "INCELL_1";

        //CHBS DB ID's
        public const string INCELL_2 = "INCELL_2";
        public const string YOKOGAWA_1 = "YOKOGAWA_1";
        public const string YOKOGAWA_2 = "YOKOGAWA_2";

        //CNZJ DB ID's
        public const string CELLOMICS_CNZJ_1 = "CELLOMICS_CNZJ_1";
    }
}
