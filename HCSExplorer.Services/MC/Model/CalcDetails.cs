﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace HCSExplorer.Services.MC.Model
{
    [DataContract]
    public class CalcDetails
    {
        [DataMember(Name = "CalcDetails")]
        public List<CalcDetail> Details { get; set; }

        [DataMember(Name = "Total")]
        public int TotalCount { get; set; }
    }

    public class CalcDetail
    {
        public String CalcId { get; set; }
        public String MethodId { get; set; }
        public String MethodName { get; set; }
        public String MethodProvider { get; set; }
        public String Status { get; set; }
        public String Feedback { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<Param> Params { get; set; }
        public List<Task> Tasks { get; set; }

        public List<TaskObjIO> TaskObjIOs { get; set; }

        public Task GetCalcTask()
        {
            if (Tasks == null)
                return null;
            foreach (Task t in Tasks)
            {
                if (MCConstants.CALC_REF_ID.Equals(t.RefId))
                    return t;
            }
            return null;
        }

        public List<Task> GetPlateTasks()
        {
            List<Task> res = new List<Task>();

            if (Tasks == null)
                return res;
            foreach (Task t in Tasks)
            {
                if (!MCConstants.CALC_REF_ID.Equals(t.RefId))
                    res.Add(t);
            }
            return res;
        }
    }
}
