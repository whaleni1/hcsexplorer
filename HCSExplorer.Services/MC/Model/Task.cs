﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace HCSExplorer.Services.MC.Model
{
    [DataContract]
    public class Task
    {
        [DataMember(Name = "RefId")]
        public string RefId { get; set; }

        [DataMember(Name = "Status")]
        public string Status { get; set; }

        [DataMember(Name = "Type")]
        public string Type { get; set; }

        [DataMember(Name = "Feedback")]
        public string Feedback { get; set; }

        [DataMember(Name = "Note")]
        public string Note { get; set; }

        [DataMember(Name = "TimeStamp")]
        public DateTime Timestamp { get; set; }
    }
}
