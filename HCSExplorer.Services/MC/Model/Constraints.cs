﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCSExplorer.Services.MC.Model
{
    public class Constraints
    {
        public List<string> MethodIds { get; set; }
        public FilterParam Param { get; set; }
    }
}
