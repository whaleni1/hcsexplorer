﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace HCSExplorer.Services.MC.Model
{
    [DataContract]
    public class TaskObjIO
    {
        [DataMember(Name = "TaskRefId")]
        public string TaskRefId { get; set; }

        [DataMember(Name = "ObjIOs")]
        public Collection<Group> Groups { get; set;}
    }

    [DataContract]
    public class InputObj
    {
        [DataMember(Name = "Type")]
        public string Type { get; set; }

        [DataMember(Name = "Rule")]
        public string Rule { get; set; }

        [DataMember(Name = "Name")]
        public string Name { get; set; }
    }

    [DataContract]
    public class Group
    {
        [DataMember(Name = "Objs")]
        public Collection<InputObj> dataObjects { get; set; }
    }
}
