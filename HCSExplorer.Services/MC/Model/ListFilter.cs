﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCSExplorer.Services.MC.Model
{
    public class ListFilter
    {
        public Constraints Constraints { get; set; }
        public long StartIndex { get; set; }
        public long PageSize { get; set; }
        public bool ReturnListCount { get; set; }
    }
}
