﻿using System.Runtime.Serialization;

namespace HCSExplorer.Services.MC.Model
{
    [DataContract]
    public class Error
    {
        [DataMember(Name = "Code")]
        public string Code { get; set; }

        [DataMember(Name = "ErrorMessage")]
        public string ErrorMessage { get; set; }
    }
}
