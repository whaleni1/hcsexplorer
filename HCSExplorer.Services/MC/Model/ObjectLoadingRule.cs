﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCSExplorer.Services.MC.Model
{
    public class ObjectLoadingRule
    {
        public bool LoadTaskDetail { get; set; }
        public bool LoadParamDetail { get; set; }
        public bool LoadCalcDetail { get; set; }
        public bool LoadTaskObjIODetail { get; set; }
    }
}
