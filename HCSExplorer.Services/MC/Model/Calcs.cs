﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCSExplorer.Services.MC.Model
{
    public class Calcs
    {
        public MCCalc[] MCCalcs { get; set; }
    }

    public class MCCalc
    {
        public string GUID { get; set; }
        public Param[] Params { get; set; }
        public InputObj[] Objs { get; set; }
    }

    public class CalcIDs
    {
        public List<string> ids { get; set; }
    }

    public class ParamList : List<Param>
    {
        public void Add(string pName, string pValue)
        {
            this.Add(new Param
            {
                Name = pName,
                Value = pValue
            });
        }
    }

    public class Param
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class FilterParam : Param
    {
        public string Operator { get; set; }
    }
}
