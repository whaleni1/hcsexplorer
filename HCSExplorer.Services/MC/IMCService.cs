﻿using HCS.Shared.Context;
using HCS.Shared.DTO;
using HCS.Shared.Filtering;

namespace HCSExplorer.Services.MC
{
    public interface IMCService
    {
        RunListPagingModel GetRuns(IExecutionContext context , RunListPagingModel pagingModel);
        RunDetails GetRun(IExecutionContext context , string calcId);
        void SubmitRun(IExecutionContext context , RunDetails run);
    }
}
