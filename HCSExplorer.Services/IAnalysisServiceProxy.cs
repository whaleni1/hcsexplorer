﻿using System.Collections.Generic;
using HCSImageAnalysis.Shared.Context;

namespace HCSImageAnalysis.Services
{
    public interface IAnalysisServiceProxy
    {
        IList<Shared.DTO.Run> GetRuns(IExecutionContext context);

        Shared.DTO.RunDetails GetRun(IExecutionContext context, int id);

        int GetNextId();

        void SubmitRun(Shared.DTO.RunDetails run);

        Shared.DTO.ProtocolFile GetProtocolFile(int runId);
    }
}
