﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using HCS.Shared.Common;
using HCS.Shared.DTO;
using HCS.Shared.Logger;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace HCSExplorer.Services
{
    /// <summary>
    /// ImageUtils
    /// </summary>
    public static class ImageUtils
    {
        public const string IMAGE_FORMAT = "jpg";
        public const string IMAGE_CONTENT_TYPE = "image/" + IMAGE_FORMAT;

        private static string[] rowId = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
                             "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF"};

        /// <summary>
        /// GetCellId
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <returns></returns>
        public static string GetCellId(int rowIndex , int colIndex)
        {
            string cellId = null;
            if (rowIndex < rowId.Length)
            {
                cellId = rowId[rowIndex] + colIndex;
            }
            return cellId;
        }

        /// <summary>
        /// GetPlateKey
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetPlateKey(string key)
        {
            string result = string.Empty;

            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] bytes = encoding.GetBytes(key);
                var hashedBytes = MD5.Create().ComputeHash(bytes);

                result = BitConverter.ToString(hashedBytes).Replace("-" , string.Empty);
            }
            catch (Exception eX)
            {
                throw new CException("ImageUtils:GetPlateKey, " + eX.Message , eX);
            }

            return result;
        }

        /// <summary>
        /// GetFileKey
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="field"></param>
        /// <param name="channel"></param>
        /// <param name="overlayParams"></param>
        /// <param name="invert"></param>
        /// <param name="brightness"></param>
        /// <param name="levels"></param>
        /// <param name="auto"></param>
        /// <param name="color"></param>
        /// <param name="thumbnailsize"></param>
        /// <param name="zindex"></param>
        /// <param name="timepoint"></param>
        /// <param name="version"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static string GetFileKey(string row , string col , string field , string channel ,
                                        string overlayParams , string invert , string brightness , string levels, string auto, string blackPoint, string whitePoint,
                                        string color , string thumbnailsize , string zindex , string timepoint , string version , string extension)
        {
            string[] paramArray;
            string concatKey = "";
            if (!string.IsNullOrEmpty(brightness))
            {
                paramArray = new string[] { row, col, field, channel, overlayParams, invert, brightness, color, thumbnailsize, zindex, timepoint, version };
                concatKey = string.Join("_", paramArray);
            }
            else if (string.IsNullOrEmpty(levels))
            {
                paramArray = new string[] { row, col, field, channel, overlayParams, invert, whitePoint, blackPoint, color, thumbnailsize, zindex, timepoint, version };
                concatKey = string.Join("_", paramArray);
            }
            else if (!string.IsNullOrEmpty(auto) )
            {
                if (auto.Contains("true"))
                {
                    paramArray = new string[] { row , col , field , channel , overlayParams , invert , auto , color , thumbnailsize , zindex , timepoint , version };
                    concatKey = string.Join("_" , paramArray);
                }
            }

            return GetHash(concatKey) + "." + extension;
        }

        /// <summary>
        /// GetFileKey
        /// </summary>
        /// <param name="imgUrl"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static string GetFileKey(string imgUrl , string extension)
        {
            return GetHash(imgUrl) + "." + extension;
        }

        /// <summary>
        /// GetHash
        ///  - Return the hexadecimal string.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetHash(string str)
        {
            StringBuilder sBuilder = new StringBuilder();
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] bytes = encoding.GetBytes(str);
                var hashedBytes = SHA1.Create().ComputeHash(bytes);


                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0 ; i < hashedBytes.Length ; i++)
                {
                    sBuilder.Append(hashedBytes[i].ToString("x2"));
                }
            }
            catch (Exception eX)
            {
                throw new CException("ImageUtils:GetHash, " + eX.Message , eX);
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// GetPlateDir
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="plateId"></param>
        /// <returns></returns>
        public static string GetPlateDir(string databaseId , string plateId)
        {
            return GetPlateKey(databaseId + "_" + plateId);
        }

        /// <summary>
        /// ImageUrlParams
        ///  - no overlay
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="plateid"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="field"></param>
        /// <param name="channelParams"></param>
        /// <param name="sizeId"></param>
        /// <param name="invert"></param>
        /// <param name="zIndex"></param>
        /// <param name="timepoint"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static string ImageUrlParams(string databaseId , string plateid , int row , int col ,
                                            string field , ChannelImageParameters channelParams , string sizeId , bool invert ,
                                            int? zIndex , int? timepoint , string version)
        {
            string urlparams = "db=" + databaseId + "&plateid=" + plateid +
                               "&row=" + row + "&col=" + col + "&field=" + field + "&channel=" + channelParams.Channel +
                               "&format=" + IMAGE_FORMAT;

            try
            {
                if (invert)
                {
                    urlparams += "&invert=1";
                }
                if (zIndex != null)
                {
                    urlparams += "&z_index=" + zIndex.Value.ToString();
                }
                if (timepoint != null)
                {
                    urlparams += "&timepoint_index=" + timepoint.Value.ToString();
                }
                if (version != null)
                {
                    urlparams += "&version=" + version;
                }

                if (!string.IsNullOrEmpty(channelParams.Auto) )
                {
                    bool autoContrast;
                    if (bool.TryParse(channelParams.Auto, out autoContrast))
                    {
                        urlparams += "&auto=" + autoContrast;
                    }
                }

                if (!string.IsNullOrEmpty(channelParams.Level))
                {
                    Int64 whitePoint;
                    Int64 blackPoint;
                    bool level;
                    if (bool.TryParse(channelParams.Level, out level))
                    {
                        if (level)
                        {
                            if(!string.IsNullOrEmpty(channelParams.BlackPoint) && !string.IsNullOrEmpty(channelParams.WhitePoint))
                            {
                                if ((Int64.TryParse(channelParams.WhitePoint, out whitePoint)) && (Int64.TryParse(channelParams.BlackPoint, out blackPoint)))
                                {
                                    urlparams += "&level=" + whitePoint + "," + blackPoint;
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(channelParams.Brightness))
                {
                    double brightness;
                    if (double.TryParse(channelParams.Brightness , out brightness))
                    {
                        urlparams += "&b=" + brightness;
                    }
                }

                if (!"Grayscale".Equals(channelParams.Color))
                {
                    urlparams += "&color=" + channelParams.Color;
                }

                if (!string.IsNullOrEmpty(sizeId))
                {
                    urlparams += "&thumb=" + GetThumbSize(sizeId);
                }
            }
            catch (Exception eX)
            {
                throw new CException("ImageUtils:ImageUrlParams, " + eX.Message , eX);
            }

            return urlparams;
        }

        /// <summary>
        /// OverlayImageUrlParams
        ///  - overlay
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="plateid"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="field"></param>
        /// <param name="sizeId"></param>
        /// <param name="overlParams"></param>
        /// <param name="zIndex"></param>
        /// <param name="timepoint"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static string OverlayImageUrlParams(string databaseId , string plateid , int row , int col ,
                                                   string field ,
                                                   string sizeId , string overlParams ,
                                                   int? zIndex , int? timepoint , string version)
        {
            string urlparams = "db=" + databaseId + "&plateid=" + plateid +
                               "&row=" + row + "&col=" + col + "&field=" + field + "&format=" + IMAGE_FORMAT;

            try
            {
                if (zIndex != null)
                {
                    urlparams += "&z_index=" + zIndex.Value.ToString();
                }
                if (timepoint != null)
                {
                    urlparams += "&timepoint_index=" + timepoint.Value.ToString();
                }
                if (version != null)
                {
                    urlparams += "&version=" + version;
                }

                urlparams += overlParams;
                if (!string.IsNullOrEmpty(sizeId))
                {
                    urlparams += "&thumb=" + GetThumbSize(sizeId);
                }
            }
            catch (Exception eX)
            {
                throw new CException("ImageUtils:OverlayImageUrlParams, " + eX.Message , eX);
            }

            return urlparams;
        }

        /// <summary>
        /// OverlayParams
        /// </summary>
        /// <param name="channelParams"></param>
        /// <returns></returns>
        public static string OverlayParams(IList<ChannelImageParameters> channelParams)
        {
            string overlParams = string.Empty;

            try
            {
                foreach (ChannelImageParameters c in channelParams)
                {
                    var chName = c.Channel;
                    var chColor = c.Color;
                    if (!"Grayscale".Equals(chColor))
                    {
                        overlParams += "&" + chColor + "=" + chName;
                        if (!string.IsNullOrEmpty(c.Auto))
                        {
                            bool autoContrast;
                            if (bool.TryParse(c.Auto, out autoContrast))
                            {
                                overlParams += "&" + chColor + "&auto=" + c.Auto;
                            }
                        }

                        if (!string.IsNullOrEmpty(c.Level))
                        {
                            Int64 whitePoint; Int64 blackPoint;
                            string[] values = c.Level.Split(',');

                            if ((Int64.TryParse(values[0], out whitePoint)) && (Int64.TryParse(values[1], out blackPoint)))
                            {
                                overlParams += "&" + chColor + "&level=" + c.Level;
                            }
                        }
                        if (!string.IsNullOrEmpty(c.Brightness))
                        {
                            double brightness;
                            if (double.TryParse(c.Brightness , out brightness))
                            {
                                overlParams += "&" + chColor + "_b=" + brightness;
                            }
                        }
                    }
                }
            }
            catch (Exception eX)
            {
                throw new CException("ImageUtils:OverlayParams, " + eX.Message , eX);
            }

            return overlParams;
        }

        /// <summary>
        /// GetThumbSize
        /// </summary>
        /// <param name="sizeId"></param>
        /// <returns></returns>
        public static string GetThumbSize(string sizeId)
        {
            switch (sizeId)
            {
                case "S":
                    return "60";
                case "M":
                    return "180";
                case "L":
                    return "500";
                default:
                    return null;
            }
        }

        /// <summary>
        /// GetFullSizeImgUrl
        /// </summary>
        /// <param name="serverUrl"></param>
        /// <param name="imgUrl"></param>
        /// <param name="barcode"></param>
        /// <param name="platePath"></param>
        /// <returns></returns>
        private static string GetFullSizeImgUrl(string serverUrl , string imgUrl , string barcode , string platePath)
        {
            string res = string.Empty;

            try
            {
                Regex rgx = new Regex("&thumb=\\d+");
                imgUrl = rgx.Replace(imgUrl , string.Empty);
                imgUrl = Uri.EscapeDataString(imgUrl);
                barcode = Uri.EscapeDataString(barcode);
                platePath = Uri.EscapeDataString(platePath);
                res = string.Format("{0}image?src={1}&barcode={2}&platePath={3}" , serverUrl , imgUrl , barcode , platePath);
            }
            catch (Exception eX)
            {
                throw new CException("ImageUtils:GetFullSizeImgUrl, " + eX.Message , eX);
            }

            return res;
        }

        /// <summary>
        /// Create pdf file and store it in the server
        /// </summary>
        /// <param name="baseUrl">Base url of application</param>
        /// <param name="siteRootDir">Pfysical root path of application on the server</param>
        /// <param name="model">Plate model</param>
        /// <param name="imagePathes">1-dimensional array of image pathes matrix</param>
        /// <returns>Path to created PDF file</returns>
        public static string CreatePdf(string cacheDir , string baseUrl , string serverUrl , string siteRootDir , string cachedImagesUrlPath , PlateImagesModel model , string[] imagePathes , string[] imageUrls)
        {
            string result = null;

            string barcode = model.Barcode;
            string brightness = model.PlateChannelParameters.Brightness;
            string level = model.PlateChannelParameters.Level;
            string auto = model.PlateChannelParameters.Auto;
            string channel = model.PlateChannelParameters.Channel;
            string color = model.PlateChannelParameters.Color;
            string field = model.Field;
            int format = model.Format;
            int numrows = model.Rows;
            int numcols = model.Cols;
            string plateName = model.Name;
            string platePath = model.Path;
            string plateTimestamp = model.DateSrc;

            try
            {
                string plateKey = GetPlateDir(model.DatabaseId , model.Id.ToString());
                string plateDir = Path.Combine(cacheDir , plateKey);
                if (!Directory.Exists(plateDir))
                {
                    Directory.CreateDirectory(plateDir);
                }
                string pdfPath = Path.Combine(plateDir , plateKey + ".pdf");
                lock (string.Intern(pdfPath))
                {
                    iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate());
                    try
                    {
                        // get pdf file instance
                        PdfWriter.GetInstance(document , new FileStream(pdfPath , FileMode.Create));

                        document.Open();

                        string brightnessText = string.IsNullOrEmpty(brightness) ? "Not Set" : brightness;
                        string autoContrastText = string.IsNullOrEmpty(auto) ? "Not Set" : auto;
                        string levelsText = string.IsNullOrEmpty(level) ? "Not Set" : level;
                        iTextSharp.text.Font infoFont = new iTextSharp.text.Font();
                        infoFont.Size = 7f;

                        // write metadata on the pdf document
                        iTextSharp.text.Phrase plateInformation = new iTextSharp.text.Phrase("Plate Barcode: " + barcode + "\nDescription: " + plateName + "\nPath: " + platePath 
                            + "\nTimestamp: " + plateTimestamp + "\nChannel: " + channel + " / Brightness: " + brightnessText + " / Auto-Contrast: " + autoContrastText
                            + " / Levels (white point, black point): " + levelsText + " / Field: " + field + " / Color: " + color + "\n" , infoFont);
                        document.Add(plateInformation);

                        // table in pdf document + 1 for row and column label
                        PdfPTable imgTable = new PdfPTable(numcols + 1);
                        imgTable.DefaultCell.Border = 0;
                        imgTable.DefaultCell.Padding = 1;
                        imgTable.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        imgTable.DefaultCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;

                        List<iTextSharp.text.Image> images = new List<iTextSharp.text.Image>();
                        bool hasSquareImages = false;
                        for (int r = 1 ; r <= numrows ; r++)
                        {
                            for (int c = 1 ; c <= numcols ; c++)
                            {
                                int indx = (r - 1) * model.Cols + (c - 1);
                                string imageUrlPath = imagePathes[indx];
                                string imgUrl = imageUrls[indx];

                                string imagePath = string.Empty;
                                if (imageUrlPath.IndexOf(cachedImagesUrlPath) > -1)
                                {
                                    imagePath = Path.Combine(cacheDir , imageUrlPath.Substring(imageUrlPath.IndexOf(cachedImagesUrlPath) + cachedImagesUrlPath.Length + 1).Replace('/' , '\\'));
                                }
                                else
                                {
                                    imagePath = Path.Combine(siteRootDir , imageUrlPath.Substring(imageUrlPath.IndexOf("Content")).Replace('/' , '\\'));
                                    hasSquareImages = true;
                                }

                                iTextSharp.text.Image cellImage = iTextSharp.text.Image.GetInstance(imagePath);
                                if (!string.IsNullOrEmpty(imgUrl))
                                {
                                    string url = GetFullSizeImgUrl(serverUrl , imgUrl , barcode , platePath);
                                    cellImage.Annotation = new Annotation(0 , 0 , 0 , 0 , url);
                                }
                                cellImage.ScalePercent(50f);
                                images.Add(cellImage);
                            }
                        }
                        if (hasSquareImages && model.Format != 1536)
                        {
                            imgTable.DefaultCell.FixedHeight = 45f;
                        }

                        iTextSharp.text.Image[] imagesArray = images.ToArray();

                        for (int r = 0 ; r <= numrows ; r++)
                        {
                            for (int c = 0 ; c <= numcols ; c++)
                            {
                                if (r == 0 && c == 0)
                                {
                                    // add nothing
                                    imgTable.AddCell("");
                                    continue;
                                }
                                if (r == 0)
                                {
                                    // add label number 1,2,3... and set font size according to the plate format
                                    if (format == 1536)
                                    {
                                        iTextSharp.text.Font myFont = new iTextSharp.text.Font();
                                        myFont.Size = 7f;
                                        imgTable.AddCell(new iTextSharp.text.Phrase(c.ToString() , myFont));
                                        continue;
                                    }
                                    imgTable.AddCell(c.ToString());
                                    continue;
                                }
                                if (c == 0)
                                {
                                    // add label character A,B,C...  
                                    if (format == 1536)
                                    {
                                        iTextSharp.text.Font myFont = new iTextSharp.text.Font();
                                        myFont.Size = 7f;
                                        imgTable.AddCell(new iTextSharp.text.Phrase(rowId[r - 1] , myFont));
                                        continue;
                                    }
                                    imgTable.AddCell(rowId[r - 1]);
                                    continue;
                                }

                                int indx = (r - 1) * model.Cols + (c - 1);
                                iTextSharp.text.Image cellImage = imagesArray[indx];
                                imgTable.AddCell(cellImage);
                            }
                        }
                        document.Add(imgTable);
                        document.Close();
                    }
                    catch (Exception eX)
                    {
                        FileLogger.Error("ImageUtils:CreatePdf, " + eX.Message);
                        document.Close();
                        throw new CException("ImageUtils:CreatePdf, " + eX.Message , eX);
                    }
                }
                result = Path.Combine(plateKey , plateKey + ".pdf");
            }
            catch (Exception eX)
            {
                FileLogger.Error("ImageUtils:CreatePdf, " + eX.Message);
                throw new CException("ImageUtils:CreatePdf, " + eX.Message , eX);
            }

            return result;
        }

        /// <summary>
        /// ClearPlateDir
        /// </summary>
        /// <param name="siteDir"></param>
        /// <param name="model"></param>
        public static void ClearPlateDir(string siteDir , PlateImagesModel model)
        {
            string plateKey = string.Empty;

            try
            {
                plateKey = GetPlateDir(model.DatabaseId , model.Id.ToString());
                if (string.IsNullOrEmpty(plateKey))
                {
                    FileLogger.Debug("!!! ClearPlateDir: platekey = " + plateKey);
                    return;
                }
                string platePath = Path.Combine(siteDir , plateKey);
                if (Directory.Exists(platePath))
                {
                    DirectoryInfo dir = new DirectoryInfo(platePath);

                    foreach (FileInfo file in dir.GetFiles())
                    {
                        file.Delete();
                    }
                }
            }
            catch (Exception eX)
            {
                throw new CException("ImageUtils:ClearPlateDir, " + eX.Message , eX);
            }
        }
    }
}
